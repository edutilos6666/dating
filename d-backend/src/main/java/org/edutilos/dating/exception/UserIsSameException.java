package org.edutilos.dating.exception;

public class UserIsSameException extends Exception {
    private static final long serialVersionUID = 1L;

    public UserIsSameException(String message) {
        super(message);
    }

    public UserIsSameException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserIsSameException(Throwable cause) {
        super(cause);
    }
}
