package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "address")
public class Address implements Serializable {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="COUNTRY")
    private String country;
    @Column(name="CITY")
    private String city;
    @Column(name="ZIPCODE")
    private String zipcode;

    public Address(String country, String city, String zipcode) {
        this.country = country;
        this.city = city;
        this.zipcode = zipcode;
    }
}
