/*
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.model.auth;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class LoginResponse {

    private String token_type;  // NOSONAR
    private String access_token; // NOSONAR
    private String refresh_token; // NOSONAR
    private Integer expires_in; // NOSONAR
    private Integer userid;
    private Date lastLogin;

    public static LoginResponse createLoginResponse(Integer userId, String accessToken, String tokenType) {
        if (userId == null) {
            return null;
        }

        LoginResponse resp = new LoginResponse();
        resp.setToken_type(tokenType);
        resp.setRefresh_token(UUID.randomUUID().toString());
        resp.setExpires_in(60);
        resp.setUserid(userId);
        resp.setAccess_token(accessToken != null ? accessToken : UUID.randomUUID().toString());

        return resp;
    }
}
