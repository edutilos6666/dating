package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "job")
public class Job implements Serializable {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="TITLE")
    private String title;
    @Column(name="COMPANY_NAME")
    private String companyName;
    @Column(name="WAGE")
    private Double wage;

    public Job(String title, String companyName, Double wage) {
        this.title = title;
        this.companyName = companyName;
        this.wage = wage;
    }
}
