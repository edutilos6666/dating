package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * created by  Nijat Aghayev on 23.03.20
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonImageForm implements Serializable {
    private Integer personId;
    private String alt;
    private String header;
    private String footer;
    private MultipartFile multipartFile;
}
