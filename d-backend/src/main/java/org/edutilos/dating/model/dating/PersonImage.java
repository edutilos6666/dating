package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.edutilos.dating.endpoint.dating.PersonImagePreRemoveHook;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * created by  Nijat Aghayev on 16.03.20
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "image")
// @EntityListeners annotation is redundant here , but i keep it for me
@EntityListeners({PersonImagePreRemoveHook.class})
public class PersonImage implements Serializable {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="MAIN")
    private Boolean main;
    @Column(name="SRC_PATH")
    private String srcPath;
    @Column(name = "ALT")
    private String alt;
    @Column(name = "HEADER")
    private String header;
    @Column(name = "FOOTER")
    private String footer;
//    @ManyToMany(cascade = { CascadeType.ALL })
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(
            name = "image_to_person_like_mapping",
            joinColumns = { @JoinColumn(name = "IMAGE_ID") },
            inverseJoinColumns = { @JoinColumn(name = "PERSON_ID") }
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Person> likes;
    // do not ever use CascadeType.ALL in @ManyToMany relationships
//    @ManyToMany(cascade = { CascadeType.ALL })
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(
            name = "image_to_person_dislike_mapping",
            joinColumns = { @JoinColumn(name = "IMAGE_ID") },
            inverseJoinColumns = { @JoinColumn(name = "PERSON_ID") }
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Person> dislikes;
//    @ManyToMany(cascade = { CascadeType.ALL })
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(
            name = "image_to_person_heart_mapping",
            joinColumns = { @JoinColumn(name = "IMAGE_ID") },
            inverseJoinColumns = { @JoinColumn(name = "PERSON_ID") }
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Person> hearts;
    @ManyToOne
    @JoinColumn(name="PERSON_ID")
    private Person person;

    public PersonImage(Boolean main, String srcPath, String alt, String header, String footer, List<Person> likes, List<Person> dislikes, List<Person> hearts, Person person) {
        this.main = main;
        this.srcPath = srcPath;
        this.alt = alt;
        this.header = header;
        this.footer = footer;
        this.likes = likes;
        this.dislikes = dislikes;
        this.hearts = hearts;
        // PersonImage is in many-to-one relationship to Person, Person is owner the relationship and CascadeType is CascadeType.ALL =>
        // We will call person.save() => then it will assign id and person_id to the personImage
        this.person = person;
    }

    public PersonImage(Boolean main, String srcPath, String alt, String header, String footer, List<Person> likes, List<Person> dislikes, List<Person> hearts) {
        this(main, srcPath, alt, header, footer, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), null);
    }


    public PersonImage(Boolean main, String srcPath, String alt, String header, String footer, Person person) {
        this(main, srcPath, alt, header, footer, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), person);
    }

    public PersonImage(Boolean main, String srcPath, String alt, String header, String footer) {
        this(main, srcPath, alt, header, footer, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
    }
}
