package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "person")
public class Person implements Serializable {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="USERNAME")
    private String username;
    @Column(name="PASSWORD")
    private String password;
    @Column(name = "PASSWORD_TYPE")
    private Integer passwordType;
    @Column(name = "LAST_LOGIN")
    private Timestamp lastLogin;
    @Column(name="FIRSTNAME")
    private String firstname;
    @Column(name="LASTNAME")
    private String lastname;
    // CascadeType.MERGE => that gives us error:  Multiple representations of the same entity [org.edutilos.dating.model.dating.Gender#2] are being merged. Detached: [org.edutilos.dating.model.dating.Gender@342b4ee9], ...
    @ManyToOne
    @JoinColumn(name = "GENDER_ID", nullable = false)
    private Gender gender;
    @Column(name="BIRTHDATE")
    private Date birthdate;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "JOB_ID", nullable = false)
    private Job job;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", nullable = false)
    private Address address;
    @Column(name="EMAIL")
    private String email;
    @Column(name="ABOUT_ME")
    private String aboutMe;
    @Column(name="ABOUT_IDEAL_PARTNER")
    private String aboutIdealPartner;
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(
            name = "PERSON_TO_FAVORITE_MAPPING",
            joinColumns = { @JoinColumn(name = "SRC_PERSON_ID") },
            inverseJoinColumns = { @JoinColumn(name = "TARGET_PERSON_ID") }
    )
    private List<Person> favoriteList;
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(
            name = "PERSON_TO_BLOCKED_MAPPING",
            joinColumns = { @JoinColumn(name = "SRC_PERSON_ID") },
            inverseJoinColumns = { @JoinColumn(name = "TARGET_PERSON_ID") }
    )
    private List<Person> blockedList;
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(
            name = "person_to_deleted_mapping",
            joinColumns = { @JoinColumn(name = "SRC_PERSON_ID") },
            inverseJoinColumns = { @JoinColumn(name = "TARGET_PERSON_ID") }
    )
    private List<Person> deletedList;
    @Column(name="STATUS")
    private Boolean status;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "person", cascade = CascadeType.ALL)
//    @JoinColumn(name = "PERSON_ID")
    List<PersonImage> images;
}
