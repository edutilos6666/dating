/**
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.model.dating_login;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Table(name = "log_access")
@Entity
@Data
public class LogAccess {

    @Id
    @Column
    private Long id;

    @Column
    private Timestamp zeitstempel;

    @Column
    private String username;

    @Column
    private String method;

    @Column
    private String endpoint;

    @Column
    private Long zeit;

    @Column
    private Integer status;

    @Column
    private Integer size;

}
