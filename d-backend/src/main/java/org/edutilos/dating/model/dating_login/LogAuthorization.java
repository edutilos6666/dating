/**
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.model.dating_login;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Table(name = "log_authorization")
@Entity
@Data
public class LogAuthorization {

    @Id
    private Long id;

    @Column(name = "PERSON_ID")
    private Integer personId;

    @Column(name = "LOGIN_AT")
    private Timestamp loginAt;

}
