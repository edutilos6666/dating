/**
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.model.dating_login;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "iauth_token")
public class IAuthToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSON_ID")
    private Integer personId;

    @Column
    private String token;

    @Column(name = "CREATED_AT")
    private Timestamp createdAt;

    @Column(name = "LAST_USE")
    private Timestamp lastUse;

}
