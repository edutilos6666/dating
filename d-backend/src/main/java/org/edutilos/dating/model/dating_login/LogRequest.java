package org.edutilos.dating.model.dating_login;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "log_request")
public class LogRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer userid;
    @Column(name = "SESSION_TOKEN")
    private String sessionToken;
    @Column(name = "TIME_START")
    private Timestamp timeStart;
    private String method;
    private String endpoint;
    @Column(name = "QUERY_STRING")
    private String queryString;
    @Column(name = "RESPONSE_STATUS")
    private Integer responseStatus;
    private Integer size;
    @Column(name = "TIME_FIRST_BYTE")
    private Long timeFirstByte;
    @Column(name = "TIME_TOTAL")
    private Long timeTotal;
}
