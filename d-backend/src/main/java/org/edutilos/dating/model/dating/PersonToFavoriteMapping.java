package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "person_to_favorite_mapping")
public class PersonToFavoriteMapping implements Serializable {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="SRC_PERSON_ID")
    private Integer srcPersonId;
    @Column(name="TARGET_PERSON_ID")
    private Integer targetPersonId;
}
