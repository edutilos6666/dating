package org.edutilos.dating.model.dating_login;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "log_session")
public class LogSession implements Serializable {
    @Id
    @Column(name = "SESSION_TOKEN")
    private String sessionToken;
    private Integer personId;
    @Column(name = "TIME_CREATED")
    private Timestamp timeCreated;
    private String ip;
}
