package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Entity
@Table(name="private_channel")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PrivateChannel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;
    @OneToOne
    @JoinColumn(name = "USER_FROM_ID")
    private Person userFrom;
    @OneToOne
    @JoinColumn(name = "USER_TO_ID")
    private Person userTo;
    @OneToMany(mappedBy = "channel", cascade = CascadeType.ALL) // "channel" is a field in PrivateMessage
//    @JoinColumn()
    @LazyCollection(LazyCollectionOption.FALSE)
    List<PrivateMessage> messages;

    public PrivateChannel(Person userFrom, Person userTo, List<PrivateMessage> messages) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.messages = messages;
    }
}
