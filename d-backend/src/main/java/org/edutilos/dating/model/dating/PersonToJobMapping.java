package org.edutilos.dating.model.dating;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@Getter
@Setter
@Entity
@Table(name = "person_to_job_mapping")
public class PersonToJobMapping implements Serializable {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="PERSON_ID")
    private Integer personId;
    @Column(name="JOB_ID")
    private Integer jobId;
}
