package org.edutilos.dating.model.dating;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * created by  Nijat Aghayev on 24.03.20
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "image_to_person_heart_mapping")
public class ImageToPersonHeartMapping implements Serializable {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="IMAGE_ID")
    private Integer imageId;
    @Column(name="PERSON_ID")
    private Integer personId;
}
