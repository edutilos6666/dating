package org.edutilos.dating.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginError {
    private String error;
    private String errorDescription;
    private String xError;
    private static final String invalid_request = "invalid_request";
    private static final String invalid_grant = "invalid_grant";


    public static LoginError getInvalidLogin() {
        LoginError error = new LoginError();
        error.setError("invalid_client");
        error.setErrorDescription("Wrong username or password");
        error.setXError("auth:unknown");
        return error;
    }

    public static LoginError getExpiredLogin() {
        LoginError error = new LoginError();
        error.setError(invalid_grant);
        error.setErrorDescription("Password expired and must be renewed.");
        error.setXError("auth:password-expired");
        return error;
    }

    public static LoginError getMissingLogin() {
        LoginError error = new LoginError();
        error.setError(invalid_request);
        error.setErrorDescription("Missing username or password");
        error.setXError("auth:invalid");
        return error;
    }

    public static LoginError getBadPassword() {
        LoginError error = new LoginError();
        error.setError(invalid_request);
        error.setErrorDescription("The password you entered doesn't meet password policy requirements");
        error.setXError("auth:bad-password");
        return error;
    }

    public static LoginError getIncorrectPassword() {
        LoginError error = new LoginError();
        error.setError(invalid_request);
        error.setErrorDescription("Password incorrect");
        error.setXError("auth:incorrect-password");
        return error;
    }

    public static LoginError getPasswordAlreadyUsed() {
        LoginError error = new LoginError();
        error.setError(invalid_request);
        error.setErrorDescription("Password already used");
        error.setXError("auth:password-already-used");
        return error;
    }

    public static LoginError getUserDisabled() {
        LoginError error = new LoginError();
        error.setError(invalid_request);
        error.setErrorDescription("Account disabled");
        error.setXError("auth:account-disabled");
        return error;
    }

    public static LoginError getUserDoesntExist() {
        LoginError error = new LoginError();
        error.setError(invalid_request);
        error.setErrorDescription("Account doesn't exist");
        error.setXError("auth:account-does-not-exist");
        return error;
    }

    @JsonProperty(value = "error")
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @JsonProperty(value = "error-description")
    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @JsonProperty(value = "x-error")
    public String getxError() {
        return xError;
    }

    public void setXError(String xError) {
        this.xError = xError;
    }
}
