package org.edutilos.dating.model.dating;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Entity
@Table(name="private_message")
@Getter
@Setter
@NoArgsConstructor
public class PrivateMessage {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;
    @OneToOne
    @JoinColumn(name = "USER_FROM_ID")
    private Person userFrom;
    @OneToOne
    @JoinColumn(name = "USER_TO_ID")
    private Person userTo;
    @ManyToOne
    @JoinColumn(name = "CHANNEL_ID")
    private PrivateChannel channel;
    @Column(name = "CONTENT", columnDefinition = "TEXT NOT NULL")
    private String content;
    @Column(name= "CREATED")
    private Date created;
    @Column(name = "OPENED")
    private boolean opened;


    public PrivateMessage(Person userFrom, Person userTo, PrivateChannel channel, String content, Date created, boolean opened) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.channel = channel;
        setContent(content);
        this.created = created;
        this.opened = opened;
    }

    public PrivateMessage(Person userFrom, Person userTo, PrivateChannel channel, String content, Date created) {
        this(userFrom, userTo, channel, content, created, false);
    }

    public void  setContent(String content) {
        try {
            this.content = URLEncoder.encode(content, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String getContent()  {
        try {
            return URLDecoder.decode(content, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
