package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by  Nijat Aghayev on 12.03.20
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
}
