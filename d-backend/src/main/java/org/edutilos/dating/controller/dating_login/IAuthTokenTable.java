/**
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.controller.dating_login;

import org.edutilos.dating.model.dating_login.IAuthToken;
import org.springframework.stereotype.Controller;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Controller
@Deprecated
public class IAuthTokenTable {

	@PersistenceContext(unitName = "dating_login")
	private EntityManager entityManager;

	@org.springframework.transaction.annotation.Transactional("dating_loginTransactionManager")
	@Deprecated
	public void insert(IAuthToken token) {
		entityManager.persist(token);
		entityManager.flush();
	}

	@Deprecated
	public IAuthToken findByToken(String token) {
		try {
			return entityManager.createQuery("from IAuthToken where token = :token", IAuthToken.class)
					.setParameter("token", token).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
