package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@Repository
@Transactional
public interface PersonRepository extends JpaRepository<Person, Integer> {
    Person findByUsername(String username);
    Person getById(Integer id);
    @Query("select p from Person p where " +
            "(:id is null or p.id = :id) " +
            "and " +
            "(:username is null or p.username LIKE :username) " +
            "and " +
            "(:firstname is null or p.firstname LIKE :firstname) " +
            "and " +
            "(:lastname is null or p.lastname LIKE :lastname) " +
            "and " +
            "(coalesce(:genderIds, null) is null or p.gender.id in (:genderIds)) " +
            "and " +
            "(:minBirthdate is null or p.birthdate <= :minBirthdate) " +
            "and " +
            "(:maxBirthdate is null or p.birthdate >= :maxBirthdate) " +
            "and " +
            "(coalesce(:jobTitleList, null) is null or p.job.title in (:jobTitleList)) " +
            "and " +
            "(:minWage is null or p.job.wage >= :minWage) " +
            "and " +
            "(:maxWage is null or p.job.wage <= :maxWage) " +
            "and " +
            "(:city is null or p.address.city = :city) " +
            "and " +
            "(:country is null or p.address.country = :country) " +
            "and " +
            "(:zipcode is null or p.address.zipcode = :zipcode) " +
            "and " +
            "(:email is null or p.email = :email) " +
            "and " +
            "(:status is null or p.status = :status) ")
    Page<Person> findPersonByWithPagination(
            @Param("id") Integer id,
            @Param("username") String username,
            @Param("firstname") String firstname,
            @Param("lastname") String lastname,
            @Param("genderIds") List<Integer> genderIds,
            @Param("minBirthdate") Date minBirthdate,
            @Param("maxBirthdate") Date maxBirthdate,
            @Param("jobTitleList") List<String> jobTitleList,
            @Param("minWage") Double minWage,
            @Param("maxWage") Double maxWage,
            @Param("city") String city,
            @Param("country") String country,
            @Param("zipcode") String zipcode,
            @Param("email") String email,
            @Param("status") Boolean status,
            Pageable pageable
    );

    @Query(
            "select p from Person p where p.id in (:idList)"
    )
    List<Person> findByIdList(List<Integer> idList);


    @Query("select p.blockedList from Person p where p.id = :id")
    List<Person> findBlockedListForPerson(Integer id);

    @Query("select p.deletedList from Person p where p.id = :id")
    List<Person> findDeletedListForPerson(Integer id);

    List<Person> findPeopleByUsernameOrEmail(@Param("username") String username, @Param("email") String email);
}
