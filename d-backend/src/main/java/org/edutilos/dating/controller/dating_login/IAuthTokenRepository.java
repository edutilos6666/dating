package org.edutilos.dating.controller.dating_login;

import org.edutilos.dating.model.dating_login.IAuthToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAuthTokenRepository extends JpaRepository<IAuthToken,Long> {

     public IAuthToken findByToken(String token);
     public IAuthToken getById(Long id);
}