package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.PersonToDeletedMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@Repository
public interface PersonToDeletedMappingRepository extends JpaRepository<PersonToDeletedMapping, Integer> {
    @Transactional("datingTransactionManager")
    @Modifying
    @Query("delete from PersonToDeletedMapping pMapping where pMapping.targetPersonId = :id")
    void deleteFromRelList(@Param("id") Integer id);
}
