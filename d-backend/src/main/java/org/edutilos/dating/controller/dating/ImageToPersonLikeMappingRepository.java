package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.ImageToPersonLikeMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * created by  Nijat Aghayev on 24.03.20
 */


@Repository
public interface ImageToPersonLikeMappingRepository extends JpaRepository<ImageToPersonLikeMapping, Integer> {
    @Transactional("datingTransactionManager")
    @Modifying
    @Query("delete from ImageToPersonLikeMapping iMapping where iMapping.personId = :id")
    void deleteFromRelList(@Param("id") Integer id);

    @Transactional("datingTransactionManager")
    @Modifying
    @Query("delete from ImageToPersonLikeMapping  iMapping where iMapping.imageId = :imageId")
    void deleteFromRelListByImageId(@Param("imageId") Integer imageId);
}
