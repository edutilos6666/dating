package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.PrivateMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Transactional
@Repository
public interface PrivateMessageRepository extends PagingAndSortingRepository<PrivateMessage, Integer> {




    @Query(
            "FROM PrivateMessage pm" +
                    " WHERE" +
                    " pm.userFrom.id IN (:userOneId, :userTwoId)" +
                    " AND" +
                    " pm.userTo.id IN (:userOneId, :userTwoId)"
    )
    List<PrivateMessage> getExistingChatMessagesBetween(
            @Param("userOneId") int userOneId,
            @Param("userTwoId") int userTwoId);

    @Query(
            "FROM PrivateMessage pm" +
                    " WHERE" +
                    " pm.userFrom.id IN (:userOneId, :userTwoId)" +
                    " AND" +
                    " pm.userTo.id IN (:userOneId, :userTwoId)"
    )
    Page<PrivateMessage> getExistingChatMessagesBetweenPageable(
            @Param("userOneId") int userOneId,
            @Param("userTwoId") int userTwoId,
            Pageable pageable);


    @Query(
            "FROM PrivateMessage pm" +
                    " WHERE" +
                    " (:channelId is null or pm.channel.id = :channelId)"
    )
    Page<PrivateMessage> getExistingChatMessagesBasedOnChannelIdPageable(
            @Param("channelId") int channelId, Pageable pageable);


    @Query(
            "SELECT COUNT(id) from PrivateMessage pm" +
                    " WHERE " +
                    " pm.channel.id = :channelId" +
                    " AND" +
                    " (pm.userFrom.id = :userId" +
                    " OR" +
                    " pm.userTo.id = :userId)"
    )
    Integer findCountByChannelIdAndUserId(@Param("channelId") int channelId,
                                       @Param("userId") int userId);


    @Modifying
    @Query("update PrivateMessage pm set pm.opened = true where pm.channel.id = :channelId and pm.userTo.id = :userToId")
    void markAsReadByChannelIdAndUserToId(@Param("channelId") int channelId, @Param("userToId") int userToId);


    @Query(
            "from PrivateMessage pm" +
                    " WHERE pm.userTo.id = :userToId" +
                    " AND" +
                    " pm.userFrom member of pm.userTo.favoriteList"
    )
    Page<PrivateMessage> findMessagesFromFavoriteListForPerson(@Param("userToId") int userToId, Pageable pageable);

    @Query(
            "from PrivateMessage pm" +
                    " WHERE pm.userTo.id = :userToId" +
                    " AND" +
                    " pm.userFrom member of pm.userTo.blockedList"
    )
    Page<PrivateMessage> findMessagesFromBlockedListForPerson(@Param("userToId") int userToId, Pageable pageable);

    @Query(
            "from PrivateMessage pm" +
                    " WHERE pm.userTo.id = :userToId" +
                    " AND" +
                    " pm.userFrom member of pm.userTo.deletedList"
    )
    Page<PrivateMessage> findMessagesFromDeletedListForPerson(@Param("userToId") int userToId, Pageable pageable);

    @Query(
            "from PrivateMessage  pm" +
                    " WHERE pm.userTo.id = :userToId" +
                    " AND" +
                    " pm.userFrom not member of pm.userTo.favoriteList" +
                    " AND" +
                    " pm.userFrom not member of pm.userTo.blockedList" +
                    " AND" +
                    " pm.userFrom not member of pm.userTo.deletedList"
    )
    Page<PrivateMessage> findNormalMessagesForPerson(@Param("userToId") int userToId, Pageable pageable);


    @Query(
            "from PrivateMessage  pm"+
                    " WHERE pm.userFrom.id = :userFromId"
    )
    Page<PrivateMessage> findSentMessages(@Param("userFromId") int userFromId, Pageable pageable);



    @Query("from PrivateMessage pm " +
            "WHERE " +
            "(:content is null or pm.content LIKE %:content%) " +
            "AND " +
            "(:createdFrom is null or pm.created >= :createdFrom) " +
            "AND " +
            "(:createdTo is null or pm.created <= :createdTo) " +
            "AND " +
            "(:opened is null or pm.opened = :opened) " +
            "AND " +
            "(:username is null or pm.userFrom.username LIKE %:username%) " +
            "AND " +
            "(:firstname is null or pm.userFrom.firstname LIKE %:firstname%) " +
            "AND " +
            "(:lastname is null or pm.userFrom.lastname LIKE %:lastname%) " +
            "AND " +
            "(:minBirthdate is null or pm.userFrom.birthdate >= :minBirthdate) " +
            "AND " +
            "(:maxBirthdate is null or pm.userFrom.birthdate <= :maxBirthdate) " +
            "AND " +
            "(:wageFrom is null or pm.userFrom.job.wage >= :wageFrom) " +
            "AND " +
            "(:wageTo is null or pm.userFrom.job.wage <= :wageTo) " +
            "AND " +
            "(:country is null or pm.userFrom.address.country LIKE %:country%) " +
            "AND " +
            "(:city is null or pm.userFrom.address.city LIKE %:city%) " +
            "AND " +
            "(:zipcode is null or pm.userFrom.address.zipcode LIKE %:zipcode%)"
    )
    Page<PrivateMessage> filterMessagesBy(
            @Param("content") String content,
            @Param("createdFrom") Date createdFrom,
            @Param("createdTo") Date createdTo,
            @Param("opened") Boolean opened,
            @Param("username") String username,
            @Param("firstname") String firstname,
            @Param("lastname") String lastname,
            @Param("minBirthdate") Date minBirthdate,
            @Param("maxBirthdate") Date maxBirthdate,
            @Param("wageFrom") Double wageFrom,
            @Param("wageTo") Double wageTo,
            @Param("country") String country,
            @Param("city") String city,
            @Param("zipcode") String zipcode,
            Pageable pageable
    );


    @Transactional("datingTransactionManager")
    @Modifying
    @Query("delete from PrivateMessage  pm where pm.userFrom.id = :userId or pm.userTo.id = :userId")
    void deleteAllByUserId(@Param("userId") Integer userId);
}
