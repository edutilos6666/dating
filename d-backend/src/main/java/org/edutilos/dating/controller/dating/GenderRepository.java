package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * created by  Nijat Aghayev on 12.03.20
 */
@Repository
public interface GenderRepository extends JpaRepository<Gender, Integer> {
    Gender findGenderByName(@Param("name") String name);
}
