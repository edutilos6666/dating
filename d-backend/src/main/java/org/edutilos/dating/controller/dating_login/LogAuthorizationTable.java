/**
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.controller.dating_login;

import org.edutilos.dating.model.dating_login.LogAuthorization;
import org.springframework.stereotype.Controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Controller
public class LogAuthorizationTable {

	@PersistenceContext(unitName = "dating_login")
	private EntityManager em;

	@org.springframework.transaction.annotation.Transactional("dating_loginTransactionManager")
	public LogAuthorization insert(LogAuthorization logAuthorization) {
		return em.merge(logAuthorization);
	}

}
