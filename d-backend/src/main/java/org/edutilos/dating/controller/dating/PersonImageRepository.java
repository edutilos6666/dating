package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.PersonImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by  Nijat Aghayev on 16.03.20
 */
@Repository
public interface PersonImageRepository extends JpaRepository<PersonImage, Integer> {
    @Query("from PersonImage pm where pm.person.id = :personId")
    List<PersonImage> findPersonImagesByPersonId(@Param("personId") int personId);

}
