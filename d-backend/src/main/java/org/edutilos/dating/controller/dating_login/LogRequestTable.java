package org.edutilos.dating.controller.dating_login;

import org.edutilos.dating.model.dating_login.LogRequest;
import org.springframework.stereotype.Controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Controller
public class LogRequestTable {
	@PersistenceContext(unitName = "dating_login")
	private EntityManager em;

	@org.springframework.transaction.annotation.Transactional("dating_loginTransactionManager")
	public void insert(LogRequest log) {
		em.persist(log);
	}
}
