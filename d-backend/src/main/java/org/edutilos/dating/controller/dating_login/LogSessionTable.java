package org.edutilos.dating.controller.dating_login;

import org.edutilos.dating.model.dating_login.LogSession;
import org.springframework.stereotype.Controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Controller
public class LogSessionTable {

	@PersistenceContext(unitName = "dating_login")
	private EntityManager em;

	@org.springframework.transaction.annotation.Transactional("dating_loginTransactionManager")
	public void insert(LogSession s) {
		em.persist(s);
	}

	public LogSession save(LogSession s) {
		return em.merge(s);
	}
}
