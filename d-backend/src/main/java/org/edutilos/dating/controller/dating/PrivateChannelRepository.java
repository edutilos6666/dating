package org.edutilos.dating.controller.dating;

import org.edutilos.dating.model.dating.PrivateChannel;
import org.edutilos.dating.model.dating.PrivateMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Transactional
@Repository
public interface PrivateChannelRepository extends PagingAndSortingRepository<PrivateChannel, Integer> {
    @Query(
            "from PrivateChannel pc" +
            " WHERE" +
            " pc.userFrom.id IN (:userOneId, :userTwoId)" +
            " AND" +
            " pc.userTo.id IN (:userOneId, :userTwoId)"
    )
    List<PrivateChannel> findPrivateChannelBy(@Param("userOneId") int userOneId,
                                              @Param("userTwoId") int userTwoId);

    @Query(
            "SELECT id" +
            " FROM PrivateChannel pc" +
            " WHERE" +
            " pc.userFrom.id IN (:userOneId, :userTwoId)" +
            " AND" +
            " pc.userTo.id IN (:userOneId, :userTwoId)"
    )
    Long getPrivateChannelIdBy(@Param("userOneId") int userOneId,
                               @Param("userTwoId") int userTwoId);


    @Query(
            "from PrivateChannel pc" +
            " WHERE " +
            " pc.userFrom.id = :userId" +
            " OR" +
            " pc.userTo.id = :userId"
    )
    Page<PrivateChannel> findPrivateChannelByUserIdWithPage(@Param("userId") int userId,
                                                            Pageable page);



    @Query(
            "from PrivateChannel pc" +
                    " WHERE " +
                    " (pc.userTo.id = :userToId" +
                    " AND" +
                    " pc.userFrom member of pc.userTo.favoriteList)" +
                    " OR " +
                    " (pc.userFrom.id = :userToId" +
                    " AND" +
                    " pc.userTo member of pc.userFrom.favoriteList) "
    )
    Page<PrivateChannel> findChannelsFromFavoriteListForPerson(@Param("userToId") int userToId, Pageable pageable);

    @Query(
            "from PrivateChannel pc" +
                    " WHERE " +
                    " (pc.userTo.id = :userToId" +
                    " AND" +
                    " pc.userFrom member of pc.userTo.blockedList)" +
                    " OR " +
                    " (pc.userFrom.id = :userToId" +
                    " AND" +
                    " pc.userTo member of pc.userFrom.blockedList) "
    )
    Page<PrivateChannel> findChannelsFromBlockedListForPerson(@Param("userToId") int userToId, Pageable pageable);

    @Query(
            "from PrivateChannel pc" +
                    " WHERE " +
                    " (pc.userTo.id = :userToId" +
                    " AND" +
                    " pc.userFrom member of pc.userTo.deletedList)" +
                    " OR " +
                    " (pc.userFrom.id = :userToId" +
                    " AND" +
                    " pc.userTo member of pc.userFrom.deletedList) "
    )
    Page<PrivateChannel> findChannelsFromDeletedListForPerson(@Param("userToId") int userToId, Pageable pageable);


    @Query(
            "from PrivateChannel  pc" +
                    " WHERE "+
                    " (pc.userTo.id = :userToId" +
                    " AND" +
                    " pc.userFrom not member of pc.userTo.favoriteList" +
                    " AND" +
                    " pc.userFrom not member of pc.userTo.blockedList" +
                    " AND" +
                    " pc.userFrom not member of pc.userTo.deletedList) " +
                    " OR " +
                    " (pc.userFrom.id = :userToId" +
                    " AND" +
                    " pc.userTo not member of pc.userFrom.favoriteList" +
                    " AND" +
                    " pc.userTo not member of pc.userFrom.blockedList" +
                    " AND" +
                    " pc.userTo not member of pc.userFrom.deletedList) "
    )
    Page<PrivateChannel> findChannelsForNormalMessagesForPerson(@Param("userToId") int userToId, Pageable pageable);

    @Transactional("datingTransactionManager")
    @Modifying
    @Query("delete from PrivateChannel  pc where pc.userFrom.id = :userId or pc.userTo.id = :userId")
    void deleteAllByUserId(@Param("userId") Integer userId);
}
