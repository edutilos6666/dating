/**
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.controller.dating_login;

import org.springframework.stereotype.Controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Controller
public class LogAccessTable {

	@PersistenceContext(unitName = "dating_login")
	private EntityManager em;

}
