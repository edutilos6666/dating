package org.edutilos.dating.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration

@EnableJpaRepositories(
        basePackages = "org.edutilos.dating.controller.dating_login",
        entityManagerFactoryRef = "dating_login",
        transactionManagerRef = "dating_loginTransactionManager"
)
public class Dating_loginConfig {
    
        @Autowired
        private Environment env;

    @Bean(name="dating_login")
        public LocalContainerEntityManagerFactoryBean dating_login() {
            LocalContainerEntityManagerFactoryBean em
                    = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(dating_loginDataSource());
            em.setPackagesToScan("org.edutilos.dating.model.dating_login");

            HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
            em.setJpaVendorAdapter(vendorAdapter);
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("hibernate.hbm2ddl.auto",
                    env.getProperty("hibernate.hbm2ddl.auto"));
            properties.put("hibernate.dialect",
                    env.getProperty("hibernate.dialect"));
            em.setJpaPropertyMap(properties);

            return em;
        }

        @Bean
    @ConfigurationProperties(prefix="spring.dating-login-datasource")
    public DataSource dating_loginDataSource() {
        return DataSourceBuilder.create().build();
    }

        @Bean
        public PlatformTransactionManager dating_loginTransactionManager() {

            JpaTransactionManager transactionManager
                    = new JpaTransactionManager();
            transactionManager.setEntityManagerFactory(
                    dating_login().getObject());
            return transactionManager;
        }
}
