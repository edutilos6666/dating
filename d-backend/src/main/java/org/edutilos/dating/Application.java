package org.edutilos.dating;


import org.edutilos.dating.controller.dating.*;
import org.edutilos.dating.model.dating.*;
import org.edutilos.dating.util.CryptoUtil;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Configuration
@SpringBootApplication(
        scanBasePackages = "org.edutilos.dating",
        exclude = {DataSourceAutoConfiguration.class,
                HibernateJpaAutoConfiguration.class,
                DataSourceTransactionManagerAutoConfiguration.class})
@EnableTransactionManagement
@EntityScan(basePackages = "org.edutilos.dating")
public class Application extends SpringBootServletInitializer {

    private static Class<Application> applicationClass = Application.class;

    public static void main(String[] args) {
        SpringApplication.run(applicationClass, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

        return application.sources(applicationClass);
    }



    @Bean
    public HttpMessageConverters additionalConverters() {
        return new HttpMessageConverters(bufferedImageHttpMessageConverter());
    }


    @Bean
    public BufferedImageHttpMessageConverter bufferedImageHttpMessageConverter() {
        BufferedImageHttpMessageConverter bufferedImageHttpMessageConverter = new BufferedImageHttpMessageConverter();
        return bufferedImageHttpMessageConverter;
    }


//    @Bean(name = "multipartResolver")
//    public CommonsMultipartResolver multipartResolver() {
//        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
//        multipartResolver.setMaxUploadSize(100000);
//        return multipartResolver;
//    }

    private Date getDateFromAge(int age) {
        return java.sql.Date.valueOf(LocalDate.now().minus(age, ChronoUnit.YEARS));
    }

    private int getRandomIndex(int upperBound, final Random random) {
        return random.nextInt(upperBound);
    }
    private int getRandomAge(final Random random) {
        return random.nextInt(60) + 18;
    }

    private List<Job> jobList = new ArrayList<>();
    private List<Address> addressList = new ArrayList<>();


    private Person constructPerson(int i,
                                   GenderRepository genderRepository,
                                   final int genderSize,
                                   final Random random) {
        return new Person(
                i,
                "foobar"+ i,
                "foobar"+i,
                0,
                new Timestamp(new Date().getTime()),
                "foo"+i,
                "bar"+i,
                genderRepository.findById(getRandomIndex(genderSize, random)+1).orElse(null),
                getDateFromAge(getRandomAge(random)),
                jobList.get(getRandomIndex(jobList.size(), random)),
                addressList.get(getRandomIndex(addressList.size(), random)),
                String.format("foo%d@bar%d.com", i, i),
                String.format("foo%dbar%d aboutMe", i, i),
                String.format("foo%dbar%d aboutIdealPartner", i, i),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                random.nextBoolean(),
                Collections.emptyList());
    }


    private List<PersonImage> constructImages(List<Person> personList, Person targetPerson) {
        // TODO: it is not good to speculate that personList.size() will be at least 1 + 2+ 3 + 4 = 10
        PersonImage personImage = new PersonImage(
                1,
          true,
          "/dating/1_1.png",
          "First Image",
          "First Image Header",
          "First Image Footer",
                Arrays.asList(personList.get(0), personList.get(1)),
                Arrays.asList(personList.get(2), personList.get(3), personList.get(4)),
                Arrays.asList(personList.get(5), personList.get(6), personList.get(7), personList.get(8)),
                targetPerson
        );

        PersonImage personImage2 = new PersonImage(
                2,
                false,
                "/dating/1_2.png",
                "Second Image",
                "Second Image Header",
                "Second Image Footer",
                Arrays.asList(personList.get(5), personList.get(6), personList.get(7), personList.get(8)),
                Arrays.asList(personList.get(2), personList.get(3)),
                Arrays.asList(personList.get(0), personList.get(1), personList.get(4)),
                targetPerson
        );
        return Arrays.asList(personImage, personImage2);
    }

    @Bean
    public CommandLineRunner loadData(GenderRepository genderRepository,
                                      PersonRepository personRepository,
                                      PersonImageRepository personImageRepository) {
        return (args)-> {
            genderRepository.save(new Gender(1, "Male"));
            genderRepository.save(new Gender(2, "Female"));

            addressList.add(new Address( "Germany", "Bochum", "1"));
            addressList.add(new Address("Germany", "Essen", "2"));
            addressList.add(new Address("Germany", "Essen", "3"));
            addressList.add(new Address("Germany", "Aachen", "4"));
            addressList.add(new Address("Germany", "Duisburg", "5"));
            addressList.add(new Address("Germany", "Düsseldorf", "6"));
            addressList.add(new Address("Germany", "Köln", "7"));

            jobList.add(new Job( "Junior Software Developer", "DDG", 1000D));
            jobList.add(new Job( "Senior Software Developer", "DDG", 3000D));
            jobList.add(new Job( "Lead Software Developer", "DDG", 4000D));
            jobList.add(new Job( "Devop", "DDG", 2000D));
            jobList.add(new Job( "Manager", "DDG", 5000D));
            jobList.add(new Job( "Scrum Master", "DDG", 2000D));
            jobList.add(new Job( "Receptionist", "DDG", 500D));


            int genderSize = genderRepository.findAll().size();
            final Random random = new Random();

            for(int i = 1; i <= 200; ++i) {
                personRepository.save(constructPerson(i, genderRepository,genderSize, random));
            }

            personRepository.findAll().forEach(one-> {
                one.setPassword(CryptoUtil.getPass(one.getPassword(), one));
                personRepository.save(one);
            });

            List<Person> personList = personRepository.findAll();
            Person person1 = personList.stream().filter(one-> one.getId().equals(1)).findFirst().get();
            personList = personList.stream().filter(one-> !one.getId().equals(1)).collect(Collectors.toList());
            constructImages(personList, person1).forEach(personImageRepository::save);
//            person1.setImages(personImageRepository.findAll());
            person1.setFavoriteList(personList.stream().filter(one-> Arrays.asList(2, 3, 4).indexOf(one.getId()) >= 0).collect(Collectors.toList()));
            person1.setBlockedList(personList.stream().filter(one-> Arrays.asList(5, 6, 7).indexOf(one.getId()) >= 0).collect(Collectors.toList()));
            person1.setDeletedList(personList.stream().filter(one-> Arrays.asList(8, 9, 10).indexOf(one.getId()) >= 0).collect(Collectors.toList()));
            personRepository.save(person1);

        };
    }
}

