package org.edutilos.dating.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class MessageCounterDTO {
    private int channelId;
    private int messageCount;

    public MessageCounterDTO(int channelId, int messageCount) {
        this.channelId = channelId;
        this.messageCount = messageCount;
    }
}
