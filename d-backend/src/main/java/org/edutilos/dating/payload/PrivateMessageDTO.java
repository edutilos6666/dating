package org.edutilos.dating.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class PrivateMessageDTO {
    private int userFrom;
    private int userTo;
    private int channelId;
    private String content;
    private Date created;
    private boolean opened;

    public PrivateMessageDTO(int userFrom, int userTo, int channelId, String content, Date created, boolean opened) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.channelId = channelId;
        this.content = content;
        this.created = created;
        this.opened = opened;
    }

    public PrivateMessageDTO(int userFrom, int userTo, int channelId, String content, Date created) {
        this(userFrom, userTo, channelId, content, created, false);
    }
}
