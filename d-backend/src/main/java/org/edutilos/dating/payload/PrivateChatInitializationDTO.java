package org.edutilos.dating.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PrivateChatInitializationDTO {
    private int userFromId;
    private int userToId;

    public PrivateChatInitializationDTO(int userFromId, int userToId) {
        this.userFromId = userFromId;
        this.userToId = userToId;
    }
}
