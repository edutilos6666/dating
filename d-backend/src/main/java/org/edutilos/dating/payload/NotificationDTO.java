package org.edutilos.dating.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class NotificationDTO {
    private String type;
    private String message;
    private int fromUserId;

    public NotificationDTO(String type, String message, int fromUserId) {
        this.type = type;
        this.message = message;
        this.fromUserId = fromUserId;
    }
}
