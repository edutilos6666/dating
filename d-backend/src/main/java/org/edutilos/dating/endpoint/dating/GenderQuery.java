package org.edutilos.dating.endpoint.dating;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.edutilos.dating.controller.dating.GenderRepository;
import org.edutilos.dating.model.dating.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@Service
public class GenderQuery implements GraphQLQueryResolver {
    @Autowired
    private GenderRepository genderRepository;
    public List<Gender> findAllGender() {
        return genderRepository.findAll();
    }
}
