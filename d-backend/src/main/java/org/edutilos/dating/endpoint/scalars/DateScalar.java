package org.edutilos.dating.endpoint.scalars;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

import java.time.Instant;
import java.util.Date;

public class DateScalar extends GraphQLScalarType {
    private static final String DEFAULT_NAME = "DateTime";

    public DateScalar() {
        this(DEFAULT_NAME);
    }

    public DateScalar(final String name) {


        super(name, "DateTime", new Coercing<Date, Long>() {
            private Date convertImpl(Object input) {
                //Date wird vom GraphQL Wrapper IMMER in UTC Zeit übertragen....
                if (input instanceof String) {
                    try {
                        Date date= Date.from(Instant.parse((String) input));
                        return date;
                    } catch (Exception e) {

                    }
                    return new Date((String) input);
                }
                if (input instanceof Number) {
                    return new Date(((Number) input).longValue());
                }
                return null;
            }

            @Override
            public Long serialize(Object input) {
                if (input instanceof Date) {
                    return ((Date) input).getTime();
                } else {
                    Date result = convertImpl(input);
                    if (result == null) {
                        throw new CoercingSerializeException("Invalid value '" + input + "' for Date");
                    }
                    return result.getTime();
                }
            }

            @Override
            public Date parseValue(Object input) {
                Date result = convertImpl(input);
                if (result == null) {
                    throw new CoercingParseValueException("Invalid value '" + input + "' for Date");
                }
                return result;
            }

            @Override
            public Date parseLiteral(Object input) {
                if (!(input instanceof StringValue)) return null;
                String value = ((StringValue) input).getValue();
                Date result = convertImpl(value);
                return result;
            }
        });
    }


}
