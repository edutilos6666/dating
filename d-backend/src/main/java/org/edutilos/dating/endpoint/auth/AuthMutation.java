package org.edutilos.dating.endpoint.auth;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import graphql.schema.DataFetchingEnvironment;
import graphql.servlet.context.DefaultGraphQLServletContext;
import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.controller.dating_login.IAuthTokenRepository;
import org.edutilos.dating.controller.dating_login.LogAuthorizationTable;
import org.edutilos.dating.controller.dating_login.LogSessionTable;
import org.edutilos.dating.model.auth.LoginError;
import org.edutilos.dating.model.auth.LoginResponse;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.model.dating_login.IAuthToken;
import org.edutilos.dating.model.dating_login.LogAuthorization;
import org.edutilos.dating.model.dating_login.LogSession;
import org.edutilos.dating.security.token.AuthorizationToken;
import org.edutilos.dating.security.token.TokenProvider;
import org.edutilos.dating.util.CryptoUtil;
import org.edutilos.dating.util.Redis;
import org.edutilos.dating.util.graphql.security.Unsecured;
import org.redisson.api.RMapAsync;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.edutilos.dating.service.auth.AuthenticationFilter.SESSION_EXPIRE_DELAY;

@Service
public class AuthMutation implements GraphQLMutationResolver {
    public static final String IAUTH = "iAuth";
    public static final String REFRESH = "REFRESH";
    public static final Integer MAPTOKEN_EXPIRETIME_MINUTES = 5;
    public static final Integer MAPREFRESH_EXPIRETIME_MINUTES = 60;
    public static final Integer PASSWORD_EXPIRETIME_DAYS = 90;
    public static final int COLUM_SIZE_IP = 128;

    @Autowired
    private LogAuthorizationTable logAuthTable;
    @Autowired
    private IAuthTokenRepository iAuthTable;
    @Autowired
    private TokenProvider tokenProvider;
    @Inject
    private LogSessionTable logSessionTable;
    @Inject
    private PersonRepository personRepository;
    @Autowired
    private Redis redis;
    private static Person currentPerson;



    public void logout() {
        currentPerson = null;
    }

    @Unsecured
    public AuthInformation loginWithUserNamePassword(String username, String password, DataFetchingEnvironment request) {
        return login(username, password, "iAuth", ((DefaultGraphQLServletContext) request.getContext()).getHttpServletRequest());
    }

    @Unsecured
    public AuthInformation loginWithToken(String username, String token, DataFetchingEnvironment request) {

        Person person = null;
        RMapAsync<String, Object> map = redis.getMap(token);
        Future<Object> fo = map.getAsync("PERSON_ID");
        try {
            Object personId = fo.get();
            if (personId == null) {
                return new AuthInformation(LoginError.getInvalidLogin(), Response.Status.FORBIDDEN.getStatusCode());
            }

            person = personRepository.findById((Integer) personId).orElse(null);
            //httpRequest.setAttribute("USER", u);
        } catch (InterruptedException | ExecutionException e) {
            LoggerFactory.getLogger(getClass()).error("InterruptedException | ExecutionException", e);
        }
        // System.out.println("news");
        map.putAsync("LAST_USE", Calendar.getInstance().getTimeInMillis());
        map.expireAsync(SESSION_EXPIRE_DELAY, TimeUnit.MINUTES);

        LoginResponse resp = LoginResponse.createLoginResponse(person.getId(), token, "IAuth");
        currentPerson = person;
        return new AuthInformation(resp, tokenProvider.getTokenFromLoginResponse(resp, person));
    }

    public AuthInformation refreshToken() {
        //TODO NAG holen aus alten Token Daten?  refreshTokenOldWay(); Ggf. aber auch über Frontend (IFRame Steuerung pushen)
        try {
            if(currentPerson == null) throw new Exception("currentPerson is null.");
            AuthorizationToken authorizationToken = tokenProvider.renewAuthorizationTokenForCurrentUser();
            LoginResponse resp = LoginResponse.createLoginResponse(currentPerson.getId(), "token", "IAuth");
            return new AuthInformation(resp, authorizationToken.getToken());
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return new AuthInformation(LoginError.getExpiredLogin(), Response.Status.FORBIDDEN.getStatusCode());
    }

    //TODO NAG  Verfeinern oder entfernen
    private void refreshTokenOldWay(String refreshOldToken, String tokenType) {
        RMapAsync<String, Object> map = redis.getMap(refreshOldToken);
        Object o = null;
        try {
            o = map.getAsync(REFRESH).get();
        } catch (InterruptedException | ExecutionException e) {
            LoggerFactory.getLogger(getClass()).error("InterruptedException | ExecutionException", e);
        }
        if (o instanceof String) {
            String sessionToken = (String) o;
            RMapAsync<String, Object> mapToken = redis.getMap(sessionToken);
            Integer personId = null;
            try {
                personId = (Integer) mapToken.getAsync("PERSON_ID").get();
            } catch (InterruptedException | ExecutionException e) {
                LoggerFactory.getLogger(getClass()).error("InterruptedException | ExecutionException", e);
            }
            if (personId != null) {
                mapToken.expireAsync(MAPTOKEN_EXPIRETIME_MINUTES, TimeUnit.MINUTES);
                LoginResponse resp = LoginResponse.createLoginResponse(personId, (String) o, tokenType);

                RMapAsync<String, Object> mapRefresh = redis.getMap(resp.getRefresh_token());
                mapRefresh.putAsync("REFRESH", resp.getAccess_token());
                mapRefresh.expireAsync(MAPREFRESH_EXPIRETIME_MINUTES, TimeUnit.MINUTES);


            }
        }
    }


    private AuthInformation loginIAuth(String token, HttpServletRequest servletRequest) {
        if (token == null || token.isEmpty()) {
            return new AuthInformation(LoginError.getMissingLogin(), Response.Status.BAD_REQUEST.getStatusCode());
        }

        final IAuthToken iAuthToken = iAuthTable.findByToken(token);

        if (iAuthToken == null) {
            return new AuthInformation(LoginError.getExpiredLogin(), Response.Status.FORBIDDEN.getStatusCode());
        }

        final Person person = personRepository.getById(iAuthToken.getPersonId());
        return checkLogin(person, IAUTH, servletRequest);
    }

    private AuthInformation checkLogin(Person person, String tokenType, HttpServletRequest httpServletRequest) {
        if (person != null) {
            LoginResponse resp = LoginResponse.createLoginResponse(person.getId(), null, tokenType);
            person.setLastLogin(new Timestamp(Calendar.getInstance().getTimeInMillis()));

            final LogAuthorization logAuth = new LogAuthorization();
            logAuth.setLoginAt(new Timestamp(new Date().getTime()));
            logAuth.setPersonId(person.getId());

            String ip = httpServletRequest.getHeader("X-Forwarded-For");
            if (ip == null || ip.isEmpty()) {
                ip = httpServletRequest.getHeader("HOST");
            }
            String sessionToken = resp.getAccess_token();
            RMapAsync<String, Object> map = redis.getMap(sessionToken);
            map.putAsync("PERSON_ID", person.getId()).thenAcceptAsync(f -> {
                personRepository.save(person);
                logAuthTable.insert(logAuth);
            });

            map.expireAsync(MAPTOKEN_EXPIRETIME_MINUTES, TimeUnit.MINUTES);

            RMapAsync<String, Object> mapRefresh = redis.getMap(resp.getRefresh_token());
            mapRefresh.putAsync(REFRESH, resp.getAccess_token());
            mapRefresh.expireAsync(5, TimeUnit.MINUTES);

            if (ip.length() > COLUM_SIZE_IP) {
                ip = ip.substring(0, COLUM_SIZE_IP);
            }

            LogSession session = new LogSession();
            session.setTimeCreated(Timestamp.from(Instant.now()));
            session.setSessionToken(resp.getAccess_token());
            session.setPersonId(person.getId());
            session.setIp(ip);

            logSessionTable.insert(session);
            currentPerson = person;
            return new AuthInformation(resp, tokenProvider.getTokenFromLoginResponse(resp, person));
        }

        return new AuthInformation(LoginError.getInvalidLogin(), Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Unsecured
    public AuthInformation changeUserPassword(String username, String password, String newPassword, DataFetchingEnvironment request) {
        if (newPassword == null || newPassword.isEmpty()) {
            return new AuthInformation(LoginError.getBadPassword(), Response.Status.BAD_REQUEST.getStatusCode());
        }

        Person person = personRepository.findByUsername(username);
        if (person == null) {
            return new AuthInformation(LoginError.getUserDoesntExist(), Response.Status.BAD_REQUEST.getStatusCode());
        }
        if (!CryptoUtil.checkPass(password, person)) {
            return new AuthInformation(LoginError.getIncorrectPassword(), Response.Status.BAD_REQUEST.getStatusCode());
        }
        person.setPasswordType(CryptoUtil.PASSWORD_TYPE_AES_SHA);
        person.setPassword(CryptoUtil.getPass(newPassword, person));
        personRepository.save(person);

        return loginWithUserNamePassword(username, newPassword, request);
    }


    private AuthInformation login(String username, String password, String tokenType, HttpServletRequest httpServletRequest) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return new AuthInformation(LoginError.getMissingLogin(), Response.Status.BAD_REQUEST.getStatusCode());
        }

        Person person = personRepository.findByUsername(username);
        if (person == null || !CryptoUtil.checkPass(password, person)) {
            return new AuthInformation(LoginError.getInvalidLogin(), Response.Status.UNAUTHORIZED.getStatusCode());
        }

        AuthInformation resp = checkLogin(person, tokenType, httpServletRequest);

        if (IAUTH.equals(tokenType) && resp.getStatus() == Response.Status.OK.getStatusCode()) {
            LoginResponse lresp = resp.getLoginResponse();

            IAuthToken token = new IAuthToken();
            token.setCreatedAt(new Timestamp(new Date().getTime()));
            token.setLastUse(token.getCreatedAt());
            token.setToken(lresp.getAccess_token());
            token.setPersonId(person.getId());

            iAuthTable.save(token); // Needs blocking
        }

        return resp;
    }

    public static Person getCurrentPerson() {
        return currentPerson;
    }
}
