package org.edutilos.dating.endpoint.dating;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import lombok.extern.slf4j.Slf4j;
import org.edutilos.dating.controller.dating.GenderRepository;
import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.model.dating.Gender;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.model.dating.PersonImage;
import org.edutilos.dating.util.CryptoUtil;
import org.edutilos.dating.util.graphql.security.Unsecured;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@Service
@Slf4j
public class PersonMutation implements GraphQLMutationResolver {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private GenderRepository genderRepository;
    @Autowired
    private PersonPreRemoveHook personPreRemoveHook;


    @Transactional("datingTransactionManager")
    public void deletePerson(Integer id) {
        personRepository.findById(id).ifPresent(person-> {
            personPreRemoveHook.preRemovePerson(person);
            personRepository.delete(person);
        });

    }



    public void updatePerson(Person person) {
        Optional<Person> personOptional = personRepository.findById(person.getId());
        if(personOptional.isPresent()) {
            Person oldPerson = personOptional.get();
            person.setDeletedList(oldPerson.getDeletedList());
            person.setBlockedList(oldPerson.getBlockedList());
            person.setFavoriteList(oldPerson.getFavoriteList());
            person.setImages(oldPerson.getImages());
            person.setPassword(oldPerson.getPassword());
            person.setPasswordType(oldPerson.getPasswordType());
            person.setLastLogin(oldPerson.getLastLogin());
            person.setStatus(oldPerson.getStatus());
            personRepository.save(person);
        }
    }

    @Unsecured
    @Transactional("datingTransactionManager")
    public void savePerson(Person person) {
        Gender fetchedGender = genderRepository.findGenderByName(person.getGender().getName());
        person.setGender(fetchedGender);
        person.setPasswordType(CryptoUtil.PASSWORD_TYPE_AES_SHA);
        person.setPassword(CryptoUtil.getPass(person.getPassword(), person));
        personRepository.save(person);
    }

    enum UpdateRelCommand {
        FAVORITE , BLOCK, DELETE
    }

    @Transactional("datingTransactionManager")
    public void updatePersonRel(
            int id,
            int idToUpdate,
            UpdateRelCommand command
    ) {
        // personOptional and personIdToUpdateOptional must exist!!!
        Optional<Person> personOptional =  personRepository.findById(id);
        Optional<Person> personIdToUpdateOptional = personRepository.findById(idToUpdate);
        if(personOptional.isPresent() && personIdToUpdateOptional.isPresent()) {
            Person person = personOptional.get();
            Person personUpdate = personIdToUpdateOptional.get();
            List<Person> favoriteList = person.getFavoriteList();
            List<Person> blockedList = person.getBlockedList();
            List<Person> deletedList = person.getDeletedList();

            favoriteList = favoriteList.stream().filter(one -> !one.getId().equals(personUpdate.getId())).collect(Collectors.toList());
            blockedList = blockedList.stream().filter(one -> !one.getId().equals(personUpdate.getId())).collect(Collectors.toList());
            deletedList = deletedList.stream().filter(one -> !one.getId().equals(personUpdate.getId())).collect(Collectors.toList());

            if(command == UpdateRelCommand.FAVORITE)
                favoriteList.add(personUpdate);
            if(command == UpdateRelCommand.BLOCK)
                blockedList.add(personUpdate);
            if(command == UpdateRelCommand.DELETE)
                deletedList.add(personUpdate);


            person.setFavoriteList(favoriteList);
            person.setBlockedList(blockedList);
            person.setDeletedList(deletedList);
            personRepository.save(person);
        }
        }


}
