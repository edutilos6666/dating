package org.edutilos.dating.endpoint.dating;

import org.edutilos.dating.controller.dating.ImageToPersonDislikeMappingRepository;
import org.edutilos.dating.controller.dating.ImageToPersonHeartMappingRepository;
import org.edutilos.dating.controller.dating.ImageToPersonLikeMappingRepository;
import org.edutilos.dating.model.dating.PersonImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * created by  Nijat Aghayev on 25.03.20
 */
@Service
public class PersonImagePreRemoveHook {
    @Autowired
    private ImageToPersonLikeMappingRepository imageToPersonLikeMappingRepository;
    @Autowired
    private ImageToPersonDislikeMappingRepository imageToPersonDislikeMappingRepository;
    @Autowired
    private ImageToPersonHeartMappingRepository imageToPersonHeartMappingRepository;
    // that Annotation(@PreRemove) does not do job, that i expected , it is called after personImageRepository.delete()
    // but i want before
//    @PreRemove
    public void preRemoveImage(PersonImage personImage) {
        imageToPersonLikeMappingRepository.deleteFromRelListByImageId(personImage.getId());
        imageToPersonDislikeMappingRepository.deleteFromRelListByImageId(personImage.getId());
        imageToPersonHeartMappingRepository.deleteFromRelListByImageId(personImage.getId());
        personImage.setPerson(null);
    }
}
