package org.edutilos.dating.endpoint.dating;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.edutilos.dating.controller.dating.PersonImageRepository;
import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.model.dating.PersonImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * created by  Nijat Aghayev on 16.03.20
 */
@Service
public class PersonImageMutation implements GraphQLMutationResolver {
    @Autowired
    private PersonImageRepository personImageRepository;
    @Autowired
    private PersonRepository personRepository;

    public void updatePersonImage(Integer id, List<Integer> likes, List<Integer> dislikes, List<Integer> hearts) {
        Optional<PersonImage> one = personImageRepository.findById(id);

        one.ifPresent(personImage -> {
            if(!likes.isEmpty())
            personImage.setLikes(personRepository.findByIdList(likes));
            if(!dislikes.isEmpty())
            personImage.setDislikes(personRepository.findByIdList(dislikes));
            if(!hearts.isEmpty())
            personImage.setHearts(personRepository.findByIdList(hearts));
            personImageRepository.save(personImage);
        });
    }

    public void setPersonImageAsProfileImage(Integer id) {
        Optional<PersonImage>  personImageOptional = personImageRepository.findById(id);
        if(personImageOptional.isPresent()) {
            PersonImage personImage = personImageOptional.get();
            List<PersonImage> personImageList = personImageRepository.findPersonImagesByPersonId(personImage.getPerson().getId());
            personImageList.forEach(one-> {
                one.setMain(one.getId().equals(id));
            });
            personImageRepository.saveAll(personImageList);
        }
    }
}
