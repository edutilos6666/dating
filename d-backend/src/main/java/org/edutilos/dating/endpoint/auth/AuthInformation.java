package org.edutilos.dating.endpoint.auth;

import org.edutilos.dating.model.auth.LoginError;
import org.edutilos.dating.model.auth.LoginResponse;

public class AuthInformation {
    private LoginResponse loginResponse;
    private LoginError loginError;
    private  String token;
    private int status;

    public AuthInformation(LoginResponse loginResponse,String token) {
        this.loginResponse = loginResponse;
        this.status = 200;
        this.token = token;
    }

    public AuthInformation(LoginError loginError, int status) {
        this.loginError = loginError;
        this.status = status;
    }

    public LoginResponse getLoginResponse() {
        return loginResponse;
    }


    public LoginError getLoginError() {
        return loginError;
    }


    public int getStatus() {
        return status;
    }


}
