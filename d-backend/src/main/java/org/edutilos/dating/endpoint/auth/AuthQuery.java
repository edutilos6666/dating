package org.edutilos.dating.endpoint.auth;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.apache.commons.io.IOUtils;
import org.edutilos.dating.security.token.TokenProvider;
import org.edutilos.dating.util.graphql.StringResult;
import org.edutilos.dating.util.graphql.security.Unsecured;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthQuery implements GraphQLQueryResolver {
    @Autowired
    private TokenProvider tokenProvider;


    @Unsecured
    public StringResult getPublicKey() {
        try {
            return new StringResult(IOUtils.toString(tokenProvider.loadPublicKey().getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
