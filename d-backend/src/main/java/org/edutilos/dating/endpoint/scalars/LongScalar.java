package org.edutilos.dating.endpoint.scalars;

import graphql.schema.Coercing;
import graphql.schema.GraphQLScalarType;

import java.math.BigDecimal;

public class LongScalar extends GraphQLScalarType {
    private static final String DEFAULT_NAME = "Long";

    public LongScalar() {
        this(DEFAULT_NAME);
    }

    public LongScalar(final String name) {


        super(name, "Long", new Coercing<BigDecimal, Long>() {
            @Override
            public Long serialize(Object input) {
                return new Double(Double.parseDouble(input.toString())).longValue();
            }

            @Override
            public BigDecimal parseValue(Object input) {
                return new BigDecimal(input.toString());
            }

            @Override
            public BigDecimal parseLiteral(Object input) {
                return new BigDecimal(input.toString());
            }
        });
    }


}
