package org.edutilos.dating.endpoint.scalars;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

import java.sql.Time;
import java.util.Date;

public class TimeScalar extends GraphQLScalarType {
    private static final String DEFAULT_NAME = "Time";

    public TimeScalar() {
        this(DEFAULT_NAME);
    }

    public TimeScalar(final String name) {


        super(name, "Time", new Coercing<Time, Long>() {
            private Time convertImpl(Object input) {
                if (input instanceof String) {
                    return new Time(new Date((String) input).getTime());
                }
                if (input instanceof Number) {
                    return new Time(((Number) input).longValue());
                }
                return null;
            }

            @Override
            public Long serialize(Object input) {
                if (input instanceof Time) {
                    return ((Date) input).getTime();
                } else {
                    Time result = convertImpl(input);
                    if (result == null) {
                        throw new CoercingSerializeException("Invalid value '" + input + "' for Date");
                    }
                    return result.getTime();
                }
            }

            @Override
            public Time parseValue(Object input) {
                Time result = convertImpl(input);
                if (result == null) {
                    throw new CoercingParseValueException("Invalid value '" + input + "' for Date");
                }
                return result;
            }

            @Override
            public Time parseLiteral(Object input) {
                if (!(input instanceof StringValue)) return null;
                String value = ((StringValue) input).getValue();
                Time result = convertImpl(value);
                return result;
            }
        });
    }


}
