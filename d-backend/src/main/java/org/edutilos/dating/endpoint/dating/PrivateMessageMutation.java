package org.edutilos.dating.endpoint.dating;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.edutilos.dating.controller.dating.PrivateMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * created by  Nijat Aghayev on 18.03.20
 */
@Service
public class PrivateMessageMutation implements GraphQLMutationResolver {
    @Autowired
    private PrivateMessageRepository privateMessageRepository;

    @Transactional
    public boolean markAsReadByChannelIdAndUserToId(
            Optional<Integer> channelId,
            Optional<Integer> userToId
    ) {
        if(channelId.isPresent() && userToId.isPresent()) {
            privateMessageRepository.markAsReadByChannelIdAndUserToId(channelId.get(), userToId.get());
            return true;
        }
        return false;
    }
}
