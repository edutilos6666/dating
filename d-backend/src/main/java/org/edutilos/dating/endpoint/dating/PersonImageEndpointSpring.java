package org.edutilos.dating.endpoint.dating;

import lombok.extern.slf4j.Slf4j;
import org.edutilos.dating.controller.dating.PersonImageRepository;
import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.model.dating.PersonImage;
import org.edutilos.dating.model.dating.PersonImageForm;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class PersonImageEndpointSpring {
    @Inject
    private PersonImageRepository personImageRepository;
    @Inject
    private PersonRepository personRepository;
    @Inject
    private PersonImagePreRemoveHook personImagePreRemoveHook;

    @GetMapping(value = "/images/download/{image_id}",  produces = "image/png")
    public ResponseEntity<byte[]> downloadImage(@PathVariable("image_id") Integer imageId) throws IOException {
        // TODO: currentPerson.getId() in AuthMutation -> throws sometimes NPE
//        if(AuthMutation.getCurrentPerson() != null) {
            Optional<PersonImage> personImage = personImageRepository.findById(imageId);
            if(personImage.isPresent()) {
                ResponseEntity<byte[]> res = new ResponseEntity<>(Files.readAllBytes(Paths.get(personImage.get().getSrcPath())), new HttpHeaders(), HttpStatus.OK);
                return res;
            } else {
                return null;
            }
//        }
//        return null;
    }

    @PostMapping(value = "/images/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public String uploadImage(@ModelAttribute PersonImageForm personImageForm) {
        MultipartFile file = personImageForm.getMultipartFile();

        Optional<Person> personOptional = personRepository.findById(personImageForm.getPersonId());
        if (personOptional.isPresent()) {
            Person person = personOptional.get();
            try {
                PersonImage personImage = new PersonImage(
                        false,
                        "",
                        personImageForm.getAlt(),
                        personImageForm.getHeader(),
                        personImageForm.getFooter(), person);
                person.getImages().add(personImage);
                personRepository.save(person);
                List<PersonImage> personImageList =
                        personImageRepository.findAll(PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "id"))).getContent();
                if(!personImageList.isEmpty()) {
                    personImage = personImageList.get(0);
                    String srcPath = String.format("/dating/%d_%d_%s", personImageForm.getPersonId(), personImage.getId(), file.getOriginalFilename());
                    byte[] bytes = file.getBytes();
                    saveFile(new ByteArrayInputStream(bytes), srcPath);
                    personImage.setSrcPath(srcPath);
                    personImageRepository.save(personImage);
                }
            } catch (IOException e) {
                // TODO remove
                e.printStackTrace();
                log.error(e.getMessage());
                return e.getMessage();
            }
            return "Successfully uploaded image and saved entry to the database table.";
        }
        return String.format("Unable to find Person with id = %d", personImageForm.getPersonId());
    }

    @DeleteMapping(value="/images/delete/{image_id}")
    @ResponseBody
    public String deleteImage(@PathVariable("image_id") Integer imageId) {
        Optional<PersonImage> personImageOptional = personImageRepository.findById(imageId);
        if(personImageOptional.isPresent()) {
            PersonImage personImage = personImageOptional.get();
            String srcPath = personImage.getSrcPath();
            try {
                Files.deleteIfExists(Paths.get(personImage.getSrcPath()));
                personImagePreRemoveHook.preRemoveImage(personImage);
                personImageRepository.delete(personImage);
                return String.format("Successfully deleted image with id = %d", imageId);
            } catch (IOException e) {
                // TODO remove
                e.printStackTrace();
                log.error(e.getMessage());
                return e.getMessage();
            }
        } else {
            return String.format("could not find image with id = %d", imageId);
        }
    }

    private void saveFile(InputStream file, String name) throws IOException {
        java.nio.file.Path path = FileSystems.getDefault().getPath(name);
        Files.deleteIfExists(path);
        Files.copy(file, path);
    }

    private String decodeFileName(String fileName) {
        try {
            return URLDecoder.decode(fileName, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage());
            return fileName;
        }
    }
}
