package org.edutilos.dating.endpoint.dating;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.edutilos.dating.controller.dating.PrivateChannelRepository;
import org.edutilos.dating.model.dating.PrivateChannel;
import org.edutilos.dating.util.graphql.GraphQLPaginationInput;
import org.edutilos.dating.util.graphql.GraphQLPaginationPageableFactory;
import org.edutilos.dating.util.graphql.GraphQLPaginationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class PrivateChannelQuery implements GraphQLQueryResolver {
    @Autowired
    private PrivateChannelRepository privateChannelRepository;


    public PrivateChannel findPrivateChannelById(int id) {
        return privateChannelRepository.findById(id).orElse(null);
    }

    public GraphQLPaginationResult<PrivateChannel> findPrivateChannelListBy(
            Optional<Integer> userId,
            GraphQLPaginationInput pagination
    ) {
        if(userId.isPresent()) {
            return new GraphQLPaginationResult<>(
                    privateChannelRepository.findPrivateChannelByUserIdWithPage(
                            userId.get(),
                            GraphQLPaginationPageableFactory.toPageable(pagination))
            );
        }
        return new GraphQLPaginationResult<>(privateChannelRepository.findAll(
                GraphQLPaginationPageableFactory.toPageable(pagination)));
    }


    public GraphQLPaginationResult<PrivateChannel> findChannelsFromFavoriteListForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateChannelRepository.findChannelsFromFavoriteListForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

    public GraphQLPaginationResult<PrivateChannel> findChannelsFromBlockedListForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateChannelRepository.findChannelsFromBlockedListForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

    public GraphQLPaginationResult<PrivateChannel> findChannelsFromDeletedListForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateChannelRepository.findChannelsFromDeletedListForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

    public GraphQLPaginationResult<PrivateChannel> findChannelsForNormalMessagesForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateChannelRepository.findChannelsForNormalMessagesForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

}
