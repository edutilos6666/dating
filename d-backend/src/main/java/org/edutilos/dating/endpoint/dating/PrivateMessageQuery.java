package org.edutilos.dating.endpoint.dating;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.edutilos.dating.controller.dating.PrivateMessageRepository;
import org.edutilos.dating.model.dating.PrivateMessage;
import org.edutilos.dating.util.graphql.GraphQLPaginationInput;
import org.edutilos.dating.util.graphql.GraphQLPaginationPageableFactory;
import org.edutilos.dating.util.graphql.GraphQLPaginationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class PrivateMessageQuery implements GraphQLQueryResolver {
    @Autowired
    private PrivateMessageRepository privateMessageRepository;


    public GraphQLPaginationResult<PrivateMessage> filterMessagesBy(
            Optional<String> content,
            Optional<Date> createdFrom,
            Optional<Date> createdTo,
            Optional<Boolean> opened,
            Optional<String> username,
            Optional<String> firstname,
            Optional<String> lastname,
            Optional<Date> minBirthdate,
            Optional<Date> maxBirthdate,
            Optional<Double> wageFrom,
            Optional<Double> wageTo,
            Optional<String> country,
            Optional<String> city,
            Optional<String> zipcode,
            GraphQLPaginationInput pagination
    ) {
        return new GraphQLPaginationResult<>(
                privateMessageRepository.filterMessagesBy(
                        content.orElse(null),
                        createdFrom.orElse(null),
                        createdTo.orElse(null),
                        opened.orElse(null),
                        username.orElse(null),
                        firstname.orElse(null),
                        lastname.orElse(null),
                        minBirthdate.orElse(null),
                        maxBirthdate.orElse(null),
                        wageFrom.orElse(null),
                        wageTo.orElse(null),
                        country.orElse(null),
                        city.orElse(null),
                        zipcode.orElse(null),
                        GraphQLPaginationPageableFactory.toPageable(pagination)
                )
        );
    }

    public GraphQLPaginationResult<PrivateMessage> findPrivateMessageListBy(
            Optional<Integer> channelId,
            Optional<Integer> userFromId,
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(channelId.isPresent()) {
            return new GraphQLPaginationResult<>(
                    privateMessageRepository.getExistingChatMessagesBasedOnChannelIdPageable(
                           channelId.get(),
                            GraphQLPaginationPageableFactory.toPageable(pagination)
                    )
            );

        }
        if((userFromId.isPresent() && userToId.isPresent())) {
            return new GraphQLPaginationResult<>(
                    privateMessageRepository.getExistingChatMessagesBetweenPageable(
                            userFromId.get(), userToId.get(),
                            GraphQLPaginationPageableFactory.toPageable(pagination)
                    )
            );
        }
        return new GraphQLPaginationResult<>(
                privateMessageRepository.findAll(GraphQLPaginationPageableFactory.toPageable(pagination))
        );
    }


    public GraphQLPaginationResult<PrivateMessage> findMessagesFromFavoriteListForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateMessageRepository.findMessagesFromFavoriteListForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

    public GraphQLPaginationResult<PrivateMessage> findMessagesFromBlockedListForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateMessageRepository.findMessagesFromBlockedListForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

    public GraphQLPaginationResult<PrivateMessage> findMessagesFromDeletedListForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateMessageRepository.findMessagesFromDeletedListForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

    public GraphQLPaginationResult<PrivateMessage> findNormalMessagesForPerson(
            Optional<Integer> userToId,
            GraphQLPaginationInput pagination
    ) {
        if(userToId.isPresent()) {
            return new GraphQLPaginationResult<>(privateMessageRepository.findNormalMessagesForPerson(userToId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }

    public GraphQLPaginationResult<PrivateMessage> findSentMessages(
            Optional<Integer> fromUserId,
            GraphQLPaginationInput pagination
    ) {
        if(fromUserId.isPresent()) {
            return new GraphQLPaginationResult<>(privateMessageRepository.findSentMessages(fromUserId.get(), GraphQLPaginationPageableFactory.toPageable(pagination)));
        }
        return null;
    }
}
