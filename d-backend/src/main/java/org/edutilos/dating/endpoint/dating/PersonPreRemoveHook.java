package org.edutilos.dating.endpoint.dating;

import lombok.extern.slf4j.Slf4j;
import org.edutilos.dating.controller.dating.*;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.model.dating.PersonImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * created by  Nijat Aghayev on 25.03.20
 */

@Slf4j
@Service
public class PersonPreRemoveHook {
    @Autowired
    private PersonToFavoriteMappingRepository personToFavoriteMappingRepository;
    @Autowired
    private PersonToBlockedMappingRepository personToBlockedMappingRepository;
    @Autowired
    private PersonToDeletedMappingRepository personToDeletedMappingRepository;
    @Autowired
    private ImageToPersonLikeMappingRepository imageToPersonLikeMappingRepository;
    @Autowired
    private ImageToPersonDislikeMappingRepository imageToPersonDislikeMappingRepository;
    @Autowired
    private ImageToPersonHeartMappingRepository imageToPersonHeartMappingRepository;
    @Autowired
    private PrivateChannelRepository privateChannelRepository;
    @Autowired
    private PrivateMessageRepository privateMessageRepository;

    public void preRemovePerson(Person person) {
        List<PersonImage> personImageList = new ArrayList<>();
        personImageList.addAll(person.getImages());
        Integer id = person.getId();
        privateMessageRepository.deleteAllByUserId(id);
        privateChannelRepository.deleteAllByUserId(id);
        personToFavoriteMappingRepository.deleteFromRelList(id);
        personToBlockedMappingRepository.deleteFromRelList(id);
        personToDeletedMappingRepository.deleteFromRelList(id);
        imageToPersonLikeMappingRepository.deleteFromRelList(id);
        imageToPersonDislikeMappingRepository.deleteFromRelList(id);
        imageToPersonHeartMappingRepository.deleteFromRelList(id);

        person.getImages().forEach(this::deleteImageFromServer);
    }

    private void deleteImageFromServer(PersonImage personImage) {
        try {
            Files.deleteIfExists(Paths.get(personImage.getSrcPath()));
        } catch (IOException e) {
            // TODO remove
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
