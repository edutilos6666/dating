package org.edutilos.dating.endpoint.dating;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.util.CurrentPerson;
import org.edutilos.dating.util.graphql.GraphQLPaginationInput;
import org.edutilos.dating.util.graphql.GraphQLPaginationPageableFactory;
import org.edutilos.dating.util.graphql.GraphQLPaginationResult;
import org.edutilos.dating.util.graphql.security.Unsecured;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * created by  Nijat Aghayev on 12.03.20
 */

@Service
public class PersonQuery implements GraphQLQueryResolver {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private CurrentPerson currentPerson;

    public Person findPersonById(Integer id) {
        return personRepository.findById(id).orElse(null);
    }


    @Unsecured
    public boolean checkIfUsernameOrEmailAlreadyTaken(
            Optional<String> username,
            Optional<String> email
    ) {
        return !personRepository.findPeopleByUsernameOrEmail(username.orElse(null), email.orElse(null)).isEmpty();
    }

    public GraphQLPaginationResult<Person> findPersonByWithPagination(
            Optional<Integer> id,
            Optional<String> username,
            Optional<String> firstname,
            Optional<String> lastname,
            Optional<List<Integer>> genderIds,
            Optional<Date> minBirthdate,
            Optional<Date> maxBirthdate,
            Optional<List<String>> jobTitleList,
            Optional<Double> minWage,
            Optional<Double> maxWage,
            Optional<String> city,
            Optional<String> country,
            Optional<String> zipcode,
            Optional<String> email,
            Optional<Boolean> status,
            GraphQLPaginationInput pagination
    ) {
        Page<Person> personPage = personRepository.findPersonByWithPagination(
                id.orElse(null),
                username.orElse(null),
                firstname.orElse(null),
                lastname.orElse(null),
                genderIds.orElse(null),
                minBirthdate.orElse(null),
                maxBirthdate.orElse(null),
                jobTitleList.orElse(null),
                minWage.orElse(null),
                maxWage.orElse(null),
                city.orElse(null),
                country.orElse(null),
                zipcode.orElse(null),
                email.orElse(null),
                status.orElse(null),
                GraphQLPaginationPageableFactory.toPageable(pagination)
        );
        return new GraphQLPaginationResult<>(
               filterPeople(personPage)
        );
    }

    private Page<Person> filterPeople(Page<Person> input) {
        Person p = currentPerson.getPerson();
        // p.blockedList and p.deletedList are lazily initialized , that is why we can not use p.getBlockedList() or p.getDeletedList()
        List<Integer> blockedList = personRepository.findBlockedListForPerson(p.getId()).stream().map(Person::getId).collect(Collectors.toList());
        List<Integer> deletedList = personRepository.findDeletedListForPerson(p.getId()).stream().map(Person::getId).collect(Collectors.toList());
        return new PageImpl<Person>(input.stream()
                .filter(one-> !p.getId().equals(one.getId()) && !blockedList.contains(one.getId()) && !deletedList.contains(one.getId()))
                .collect(Collectors.toList()), PageRequest.of(input.getNumber(), input.getSize()), input.getTotalElements());
    }

}
