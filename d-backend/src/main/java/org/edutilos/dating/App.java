package org.edutilos.dating;

import graphql.schema.GraphQLScalarType;
import graphql.servlet.core.ApolloScalars;
import org.edutilos.dating.endpoint.scalars.DateScalar;
import org.edutilos.dating.endpoint.scalars.LongScalar;
import org.edutilos.dating.endpoint.scalars.TimeScalar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Hello world!
 * Hallo Welt!
 */
@ApplicationPath("/")
public class App extends Application {
    // this seems to be usefull
    // actually it really is, as the Application starts here ;)

    @Configuration
    public static class MainConfig {

        private static DateScalar graphQLDate = new DateScalar();
        private static TimeScalar timeScalar = new TimeScalar();
        private static LongScalar longScalar = new LongScalar();

        @Bean
        public GraphQLScalarType dateTime() {
            return graphQLDate;
        }

        @Bean
        public GraphQLScalarType time() {
            return timeScalar;
        }
        @Bean
        public GraphQLScalarType date() {
            return graphQLDate;
        }

        @Bean
        GraphQLScalarType upload() {
            return ApolloScalars.Upload;
        }

        @Bean
        public GraphQLScalarType longType() {
            return longScalar;
        }

    }
}
