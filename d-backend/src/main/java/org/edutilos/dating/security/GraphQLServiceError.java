package org.edutilos.dating.security;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphQLServiceError implements GraphQLError {
    private List<SourceLocation> locations;
    private String message;
    private List<ErrorMessageField> errorMessages;

    public GraphQLServiceError(String message) {
        this.message = message;
    }

    public GraphQLServiceError(String message, List<ErrorMessageField> errorMessages) {
        if (message.startsWith("Exception while fetching data")) {
            message = message.substring(message.indexOf(":") + 1).trim();
        }
        this.message = message;
        this.errorMessages = errorMessages;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return new ArrayList<>();
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DataFetchingException;
    }

    @Override
    public Map<String, Object> getExtensions() {
        Map<String, Object> stringObjectMap = new HashMap<>();
        if (errorMessages != null) {

            stringObjectMap.put("fields", errorMessages);
        }
        if (message != null) {
            stringObjectMap.put("errorMessage", message);
        }
        return stringObjectMap;
    }
}
