package org.edutilos.dating.security;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GraphQLServiceException extends RuntimeException {

    private String message;
    private List<ErrorMessageField> errorMessageList;

    public GraphQLServiceException(String message) {
        this.message = message;

    }

    public GraphQLServiceException(String message, List<ErrorMessageField> errorMessages) {
        this.message = message;
        this.errorMessageList = errorMessages;
    }

    public GraphQLServiceException(String message, ErrorMessageField... errorMessages) {
        this.message = message;
        this.errorMessageList = new ArrayList<>(Arrays.asList(errorMessages));
    }

    @Override
    public String getMessage() {
        return message;
    }

    public List<ErrorMessageField> getErrorMessageList() {
        return errorMessageList;
    }
}
