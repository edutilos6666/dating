package org.edutilos.dating.security;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Data
@XmlRootElement
public class ErrorMessage {
    private String message;
    private List<ErrorMessageField> affectedFields;

    public ErrorMessage() {

    }

    public ErrorMessage(String message) {
        this.message = message;
    }

    public ErrorMessage(String message, List<ErrorMessageField> affectedFields) {
        this.message = message;
        this.affectedFields = affectedFields;
    }

    public List<ErrorMessageField> getAffectedFields() {
        if (affectedFields == null) {
            affectedFields = new ArrayList<>();
        }
        return affectedFields;
    }

    public ErrorMessage message(String message) {
        this.message = message;
        return this;
    }

    public ErrorMessage affectedFields(List<ErrorMessageField> affectedFields) {
        this.affectedFields = affectedFields;
        return this;
    }
}
