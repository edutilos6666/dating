package org.edutilos.dating.security;

import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import org.springframework.stereotype.Component;

import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component
public class GraphQLErrorHandler implements graphql.servlet.core.GraphQLErrorHandler {
    @Override
    public boolean errorsPresent(List<GraphQLError> errors) {

        return errors.size() > 0;


    }

    @Override
    public List<GraphQLError> processErrors(List<GraphQLError> errors) {
        List<GraphQLError> graphQLErrors = errors.stream().map(graphQLError -> {
            if (graphQLError instanceof ExceptionWhileDataFetching) {
                ExceptionWhileDataFetching exceptionWhileDataFetching = (ExceptionWhileDataFetching) graphQLError;
                if (exceptionWhileDataFetching.getException() instanceof NotAuthentificatedException) {
                    return new GraphQLNotAuthentificatedError(graphQLError.getLocations());
                } else if (exceptionWhileDataFetching.getException() instanceof GraphQLServiceException) {
                    return new GraphQLServiceError(graphQLError.getMessage(), ((GraphQLServiceException) exceptionWhileDataFetching.getException()).getErrorMessageList());
                } else if (exceptionWhileDataFetching.getException() instanceof AccessDeniedException) {
                    return new GraphQLServiceError(graphQLError.getMessage());
                } else {
                    Logger.getLogger(GraphQLErrorHandler.class.getName()).log(Level.SEVERE, graphQLError.getMessage(), ((ExceptionWhileDataFetching) graphQLError).getException());
                }
            }
            return graphQLError;
        }).collect(Collectors.toList());
        return graphQLErrors;
    }
}
