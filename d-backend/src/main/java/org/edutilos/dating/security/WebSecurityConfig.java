package org.edutilos.dating.security;

import org.edutilos.dating.security.token.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;


@Configuration
@EnableAspectJAutoProxy
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private TokenProvider tokenProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf().disable()
                .securityContext().and()
                .addFilterAfter(new JwtAuthenticationMechanismFilter(tokenProvider), BasicAuthenticationFilter.class)
                //.exceptionHandling().accessDeniedHandler(graphQLAccessDeniedHandler).authenticationEntryPoint(graphQLAutentificationEndpoint).and()

                .authorizeRequests()
                .antMatchers("/private-chat/**").permitAll()
                .antMatchers("/api/private-chat/**").permitAll()
                .antMatchers("/images/**").permitAll()
                .antMatchers("/hello-world-service/**").permitAll()
                .antMatchers("/email-service/**").permitAll()
                .antMatchers("/eskalation-service/**").permitAll()
                .antMatchers("/files/ekf/**").permitAll()
                .antMatchers("/files/rueckforderung/**").permitAll()
                .antMatchers("/mnt/upload/**").permitAll()
                .antMatchers("/diasleimage/**").permitAll()
                .antMatchers("/graphql").permitAll()
                .antMatchers("/vendor/**").permitAll()
                .antMatchers("/graphiql").permitAll()
                .antMatchers("/socket/**").anonymous()
                .anyRequest().authenticated().and()

                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);


    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/socket", "/socket/**");
    }
}