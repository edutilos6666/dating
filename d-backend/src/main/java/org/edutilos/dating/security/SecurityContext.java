package org.edutilos.dating.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityContext {

    public boolean isCallerInRole(String role) {
        if (!role.toUpperCase().startsWith("ROLE_")) {
            role = "ROLE_" + role;
        }
        final String roleCheck = role;
        if (getPrincipal() != null) {
            return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().filter(grantedAuthority -> {
                return grantedAuthority.getAuthority().equalsIgnoreCase(roleCheck);
            }).findFirst().isPresent();
        }
        return false;
    }

    public String getPrincipal() {
        if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null) {
            return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        return null;
    }
}
