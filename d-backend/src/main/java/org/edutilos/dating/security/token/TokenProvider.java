/*
 * Copyright (C) open knowledge GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.edutilos.dating.security.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.poi.util.IOUtils;
import org.edutilos.dating.model.auth.LoginResponse;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.service.auth.CurrentPersonImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@Scope("application")
public class TokenProvider {
    private RSAPublicKey publicKey;
    private RSAPrivateKey privateKey;
    @Value("${security.token.validity}")
    private int tokenValidity;
    @Value("${security.token.issuer}")
    private String issuer;
    @Inject
    private CurrentPersonImpl currentPerson;

    public RSAPrivateKey getPrivateKey() {
        if (privateKey == null) {
            try {
                InputStream privateKeyStream = loadPrivateKey().getInputStream();
                String privateKeyContent = new String(IOUtils.toByteArray(privateKeyStream));
                privateKeyContent = privateKeyContent.replaceAll("\\r", "").replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
                KeyFactory kf = KeyFactory.getInstance("RSA");

                PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
                privateKey = (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);
            } catch (Exception e) {
                Logger.getLogger(TokenProvider.class.getName()).log(Level.SEVERE, "Fehler beim initialisieren der Schlüsselpaare", e);
            }

        }
        return privateKey;
    }

    public Resource loadPublicKey() {
        return new ClassPathResource("keys/crosscheck_web.pub");
    }

    public Resource loadPrivateKey() {
        return new ClassPathResource("keys/crosscheck_web.pkcs8");
    }


    public RSAPublicKey getPublicKey() {
        if (publicKey == null) {
            try {
                InputStream publicKeyStream = loadPublicKey().getInputStream();
                String publicKeyContent = new String(IOUtils.toByteArray(publicKeyStream));

                publicKeyContent = publicKeyContent.replaceAll("\\r", "").replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
                KeyFactory kf = KeyFactory.getInstance("RSA");
                X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
                publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
            } catch (Exception e) {
                Logger.getLogger(TokenProvider.class.getName()).log(Level.SEVERE, "Fehler beim initialisieren der Schlüsselpaare", e);
            }

        }
        return publicKey;
    }


    public AuthorizationToken renewAuthorizationTokenForCurrentUser() throws JWTCreationException, UnsupportedEncodingException {
        LocalDateTime now = LocalDateTime.now();
        Date issuedAt = toDate(now);
        Date expiresAt = toDate(now.plus(tokenValidity, ChronoUnit.MINUTES));
        Date notBefore = toDate(now.minus(10,ChronoUnit.MINUTES));

        //String issuer = "http://localhost:8080/jaxrs-security-jwt";
        currentPerson.reset();
        String subject = currentPerson.getId()+"";
        Person person = currentPerson.getPerson();
        String token = issueToken(issuer, issuedAt, expiresAt, notBefore, subject, person.getFirstname(), person.getLastname(),"");

        return new AuthorizationToken(token, subject, issuer, issuedAt, expiresAt, person.getFirstname(), person.getLastname(), "",tokenValidity);
    }



    public DecodedJWT verifyAndDecodeJwt(String token) {
        Algorithm algorithmRS = Algorithm.RSA256(getPublicKey(), getPrivateKey());
        return JWT.require(algorithmRS)
                .withIssuer(issuer)
                .acceptLeeway(1)
                .acceptExpiresAt(5)
                .build()
                .verify(token);
    }

    private Date toDate(final LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public String getTokenFromLoginResponse(LoginResponse loginResponse, Person person) {
        try {
            return issueToken(issuer, new Date(),
                    new Date(System.currentTimeMillis() + (loginResponse.getExpires_in() * 60 * 1000)),
                    toDate(LocalDateTime.now().minus(30,ChronoUnit.MINUTES)), loginResponse.getUserid() + "",
                    person.getFirstname(), person.getLastname(), "");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String issueToken(final String issuer, final Date issuedAt, final Date expiresAt, final Date notBefore, final String subject, String firstName, String lastName, String picture) throws JWTCreationException, UnsupportedEncodingException {

        Algorithm algorithmRS = Algorithm.RSA256(getPublicKey(), getPrivateKey());
        return JWT.create()
                .withIssuer(issuer)
                .withIssuedAt(issuedAt)
                .withExpiresAt(expiresAt)
                .withNotBefore(notBefore)
                .withSubject(subject)
                .withClaim("firstName", firstName)
                .withClaim("lastName", lastName)
                .withClaim("picture", picture)
                .sign(algorithmRS);
    }
}
