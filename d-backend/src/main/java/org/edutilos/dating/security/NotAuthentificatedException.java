package org.edutilos.dating.security;

public class NotAuthentificatedException extends RuntimeException {
    public NotAuthentificatedException(String message) {
        super(message);
    }
}
