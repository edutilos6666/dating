package org.edutilos.dating.security;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphQLNotAuthentificatedError implements GraphQLError {
    private List<SourceLocation> locations;
    public GraphQLNotAuthentificatedError(List<SourceLocation> locations){
        //this.locations = locations;
    }
    @Override
    public String getMessage() {
        return "Not Authentificated";
    }

    @Override
    public List<SourceLocation> getLocations() {
        return new ArrayList<>();
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DataFetchingException;
    }

    @Override
    public Map<String, Object> getExtensions() {
        Map<String,Object> stringObjectMap = new HashMap<>();
        stringObjectMap.put("code","UNAUTHENTICATED");
        return stringObjectMap;
    }
}
