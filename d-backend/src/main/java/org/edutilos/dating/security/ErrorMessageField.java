package org.edutilos.dating.security;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement

public class ErrorMessageField {

    private String fieldName;
    private String errorMessage;

    public ErrorMessageField() {

    }

    public ErrorMessageField(String fieldName) {
        this.fieldName = fieldName;
    }

    public ErrorMessageField(String fieldName, String errorMessage) {
        this.fieldName = fieldName;
        this.errorMessage = errorMessage;
    }

    public ErrorMessageField fieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public ErrorMessageField errorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }
}
