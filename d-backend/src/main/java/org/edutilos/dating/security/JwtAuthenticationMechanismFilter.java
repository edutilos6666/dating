/*
 * Copyright (C) open knowledge GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.edutilos.dating.security;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.edutilos.dating.security.token.TokenProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class JwtAuthenticationMechanismFilter extends OncePerRequestFilter  {

    private static final Logger LOGGER = Logger.getLogger(JwtAuthenticationMechanismFilter.class.getName());
    private static final Pattern PATTERN_AUTHORIZATION_HEADER = Pattern.compile("^Bearer [a-zA-Z0-9\\-_\\.]+$", Pattern.CASE_INSENSITIVE);
    private static final Pattern PATTERN_AUTHORIZATION_COOKIE = Pattern.compile("^[a-zA-Z0-9\\-_\\.]+$", Pattern.CASE_INSENSITIVE);

    private TokenProvider tokenProvider;

    public JwtAuthenticationMechanismFilter(TokenProvider tokenProvider){
        this.tokenProvider = tokenProvider;
    }


    private String getOrigin(HttpServletRequest httpServletRequest) {
        if (httpServletRequest.getHeader("origin") != null) {
            return httpServletRequest.getHeader("origin");
        } else return "*";
    }

    private void addCorsHeader(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", getOrigin(request));
        response.setHeader("Access-Control-Allow-Methods", "POST, PATCH, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization,X-Test,Set-Cookie,*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Max-Age", "86400");

        String header = request.getHeader("Access-Control-Request-Headers");
        if (header != null) {
            response.setHeader("Access-Control-Allow-Headers", header);
        }
    }

    public String getAuthorisation(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase("auth_token")) {
                    return cookie.getValue();
                }
            }
        }
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }


    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        LOGGER.log(Level.INFO, "validateRequest: {0}", request.getRequestURI());


        // NOT HERE addCorsHeader(request, response);
        if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
             chain.doFilter(request,response);
            return;
        }




        String header = getAuthorisation(request);//request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null) {
            LOGGER.log(Level.WARNING, "Authorization header is missing");
            //Die eigentliche Abfrage erfolgt im im GraphQLSecurityAspect, sonst würde der Login nicht funktionieren bzw. kann nicht auf @Unprotected zugregriffen werden
            chain.doFilter(request,response);
            // response.sendError(503);
             return;
        }

        if (!isValidAuthorizationCookie(header) && !isValidAuthorizationHeader(header)) {
            LOGGER.log(Level.WARNING, "Authorization header is invalid");
            chain.doFilter(request,response);
            //Die eigentliche Abfrage erfolgt im im GraphQLSecurityAspect, sonst würde der Login nicht funktionieren bzw. kann nicht auf @Unprotected zugregriffen werden
           // response.sendError(503);
            return;
        }

        try {
            String token = header;
            if (header.contains(" ")) {
                String[] headerComponents = header.split(" ");
                token = headerComponents[1];
            }

            DecodedJWT jwt = tokenProvider.verifyAndDecodeJwt(token);

            String username = jwt.getSubject();
            List<String> roles = jwt.getClaim("roles").asList(String.class);
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    username, null, null);

            SecurityContextHolder.getContext().setAuthentication(auth);
        } catch (JWTVerificationException e) {
            LOGGER.log(Level.WARNING, "JWT token verification failed", e);
            SecurityContextHolder.clearContext();
        }
        chain.doFilter(request,response);
        //return context.responseUnauthorized();
    }

    private boolean isValidAuthorizationHeader(final String header) {
        return PATTERN_AUTHORIZATION_HEADER.matcher(header).matches();
    }

    private boolean isValidAuthorizationCookie(final String header) {
        return PATTERN_AUTHORIZATION_COOKIE.matcher(header).matches();
    }
}
