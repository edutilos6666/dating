package org.edutilos.dating.util.graphql;

import org.springframework.data.domain.Page;

import java.util.List;

public class GraphQLPaginationResult<T> {
    private GraphQLPaginationPageInfo pageInfo;
    private List<T> result;

    public GraphQLPaginationResult(Page<T> result){
        this.result = result.getContent();
        this.pageInfo = new GraphQLPaginationPageInfo(result);
    }

    public GraphQLPaginationPageInfo getPageInfo() {
        return pageInfo;
    }

    public List<T> getResult() {
        return result;
    }
}
