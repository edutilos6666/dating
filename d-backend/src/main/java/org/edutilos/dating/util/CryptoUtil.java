package org.edutilos.dating.util;

import jodd.util.BCrypt;
import org.edutilos.dating.model.dating.Person;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class CryptoUtil {

    public static final Integer PASSWORD_TYPE_AES_SHA = 0;
    public static final Integer PASSWORD_TYPE_BCRYPT = 1;
    public static final Integer KEY_SIZE = 16;

    private static final SecureRandom secureRandom = new SecureRandom();

    private static SecretKeySpec getAESKey(final String key, final String encoding) throws UnsupportedEncodingException{
        final byte[] finalKey = new byte[KEY_SIZE];
        int i = 0;
        for (byte b : key.getBytes(encoding)) {
            finalKey[i++ % KEY_SIZE] ^= b;
        }
        return new SecretKeySpec(finalKey, "AES");
    }

    private static byte[] getAes(String password, String key) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        Cipher encryptCipher = Cipher.getInstance("AES");
        encryptCipher.init(Cipher.ENCRYPT_MODE, getAESKey(key, "UTF-8"));
        return encryptCipher.doFinal(password.getBytes("UTF-8"));
    }

    private static byte[] getSha(byte[] bytes) throws NoSuchAlgorithmException {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        sha256.reset();
        sha256.update(bytes);
        return sha256.digest();
    }

    private static String getShaAes(String passphrase, String key) {
        try {
            return HexDump.toHexString(getSha(getAes(passphrase, key))).toLowerCase();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException
                | InvalidKeyException | UnsupportedEncodingException
                | IllegalBlockSizeException | BadPaddingException e) {
            return null;
        }
    }

    public static String getPass(String passphrase, Person person) {
        if ((passphrase != null && person != null) && PASSWORD_TYPE_BCRYPT.equals(person.getPasswordType())) {
            return BCrypt.hashpw(passphrase, BCrypt.gensalt(12, secureRandom));
        } else if ((passphrase != null && person != null) && PASSWORD_TYPE_AES_SHA.equals(person.getPasswordType())) {
            return getShaAes(passphrase, person.getUsername());
        }
        return null;
    }

    public static boolean checkPass(String plainPassword, String hashPassword, String username, Integer passwordType) {
        if (plainPassword == null || hashPassword == null || username == null || passwordType == null) {
            return false;
        }
        if (PASSWORD_TYPE_BCRYPT.equals(passwordType)) {
            return BCrypt.checkpw(plainPassword, hashPassword);
        } else if (PASSWORD_TYPE_AES_SHA.equals(passwordType)) {
            return hashPassword.equals(getShaAes(plainPassword, username));
        }
        return false;
    }

    public static boolean checkPass(String plainPassword, Person person) {
        return checkPass(plainPassword, person.getPassword(), person.getUsername(), person.getPasswordType());
    }

}
