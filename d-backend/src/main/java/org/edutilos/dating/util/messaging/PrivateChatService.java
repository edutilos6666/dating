package org.edutilos.dating.util.messaging;

import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.controller.dating.PrivateChannelRepository;
import org.edutilos.dating.controller.dating.PrivateMessageRepository;
import org.edutilos.dating.exception.UserIsSameException;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.model.dating.PrivateChannel;
import org.edutilos.dating.model.dating.PrivateMessage;
import org.edutilos.dating.payload.MessageCounterDTO;
import org.edutilos.dating.payload.NotificationDTO;
import org.edutilos.dating.payload.PrivateChatInitializationDTO;
import org.edutilos.dating.payload.PrivateMessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PrivateChatService {
    private final PrivateChannelRepository pcRepo;
    private final PrivateMessageRepository pmRepo;
    private final PersonRepository personRepository;
    private final MessageMapper pmMapper;
    private final NotificationService notificationService;

    @Autowired
    public PrivateChatService(
            PrivateChannelRepository pcRepo,
            PrivateMessageRepository pmRepo,
            PersonRepository personRepository,
            MessageMapper pmMapper,
            NotificationService notificationService) {
        this.pcRepo = pcRepo;
        this.pmRepo = pmRepo;
        this.personRepository = personRepository;
        this.pmMapper = pmMapper;
        this.notificationService = notificationService;
    }

    private Integer newChatSession(PrivateChatInitializationDTO payload) {
        Person userFrom = personRepository.findById(payload.getUserFromId()).get();
        Person userTo = personRepository.findById(payload.getUserToId()).get();

        return pcRepo.save(new PrivateChannel(userFrom, userTo, Collections.emptyList())).getId();
    }

    private Integer getExistingChannel(PrivateChatInitializationDTO payload) {
        List<PrivateChannel> some=
                pcRepo.findPrivateChannelBy(payload.getUserFromId(), payload.getUserToId());
        return !some.isEmpty()? some.get(0).getId(): null;
    }

    public Integer establishChatSesion(PrivateChatInitializationDTO payload)
     throws UserIsSameException {
        if(payload.getUserFromId() == payload.getUserToId()) {
            throw new UserIsSameException("UserFrom and UserTo are the same");
        }

        Integer id = getExistingChannel(payload);
        if(id == null) {
            id = newChatSession(payload);
            notificationService.notifyUserAboutPrivateChannel(payload.getUserToId(),
                    new NotificationDTO(NotificationConstants.PRIVATE_CHANNEL_CREATED,
                            NotificationConstants.PRIVATE_CHANNEL_CREATED,
                            payload.getUserFromId()));
        }
        return id;
    }

    public void submitMessage(int channelId, PrivateMessageDTO payload) {
        Person userFrom = personRepository.findById(payload.getUserFrom()).get();
        Person userTo = personRepository.findById(payload.getUserTo()).get();
        PrivateChannel channel = pcRepo.findById(payload.getChannelId()).get();
        pmRepo.save(pmMapper.mapPayloadToModel(payload));
        notificationService.notifyUsersAfterPrivateMessageSent(payload.getUserFrom(),
                new MessageCounterDTO(channelId, pmRepo.findCountByChannelIdAndUserId(channelId, payload.getUserFrom())));
        notificationService.notifyUsersAfterPrivateMessageSent(payload.getUserTo(),
                new MessageCounterDTO(channelId, pmRepo.findCountByChannelIdAndUserId(channelId, payload.getUserTo())));
    }


    public List<PrivateMessageDTO> getExistingChatMessages(int channelId) {
        PrivateChannel privateChannel = pcRepo.findById(channelId).get();
        if(privateChannel == null) return Collections.emptyList();
        return pmRepo.getExistingChatMessagesBetween(
                privateChannel.getUserFrom().getId(),
                privateChannel.getUserTo().getId())
                .stream()
                .map(one-> pmMapper.mapModelToPayload(one))
                .collect(Collectors.toList());
    }

    public List<MessageCounterDTO> getMessageCountsForChannelsByUserId(String channelIds, int userId) {
        List<MessageCounterDTO> ret = new ArrayList<>();
        Arrays.asList(channelIds.split(","))
                .stream()
                .map(one-> Integer.parseInt(one))
                .forEach(channelId->
                        ret.add(new MessageCounterDTO(channelId,  pmRepo.findCountByChannelIdAndUserId(channelId, userId))));

        return ret;
    }
}
