package org.edutilos.dating.util.messaging;

import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.controller.dating.PrivateChannelRepository;
import org.edutilos.dating.model.dating.PrivateMessage;
import org.edutilos.dating.payload.PrivateMessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Service
public class MessageMapper {
    private final PersonRepository userRepo;
    private final PrivateChannelRepository pcRepo;

    @Autowired
    public MessageMapper(
            PersonRepository userRepo,
            PrivateChannelRepository pcRepo) {
        this.userRepo = userRepo;
        this.pcRepo = pcRepo;
    }

    public PrivateMessage mapPayloadToModel(PrivateMessageDTO payload) {
        return new PrivateMessage(userRepo.findById(payload.getUserFrom()).get(),
                userRepo.findById(payload.getUserTo()).get(),
                pcRepo.findById(payload.getChannelId()).get(),
                payload.getContent(),
                payload.getCreated(),
                payload.isOpened());
    }

    public PrivateMessageDTO mapModelToPayload(PrivateMessage model) {
        return new PrivateMessageDTO(model.getUserFrom().getId(),
                model.getUserTo().getId(),
                model.getChannel().getId(),
                model.getContent(),
                model.getCreated(),
                model.isOpened());
    }
}
