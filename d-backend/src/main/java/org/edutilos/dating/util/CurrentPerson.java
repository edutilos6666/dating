package org.edutilos.dating.util;

import org.edutilos.dating.model.dating.Person;

/**
 * Created by maurice on 3/15/18
 */
public interface CurrentPerson {

    Person getPerson();
    Integer getId();
}
