package org.edutilos.dating.util.messaging;

import org.edutilos.dating.payload.MessageCounterDTO;
import org.edutilos.dating.payload.NotificationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
@Service
public class NotificationService {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    public void notifyUserAboutPrivateChannel(long toUserId, NotificationDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.private.channel."+ toUserId, payload);
    }

    public void notifyUsersAfterPrivateMessageSent(long toUserId, MessageCounterDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.private.message."+ toUserId, payload);
    }
}
