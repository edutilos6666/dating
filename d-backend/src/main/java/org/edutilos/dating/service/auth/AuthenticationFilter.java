/*
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.service.auth;

import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.controller.dating_login.LogRequestTable;
import org.edutilos.dating.controller.dating_login.IAuthTokenTable;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.model.dating_login.LogRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;

@WebFilter("/*")
@Controller
public class AuthenticationFilter implements Filter {

	public static final Integer SESSION_EXPIRE_DELAY = 5;
	private Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
	@Inject
	private PersonRepository personRepository;
	@Inject
	private IAuthTokenTable iAuthTokenTable;
	@Inject
	private LogRequestTable logRequestTable;

	public void handleRequest(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

		filterChain.doFilter(servletRequest, httpResponse);
	}

	private String getOrigin(HttpServletRequest httpServletRequest) {
		if (httpServletRequest.getHeader("origin") != null) {
			return httpServletRequest.getHeader("origin");
		} else return "*";
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		long startTime = System.currentTimeMillis();

		HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

		httpResponse.setHeader("Access-Control-Allow-Origin", getOrigin(httpRequest));
		httpResponse.setHeader("Access-Control-Allow-Methods", "POST, PATCH, GET, OPTIONS, DELETE, PUT");
		httpResponse.setHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization,X-Test");
		httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
		httpResponse.setHeader("Access-Control-Max-Age", "86400");

		String header = httpRequest.getHeader("Access-Control-Request-Headers");
		if (header != null) {
			httpResponse.setHeader("Access-Control-Allow-Headers", header);
		}

		if ("OPTIONS".equalsIgnoreCase(httpRequest.getMethod())) {
			logger.debug("XHR request handled");
			return;
		}

		if (httpRequest.getRequestURL().toString().endsWith("ArquillianServletRunner")) {
			// hack around bis ich weiß was hier los ist
			Person person = personRepository.findById(1893).orElse(null);
			httpRequest.setAttribute("PERSON", person);
			filterChain.doFilter(servletRequest, httpResponse);
			return;
		}

		LogRequest log = new LogRequest();

		log.setEndpoint(httpRequest.getPathInfo());
		log.setQueryString(httpRequest.getQueryString());
		log.setMethod(httpRequest.getMethod());
		log.setTimeStart(Timestamp.from(Instant.now()));

		handleRequest(servletRequest, servletResponse, filterChain);

		log.setTimeFirstByte(System.currentTimeMillis() - startTime);
		log.setResponseStatus(httpResponse.getStatus());

		Person person = (Person) httpRequest.getAttribute("PERSON");
		if (person != null) {
			log.setUserid(person.getId());
		}

		log.setSize(-1);
		log.setTimeTotal(System.currentTimeMillis() - startTime);
		log.setSessionToken((String) httpRequest.getAttribute("SESSION"));

		if (log.getEndpoint() != null)
		logRequestTable.insert(log);
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig filterConfig) {
	}
}
