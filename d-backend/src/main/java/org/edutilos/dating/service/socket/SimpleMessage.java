package org.edutilos.dating.service.socket;

public class SimpleMessage {
    private String message;

    public SimpleMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
