package org.edutilos.dating.service.socket;

import org.edutilos.dating.exception.UserIsSameException;
import org.edutilos.dating.payload.MessageCounterDTO;
import org.edutilos.dating.payload.PrivateChatInitializationDTO;
import org.edutilos.dating.payload.PrivateMessageDTO;
import org.edutilos.dating.util.messaging.JSONResponseHelper;
import org.edutilos.dating.util.messaging.PrivateChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Controller
public class PrivateChatController {
    private final PrivateChatService chatService;
    private final JSONResponseHelper jsonResponseHelper;

    @Autowired
    public PrivateChatController(PrivateChatService chatService, JSONResponseHelper jsonResponseHelper) {
        this.chatService = chatService;
        this.jsonResponseHelper = jsonResponseHelper;
    }

    @PutMapping(value="/api/private-chat/channel",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> establishPrivateChannel(@RequestBody PrivateChatInitializationDTO payload)
    throws UserIsSameException {
        int channelId = chatService.establishChatSesion(payload);
        return jsonResponseHelper.createResponse(channelId, HttpStatus.OK);
    }

    @MessageMapping("/private.chat.{channelId}")
    @SendTo("/topic/private.chat.{channelId}")
    public PrivateMessageDTO sendMessage(@DestinationVariable int channelId,
                                         @Payload PrivateMessageDTO chatMessage) {
        chatService.submitMessage(channelId, chatMessage);
        return chatMessage;
    }

    @GetMapping(value="/api/private-chat/channel/{channelId}",
            produces = "application/json")
    public ResponseEntity<String> getExistingChatMessages(@PathVariable("channelId") int channelId) {
        List<PrivateMessageDTO> res = chatService.getExistingChatMessages(channelId);
        return jsonResponseHelper.createResponse(res, HttpStatus.OK);
    }


    @GetMapping(value="/api/private-chat/message-count/{channelIds}/{userId}",
            produces = "application/json")
    public ResponseEntity<String> getMessageCountsForChannelsByUserId(@PathVariable("channelIds") String channelIds,
                                                                      @PathVariable("userId") int userId) {
        List<MessageCounterDTO> ret =
                chatService.getMessageCountsForChannelsByUserId(channelIds, userId);
        return jsonResponseHelper.createResponse(ret, HttpStatus.OK);
    }

}
