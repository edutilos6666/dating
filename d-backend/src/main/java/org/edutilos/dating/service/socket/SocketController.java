package org.edutilos.dating.service.socket;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Controller
public class SocketController {
    @MessageMapping("/refresh")
    @SendToUser("/queue/refresh")
    public SimpleMessage refresh(@Payload String message,
                                 Principal principal) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new SimpleMessage(message);
    }
}
