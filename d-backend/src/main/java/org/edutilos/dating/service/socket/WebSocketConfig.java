package org.edutilos.dating.service.socket;

import com.auth0.jwt.interfaces.DecodedJWT;
import org.edutilos.dating.security.token.TokenProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Inject
    private TokenProvider tokenProvider;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
//        config.enableSimpleBroker("/topic/", "/queue/");

        config.setApplicationDestinationPrefixes("/app");

        config.enableStompBrokerRelay("/topic")
                .setRelayHost("localhost")
                .setRelayPort(61613)
                .setClientLogin("guest")
                .setClientPasscode("guest");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //TODO !!
        registry.addEndpoint("/socket").setAllowedOrigins("*").withSockJS();

    }

    // TODO !!!
//    @Override
//    public void configureClientInboundChannel(ChannelRegistration registration) {
//
//        registration.interceptors(new ChannelInterceptor() {
//
//            @Override
//            public Message<?> preSend(Message<?> message, MessageChannel channel) {
//                StompHeaderAccessor accessor =
//                        MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
//                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
//                    List<String> authHeader = accessor.getNativeHeader("authorization");
//                    if (authHeader.size() > 0) {
//                        DecodedJWT jwt = tokenProvider.verifyAndDecodeJwt(authHeader.get(0));
//
//                        String username = jwt.getSubject();
//                        List<String> roles = jwt.getClaim("roles").asList(String.class);
//                        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
//                                username, null, roles.stream().map((role) -> {
//                            return "ROLE_" + role;
//                        }).map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
//
//                        Authentication user = auth; // access authentication header(s)
//                        accessor.setUser(user);
//                    }
//
//                }
//                return message;
//            }
//        });
//    }
}