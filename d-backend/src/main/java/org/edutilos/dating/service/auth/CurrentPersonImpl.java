/*
 * © 2016 Deutsches Dienstleistungszentrum für das Gesundheitswesen GmbH
 */
package org.edutilos.dating.service.auth;

import org.edutilos.dating.controller.dating.PersonRepository;
import org.edutilos.dating.model.dating.Person;
import org.edutilos.dating.util.CurrentPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Scope(value="session",  proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class CurrentPersonImpl implements CurrentPerson {
    private Person person;

    @Autowired
    private PersonRepository personRepository;


    public Person getPerson() {
        if (person == null){
            if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()){
                String username = (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                if (username != null && username.length() >0) {
                    person = personRepository.getById(Integer.parseInt(username));
                }
            }


        }
       return person;
    }

    public void reset() {
        person = null;
        getPerson();
    }

    public Integer getId() {
        if(getPerson() == null) return null;
        return getPerson().getId();
    }

}
