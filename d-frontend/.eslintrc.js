module.exports = {
  extends: ['airbnb-typescript', 'prettier', 'prettier/react', 'prettier/@typescript-eslint'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'prettier'],

  parserOptions: {
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    ecmaFeatures: {
      jsx: true, // Allows for the parsing of JSX
    },
  },
  plugins: ['react-hooks', 'no-autofix'],
  env: {
    browser: true,
  },
  globals: {
    it: true,
  },
  rules: {
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
    // e.g. "@typescript-eslint/explicit-function-return-type": "off",
    'react/no-multi-comp': [1, { ignoreStateless: true }],
    '@typescript-eslint/explicit-function-return-type': 'off',
    'react/jsx-closing-bracket-location': 0,

    'react/jsx-pascal-case': [1],
    'react/prefer-stateless-function': [1, { ignorePureComponents: false }],
    'react/prop-types': 'off',
    '@typescript-eslint/no-use-before-define': 'off', //Erstmal OFF. Hier ging es explizit um Methodenaufrufe aus useEffect Hooks. Mal schauen wie man das umgehen kann
    'react/jsx-filename-extension': [2, { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    '@typescript-eslint/indent': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
  },
  settings: {
    react: {
      version: 'detect', // Tells eslint-plugin-react to automatically detect the version of React to use
    },
  },
};
