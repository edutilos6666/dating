module.exports = {
    client: {
        excludes: ['**/*.tsx', '**/*.ts'],
        service: {
            name: 'crosscheckweb-spring-backend',
            url: 'http://localhost:9090/graphql',
        },
    },
};
