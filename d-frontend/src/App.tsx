import React from 'react';
import './App.css';
// import './custom.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-day-picker/lib/style.css';
import { ApolloProvider } from 'react-apollo';
import { ApolloProvider as ApolloHooksProvider } from '@apollo/react-hooks';
import { createUploadLink } from 'apollo-upload-client';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';
import { ApolloClient } from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { ApolloLink } from 'apollo-link';
import AppContent from './pages/App/Content';
import { AppProvider } from './context/AppContext';
import { AppIFrameProvider } from './context/AppIFrameContext';
import UserListenerContainer from './containers/UserListenerContainer/UserListenerContainer';
import introspectionQueryResultData from './data/fragmentTypes.json';

const browserWindow: CustomWindow = window;
export type CustomWindow = Window & WindowExtension;

interface ENV {
  REACT_APP_HTTP_URL?: string;
}

interface WindowExtension {
  ENV?: ENV;
}

const App: React.FC = () => {
  const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('token');
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : '',
      },
    };
  });

  const uploadLink = createUploadLink({
    // credentials: 'include',
    uri:
      browserWindow.ENV && browserWindow.ENV.REACT_APP_HTTP_URL
        ? browserWindow.ENV.REACT_APP_HTTP_URL
        : process.env.REACT_APP_HTTP_URL,
  });

  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData,
  });
  // we can also pass a custom map of functions. These will have priority over the GraphQLTypes parsing and serializing functions from the Schema.
  /* const typesMap = {
    DateTime: {
      serialize: (parsed: Date) => parsed.toString(),
      parseValue: (raw: string | number | null): Date | null => {
        return raw ? new Date(raw) : null;
      },
    },
  }; */
  const apolloClient = new ApolloClient({
    cache: new InMemoryCache({ fragmentMatcher }),
    link: ApolloLink.from([
      // withScalars(),
      onError(({ graphQLErrors, networkError, forward, operation, response }) => {
        if (graphQLErrors) {
          let needsReload = false;
          graphQLErrors.forEach(async err => {
            if (
              err &&
              err.extensions &&
              err.extensions.code &&
              err.extensions.code === 'UNAUTHENTICATED'
            ) {
              apolloClient.resetStore();
              localStorage.removeItem('token');
              localStorage.removeItem('sessionInfo');
              window.location.reload();
              needsReload = true;
            }
          });

          if (!needsReload) {
            if (response) response.errors = graphQLErrors;
            forward(operation);
          }
        }
        if (networkError) {
          // eslint-disable-next-line no-console
          console.error(networkError);
        }
      }),
      authLink.concat(uploadLink),
    ]),
  });

  return (
    <ApolloProvider client={apolloClient}>
      <ApolloHooksProvider client={apolloClient}>
        <AppProvider>
          <AppIFrameProvider>
            <UserListenerContainer>
              <AppContent />
            </UserListenerContainer>
          </AppIFrameProvider>
        </AppProvider>
      </ApolloHooksProvider>
    </ApolloProvider>
  );
};

export default App;
