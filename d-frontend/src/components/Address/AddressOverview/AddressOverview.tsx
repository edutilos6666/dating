import React, { FC, Component } from 'react';
import { AddressFragment } from '../../../data/address.generated';
import { Maybe } from '../../../data/datatypes';

interface Props {
  address: Maybe<AddressFragment>;
}
const AddressOverview: FC<Props> = ({ address }): JSX.Element => {
  if (address)
    return (
      <>
        {address.country} {address.city}
      </>
    );
  return <></>;
};

export default AddressOverview;
