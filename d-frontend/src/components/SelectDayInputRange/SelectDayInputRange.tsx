import React, { useState, useRef } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import './SelectDayInputRange.css';
import { useSelectDayInputHelpers } from '../../pages/Util/useSelectDayInputHelpers';
import { DateUtils } from 'react-day-picker';

const SelectDayInputRange = () => {
  const [from, setFrom] = useState<Date>();
  const [to, setTo] = useState<Date>();
  const toRef = useRef<DayPickerInput>(null);
  const modifiers = { start: from, end: to };
  const { formatDate, parseDate } = useSelectDayInputHelpers();

  const onDayClick = () => {
    console.log(toRef.current);
    toRef.current && toRef.current.getInput().focus();
  };

  const handleDayClick = (day: Date) => {
    toRef.current && toRef.current.getInput().focus();
    const range = DateUtils.addDayToRange(day, { from: from!, to: to! });
    setFrom(range.from);
    setTo(range.to);
  };

  return (
    <div className="InputFromTo">
      <DayPickerInput
        formatDate={formatDate}
        parseDate={parseDate}
        format="DD-MM-YYYY"
        value={from}
        placeholder="From"
        dayPickerProps={{
          selectedDays: { from: from!, to: to! },
          disabledDays: { after: to! },
          modifiers,
          onDayClick: handleDayClick,
        }}
        onDayChange={() => setFrom(from)}
      />
      &nbsp;—&nbsp;
      <span className="InputFromTo-to">
        <DayPickerInput
          formatDate={formatDate}
          parseDate={parseDate}
          format="DD-MM-YYYY"
          ref={toRef}
          value={to}
          placeholder="To"
          dayPickerProps={{
            selectedDays: { from: from!, to: to! },
            disabledDays: { before: from! },
            modifiers,
            month: from,
            fromMonth: from,
            toMonth: to,
          }}
          onDayChange={() => setTo(to)}
        />
      </span>
    </div>
  );
};

export default SelectDayInputRange;
