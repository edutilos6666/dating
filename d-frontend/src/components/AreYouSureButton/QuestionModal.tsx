/* eslint-disable react/display-name */
import React, { forwardRef } from 'react';
import { Button, Modal } from 'react-bootstrap';

interface AreYouSureQuestionModal {
    title: string;
    message: string;
    onSuccessPressed: () => void;
    saveLabel?: string;
    show: boolean;
    hide: () => void;
}

const AreYouSureButtonQuestionModal = forwardRef(
    ({ title, message, onSuccessPressed, saveLabel, show, hide }: AreYouSureQuestionModal) => {
        return (
          <Modal show={show} onHide={hide}>
            <Modal.Header closeButton>
              <Modal.Title>{title}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <p>{message}</p>
            </Modal.Body>

            <Modal.Footer>
              <Button variant="secondary" onClick={hide}>
                        Abbrechen
              </Button>
              <Button variant="danger" onClick={onSuccessPressed}>
                {saveLabel || 'Speichern'}
              </Button>
            </Modal.Footer>
          </Modal>
        );
    },
);
export default AreYouSureButtonQuestionModal;
