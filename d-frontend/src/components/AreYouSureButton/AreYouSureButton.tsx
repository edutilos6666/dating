/* eslint @typescript-eslint/no-explicit-any: "off" */
import React, { useRef, useState, FC, MutableRefObject } from 'react';
import { Button, ButtonProps } from 'react-bootstrap';
import AreYouSureButtonQuestionModal from './QuestionModal';

export interface AreYouSureButtonInterface {
    title: string;
    message: string;
    label?: string;
    variant: ButtonProps['variant'];
    onSuccess: (object: MutableRefObject<any>) => void;
    saveLabel?: string;
    className?: string;
}
const AreYouSureButton: FC<AreYouSureButtonInterface> = ({
    title,
    message,
    label,
    variant,
    onSuccess,
    saveLabel,
    className,
}) => {
    const [show, setShow] = useState(false);
    const modalRef = useRef(null);

    const hide = () => {
        setShow(false);
    };
    return (
      <div className={className}>
        <AreYouSureButtonQuestionModal
          ref={modalRef}
          title={title}
          message={message}
          hide={hide}
          onSuccessPressed={() => {
                    onSuccess(modalRef);
                    setShow(false);
                }}
          saveLabel={saveLabel}
          show={show}
            />
        <Button
          type="button"
          variant={variant}
          onClick={() => {
                    setShow(true);
                }}
            >
          {label}
        </Button>
      </div>
    );
};
export default AreYouSureButton;
