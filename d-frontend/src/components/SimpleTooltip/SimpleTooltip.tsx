import React, { FC } from 'react';
import { OverlayTrigger, Popover, Tooltip } from 'react-bootstrap';

type TooltipMode = 'Dark' | 'White';
interface Props {
  title?: any | undefined;
  content: any;
  mode?: TooltipMode;
  placement?: any | undefined;
}
const SimpleTooltip: FC<Props> = ({ title, content, mode, placement }) => {
  if (mode === 'Dark') {
    return (
      <OverlayTrigger
        placement={placement ? placement : 'bottom'}
        overlay={
          <Tooltip id="popover-basic" className="border-0 bg-white">
            <span>{title ? title : content}</span>
          </Tooltip>
        }
      >
        <span>{content}</span>
      </OverlayTrigger>
    );
  } else {
    return (
      <OverlayTrigger
        placement="bottom"
        overlay={
          <Popover id="popover-basic" className="border-0 bg-white">
            <Popover.Title className="border-0 bg-white">{title ? title : content}</Popover.Title>
          </Popover>
        }
      >
        <span>{content}</span>
      </OverlayTrigger>
    );
  }
};

SimpleTooltip.defaultProps = {
  title: undefined,
  mode: 'Dark',
};

export default SimpleTooltip;
