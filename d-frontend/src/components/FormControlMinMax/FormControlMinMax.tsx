import React, { FC, KeyboardEvent, useRef } from 'react';
import { FormLabel, FormControl } from 'react-bootstrap';

export interface MinMaxInterface {
  name: string;
  value: any;
  placeholder: string;
}

interface Props {
  label: string;
  onBlur: (e: React.FocusEvent<any>) => void;
  onChange: (e: React.ChangeEvent<any>) => void;
  minPlaceholder: string;
  minName: string;
  minValue: any;
  maxPlaceholder: string;
  maxName: string;
  maxValue: any;
}

export const FormControlMinMax: FC<Props> = ({
  label,
  onBlur,
  onChange,
  minPlaceholder,
  minName,
  minValue,
  maxPlaceholder,
  maxName,
  maxValue,
}) => {
  const maxInputRef = useRef<FormControl & HTMLInputElement>(null);
  return (
    <div className="d-flex flex-column">
      <FormLabel>{label}</FormLabel>
      <div className="d-flex flex-row">
        <FormControl
          type="number"
          placeholder={minPlaceholder}
          name={minName}
          onBlur={onBlur}
          onChange={(e: any) => {
            onChange(e);
          }}
          value={minValue}
          onKeyDown={(evt: KeyboardEvent<HTMLInputElement>) => {
            if (evt.keyCode === 13) {
              console.log('ENTER was clicked.');
              maxInputRef && maxInputRef.current && maxInputRef.current.focus();
            }
          }}
        />
        <label className="mt-2">—</label>

        <FormControl
          ref={maxInputRef}
          type="number"
          placeholder={maxPlaceholder}
          name={maxName}
          onBlur={onBlur}
          onChange={onChange}
          value={maxValue}
        />
      </div>
    </div>
  );
};
