import React, { FC, KeyboardEvent, useRef } from 'react';
import { FormLabel, FormControl } from 'react-bootstrap';

interface Props {
  label: string;
  onMinChange: (e: React.ChangeEvent<any>) => void;
  onMaxChange: (e: React.ChangeEvent<any>) => void;
  minPlaceholder: string;
  minValue: any;
  maxPlaceholder: string;
  maxValue: any;
}

export const FormControlMinMaxV2: FC<Props> = ({
  label,
  onMinChange,
  onMaxChange,
  minPlaceholder,
  minValue,
  maxPlaceholder,
  maxValue,
}) => {
  const maxInputRef = useRef<FormControl & HTMLInputElement>(null);
  return (
    <div className="d-flex flex-column">
      <FormLabel>{label}</FormLabel>
      <div className="d-flex flex-row">
        <FormControl
          type="number"
          placeholder={minPlaceholder}
          onChange={onMinChange}
          value={minValue}
          onKeyDown={(evt: KeyboardEvent<HTMLInputElement>) => {
            if (evt.keyCode === 13) {
              console.log('ENTER was clicked.');
              maxInputRef && maxInputRef.current && maxInputRef.current.focus();
            }
          }}
        />
        <label className="mt-2">—</label>

        <FormControl
          ref={maxInputRef}
          type="number"
          placeholder={maxPlaceholder}
          onChange={onMaxChange}
          value={maxValue}
        />
      </div>
    </div>
  );
};
