import React, { useState, useRef, FC } from 'react';
import moment from 'moment';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { useSelectDayInputHelpers } from '../../pages/Util/useSelectDayInputHelpers';
import './SelectDayInputRangeV3.css';
import { MOMENTJS_FORMAT } from '../../pages/Util/Constants';

interface Props {
  from: Date | undefined;
  handleFromChange: (from: Date | undefined) => void;
  to: Date | undefined;
  handleToChange: (to: Date | undefined) => void;
  format?: string;
}

const SelectDayInputRangeV3: FC<Props> = ({
  from,
  handleFromChange,
  to,
  handleToChange,
  format,
}) => {
  // const [from, setFrom] = useState<Date>();
  // const [to, setTo] = useState<Date>();
  const { formatDate, parseDate } = useSelectDayInputHelpers();
  const refTo = useRef<DayPickerInput>(null);

  const showFromMonth = () => {
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      refTo.current && refTo.current.getInput().showMonth(from);
    }
  };

  const modifiers = { start: from!, end: to! };
  return (
    <div className="InputFromTo">
      <DayPickerInput
        inputProps={{ className: 'col-12', style: { height: '35px' } }}
        value={from || ''}
        placeholder="From"
        format={format}
        formatDate={formatDate}
        parseDate={parseDate}
        dayPickerProps={{
          selectedDays: [from!, { from: from!, to: to! }],
          disabledDays: { after: to! },
          toMonth: to,
          modifiers,
          numberOfMonths: 2,
          onDayClick: () => refTo.current && refTo.current.getInput().focus(),
          // className: 'form-control',
        }}
        onDayChange={handleFromChange}
      />{' '}
      —{' '}
      <span className="InputFromTo-to">
        <DayPickerInput
          inputProps={{ className: 'col-12', style: { height: '35px' } }}
          ref={refTo}
          value={to}
          placeholder="To"
          format={format}
          formatDate={formatDate}
          parseDate={parseDate}
          dayPickerProps={{
            selectedDays: [from!, { from: from!, to: to! }],
            disabledDays: { before: from! },
            modifiers,
            month: from,
            fromMonth: from,
            numberOfMonths: 2,
          }}
          onDayChange={handleToChange}
        />
      </span>
    </div>
  );
};

SelectDayInputRangeV3.defaultProps = {
  format: MOMENTJS_FORMAT,
};
export default SelectDayInputRangeV3;
