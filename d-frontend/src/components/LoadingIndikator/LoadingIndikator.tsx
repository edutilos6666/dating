/* eslint-disable react/display-name */
import React, { forwardRef, useImperativeHandle, useState, ReactNode } from 'react';

interface LoadingIndikatorInterface {
    children?: ReactNode;
    isInitLoading?: boolean;
}

export interface LoadingIndikatorRefInterface {
    loadingStart: () => void;
    loadingEnds: () => void;
}
const LoadingIndikator = forwardRef(({ children, isInitLoading }: LoadingIndikatorInterface, ref) => {
    const [loading, setLoading] = useState<boolean>(isInitLoading || false);
    const [startTimer, setStartTimer] = useState<NodeJS.Timeout | null>(null);
    useImperativeHandle(ref, () => ({
        loadingStart: () => {
            if (!startTimer)
                setStartTimer(
                    setTimeout(() => {
                        setLoading(true);
                    }, 300),
                );
        },
        loadingEnds: () => {
            if (startTimer) clearTimeout(startTimer);
            setLoading(false);
        },
    }));

    return (
      <div className={loading ? 'content-overlay' : ''}>
        {loading && (
        <div className="overlay-container">
          <div className="overlay-content">
            <div className="spinner-border" />
          </div>
        </div>
            )}
        {children}
      </div>
    );
});

export default LoadingIndikator;
