import React, { useState, FC, useEffect, FormEvent, KeyboardEvent } from 'react';
import { FormControlProps, Form, FormControl } from 'react-bootstrap';

interface CurrencyEditField {
  value: number | undefined | null;
  placeholder?: string;
  disabled?: boolean;
  setValue: (value: number) => void;
}
const CurrencyEditField: FC<CurrencyEditField> = ({ value, setValue, placeholder, disabled }) => {
  const [editValue, setEditValue] = useState<string>(value ? value.toFixed(2) : '');
  useEffect(() => {
    setEditValue(value ? value.toFixed(2) : '');
  }, [value]);
  return (
    <Form.Control
      type="number"
      pattern="[0-9]{10},[0-9]{2}"
      placeholder={placeholder}
      // className="pl-1 pt-2 pb-2"
      value={editValue}
      onBlur={() => {
        setValue(Number(editValue));
      }}
      onChange={(evt: FormEvent<FormControlProps & FormControl>) => {
        const val = evt.currentTarget.value;
        if (val) {
          setEditValue(val);
        } else if (val === '') {
          setEditValue('');
        }
      }}
      onKeyDown={(evt: KeyboardEvent<HTMLInputElement>) => {
        console.log(evt.keyCode);
        if (evt.keyCode === 13) {
          // enter
          setValue(Number(editValue));
        }
      }}
      disabled={disabled}
    />
  );
};
export default CurrencyEditField;
