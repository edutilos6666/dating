import React, { SFC, useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';

export interface CustomSpinnerProps {
  loading: boolean;
}
const CustomSpinner: SFC<CustomSpinnerProps> = ({ loading }) => {
  const [show, setShow] = useState<boolean>(false);
  useEffect(() => {
    setShow(loading);
  }, [loading]);
  return (
    <div className="position-absolute w-100 h-100" hidden={!show}>
      <Spinner
        animation="border"
        className="offset-5 mt-5 bold"
        style={{ width: '50px', height: '50px' }}
      />
    </div>
  );
};

export default CustomSpinner;
