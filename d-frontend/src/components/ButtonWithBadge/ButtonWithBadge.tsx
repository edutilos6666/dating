import React, { FC } from 'react';
import { Button, Badge } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export type ButtonVariantType =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'dark'
  | 'light'
  | 'link'
  | 'outline-primary'
  | 'outline-secondary'
  | 'outline-success'
  | 'outline-danger'
  | 'outline-warning'
  | 'outline-info'
  | 'outline-dark'
  | 'outline-light';

export type BadgeVariantType =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark';

interface Props {
  iconName: any;
  btnVariant: ButtonVariantType;
  badgeVariant: BadgeVariantType;
  value: any;
  handleClick?: () => void;
  disabled?: boolean;
}

export const ButtonWithBadge: FC<Props> = ({
  iconName,
  btnVariant,
  badgeVariant,
  value,
  handleClick,
  disabled,
}) => {
  return (
    <Button variant={btnVariant} className="mr-2" onClick={handleClick} disabled={disabled}>
      <FontAwesomeIcon icon={iconName} />
      {'  '}
      <Badge variant={badgeVariant} pill>
        {value}
      </Badge>
    </Button>
  );
};

ButtonWithBadge.defaultProps = {
  handleClick: undefined,
  disabled: false,
};
