/* eslint-disable react/display-name */
import React, { forwardRef, useEffect, useImperativeHandle, useRef } from 'react';
// Import css
import 'viewerjs/dist/viewer.css';
import Viewer from 'viewerjs';

export interface ImViewerInterface {
  image: string;
  height: number;
  rotate: number;
}
export interface ImViewerRefInterface {
  zoomIn: () => void;
  zoomOut: () => void;
  rotate: (rotate: number) => void;
}

interface ViewerHTMLImageElement extends HTMLImageElement {
  viewer?: Viewer;
}
const ImViewer = forwardRef(({ image, height }: ImViewerInterface, ref) => {
  const imageRef = useRef<ViewerHTMLImageElement>(null);
  useImperativeHandle(ref, () => ({
    zoomIn: () => {
      if (imageRef.current && imageRef.current.viewer) {
        imageRef.current.viewer.zoom(0.1);
      }
    },
    zoomOut: () => {
      if (imageRef.current && imageRef.current.viewer) {
        imageRef.current.viewer.zoom(-0.1);
      }
    },
    rotate: (rotate: number) => {
      if (imageRef.current && imageRef.current.viewer) {
        imageRef.current.viewer.rotateTo(rotate * 90);
      }
    },
  }));
  useEffect(() => {
    if (image) {
      if (imageRef.current && !imageRef.current.viewer) {
        const curViewer = new Viewer(imageRef.current, {
          loading: false,
          transition: false,
          keyboard: false,
          inline: true,
          toolbar: false,
          navbar: false,
          title: false,
        });
        imageRef.current.style.display = 'none';
        imageRef.current.viewer = curViewer;
      }
    }
    return () => {
      if (imageRef.current && imageRef.current.viewer) {
        imageRef.current.viewer.destroy();
        imageRef.current.viewer = undefined;
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [image]);

  return (
    <div>
      <div style={{ height: `${height || 800}px` }}>
        <img alt="Bild" id="image123" ref={imageRef} src={image} />
      </div>
    </div>
  );
});
export default ImViewer;
