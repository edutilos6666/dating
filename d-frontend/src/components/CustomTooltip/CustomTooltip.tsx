import React, { SFC, useState, useEffect, useRef } from 'react';
import { Popover } from 'react-bootstrap';
import { Placement } from 'react-bootstrap/Overlay';

interface Props {
  el: React.ReactNode;
  hidePopover: boolean;
  setHidePopover: (input: boolean) => void;
  tooltipTitle: any;
  tooltipContent?: any;
  tooltipLeft: string;
  tooltipPlacement: Placement; // left or right
  whiteSpaceForPopoverTitle?:
    | '-moz-initial'
    | 'inherit'
    | 'initial'
    | 'revert'
    | 'unset'
    | 'pre'
    | 'nowrap'
    | '-moz-pre-wrap'
    | 'normal'
    | 'pre-line'
    | 'pre-wrap'
    | undefined;
  expandable?: boolean;
}

const CustomTooltip: SFC<Props> = ({
  el,
  hidePopover,
  setHidePopover,
  tooltipTitle,
  tooltipContent,
  tooltipLeft,
  tooltipPlacement,
  whiteSpaceForPopoverTitle,
  expandable,
}) => {
  const [expanded, setExpanded] = useState<boolean>(false);
  const popoverRef = useRef<Popover & HTMLDivElement>(null);
  const contentChildRef = useRef<HTMLDivElement>(null);
  const [tooltipPos, setTooltipPos] = useState<string>();
  const [popoverVisiblity, setPopoverVisiblity] = useState<
    | '-moz-initial'
    | 'inherit'
    | 'initial'
    | 'revert'
    | 'unset'
    | 'hidden'
    | 'collapse'
    | 'visible'
    | undefined
  >('hidden');

  const popover = (
    <Popover
      ref={popoverRef}
      style={{
        position: 'absolute',
        left: tooltipPos,
        visibility: popoverVisiblity,
      }}
      id="popover-basic"
      className="border-0 p-1"
      placement={tooltipPlacement}
      hidden={hidePopover}
      onMouseLeave={() => {
        setHidePopover(true);
        setExpanded(false);
      }}
      onMouseEnter={() => {
        expandable && setExpanded(true);
        setHidePopover(false);
      }}
    >
      <Popover.Title
        as="h3"
        className="bg-white border-0"
        style={{ whiteSpace: whiteSpaceForPopoverTitle, maxWidth: '200px' }}
      >
        {tooltipTitle}
      </Popover.Title>
      <Popover.Content className="pt-0" style={{ minWidth: '200px' }} hidden={!expanded}>
        {tooltipContent}
      </Popover.Content>
    </Popover>
  );

  useEffect(() => {
    if (tooltipPos) return; // wenn Position bereits berechnet wurde, berechne wieder nicht
    if (popoverRef && popoverRef.current && popoverRef.current.clientWidth > 0) {
      if (tooltipPlacement === 'left')
        setTooltipPos(`-${popoverRef.current.clientWidth + parseInt(tooltipLeft) + 10}px`);
      else if (tooltipPlacement === 'right') setTooltipPos(`0`);
      setPopoverVisiblity('visible');
    } else {
      setPopoverVisiblity('hidden');
    }
  }, [popover, tooltipLeft, tooltipPlacement, tooltipPos]);

  const left =
    contentChildRef && contentChildRef.current && contentChildRef.current.getBoundingClientRect()
      ? contentChildRef.current.getBoundingClientRect().width
      : 0;

  return (
    <div style={{ display: 'flex', flexDirection: 'row' }}>
      <div ref={contentChildRef} style={{ overflow: 'hidden' }}>
        {el}
      </div>
      <div
        style={{
          position: 'absolute',
          width: '100%',
          minWidth: '200px',
          left: `+${left}px`,
        }}
      >
        {popover}
      </div>
    </div>
  );
};

CustomTooltip.defaultProps = {
  whiteSpaceForPopoverTitle: 'nowrap',
  expandable: true,
};

export default CustomTooltip;
