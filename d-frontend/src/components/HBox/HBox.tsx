import React, { FC, Component } from 'react';
import { Row, Col } from 'react-bootstrap';

export interface KeyValueString {
  key?: string;
  value: string;
}

export interface KeyValueJSXElement {
  key?: string;
  value: JSX.Element;
}

interface Props {
  boldKey?: KeyValueString;
  elements: KeyValueJSXElement[];
  keyClassName?: string;
}
export const HBox: FC<Props> = ({ boldKey, elements, keyClassName }) => {
  return (
    <Row>
      {boldKey && boldKey.value && boldKey.value.length > 0 ? (
        <Col className={boldKey.key ? boldKey.key : keyClassName}>
          <strong>{`${String(boldKey.value)[0].toUpperCase()}${String(boldKey.value).substr(
            1,
          )}: `}</strong>
        </Col>
      ) : (
        ''
      )}
      {elements.map(({ key, value }, i) => (
        <Col key={i} className={key ? key : ''}>
          {value}
        </Col>
      ))}
    </Row>
  );
};

HBox.defaultProps = {
  boldKey: { value: '' },
  keyClassName: 'col-5',
};
