import React, { FC } from 'react';

interface Props {
  content: any;
}
export const DefaultFC: FC<Props> = ({ content }) => {
  return <>{String(content)}</>;
};
