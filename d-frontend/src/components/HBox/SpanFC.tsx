import React, { FC } from 'react';

interface Props {
  content: any;
}
export const SpanFC: FC<Props> = ({ content }) => {
  return <span>{String(content)}</span>;
};
