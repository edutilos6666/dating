import React, { FC } from 'react';

interface Props {
  src: any;
  className: string;
  width?: string;
  height?: string;
}
export const ImageFC: FC<Props> = ({ src, className, width, height }) => {
  return <img src={src} className={className} width={width} height={height} />;
};

ImageFC.defaultProps = {
  width: '60',
  height: '60',
};
