import React , { SFC } from 'react';
import {
    Modal, Button
} from 'react-bootstrap';

interface Props {
    id: number,
    heading: string,
    body: string,
    show: boolean,
    handleReject: ()=>void,
    handleConfirm: (id: number)=>void,
    btnConfirmText: string,
    btnRejectText: string
}
const CustomModal: SFC<Props> = ({ id, heading, body, show, handleReject, handleConfirm, btnConfirmText = "Yes", btnRejectText = "No" }) => {
    return (
        <>
        <Modal show={show} onHide={handleReject}>
        <Modal.Header closeButton>
          <Modal.Title>{heading}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{body}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={()=>handleConfirm(id)}>
            {btnConfirmText}
          </Button>
          <Button variant="primary" onClick={handleReject}>
            {btnRejectText}
          </Button>
        </Modal.Footer>
        </Modal>
        </>
    );
}

export default CustomModal;