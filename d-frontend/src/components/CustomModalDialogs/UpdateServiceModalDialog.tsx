import React, { FC } from 'react';
import { timeUnits, TimeUnitType } from '../../models/CustomServiceConfigModel';
import { Modal, Button, Row, Col, FormControl, FormControlProps } from 'react-bootstrap';
import Select, { ValueType } from 'react-select';

interface Props {
  title: string;
  period: number | undefined;
  setPeriod: (period: number | undefined) => void;
  timeUnit: TimeUnitType;
  setTimeUnit: (timeUnit: TimeUnitType) => void;
  show: boolean;
  hide: () => void;
  updateService: (period: number, timeUnit: string) => Promise<Response>;
  refreshModel: () => void;
  setAlertMessage: (_: string) => void;
  setShowAlert: (_: boolean) => void;
}
const UpdateServiceModalDialog: FC<Props> = ({
  title,
  period,
  setPeriod,
  timeUnit,
  setTimeUnit,
  show,
  hide,
  updateService,
  refreshModel,
  setAlertMessage,
  setShowAlert,
}) => {
  const handleBtnOkOnClick = () => {
    if (period && timeUnits.indexOf(timeUnit) >= 0) {
      updateService(period, timeUnit.value)
        .then(res => {
          if (res.status === 401) throw new Error('Sie haben kein Admin-Recht');
          setPeriod(undefined);
          setTimeUnit(timeUnits[0]);
          refreshModel();
          hide();
        })
        .catch(ex => {
          hide();
          setAlertMessage(ex.message);
          setShowAlert(true);
        });
    }
  };
  return (
    <Modal show={show} onHide={hide}>
      <Modal.Header closeButton>
        <h3>Update {title}</h3>
      </Modal.Header>

      <Modal.Body>
        <div className="col-12">
          <Row className="mt-auto">
            <Col className="col-2 mt-2">Period:</Col>
            <Col className="offset-1 col-9">
              <FormControl
                type="number"
                placeholder="Period"
                name="period"
                value={period ? String(period) : ''}
                onChange={(evt: React.FormEvent<FormControl & FormControlProps>) =>
                  evt.currentTarget.value
                    ? setPeriod(parseInt(evt.currentTarget.value))
                    : setPeriod(undefined)
                }
              />
            </Col>
          </Row>

          <Row className="mt-2">
            <Col className="col-2 mt-2">TimeUnit:</Col>
            <Col className="offset-1 col-9">
              <Select
                value={timeUnit}
                options={timeUnits}
                onChange={(value: ValueType<TimeUnitType>) => {
                  const selectedValue = value as TimeUnitType;
                  setTimeUnit(selectedValue);
                }}
              />
            </Col>
          </Row>
        </div>
      </Modal.Body>

      <Modal.Footer>
        <Button
          className="mr-3"
          variant="outline-primary"
          onClick={handleBtnOkOnClick}
          disabled={!period}
        >
          Ok
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UpdateServiceModalDialog;
