import React, { SFC } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSkullCrossbones } from '@fortawesome/free-solid-svg-icons';

interface Props {
    title: string;
    message: string;
    show: boolean;
    hide: () => void;
}
const WarningModalDialog: SFC<Props> = ({ title, message, show, hide }) => {
    const warningIcon = <FontAwesomeIcon icon={faSkullCrossbones} />;
    return (
        <Modal show={show} onHide={hide}>
            <Modal.Header closeButton>
                <div className="d-flex">
                    <Button variant="danger" className="mr-2 rounded-circle">
                        {warningIcon}
                    </Button>
                    <Modal.Title>{title}</Modal.Title>
                </div>
            </Modal.Header>

            <Modal.Body>
                <div className="d-flex">
                    <p>{message}</p>
                </div>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="primary" onClick={hide}>
                    Ok
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default WarningModalDialog;
