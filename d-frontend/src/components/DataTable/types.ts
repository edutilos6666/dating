/* eslint @typescript-eslint/no-explicit-any: "off" */
export interface DataTableInterface {
  showHeader?: boolean;
  data: Array<any> | null | undefined;
  idColumnName: string;
  columns: Array<DataTableColumn<any>>;
  pagination?: DataTablePaginationDataTableInterface;
  rowExpander?: DataTableRowExpander<any>;
  scrollPagination?: DataTableInfinityLoaderScrollNavigator;
  tableHeight?: number;
  rowOnClickHandler?: (entry: any) => void;
}

export interface DataTableColumn<E> {
  name: any;
  sortBy?: PaginationInputSort;
  width?: number | string | undefined;
  accessor: (data: E) => {} | null | undefined;
  title?: (data: E) => string;
}

export interface DataTablePaginationDataTableInterface {
  recordCount: number;
  first: number;
  pageSize: number;
  totalPages: number;
  setPage: (first: number) => void;
  firstPage: () => void;
  lastPage: () => void;
  nextPage: () => void;
  previosPage: () => void;
  onPageSizeChanged?: (pageSize: number) => void;
}

export interface DataTableScrollPaginationInterface extends InfiniteLoaderInterface {
  loadMore: (data: PaginationResult) => Promise<any>;
  totalItems: number;
  resultData: PaginationResult;
}
export interface InfiniteLoaderInterface {
  data: Array<any>;
  columns: Array<DataTableColumn<any>>;
  navigator: DataTableInfinityLoaderScrollNavigator;
  tableHeight?: number;
  rowExpander?: DataTableRowExpander<any>;
  rowOnClickHandler?: (entry: any) => void;
}
export interface DataTableRowExpander<E> {
  accessor: (data: E) => {} | null | undefined;
  getSize: (data: E) => number;
  expandedRows: Array<Number | String> | undefined;
}

export interface DataTableInfinityLoaderItemInterface {
  index: number;
  showExpandedRow?: boolean;
  style: any;
}
export interface DataTableInfinityLoaderScrollNavigator {
  data: PaginationResult | null | undefined;
  recordData: [];
  pageLoadMore: (data: PaginationResult | null | undefined) => Promise<any>;
  sortChanged: (data: PaginationInputSort, append: boolean) => Promise<any> | void;
  sorts?: Array<PaginationInputSort | null | undefined>;
  getKeyForData: (row: any, index: number) => string;
  loading: boolean;
}

/** Wrapper GraphQLPaginationInput etc.pp um die Komponenten exportierbar zu machen. Maybe wird hierbei zu  | null | undefined * */

export type PaginationInput = {
  offsetPage?: number | null | undefined;
  pageSize?: number | null | undefined;
  sorts?: Array<PaginationInputSort | null | undefined> | null | undefined;
};
export type PaginationResult = {
  pageInfo: PaginationPageInfo;
  result: Array<any>;
};
export type PaginationInputSort = {
  fieldName: string;
  sortDirection?: SortDirection | null | undefined;
};
// eslint-disable-next-line import/prefer-default-export
export enum SortDirection {
  Ascending = 'ASCENDING',
  Descending = 'DESCENDING',
}

export type PaginationPageInfo = {
  totalElements: number;
  totalPages: number;
  number: number;
  size: number;
};
