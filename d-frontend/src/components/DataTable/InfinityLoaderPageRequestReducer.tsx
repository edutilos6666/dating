import React from 'react';

export interface RequestedPage {
  page: number;
  pageSize: number;
  success: boolean;
}

export type PageRequestReducer =
  | { type: 'addPageRequest'; page: number; pageSize: number }
  | { type: 'pageRequestFinished'; page: number };

const PageRequestReducer = (state: Array<RequestedPage>, action: PageRequestReducer) => {
  switch (action.type) {
    case 'addPageRequest': {
      return [...state, { page: action.page, pageSize: action.pageSize, success: false }];
    }
    case 'pageRequestFinished': {
      const newState = [...state];
      const newPage = newState.find(page => page.page === action.page);
      if (newPage) {
        newPage.success = true;
      }
      return newState;
    }
    default:
      return state;
  }
};
export default PageRequestReducer;
