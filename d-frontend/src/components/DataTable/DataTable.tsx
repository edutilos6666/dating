import React, { FC, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { uuid } from 'uuidv4';
import { DataTableInterface } from './types';
import DataTableInfinityLoader from './InfinityLoader';
import DataTablePagination from './Pagination';

/* eslint @typescript-eslint/no-explicit-any: "off" */
const DataTable: FC<DataTableInterface> = ({
  showHeader,
  data,
  columns,
  pagination,
  rowExpander,
  scrollPagination,
  idColumnName,
  tableHeight,
  rowOnClickHandler = (entry: any) => {},
}) => {
  if (scrollPagination) {
    return (
      <DataTableInfinityLoader
        columns={columns}
        data={data || []}
        rowExpander={rowExpander}
        navigator={scrollPagination}
        tableHeight={tableHeight}
        rowOnClickHandler={rowOnClickHandler}
      />
    );
  }

  let header = (
    <thead>
      <tr>
        {columns.map(col => (
          <td key={col.name}>{col.name}</td>
        ))}
      </tr>
    </thead>
  );
  if (!showHeader) {
    header = <></>;
  }
  let paginationComponent = <></>;
  if (pagination) {
    paginationComponent = (
      <DataTablePagination
        setPage={pagination.setPage}
        previosPage={pagination.previosPage}
        nextPage={pagination.nextPage}
        firstPage={pagination.firstPage}
        lastPage={pagination.lastPage}
        first={pagination.first}
        pageSize={pagination.pageSize}
        totalPages={pagination.totalPages}
        recordCount={pagination.recordCount}
      />
    );
  }
  return (
    <>
      {paginationComponent}
      <Table className="table" hover bordered>
        {header}
        <tbody>
          {data
            ? data.map(entry => (
                <tr key={entry[idColumnName]} onClick={() => rowOnClickHandler(entry)}>
                  {columns.map((column, index) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <td key={uuid()}>{column.accessor(entry)}</td>
                  ))}
                </tr>
              ))
            : ''}
        </tbody>
      </Table>
    </>
  );
};
export default DataTable;
