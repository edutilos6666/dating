/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import InfiniteLoader from 'react-window-infinite-loader';
import { VariableSizeList } from 'react-window';
import React, { FC, useEffect, useState, useRef, useReducer } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';

import { uuid } from 'uuidv4';
import {
  InfiniteLoaderInterface,
  DataTableInfinityLoaderItemInterface,
  DataTableColumn,
  SortDirection,
  PaginationInputSort,
  PaginationResult,
} from './types';
import useCtrlKeyPressed from '../../hooks/useCtrlKeyPressed';
// eslint-disable-next-line import/no-named-as-default
import PageRequestReducer from './InfinityLoaderPageRequestReducer';

/* eslint @typescript-eslint/no-explicit-any: "off" */

const DataTableInfinityLoader: FC<InfiniteLoaderInterface> = ({
  data,
  columns,
  navigator,
  rowExpander,
  tableHeight,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  rowOnClickHandler = (entry: any) => {},
}) => {
  const ctrlKeyPressed = useCtrlKeyPressed();
  const fixedSizeListRef = useRef<VariableSizeList | undefined | null>(null);
  const defaultItemHeight = 30;
  const [pageRequest, pageRequestDispatch] = useReducer(PageRequestReducer, []);

  useEffect(() => {
    if (fixedSizeListRef.current && rowExpander && rowExpander.expandedRows) {
      fixedSizeListRef.current.resetAfterIndex(-1, true);
    }
  }, [rowExpander]);

  const getPageSize = () => {
    return navigator.data && navigator.data.pageInfo ? navigator.data.pageInfo.size : 20;
  };

  const getPageForIndex = (index: number) => {
    const pageSize = getPageSize();
    return Math.floor(index / pageSize);
  };

  const isItemLoaded = (index: number) => {
    const pageIndex = getPageForIndex(index);
    if (pageIndex) {
      const allreadyRequested = !!pageRequest.find(page => page.page === pageIndex);
      // if (!allreadyRequested) console.log(index, pageIndex, allreadyRequested);
      return allreadyRequested;
    }
    return true;
  };
  const [totalItems, setTotalItems] = useState(
    navigator.data && navigator.data.pageInfo && navigator.data.pageInfo.totalElements
      ? navigator.data.pageInfo.totalElements
      : 0,
  );

  useEffect(() => {
    if (!navigator.loading) {
      setTotalItems(
        navigator.data && navigator.data.pageInfo && navigator.data.pageInfo.totalElements
          ? navigator.data.pageInfo.totalElements
          : 0,
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  // Re

  const loadMoreItems = (startIndex: number, stopIndex: number) => {
    // console.log('loadMoreItems', startIndex, stopIndex);
    const allreadyRequested = !!pageRequest.find(page => page.page === getPageForIndex(stopIndex));
    if (!allreadyRequested) {
      pageRequestDispatch({
        type: 'addPageRequest',
        page: getPageForIndex(stopIndex),
        pageSize: getPageSize(),
      });
    } else {
      return new Promise(() => {});
    }

    if (navigator.data) {
      const navData: PaginationResult = { ...navigator.data };
      // Es wird wieder +1 gerechnet im pageLoadMore
      navData.pageInfo.number = getPageForIndex(stopIndex) - 1;

      const promise = navigator.pageLoadMore(navData);
      if (promise) return promise;
    }

    return new Promise(() => {});
  };

  const IColumnData = ({ column, value }: { column: DataTableColumn<any>; value: any }) => {
    const memoData: any = column.accessor(value);
    // eslint-disable-next-line no-nested-ternary
    const title = column.title
      ? column.title(value)
      : memoData && memoData.type === undefined
      ? memoData
      : '';

    const style: React.CSSProperties = {
      flexGrow: 1,
      flexShrink: 1,
      flexBasis: column.width ? column.width : '0%',
    };

    return (
      <div
        key={column.name}
        style={style}
        // title={title}
        className="pl-1 pr-2 text-truncate"
        onClick={() => rowOnClickHandler(value)}
      >
        {memoData}
      </div>
    );
  };
  const ColumnData = React.memo(IColumnData);

  const Row: FC<DataTableInfinityLoaderItemInterface> = ({ index, style }) => {
    let content;
    if (!isItemLoaded(index)) {
      content = 'Lade...';
    } else if (data.length > index) {
      content = columns.map(column => <ColumnData column={column} value={data[index]} />);
    }

    const returnContent = (
      <div className="d-flex pb-2  table-row" style={style}>
        {content}
      </div>
    );
    if (
      rowExpander &&
      rowExpander.expandedRows &&
      rowExpander.expandedRows.find(val => val === navigator.getKeyForData(data[index], index))
    ) {
      const originalStyle = { ...style };
      originalStyle.height = defaultItemHeight;

      const expandedStyle = { ...style };
      expandedStyle.height = rowExpander.getSize(data[index]) - defaultItemHeight;
      expandedStyle.top += defaultItemHeight;

      content = (
        <>
          <div className="d-flex table-row border-bottom pb-2" style={originalStyle}>
            {content}
          </div>
          <div className="w-100 border-bottom pb-2" style={expandedStyle}>
            {rowExpander.accessor(data[index])}
          </div>
        </>
      );
      return content;
    }

    return returnContent;
  };
  const getItemSize = (index: number) => {
    if (
      rowExpander &&
      rowExpander.expandedRows &&
      rowExpander.expandedRows.find(val => val === navigator.getKeyForData(data[index], index))
    ) {
      return rowExpander.getSize(data[index]);
    }
    return defaultItemHeight;
  };

  const changeOrderBy = (column: DataTableColumn<any>) => {
    if (column && column.sortBy) {
      if (fixedSizeListRef && fixedSizeListRef.current) {
        fixedSizeListRef.current.scrollToItem(0);
      }
      if (navigator && navigator.sorts) {
        const sortColumn = navigator.sorts.find(
          sort => column.sortBy && sort && sort.fieldName === column.sortBy.fieldName,
        );
        if (sortColumn) {
          const newSort = { ...sortColumn };
          switch (sortColumn.sortDirection) {
            case SortDirection.Ascending:
              newSort.sortDirection = SortDirection.Descending;
              break;
            case SortDirection.Descending:
              newSort.sortDirection = SortDirection.Ascending;
              break;
            default:
              newSort.sortDirection = SortDirection.Ascending;
          }
          navigator.sortChanged(newSort, ctrlKeyPressed);
        } else {
          navigator.sortChanged(
            { ...column.sortBy, sortDirection: SortDirection.Descending },
            ctrlKeyPressed,
          );
        }
      }
    }
  };

  return (
    <>
      <div className="pl-2">
        <div className="d-flex mb-2 ">
          {columns.map((column, index) => (
            <div
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              onClick={column.sortBy ? () => changeOrderBy(column) : undefined}
              // title={column.name}
              style={{
                flexGrow: 1,
                flexShrink: 1,
                flexBasis: column.width ? column.width : '0%',
              }}
              className={`pl-1 pr-2 text-truncate ${column.sortBy ? ' cursor-pointer' : ''}`}
            >
              <span>
                <strong>{column.name}</strong>
              </span>
              <OrderByListener sorts={navigator.sorts} column={column} />
            </div>
          ))}
        </div>

        <InfiniteLoader
          isItemLoaded={isItemLoaded}
          itemCount={totalItems}
          threshold={1}
          loadMoreItems={loadMoreItems}
        >
          {({ onItemsRendered, ref }) => (
            <VariableSizeList
              className="table-shadow overflow-x-hidden"
              height={tableHeight || 600}
              itemCount={totalItems}
              itemSize={getItemSize}
              itemKey={index => {
                // Nicht gut den Primary Key zu ignorieren wenn es einen gibt. Problem dabei...
                // Die UUID würde bei jedem Rerender neugeneriert werden und die Zeilen hätten damit jedesmal nen neuen Key
                // Nächste Zeile auskommentiert..
                // return uuid();
                // Wenn dann bitte so:
                let key = navigator.getKeyForData(data[index], index);
                if (!key) {
                  key = uuid();
                }

                return key;
              }}
              onItemsRendered={onItemsRendered}
              ref={el => {
                fixedSizeListRef.current = el;
                return ref;
              }}
              width="100%"
            >
              {Row}
            </VariableSizeList>
          )}
        </InfiniteLoader>
      </div>
    </>
  );
};
export default DataTableInfinityLoader;

interface OrderByListenerInterface {
  sorts?: Array<PaginationInputSort | undefined | null>;
  column: DataTableColumn<any>;
}
const OrderByListener: FC<OrderByListenerInterface> = ({ sorts, column }) => {
  if (sorts) {
    const sortColumns = sorts.find(
      sort => sort && column && column.sortBy && sort.fieldName === column.sortBy.fieldName,
    );
    if (sortColumns) {
      return (
        <OrderByIndicator
          direction={sortColumns.sortDirection}
          position={sorts.indexOf(sortColumns)}
        />
      );
    }
  }

  return <></>;
};
interface OrderByIndicatorInterface {
  direction: SortDirection | null | undefined;
  position: number;
}
const OrderByIndicator: FC<OrderByIndicatorInterface> = ({ direction, position }) => {
  let result = <></>;
  const pos = (
    <small className="pl-1">
      <sup>{position + 1}</sup>
    </small>
  );
  switch (direction) {
    case SortDirection.Ascending:
      result = (
        <>
          {' '}
          <FontAwesomeIcon icon={faSortUp} />
          {pos}
        </>
      );
      break;
    case SortDirection.Descending:
      result = (
        <>
          {' '}
          <FontAwesomeIcon icon={faSortDown} />
          {pos}
        </>
      );
      break;
    default:
  }
  return <>{result}</>;
};
