import React, { FC, useEffect } from 'react';
import { DataTableInterface } from '../DataTable/types';
import CustomSpinner, { CustomSpinnerProps } from '../CustomSpinner/CustomSpinner';
import DataTable from '../DataTable/DataTable';

interface DataTableWithSpinnerProps extends DataTableInterface, CustomSpinnerProps {}
const DataTableWithSpinner: FC<DataTableWithSpinnerProps> = ({
  showHeader,
  data,
  columns,
  pagination,
  rowExpander,
  scrollPagination,
  idColumnName,
  tableHeight,
  rowOnClickHandler = () => {},
  loading,
}) => {
  return (
    <>
      <CustomSpinner loading={loading} />
      <DataTable
        showHeader={showHeader}
        data={data}
        columns={columns}
        pagination={pagination}
        rowExpander={rowExpander}
        scrollPagination={scrollPagination}
        idColumnName={idColumnName}
        tableHeight={tableHeight}
        rowOnClickHandler={rowOnClickHandler}
      />
    </>
  );
};

export default DataTableWithSpinner;
