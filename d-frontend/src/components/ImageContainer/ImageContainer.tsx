import React, { useEffect, useRef, useState, FC } from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import InlineSVG from 'react-inlinesvg';
import ImViewer, { ImViewerRefInterface } from '../Viewer/Viewer';
import { Maybe } from '../../data/datatypes';

export interface ImageDataInterface {
  rotate: number;
  imageBasePath: string;
  ifd: number;
  ifdBack?: Maybe<number>;
  name: string;
}

export interface ImageContainerInterface {
  imageData: Array<ImageDataInterface>;
  width: number;
  height: number;
  showLeftNavigation: boolean;
}

const ImageContainer: FC<ImageContainerInterface> = ({
  imageData,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  width,
  height,
  showLeftNavigation,
}) => {
  const [currentRecordIndex, setCurrentRecordIndex] = useState(0);
  const [currentRecord, setCurrentRecord] = useState<ImageDataInterface>({
    rotate: 0,
    imageBasePath: '',
    ifd: 0,
    ifdBack: 0,
    name: '',
  });
  const [currentImageUrl, setCurrentImageUrl] = useState('');
  const [showBackSite, setShowBackSite] = useState(false);
  const [rotate, setRotate] = useState(0);
  const [hasNextRecord, setHasNextRecord] = useState(false);
  const [hasPreviosRecord, setHasPreviosRecord] = useState(false);

  const viewer = useRef<ImViewerRefInterface>(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const currentRecordIndexChanged = () => {
    setShowBackSite(false);
    // console.log('currentRecordIndex CHANGED');
    if (imageData && imageData.length > 0 && currentRecordIndex != null) {
      setCurrentRecord(imageData[currentRecordIndex]);

      if (imageData.length > 0) {
        if (currentRecordIndex > 0) {
          setHasPreviosRecord(true);
        } else {
          setHasPreviosRecord(false);
        }
        if (currentRecordIndex < imageData.length - 1) {
          setHasNextRecord(true);
        } else {
          setHasNextRecord(false);
        }
      } else {
        setHasPreviosRecord(false);
        setHasNextRecord(false);
      }
    } else {
      setRotate(0);
      setHasNextRecord(false);
      setHasPreviosRecord(false);
    }
  };

  useEffect(() => {
    if (imageData) {
      if (imageData && imageData.length > 0) {
        setCurrentRecordIndex(0);
      } else {
        currentRecordIndexChanged();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [imageData]);

  useEffect(() => {
    currentRecordIndexChanged();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentRecordIndex]);
  useEffect(() => {
    if (currentRecord) {
      setCurrentImageUrl(
        `${currentRecord.imageBasePath}/${
          showBackSite ? currentRecord.ifdBack : currentRecord.ifd
        }?width=-1&height=-1`,
      );
      if (currentRecord.rotate) {
        setTimeout(() => {
          if (viewer.current) {
            viewer.current.rotate(currentRecord.rotate);
          }
        }, 100);

        setRotate(currentRecord.rotate);
      } else {
        setRotate(0);
      }
    } else {
      setCurrentImageUrl('');
      setRotate(0);
    }
  }, [currentRecord, showBackSite]);

  const switchBackSite = () => {
    setShowBackSite(!showBackSite);
  };
  const nextRecord = () => {
    if (hasNextRecord) {
      setCurrentRecordIndex(currentRecordIndex + 1);
    }
  };
  const rotateForward = () => {
    if (rotate === 3) {
      if (viewer.current) {
        viewer.current.rotate(0);
      }
      setRotate(0);
    } else {
      if (viewer.current) {
        viewer.current.rotate(rotate + 1);
      }
      setRotate(rotate + 1);
    }
  };
  const rotateBackward = () => {
    if (rotate === 0) {
      if (viewer.current) {
        viewer.current.rotate(3);
      }
      setRotate(3);
    } else {
      if (viewer.current) {
        viewer.current.rotate(rotate - 1);
      }
      setRotate(rotate - 1);
    }
  };
  const previosRecord = () => {
    if (hasPreviosRecord) {
      setCurrentRecordIndex(currentRecordIndex - 1);
    }
  };
  const firstRecord = () => {
    if (hasPreviosRecord) {
      setCurrentRecordIndex(0);
    }
  };
  const lastRecord = () => {
    if (hasNextRecord) {
      setCurrentRecordIndex(imageData.length - 1);
    }
  };

  const zoomIn = () => {
    if (viewer && viewer.current != null) viewer.current.zoomIn();
    // console.log('zoomIn '+viewer.current.zoomIn());
  };
  const zoomOut = () => {
    if (viewer && viewer.current != null) viewer.current.zoomOut();
    // console.log('zoomOut '+viewer.current.zoomOut());
  };

  let navigation = (
    <div className="col-12 text-center ">
      <ImViewer ref={viewer} height={height} image={currentImageUrl} rotate={rotate} />
    </div>
  );
  if (showLeftNavigation) {
    navigation = (
      <>
        <div className="col-3">
          <ListGroup as="ul">
            {imageData.map((item, index) => (
              <ListGroup.Item
                // eslint-disable-next-line react/no-array-index-key
                key={index}
                as="li"
                onClick={() => {
                  setCurrentRecordIndex(index);
                }}
                active={index === currentRecordIndex}
              >
                {item.name}
              </ListGroup.Item>
            ))}
          </ListGroup>
        </div>
        <div className="col-9 text-center border ">
          <ImViewer ref={viewer} height={height} image={currentImageUrl} rotate={rotate} />
        </div>
      </>
    );
  }

  let backsideIcon = (
    <InlineSVG cacheRequests src="/assets/components/baseline-flip_to_front-24px.svg" />
  );
  if (showBackSite) {
    backsideIcon = (
      <InlineSVG cacheRequests src="/assets/components/baseline-flip_to_back-24px.svg" />
    );
  }

  return (
    <div className="container border rounded mb-2">
      <div className="row mb-2 bg-light p-2">
        <div className="col-6 col-lg-2 text-left ">
          <Button type="button" className="mr-1" onClick={zoomOut} variant="outline-secondary">
            <InlineSVG cacheRequests src="/assets/components/baseline-zoom_out-24px.svg" />
          </Button>
          <Button type="button" className="mr-1" onClick={zoomIn} variant="outline-secondary">
            <InlineSVG cacheRequests src="/assets/components/baseline-zoom_in-24px.svg" />
          </Button>
        </div>
        <div className="col-6 col-lg-3 text-right">
          <div className="d-flex pt-1">
            <span className="pr-2 pt-1"> Seite</span>

            <select
              className="form-control form-control-sm"
              style={{ width: '50px' }}
              onChange={event => {
                setCurrentRecordIndex(Number(event.target.value));
              }}
              defaultValue={`${currentRecordIndex}`}
            >
              {imageData.map((imageDate, index) => (
                // eslint-disable-next-line react/no-array-index-key
                <option key={index} value={index}>
                  {index + 1}
                </option>
              ))}
            </select>
            <span className="pl-2 pt-1">/{imageData.length}</span>
          </div>
        </div>
        <div className="col-12 col-lg-7 mt-2 mt-lg-0 text-left text-lg-right">
          <Button
            type="button"
            className="mr-4"
            disabled={!currentRecord.ifdBack}
            onClick={switchBackSite}
            variant="outline-secondary"
          >
            {backsideIcon}
          </Button>

          <Button
            type="button"
            className="mr-1"
            disabled={!hasPreviosRecord}
            onClick={firstRecord}
            variant="outline-secondary"
          >
            <InlineSVG cacheRequests src="/assets/components/baseline-first_page-24px.svg" />
          </Button>
          <Button
            type="button"
            className="mr-1"
            disabled={!hasPreviosRecord}
            onClick={previosRecord}
            variant="outline-secondary"
          >
            <InlineSVG cacheRequests src="/assets/components/baseline-navigate_before-24px.svg" />
          </Button>
          <Button
            type="button"
            className="mr-1"
            disabled={!hasNextRecord}
            onClick={nextRecord}
            variant="outline-secondary"
          >
            <InlineSVG cacheRequests src="/assets/components/baseline-navigate_next-24px.svg" />
          </Button>
          <Button
            type="button"
            className="mr-4"
            disabled={!hasNextRecord}
            onClick={lastRecord}
            variant="outline-secondary"
          >
            <InlineSVG cacheRequests src="/assets/components/baseline-last_page-24px.svg" />
          </Button>
          <Button
            type="button"
            className="mr-1"
            onClick={rotateBackward}
            variant="outline-secondary"
          >
            <InlineSVG cacheRequests src="/assets/components/baseline-rotate_left-24px.svg" />
          </Button>
          <Button
            type="button"
            className="mr-1"
            onClick={rotateForward}
            variant="outline-secondary"
          >
            <InlineSVG cacheRequests src="/assets/components/baseline-rotate_right-24px.svg" />
          </Button>
        </div>
      </div>
      <div className="row pb-2 pr-1">{navigation}</div>
    </div>
  );
};
export default ImageContainer;
