import React, { FC, useState, useEffect, useContext } from 'react';
import { PersonFragment, useFindPersonByIdQuery } from '../../../data/person.generated';
import { RouteComponentProps, match } from 'react-router';
import { PersonCarousel } from '../Carousel/Carousel';
import { PersonDetailsBody } from './Body';
import { HistoryContext } from '../../../context/HistoryContext';
import { ActiveTabContext } from '../../../context/ActiveTabContext';
import { MESSAGE_BOX_TAB } from '../../../pages/Util/TabNames';

interface PathVariables {
  id: string;
}

interface Props extends RouteComponentProps {
  match: match<PathVariables>;
}

export const PersonDetails: FC<Props> = ({ history, match }) => {
  const [triggerRefetchFlag, setTriggerRefetchFlag] = useState<boolean>(false);

  const { data, refetch } = useFindPersonByIdQuery({
    variables: {
      id: parseInt(match.params.id),
    },
  });

  const { setActiveTab } = useContext(ActiveTabContext);

  useEffect(() => {
    refetch();
  }, [triggerRefetchFlag]);

  useEffect(() => {
    setActiveTab(MESSAGE_BOX_TAB);
  }, []);

  // useEffect(() => {
  //   if (history.location.state && history.location.state.person) {
  //     setPerson(history.location.state.person);
  //   }
  // }, [history]);
  if (data && data.findPersonById)
    return (
      <HistoryContext.Provider
        value={{
          history: history,
        }}
      >
        <div className="d-flex flex-column offset-2 col-8">
          <PersonCarousel
            person={data.findPersonById}
            triggerRefetch={() => setTriggerRefetchFlag(!triggerRefetchFlag)}
          />
          <PersonDetailsBody person={data.findPersonById} />
        </div>
      </HistoryContext.Provider>
    );
  return <h2>ERROR</h2>;
};
