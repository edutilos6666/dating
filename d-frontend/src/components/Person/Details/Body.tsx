import React, { FC, useEffect, useContext, useState } from 'react';
import { PersonFragment } from '../../../data/person.generated';
import { Card, Button } from 'react-bootstrap';
import { useTimeUtil } from '../../../pages/Util/useTimeUtil';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faEnvelope, faUserSlash, faTrash } from '@fortawesome/free-solid-svg-icons';
import { HistoryContext } from '../../../context/HistoryContext';
import { useChatUtil } from '../../../pages/Util/useChatUtil';
import { useFunctionHelper, UpdateRelCommand } from '../MessageBox/useFunctionHelper';
import { InformationModalDialog } from '../../Utils/CustomModal/InformationModalDialog';

interface Props {
  person: PersonFragment;
}

interface PersonDetailsRowProps {
  name: string;
  value: any;
  valueColor?: string;
}
const PersonDetailsRow: FC<PersonDetailsRowProps> = ({ name, value, valueColor }) => {
  return (
    <div className="d-flex flex-row">
      <strong className="mr-3">{`${name[0].toUpperCase()}${name.substr(1)}`}:</strong>
      <label style={{ color: valueColor }}>{value}</label>
    </div>
  );
};
PersonDetailsRow.defaultProps = {
  valueColor: 'black',
};

export const PersonDetailsBody: FC<Props> = ({ person }) => {
  const { convertTimestampToDateString } = useTimeUtil();
  const { history } = useContext(HistoryContext);
  const { establishChatChannel } = useChatUtil();
  const { handleUpdatePersonRelByUserToId } = useFunctionHelper();
  const [showModalDialog, setShowModalDialog] = useState<boolean>(false);
  const [messageModalDialog, setMessageModalDialog] = useState<string>('');

  const handleBtnMessageClicked = () => {
    establishChatChannel(history, parseInt(person.id));
  };

  const callbackForUpdatePersonRel = (message: string) => {
    setMessageModalDialog(message);
    setShowModalDialog(true);
  };

  const handleBtnFavorite = () => {
    if (person && person.id)
      handleUpdatePersonRelByUserToId(person.id, UpdateRelCommand.FAVORITE, () => {
        callbackForUpdatePersonRel(`User ${person.id} was added to favorite list.`);
      });
  };

  const handleBtnBlock = () => {
    if (person && person.id)
      handleUpdatePersonRelByUserToId(person.id, UpdateRelCommand.BLOCK, () => {
        callbackForUpdatePersonRel(`User ${person.id} was added to blocked list.`);
      });
  };

  const handleBtnDelete = () => {
    if (person && person.id)
      handleUpdatePersonRelByUserToId(person.id, UpdateRelCommand.DELETE, () => {
        callbackForUpdatePersonRel(`User ${person.id} was added to deleted list.`);
      });
  };

  return (
    <div className="d-flex flex-column col-8 offset-2">
      <Card>
        <Card.Body>
          <div className="d-flex flex-column">
            <PersonDetailsRow
              name="status"
              value={person.status ? 'Online' : 'Offline'}
              valueColor={person.status ? 'green' : 'red'}
            />
            <PersonDetailsRow name="username" value={person.username} />
            <PersonDetailsRow name="name" value={`${person.firstname} ${person.lastname}`} />
            {person.gender ? <PersonDetailsRow name="gender" value={person.gender.name} /> : ''}
            {person.job ? (
              <>
                <PersonDetailsRow name="company" value={person.job.companyName} />
                <PersonDetailsRow name="occupation" value={person.job.title} />
                <PersonDetailsRow name="salary" value={`${person.job.wage} €`} />
              </>
            ) : (
              ''
            )}
            <PersonDetailsRow
              name="birthdate"
              value={convertTimestampToDateString(person.birthdate)}
            />
            {person.address ? (
              <>
                <PersonDetailsRow name="country" value={person.address.country} />
                <PersonDetailsRow name="city" value={person.address.city} />
                <PersonDetailsRow name="zipcode" value={person.address.zipcode} />
              </>
            ) : (
              ''
            )}

            <PersonDetailsRow name="email" value={person.email} />
            <PersonDetailsRow name="about me" value={person.aboutMe} />
            <PersonDetailsRow name="about ideal partner" value={person.aboutIdealPartner} />
          </div>
          <div className="d-flex flex-row">
            <Button variant="primary" className="mr-3" onClick={handleBtnMessageClicked}>
              Message
              <FontAwesomeIcon className="ml-2" icon={faEnvelope} />
            </Button>
            <Button variant="success" className="mr-3" onClick={handleBtnFavorite}>
              Favorite
              <FontAwesomeIcon className="ml-2" icon={faHeart} />
            </Button>
            <Button variant="warning" className="mr-3" onClick={handleBtnBlock}>
              Block
              <FontAwesomeIcon className="ml-2" icon={faUserSlash} />
            </Button>

            <Button variant="danger" className="mr-3" onClick={handleBtnDelete}>
              Delete
              <FontAwesomeIcon className="ml-2" icon={faTrash} />
            </Button>
          </div>
        </Card.Body>
      </Card>
      <InformationModalDialog
        message={messageModalDialog}
        show={showModalDialog}
        hide={() => setShowModalDialog(false)}
      />
    </div>
  );
};
