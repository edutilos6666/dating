import React, { FC, useState, useEffect, useReducer } from 'react';
import { useUpdatePersonMutation, FindPersonByIdQuery } from '../../../data/person.generated';
import { PersonCarousel } from '../Carousel/Carousel';
import { PersonProfileBody } from './Body';
import { SET_OBJECT } from './TypesAndReducer';
import { PersonProfileDataReducer, emptyInstance_PersonProfileData } from './TypesAndReducer';
import { PersonInput } from '../../../data/datatypes';
import { useFindAllGenderHook } from '../../People/listDataHooks';
import { InformationModalDialog } from '../../Utils/CustomModal/InformationModalDialog';

interface Props {
  data: FindPersonByIdQuery | undefined;
}

export const PersonProfile: FC<Props> = ({ data }) => {
  const { genders } = useFindAllGenderHook();

  const [reducerData, reducerDispatch] = useReducer(
    PersonProfileDataReducer,
    emptyInstance_PersonProfileData,
  );

  const [showModalDialog, setShowModalDialog] = useState<boolean>(false);

  useEffect(() => {
    if (data && data.findPersonById) {
      const person = data.findPersonById;
      reducerDispatch({
        type: SET_OBJECT,
        value: {
          username: person.username ? person.username : undefined,
          firstname: person.firstname ? person.firstname : undefined,
          lastname: person.lastname ? person.lastname : undefined,
          gender: person.gender && person.gender.name ? person.gender.name : undefined,
          companyName: person.job && person.job.companyName ? person.job.companyName : undefined,
          jobTitle: person.job && person.job.title ? person.job.title : undefined,
          wage: person.job && person.job.wage ? person.job.wage : undefined,
          birthdate: person.birthdate ? person.birthdate : undefined,
          country: person.address && person.address.country ? person.address.country : undefined,
          city: person.address && person.address.city ? person.address.city : undefined,
          zipcode: person.address && person.address.zipcode ? person.address.zipcode : undefined,
          email: person.email ? person.email : undefined,
          aboutMe: person.aboutMe ? person.aboutMe : undefined,
          aboutIdealPartner: person.aboutIdealPartner ? person.aboutIdealPartner : undefined,
        },
      });
    }
  }, [data]);

  const [updatePerson] = useUpdatePersonMutation();

  const getPersonInputFromReducerData = (): PersonInput | undefined => {
    let ret: PersonInput | undefined;
    if (data && data.findPersonById) {
      const person = data.findPersonById;
      ret = {
        id: parseInt(person.id),
        username: reducerData.username,
        firstname: reducerData.firstname,
        lastname: reducerData.lastname,
        gender: {
          id: genders.filter(one => one.name === reducerData.gender)[0].id,
          name: reducerData.gender,
        },
        birthdate: reducerData.birthdate,
        job: {
          id: person.job ? person.job.id : undefined,
          companyName: reducerData.companyName,
          title: reducerData.jobTitle,
          wage: reducerData.wage,
        },
        address: {
          id: person.address ? person.address.id : undefined,
          country: reducerData.country,
          city: reducerData.city,
          zipcode: reducerData.zipcode,
        },
        email: reducerData.email,
        aboutMe: reducerData.aboutMe,
        aboutIdealPartner: reducerData.aboutIdealPartner,
      };
    }

    return ret;
  };

  useEffect(() => {
    if (reducerData.triggerUpdate === undefined) return;
    updatePerson({
      variables: {
        person: getPersonInputFromReducerData(),
      },
      update: (proxy, { data: mutatedData }) => {
        setShowModalDialog(true);
      },
    });
  }, [reducerData.triggerUpdate]);

  if (data && data.findPersonById)
    return (
      <div className="d-flex flex-column offset-2 col-8">
        <PersonCarousel
          person={data.findPersonById}
          // triggerRefetch={() => setTriggerRefetchFlag(!triggerRefetchFlag)}
          currentUserProfile={true}
        />
        <PersonProfileBody reducerData={reducerData} reducerDispatch={reducerDispatch} />
        <InformationModalDialog
          message={'User was successfully updated.'}
          show={showModalDialog}
          hide={() => setShowModalDialog(false)}
        />
      </div>
    );
  return <h2></h2>;
};
