import { Maybe } from '../../../data/datatypes';

export interface PersonProfileData {
  username?: Maybe<string>;
  firstname?: Maybe<string>;
  lastname?: Maybe<string>;
  gender?: Maybe<string>;
  companyName?: Maybe<string>;
  jobTitle?: Maybe<string>;
  wage?: Maybe<number>;
  birthdate?: Maybe<Date>;
  country?: Maybe<string>;
  city?: Maybe<string>;
  zipcode?: Maybe<string>;
  email?: Maybe<string>;
  aboutMe?: Maybe<string>;
  aboutIdealPartner?: Maybe<string>;
  triggerUpdate?: Maybe<boolean>;
}

export const emptyInstance_PersonProfileData: PersonProfileData = {
  username: undefined,
  firstname: undefined,
  lastname: undefined,
  gender: undefined,
  companyName: undefined,
  jobTitle: undefined,
  wage: undefined,
  birthdate: undefined,
  country: undefined,
  city: undefined,
  zipcode: undefined,
  email: undefined,
  aboutMe: undefined,
  aboutIdealPartner: undefined,
  triggerUpdate: undefined,
};

export enum PersonProfileDataActionTypes {
  SET_USERNAME = 'setUsername',
  SET_FIRSTNAME = 'setFirstname',
  SET_LASTNAME = 'setLastname',
  SET_GENDER = 'setGender',
  SET_COMPANYNAME = 'setCompanyName',
  SET_JOBTITLE = 'setJobTitle',
  SET_WAGE = 'setWage',
  SET_BIRTHDATE = 'setBirthdate',
  SET_COUNTRY = 'setCountry',
  SET_CITY = 'setCity',
  SET_ZIPCODE = 'setZipcode',
  SET_EMAIL = 'setEmail',
  SET_ABOUTME = 'setAboutMe',
  SET_ABOUTIDEALPARTNER = 'setAboutIdealPartner',
  SET_TRIGGERUPDATE = 'setTriggerUpdate',
  SET_OBJECT = 'SET_OBJECT',
}

export const {
  SET_USERNAME,
  SET_FIRSTNAME,
  SET_LASTNAME,
  SET_GENDER,
  SET_COMPANYNAME,
  SET_JOBTITLE,
  SET_WAGE,
  SET_BIRTHDATE,
  SET_COUNTRY,
  SET_CITY,
  SET_ZIPCODE,
  SET_EMAIL,
  SET_ABOUTME,
  SET_ABOUTIDEALPARTNER,
  SET_TRIGGERUPDATE,
  SET_OBJECT,
} = PersonProfileDataActionTypes;

export type PersonProfileDataReducerAction =
  | {
      type: PersonProfileDataActionTypes.SET_WAGE;
      value: number;
    }
  | {
      type:
        | PersonProfileDataActionTypes.SET_USERNAME
        | PersonProfileDataActionTypes.SET_FIRSTNAME
        | PersonProfileDataActionTypes.SET_LASTNAME
        | PersonProfileDataActionTypes.SET_GENDER
        | PersonProfileDataActionTypes.SET_COMPANYNAME
        | PersonProfileDataActionTypes.SET_JOBTITLE
        | PersonProfileDataActionTypes.SET_COUNTRY
        | PersonProfileDataActionTypes.SET_CITY
        | PersonProfileDataActionTypes.SET_ZIPCODE
        | PersonProfileDataActionTypes.SET_EMAIL
        | PersonProfileDataActionTypes.SET_ABOUTME
        | PersonProfileDataActionTypes.SET_ABOUTIDEALPARTNER;
      value: string;
    }
  | {
      type: PersonProfileDataActionTypes.SET_BIRTHDATE;
      value: Date;
    }
  | {
      type: PersonProfileDataActionTypes.SET_TRIGGERUPDATE;
      value: boolean;
    }
  | {
      type: PersonProfileDataActionTypes.SET_OBJECT;
      value: PersonProfileData;
    };

export const PersonProfileDataReducer = (
  state: PersonProfileData,
  action: PersonProfileDataReducerAction,
): PersonProfileData => {
  let newValue;
  switch (action.type) {
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_FIRSTNAME:
      newValue = { ...state, firstname: action.value };
      break;
    case SET_LASTNAME:
      newValue = { ...state, lastname: action.value };
      break;
    case SET_GENDER:
      newValue = { ...state, gender: action.value };
      break;
    case SET_COMPANYNAME:
      newValue = { ...state, companyName: action.value };
      break;
    case SET_JOBTITLE:
      newValue = { ...state, jobTitle: action.value };
      break;
    case SET_WAGE:
      newValue = { ...state, wage: action.value };
      break;
    case SET_BIRTHDATE:
      newValue = { ...state, birthdate: action.value };
      break;
    case SET_COUNTRY:
      newValue = { ...state, country: action.value };
      break;
    case SET_CITY:
      newValue = { ...state, city: action.value };
      break;
    case SET_ZIPCODE:
      newValue = { ...state, zipcode: action.value };
      break;
    case SET_EMAIL:
      newValue = { ...state, email: action.value };
      break;
    case SET_ABOUTME:
      newValue = { ...state, aboutMe: action.value };
      break;
    case SET_ABOUTIDEALPARTNER:
      newValue = { ...state, aboutIdealPartner: action.value };
      break;
    case SET_TRIGGERUPDATE:
      newValue = { ...state, triggerUpdate: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }

  return newValue;
};
