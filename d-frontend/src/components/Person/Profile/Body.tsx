import React, { FC, useState, Dispatch } from 'react';
import { Card, Button, Row, Col, Form, FormLabel, FormControl } from 'react-bootstrap';
import { useTimeUtil } from '../../../pages/Util/useTimeUtil';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faSave } from '@fortawesome/free-solid-svg-icons';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { countryList } from '../../../pages/Util/jsonData/countryList';
import { cityList } from '../../../pages/Util/jsonData/cityList';
import SingleSelectV2 from '../../SingleSelect/SingleSelectV2';
import { SET_OBJECT, PersonProfileData, PersonProfileDataReducerAction } from './TypesAndReducer';
import ReactDatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

interface Props {
  reducerData: PersonProfileData;
  reducerDispatch: Dispatch<PersonProfileDataReducerAction>;
}

export const PersonProfileBody: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const { convertDateToUTCDate } = useTimeUtil();
  const [readonlyMode, setReadonlyMode] = useState<boolean>(true);

  const countries = countryList.countries;
  const cities = cityList.cities;

  const handleBtnEditClicked = () => {
    setReadonlyMode(false);
  };

  const validationSchema = Yup.object().shape({
    username: Yup.string().required('username is required'),
    firstname: Yup.string().required('firstname is required'),
    lastname: Yup.string().required('lastname is required'),
    // birthdate: Yup.date().required('birthdate is required'),
    companyName: Yup.string().required('company name is required'),
    jobTitle: Yup.string().required('occupation is required'),
    wage: Yup.number().required('salary is required'),
    zipcode: Yup.string().required('zipcode is required'),
    email: Yup.string().required('email is required'),
    aboutMe: Yup.string().required('about me is required'),
    aboutIdealPartner: Yup.string().required('about ideal partner is required'),
    // lastName: Yup.string().required('Last Name is required'),
    // email: Yup.string()
    //   .email('Email is invalid')
    //   .required('Email is required'),
    // password: Yup.string()
    //   .min(6, 'Password must be at least 6 characters')
    //   .required('Password is required'),
    // confirmPassword: Yup.string()
    //   .oneOf([Yup.ref('password'), null], 'Passwords must match')
    //   .required('Confirm Password is required'),
  });

  return (
    <div className="d-flex flex-column col-8 offset-2">
      <Card>
        <Card.Body>
          <Formik
            enableReinitialize
            validationSchema={validationSchema}
            initialValues={reducerData}
            onSubmit={(values, { setSubmitting }) => {
              reducerDispatch({
                type: SET_OBJECT,
                value: { ...values, triggerUpdate: !reducerData.triggerUpdate },
              });
              setReadonlyMode(true);
              setSubmitting(false);
            }}
            onReset={fields => {
              setReadonlyMode(false);
              // formikActions.setValues({});
              // reducerDispatch({ type: SET_OBJECT, value: emptyInstance_PeopleViewData });
            }}
          >
            {({
              isSubmitting,
              values,
              touched,
              handleSubmit,
              setFieldValue,
              handleChange,
              handleBlur,
              errors,
              resetForm,
              handleReset,
            }) => (
              <Form noValidate onSubmit={handleSubmit}>
                <Row className="mb-3 mt-3">
                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Username</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Username"
                        name="username"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.username || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' +
                          (errors.username && touched.username ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="username" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Firstname</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Firstname"
                        name="firstname"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.firstname || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' +
                          (errors.firstname && touched.firstname ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="firstname" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Lastname</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Lastname"
                        name="lastname"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.lastname || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' +
                          (errors.lastname && touched.lastname ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="lastname" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Gender</FormLabel>
                      <SingleSelectV2
                        selectedObject={values.gender}
                        setSelectedObject={gender => {
                          setFieldValue('gender', gender);
                        }}
                        options={['', 'Male', 'Female']}
                        getLabel={(one: string) => one}
                        getSelectedLabel={(one: string) => one}
                        disabled={readonlyMode}
                      />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Birthdate</FormLabel>
                      <ReactDatePicker
                        selected={values.birthdate}
                        onChange={(date: Date) => {
                          let value;
                          if (date !== null) value = convertDateToUTCDate(date);
                          setFieldValue('birthdate', value);
                        }}
                        placeholderText="Birthdate"
                        dateFormat="dd.MM.yyyy"
                        maxDate={moment().toDate()}
                        disabled={readonlyMode}
                      />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Company</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Company"
                        name="companyName"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.companyName || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' +
                          (errors.companyName && touched.companyName ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage
                        name="companyName"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Occupation</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Occupation"
                        name="jobTitle"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.jobTitle || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' +
                          (errors.jobTitle && touched.jobTitle ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="jobTitle" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Salary</FormLabel>
                      <FormControl
                        type="number"
                        placeholder="Salary"
                        name="wage"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.wage || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' + (errors.wage && touched.wage ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="wage" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Country</FormLabel>
                      <SingleSelectV2
                        selectedObject={values.country}
                        setSelectedObject={country => {
                          setFieldValue('country', country);
                        }}
                        options={['', ...countries]}
                        getLabel={(one: string) => one}
                        getSelectedLabel={(one: string) => one}
                        disabled={readonlyMode}
                      />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>City</FormLabel>
                      <SingleSelectV2
                        selectedObject={values.city}
                        setSelectedObject={city => {
                          setFieldValue('city', city);
                        }}
                        options={['', ...cities]}
                        getLabel={(one: string) => one}
                        getSelectedLabel={(one: string) => one}
                        disabled={readonlyMode}
                      />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Zipcode</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Zipcode"
                        name="zipcode"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.zipcode || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' + (errors.zipcode && touched.zipcode ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="zipcode" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-3">
                    <div className="d-flex flex-column">
                      <FormLabel>Email</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Email"
                        name="email"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.email || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' + (errors.email && touched.email ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="email" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-12">
                    <div className="d-flex flex-column">
                      <FormLabel>About me</FormLabel>
                      <FormControl
                        as="textarea"
                        rows="4"
                        placeholder="Write about yourself"
                        name="aboutMe"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.aboutMe || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' + (errors.aboutMe && touched.aboutMe ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="aboutMe" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-12">
                    <div className="d-flex flex-column">
                      <FormLabel>About ideal partner</FormLabel>
                      <FormControl
                        as="textarea"
                        rows="4"
                        placeholder="Write about your ideal partner"
                        name="aboutIdealPartner"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.aboutIdealPartner || ''}`}
                        readOnly={readonlyMode}
                        className={
                          'form-control' +
                          (errors.aboutIdealPartner && touched.aboutIdealPartner
                            ? ' is-invalid'
                            : '')
                        }
                      />
                      <ErrorMessage
                        name="aboutIdealPartner"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                  </Col>
                </Row>

                <div className="d-flex flex-row" style={{ float: 'right' }}>
                  {readonlyMode ? (
                    <Button
                      // type="reset"
                      variant="primary"
                      className="mr-3"
                      onClick={handleBtnEditClicked}
                    >
                      Edit Infos
                      <FontAwesomeIcon className="ml-2" icon={faEdit} />
                    </Button>
                  ) : (
                    <Button variant="primary" className="mr-3" onClick={() => handleSubmit()}>
                      Save Changes
                      <FontAwesomeIcon className="ml-2" icon={faSave} />
                    </Button>
                  )}
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};
