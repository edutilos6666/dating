import React, { FC } from 'react';
import { Card } from 'react-bootstrap';
import PersonOverviewBody from './Body';
import { PersonOverviewFooter } from './Footer';
import { useAvatarUtil } from '../../../pages/Util/useAvatarUtil';
import { Person } from '../../../data/datatypes';
import { HBox } from '../../HBox/HBox';
import { ImageFC } from '../../HBox/ImageFC';

interface Props {
  person: Person;
}
const PersonOverview: FC<Props> = ({ person }) => {
  const { imageBase64 } = useAvatarUtil(person);
  return (
    <Card className="col-12">
      <Card.Body>
        <Card.Title>{person.username} Overview</Card.Title>
        <div>
          <HBox
            elements={[
              {
                key: 'col-3',
                value: <ImageFC src={imageBase64} className="mt-2 pr-2" width="60" height="60" />,
              },
              {
                key: 'col-9',
                value: <PersonOverviewBody person={person} />,
              },
            ]}
          />

          <HBox
            elements={[
              {
                key: 'mt-5',
                value: <PersonOverviewFooter person={person} />,
              },
            ]}
          />
        </div>
      </Card.Body>
    </Card>
  );
};

export default PersonOverview;
