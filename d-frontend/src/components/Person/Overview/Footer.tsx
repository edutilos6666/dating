import React, { FC, useContext, useState } from 'react';
import { Button } from 'react-bootstrap';
import { HistoryContext } from '../../../context/HistoryContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAddressCard,
  faEnvelope,
  faHeart,
  faUserSlash,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import { useChatUtil } from '../../../pages/Util/useChatUtil';
import { Person } from '../../../data/datatypes';
import { useFunctionHelper, UpdateRelCommand } from '../MessageBox/useFunctionHelper';
import { InformationModalDialog } from '../../Utils/CustomModal/InformationModalDialog';

interface Props {
  person: Person;
}

type BtnFavoriteOrBlockOrDeleteActionType = 'addFavorite' | 'addBlock' | 'addDelete';
export const PersonOverviewFooter: FC<Props> = ({ person }) => {
  const { history } = useContext(HistoryContext);
  const { establishChatChannel } = useChatUtil();
  const { handleUpdatePersonRelByUserToId } = useFunctionHelper();
  const [showModalDialog, setShowModalDialog] = useState<boolean>(false);
  const [messageModalDialog, setMessageModalDialog] = useState<string>('');
  const handleBtnDetailsClicked = () => {
    history.push(`/person-details/${person.id}`, {
      person: person,
    });
  };

  const handleBtnMessageClicked = () => {
    // history.push('/messaging');
    establishChatChannel(history, parseInt(person.id));
  };

  const callbackForUpdatePersonRel = (message: string) => {
    setMessageModalDialog(message);
    setShowModalDialog(true);
  };

  const handleBtnFavorite = () => {
    if (person && person.id)
      handleUpdatePersonRelByUserToId(person.id, UpdateRelCommand.FAVORITE, () => {
        callbackForUpdatePersonRel(`User ${person.id} was added to favorite list.`);
      });
  };

  const handleBtnBlock = () => {
    if (person && person.id)
      handleUpdatePersonRelByUserToId(person.id, UpdateRelCommand.BLOCK, () => {
        callbackForUpdatePersonRel(`User ${person.id} was added to blocked list.`);
      });
  };

  const handleBtnDelete = () => {
    if (person && person.id)
      handleUpdatePersonRelByUserToId(person.id, UpdateRelCommand.DELETE, () => {
        callbackForUpdatePersonRel(`User ${person.id} was added to deleted list.`);
      });
  };

  return (
    <div className="d-flex flex-row">
      <Button variant="info" className="mr-3 pl-4 pr-4" onClick={handleBtnDetailsClicked}>
        {/* Details */}
        <FontAwesomeIcon className="ml-2" icon={faAddressCard} />
      </Button>
      <Button variant="primary" className="mr-3 pl-4 pr-4" onClick={handleBtnMessageClicked}>
        {/* Message */}
        <FontAwesomeIcon className="ml-2" icon={faEnvelope} />
      </Button>
      <Button variant="success" className="mr-3 pl-4 pr-4" onClick={handleBtnFavorite}>
        {/* Favorite */}
        <FontAwesomeIcon className="ml-2" icon={faHeart} />
      </Button>
      <Button variant="warning" className="mr-3 pl-4 pr-4" onClick={handleBtnBlock}>
        {/* Block */}
        <FontAwesomeIcon className="ml-2" icon={faUserSlash} />
      </Button>

      <Button variant="danger" className="mr-3 pl-4 pr-4" onClick={handleBtnDelete}>
        {/* Delete */}
        <FontAwesomeIcon className="ml-2" icon={faTrash} />
      </Button>

      <InformationModalDialog
        message={messageModalDialog}
        show={showModalDialog}
        hide={() => setShowModalDialog(false)}
      />
    </div>
  );
};
