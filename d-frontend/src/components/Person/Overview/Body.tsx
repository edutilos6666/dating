import React, { FC } from 'react';
import { Container } from 'react-bootstrap';
import AddressOverview from '../../Address/AddressOverview/AddressOverview';
import { useTimeUtil } from '../../../pages/Util/useTimeUtil';
import { Person, Maybe } from '../../../data/datatypes';
import { HBox } from '../../HBox/HBox';
import { SpanFC } from '../../HBox/SpanFC';

interface Props {
  person: Person;
  messageCreated?: Maybe<number>;
}
const PersonOverviewBody: FC<Props> = ({ person, messageCreated }) => {
  const { convertTimestampToDateString } = useTimeUtil();
  return (
    <Container fluid>
      <HBox
        boldKey={{ value: 'name' }}
        elements={[{ value: <SpanFC content={`${person.firstname} ${person.lastname}`} /> }]}
      />
      <HBox
        boldKey={{ value: 'birthdate' }}
        elements={[
          { value: <SpanFC content={convertTimestampToDateString(String(person.birthdate))} /> },
        ]}
      />
      <HBox
        boldKey={{ value: 'address' }}
        elements={[{ value: <AddressOverview address={person.address} /> }]}
      />
      {messageCreated ? (
        <HBox
          boldKey={{ value: 'created' }}
          elements={[
            { value: <SpanFC content={convertTimestampToDateString(messageCreated + '')} /> },
          ]}
        />
      ) : (
        ''
      )}
    </Container>
  );
};

export default PersonOverviewBody;
