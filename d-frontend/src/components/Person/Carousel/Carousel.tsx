import React, { FC, useState, useRef, useEffect, useReducer, Dispatch } from 'react';
import { Carousel } from 'react-bootstrap';
import { faThumbsUp, faHeart, faThumbsDown } from '@fortawesome/free-solid-svg-icons';
import { ButtonWithBadge } from '../../ButtonWithBadge/ButtonWithBadge';
import { PersonFragment, useUpdatePersonMutation } from '../../../data/person.generated';
import { MALE_AVATAR_PATH } from '../../../pages/Util/Constants';
import { constructPersonCarouselItemProps } from '../../../pages/Util/ImageUtil';
import useBasePath from '../../../hooks/useBasePath';
import './Carousel.css';
import { PersonImage } from '../../../data/datatypes';
import { useUpdatePersonImageMutation } from '../../../data/personImage.generated';
import useAppController from '../../../hooks/useAppController';

interface Props {
  person: PersonFragment;
  triggerRefetch?: () => void;
  currentUserProfile?: boolean;
}

export interface PersonCarouselItemProps {
  src: string;
  alt: string;
  header: any;
  footer: any;
  likeCount: number;
  disllikeCount: number;
  heartCount: number;
}

enum ClickType {
  LIKE,
  DISLIKE,
  HEART,
}

export const PersonCarousel: FC<Props> = ({ person, triggerRefetch, currentUserProfile }) => {
  const { baseUrl } = useBasePath();
  const { userinfo } = useAppController();
  const [carouselItemValues, setCarouselItemValues] = useState<PersonCarouselItemProps[]>([
    {
      src: MALE_AVATAR_PATH,
      alt: 'male avatar',
      header: '',
      footer: '',
      likeCount: 0,
      disllikeCount: 0,
      heartCount: 0,
    },
  ]);

  useEffect(() => {
    if (person && person.images && person.images.length > 0) {
      constructPersonCarouselItemProps(baseUrl!, person.images).then(res => {
        setCarouselItemValues(res);
      });
    }
  }, [person]);

  const [index, setIndex] = useState<number>(0);

  const handleSelect = (selectedIndex: number) => {
    setIndex(selectedIndex);
  };

  const carouselRef = useRef<Carousel<'div'> & HTMLDivElement>(null);

  const [updatePersonImage] = useUpdatePersonImageMutation();
  const [updatePerson] = useUpdatePersonMutation();
  // const [reducerData, reducerDispatch] = useReducer(PeopleViewDataReducer, {});

  const handleGeneralClick = (clickType: ClickType) => {
    const personImage: PersonImage = person.images![index];
    const currentPersonId = parseInt(userinfo && userinfo.username ? userinfo.username : '');
    let likes = personImage.likes ? personImage.likes.map(one => parseInt(one.id)) : [];
    let dislikes = personImage.dislikes ? personImage.dislikes.map(one => parseInt(one.id)) : [];
    let hearts = personImage.hearts ? personImage.hearts.map(one => parseInt(one.id)) : [];

    switch (clickType) {
      case ClickType.LIKE:
        if (likes.indexOf(currentPersonId) < 0) {
          likes = [...likes, currentPersonId];
        } else {
          likes = likes.filter(one => one !== currentPersonId);
        }
        dislikes = dislikes.filter(one => one !== currentPersonId);
        hearts = hearts.filter(one => one !== currentPersonId);
        break;
      case ClickType.DISLIKE:
        if (dislikes.indexOf(currentPersonId) < 0) {
          dislikes = [...dislikes, currentPersonId];
        } else {
          dislikes = dislikes.filter(one => one !== currentPersonId);
        }
        likes = likes.filter(one => one !== currentPersonId);
        hearts = hearts.filter(one => one !== currentPersonId);

        break;
      case ClickType.HEART:
        if (hearts.indexOf(currentPersonId) < 0) {
          hearts = [...hearts, currentPersonId];
        } else {
          hearts = hearts.filter(one => one !== currentPersonId);
        }
        likes = likes.filter(one => one !== currentPersonId);
        dislikes = dislikes.filter(one => one !== currentPersonId);
    }

    updatePersonImage({
      variables: {
        id: parseInt(personImage.id),
        likes: likes,
        dislikes: dislikes,
        hearts: hearts,
      },
      update: (proxy, { data: mutatedData }) => {
        const personToUpdate = {
          ...person,
          images: mutatedData,
        };

        triggerRefetch && triggerRefetch();
      },
    });
  };

  return (
    <div className="d-flex flex-column col-8 offset-2">
      {carouselItemValues && carouselItemValues.length > 0 ? (
        <>
          <Carousel ref={carouselRef} interval={null} onSelect={handleSelect}>
            {carouselItemValues.map(({ src, alt, header, footer }, i) => (
              <Carousel.Item key={i}>
                <img
                  className="d-block w-100"
                  src={src}
                  alt={alt}
                  width="200"
                  height="400"
                  style={{ zIndex: -1000 }}
                />
                <Carousel.Caption>
                  <h3 color="black">{header}</h3>
                  <p color="black">{footer}</p>
                </Carousel.Caption>
              </Carousel.Item>
            ))}
          </Carousel>
          {!currentUserProfile ? (
            <div className="d-flex flex-row mt-2 mb-2 offset-4">
              <ButtonWithBadge
                iconName={faThumbsUp}
                btnVariant="outline-primary"
                badgeVariant="primary"
                value={carouselItemValues[index].likeCount}
                handleClick={() => handleGeneralClick(ClickType.LIKE)}
              />

              <ButtonWithBadge
                iconName={faThumbsDown}
                btnVariant="outline-primary"
                badgeVariant="primary"
                value={carouselItemValues[index].disllikeCount}
                handleClick={() => handleGeneralClick(ClickType.DISLIKE)}
              />

              <ButtonWithBadge
                iconName={faHeart}
                btnVariant="outline-success"
                badgeVariant="success"
                value={carouselItemValues[index].heartCount}
                handleClick={() => handleGeneralClick(ClickType.HEART)}
              />
            </div>
          ) : (
            ''
          )}
        </>
      ) : (
        ''
      )}
    </div>
  );
};

PersonCarousel.defaultProps = {
  currentUserProfile: false,
};
