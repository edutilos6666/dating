import React, { FC, Dispatch, useContext } from 'react';
import {
  HomeData,
  HomeDataReducerAction,
  SET_NEWMSGDELETED,
  SET_MSGDELETEDCOUNT,
} from '../../../../pages/Home/TypesAndReducer';
import { GenericTab } from '../GenericTab/GenericTab';
import useAppController from '../../../../hooks/useAppController';
import useDataTableWithPagination from '../../../../hooks/useDataTableWithPagination';
import { HistoryContext } from '../../../../context/HistoryContext';
import { FooterButtonInterface } from '../GenericTab/Item/Item';
import { faHeart, faUserSlash, faTrash, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { useFindChannelsFromDeletedListForPersonQuery } from '../../../../data/privateChannel.generated';
import { useFunctionHelper, UpdateRelCommand } from '../useFunctionHelper';

interface Props {
  reducerData: HomeData;
  reducerDispatch: Dispatch<HomeDataReducerAction>;
}

export const DeletedTab: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const { userinfo } = useAppController();
  const { history } = useContext(HistoryContext);

  const {
    data,
    refetch,
    loading,
    variables,
    fetchMore,
  } = useFindChannelsFromDeletedListForPersonQuery({
    variables: {
      userToId: parseInt(userinfo!.username!),
      pagination: { offsetPage: 0, pageSize: 2 },
    },
    fetchPolicy: 'network-only',
    skip: !userinfo || !userinfo.username,
  });

  const paginationHelper = useDataTableWithPagination(
    variables,
    data ? data.findChannelsFromDeletedListForPerson : null,
    fetchMore,
  );

  const { handleUpdatePersonRel } = useFunctionHelper(reducerData, reducerDispatch);

  const btnFavorite: FooterButtonInterface = {
    variant: 'success',
    text: 'Add to favorite',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.FAVORITE);
    },
    icon: faHeart,
  };

  const btnBlock: FooterButtonInterface = {
    variant: 'warning',
    text: 'Block user',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.BLOCK);
    },
    icon: faUserSlash,
  };

  const btnDelete: FooterButtonInterface = {
    variant: 'outline-danger',
    text: 'Undelete user',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.UNDO_ALL);
    },
    icon: faTrash,
  };

  const btnOpenChatHistory: FooterButtonInterface = {
    variant: 'info',
    text: 'Open chat history',
    handler: (_, channel) => {
      channel && history.push(`/chat-history/${channel.id}`);
    },
    icon: faEnvelope,
  };

  return (
    <GenericTab
      reducerData={reducerData}
      reducerDispatch={reducerDispatch}
      queryResult={{ refetch, loading }}
      paginationHelper={paginationHelper}
      listData={
        data && data.findChannelsFromDeletedListForPerson
          ? data.findChannelsFromDeletedListForPerson.result
          : null
      }
      reducerActionType1={SET_NEWMSGDELETED}
      reducerActionType2={SET_MSGDELETEDCOUNT}
      buttons={[btnFavorite, btnBlock, btnDelete, btnOpenChatHistory]}
    />
  );
};
