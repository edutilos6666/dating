import React, { FC, Dispatch, useContext } from 'react';
import {
  HomeData,
  HomeDataReducerAction,
  SET_NEWMSGFAVORITE,
  SET_MSGFAVORITECOUNT,
} from '../../../../pages/Home/TypesAndReducer';
import { GenericTab } from '../GenericTab/GenericTab';
import useAppController from '../../../../hooks/useAppController';
import useDataTableWithPagination from '../../../../hooks/useDataTableWithPagination';
import { HistoryContext } from '../../../../context/HistoryContext';
import { FooterButtonInterface } from '../GenericTab/Item/Item';
import {
  faHeart,
  faUserSlash,
  faTrash,
  faEnvelope,
  faAddressCard,
} from '@fortawesome/free-solid-svg-icons';
import { useFindChannelsFromFavoriteListForPersonQuery } from '../../../../data/privateChannel.generated';
import { useFunctionHelper, UpdateRelCommand } from '../useFunctionHelper';

interface Props {
  reducerData: HomeData;
  reducerDispatch: Dispatch<HomeDataReducerAction>;
}

export const FavoriteTab: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const { userinfo } = useAppController();
  const { history } = useContext(HistoryContext);

  const {
    data,
    refetch,
    loading,
    variables,
    fetchMore,
  } = useFindChannelsFromFavoriteListForPersonQuery({
    variables: {
      userToId: parseInt(userinfo!.username!),
      pagination: { offsetPage: 0, pageSize: 2 },
    },
    fetchPolicy: 'network-only',
    skip: !userinfo || !userinfo.username,
  });

  const paginationHelper = useDataTableWithPagination(
    variables,
    data ? data.findChannelsFromFavoriteListForPerson : null,
    fetchMore,
  );

  const { handleUpdatePersonRel } = useFunctionHelper(reducerData, reducerDispatch);

  const btnDetails: FooterButtonInterface = {
    variant: 'info',
    text: 'User Details',
    handler: (_, channel) => {
      if (channel && channel.userFrom && channel.userTo && userinfo) {
        if (channel.userFrom.id !== userinfo.username) {
          history.push(`/person-details/${channel.userFrom.id}`);
        } else {
          history.push(`/person-details/${channel.userTo.id}`);
        }
      }
    },
    icon: faAddressCard,
  };

  const btnFavorite: FooterButtonInterface = {
    variant: 'outline-success',
    text: 'Remove from favorite',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.UNDO_ALL);
    },
    icon: faHeart,
  };

  const btnBlock: FooterButtonInterface = {
    variant: 'warning',
    text: 'Block user',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.BLOCK);
    },
    icon: faUserSlash,
  };

  const btnDelete: FooterButtonInterface = {
    variant: 'danger',
    text: 'Delete user',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.DELETE);
    },
    icon: faTrash,
  };

  const btnOpenChatHistory: FooterButtonInterface = {
    variant: 'info',
    text: 'Open chat history',
    handler: (_, channel) => {
      channel && history.push(`/chat-history/${channel.id}`);
    },
    icon: faEnvelope,
  };

  return (
    <GenericTab
      reducerData={reducerData}
      reducerDispatch={reducerDispatch}
      queryResult={{ refetch, loading }}
      paginationHelper={paginationHelper}
      listData={
        data && data.findChannelsFromFavoriteListForPerson
          ? data.findChannelsFromFavoriteListForPerson.result
          : null
      }
      reducerActionType1={SET_NEWMSGFAVORITE}
      reducerActionType2={SET_MSGFAVORITECOUNT}
      buttons={[btnDetails, btnFavorite, btnBlock, btnDelete, btnOpenChatHistory]}
    />
  );
};
