import { PrivateChannel, Maybe } from '../../../data/datatypes';
import useAppController from '../../../hooks/useAppController';
import { useUpdatePersonRelMutation } from '../../../data/person.generated';
import {
  HomeData,
  HomeDataReducerAction,
  SET_REFETCHCHANNELS,
} from '../../../pages/Home/TypesAndReducer';
import { Dispatch } from 'react';

export interface UpdatePersonRelBooleans {
  addFavorite?: Maybe<boolean>;
  addBlock?: Maybe<boolean>;
  addDelete?: Maybe<boolean>;
  removeFavorite?: Maybe<boolean>;
  removeBlock?: Maybe<boolean>;
  removeDelete?: Maybe<boolean>;
}

export enum UpdateRelCommand {
  FAVORITE,
  BLOCK,
  DELETE,
  UNDO_ALL,
}

export const useFunctionHelper = (
  reducerData?: HomeData,
  reducerDispatch?: Dispatch<HomeDataReducerAction>,
) => {
  const { userinfo } = useAppController();
  const [updatePersonRel] = useUpdatePersonRelMutation();
  const getUserIdToUpdate = (idLoggedIn: string, channel: PrivateChannel): string => {
    let idToUpdate = '';
    if (channel.userFrom && channel.userFrom.id !== idLoggedIn) idToUpdate = channel.userFrom.id;
    else if (channel.userTo && channel.userTo.id !== idLoggedIn) idToUpdate = channel.userTo.id;
    return idToUpdate;
  };

  const handleUpdatePersonRel = (
    channel: Maybe<PrivateChannel>,
    command: UpdateRelCommand,
    callback?: () => void,
  ) => {
    if (userinfo && channel && channel.userFrom && channel.userTo) {
      const idToUpdate = getUserIdToUpdate(userinfo.username!, channel);
      handleUpdatePersonRelByUserToId(idToUpdate, command, callback);
    }
  };

  const handleUpdatePersonRelByUserToId = (
    userToId: string,
    command: UpdateRelCommand,
    callback?: () => void,
  ) => {
    if (userinfo && userinfo.username) {
      const idLoggedIn = userinfo.username;

      updatePersonRel({
        variables: {
          id: parseInt(idLoggedIn),
          idToUpdate: parseInt(userToId),
          command: command,
        },
        update: (proxy, { data: mutatedData }) => {
          if (reducerDispatch && reducerData)
            reducerDispatch({
              type: SET_REFETCHCHANNELS,
              value: !reducerData.refetchChannels,
            });

          if (callback) callback();
        },
      });
    }
  };

  return {
    getUserIdToUpdate,
    handleUpdatePersonRel,
    handleUpdatePersonRelByUserToId,
  };
};
