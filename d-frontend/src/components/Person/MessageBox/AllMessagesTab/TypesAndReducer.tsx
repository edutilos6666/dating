import { Maybe } from '../../../../data/datatypes';

export interface AllMessagesTabReducerData {
  content?: Maybe<string>;
  createdFrom?: Maybe<Date>;
  createdTo?: Maybe<Date>;
  opened?: Maybe<boolean>;
  username?: Maybe<string>;
  firstname?: Maybe<string>;
  lastname?: Maybe<string>;
  minBirthdate?: Maybe<Date>;
  maxBirthdate?: Maybe<Date>;
  wageFrom?: Maybe<number>;
  wageTo?: Maybe<number>;
  country?: Maybe<string>;
  city?: Maybe<string>;
  zipcode?: Maybe<string>;
}

export const emptyInstance_AllMessagesTabReducerData: AllMessagesTabReducerData = {
  content: undefined,
  createdFrom: undefined,
  createdTo: undefined,
  opened: undefined,
  username: undefined,
  firstname: undefined,
  lastname: undefined,
  minBirthdate: undefined,
  maxBirthdate: undefined,
  wageFrom: undefined,
  wageTo: undefined,
  country: undefined,
  city: undefined,
  zipcode: undefined,
};

export enum AllMessagesTabReducerDataActionTypes {
  SET_CONTENT = 'setContent',
  SET_CREATEDFROM = 'setCreatedFrom',
  SET_CREATEDTO = 'setCreatedTo',
  SET_OPENED = 'setOpened',
  SET_USERNAME = 'setUsername',
  SET_FIRSTNAME = 'setFirstname',
  SET_LASTNAME = 'setLastname',
  SET_MINBIRTHDATE = 'setMinBirthdate',
  SET_MAXBIRTHDATE = 'setMaxBirthdate',
  SET_WAGEFROM = 'setWageFrom',
  SET_WAGETO = 'setWageTo',
  SET_COUNTRY = 'setCountry',
  SET_CITY = 'setCity',
  SET_ZIPCODE = 'setZipcode',
  SET_OBJECT = 'SET_OBJECT',
}

export const {
  SET_CONTENT,
  SET_CREATEDFROM,
  SET_CREATEDTO,
  SET_OPENED,
  SET_USERNAME,
  SET_FIRSTNAME,
  SET_LASTNAME,
  SET_MINBIRTHDATE,
  SET_MAXBIRTHDATE,
  SET_WAGEFROM,
  SET_WAGETO,
  SET_COUNTRY,
  SET_CITY,
  SET_ZIPCODE,
  SET_OBJECT,
} = AllMessagesTabReducerDataActionTypes;

export type AllMessagesTabReducerDataAction =
  | {
      type:
        | AllMessagesTabReducerDataActionTypes.SET_CONTENT
        | AllMessagesTabReducerDataActionTypes.SET_USERNAME
        | AllMessagesTabReducerDataActionTypes.SET_FIRSTNAME
        | AllMessagesTabReducerDataActionTypes.SET_LASTNAME
        | AllMessagesTabReducerDataActionTypes.SET_COUNTRY
        | AllMessagesTabReducerDataActionTypes.SET_CITY
        | AllMessagesTabReducerDataActionTypes.SET_ZIPCODE;
      value: string;
    }
  | {
      type:
        | AllMessagesTabReducerDataActionTypes.SET_CREATEDFROM
        | AllMessagesTabReducerDataActionTypes.SET_CREATEDTO
        | AllMessagesTabReducerDataActionTypes.SET_MINBIRTHDATE
        | AllMessagesTabReducerDataActionTypes.SET_MAXBIRTHDATE;
      value: Date;
    }
  | {
      type: AllMessagesTabReducerDataActionTypes.SET_OPENED;
      value: boolean;
    }
  | {
      type:
        | AllMessagesTabReducerDataActionTypes.SET_WAGEFROM
        | AllMessagesTabReducerDataActionTypes.SET_WAGETO;
      value: number;
    }
  | {
      type: AllMessagesTabReducerDataActionTypes.SET_OBJECT;
      value: AllMessagesTabReducerData;
    };

export const AllMessagesTabReducerDataReducer = (
  state: AllMessagesTabReducerData,
  action: AllMessagesTabReducerDataAction,
): AllMessagesTabReducerData => {
  let newValue;
  switch (action.type) {
    case SET_CONTENT:
      newValue = { ...state, content: action.value };
      break;
    case SET_CREATEDFROM:
      newValue = { ...state, createdFrom: action.value };
      break;
    case SET_CREATEDTO:
      newValue = { ...state, createdTo: action.value };
      break;
    case SET_OPENED:
      newValue = { ...state, opened: action.value };
      break;
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_FIRSTNAME:
      newValue = { ...state, firstname: action.value };
      break;
    case SET_LASTNAME:
      newValue = { ...state, lastname: action.value };
      break;
    case SET_MINBIRTHDATE:
      newValue = { ...state, minBirthdate: action.value };
      break;
    case SET_MAXBIRTHDATE:
      newValue = { ...state, maxBirthdate: action.value };
      break;
    case SET_WAGEFROM:
      newValue = { ...state, wageFrom: action.value };
      break;
    case SET_WAGETO:
      newValue = { ...state, wageTo: action.value };
      break;
    case SET_COUNTRY:
      newValue = { ...state, country: action.value };
      break;
    case SET_CITY:
      newValue = { ...state, city: action.value };
      break;
    case SET_ZIPCODE:
      newValue = { ...state, zipcode: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }
  return newValue;
};
