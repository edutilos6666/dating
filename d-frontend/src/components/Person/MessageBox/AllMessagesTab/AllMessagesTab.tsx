import React, { FC, Dispatch, useContext, useReducer } from 'react';
import {
  HomeData,
  HomeDataReducerAction,
  SET_NEWMSGNORMAL,
  SET_MSGNORMALCOUNT,
} from '../../../../pages/Home/TypesAndReducer';
import { GenericTab } from '../GenericTab/GenericTab';
import useAppController from '../../../../hooks/useAppController';
import useDataTableWithPagination from '../../../../hooks/useDataTableWithPagination';
import { HistoryContext } from '../../../../context/HistoryContext';
import { FooterButtonInterface } from '../GenericTab/Item/Item';
import { faEnvelope, faAddressCard } from '@fortawesome/free-solid-svg-icons';
import { useFunctionHelper, UpdateRelCommand } from '../useFunctionHelper';
import { useFilterMessagesByQuery } from '../../../../data/privateMessage.generated';
import {
  AllMessagesTabReducerDataReducer,
  emptyInstance_AllMessagesTabReducerData,
} from './TypesAndReducer';
import { AllMessagesTabFilter } from './Filter';
import { useTimeUtil } from '../../../../pages/Util/useTimeUtil';

interface Props {
  reducerData: HomeData;
  reducerDispatch: Dispatch<HomeDataReducerAction>;
}

export const AllMessagesTab: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const { userinfo } = useAppController();
  const { history } = useContext(HistoryContext);
  const { calculateBirthdateFromAge, updateAndGetDate } = useTimeUtil();

  const [reducerDataAllMessagesTab, reducerDispatchAllMessagesTab] = useReducer(
    AllMessagesTabReducerDataReducer,
    emptyInstance_AllMessagesTabReducerData,
  );

  const { data, refetch, loading, variables, fetchMore } = useFilterMessagesByQuery({
    variables: {
      content: reducerDataAllMessagesTab.content,
      createdFrom: updateAndGetDate(reducerDataAllMessagesTab.createdFrom!),
      createdTo: updateAndGetDate(reducerDataAllMessagesTab.createdTo!, false),
      opened: reducerDataAllMessagesTab.opened,
      username: reducerDataAllMessagesTab.username,
      firstname: reducerDataAllMessagesTab.firstname,
      lastname: reducerDataAllMessagesTab.lastname,
      minBirthdate: updateAndGetDate(reducerDataAllMessagesTab.minBirthdate!),
      maxBirthdate: updateAndGetDate(reducerDataAllMessagesTab.maxBirthdate!, false),
      wageFrom: reducerDataAllMessagesTab.wageFrom,
      wageTo: reducerDataAllMessagesTab.wageTo,
      country: reducerDataAllMessagesTab.country,
      city: reducerDataAllMessagesTab.city,
      zipcode: reducerDataAllMessagesTab.zipcode,
      pagination: { offsetPage: 0, pageSize: 2 },
    },
    fetchPolicy: 'network-only',
    skip: !userinfo || !userinfo.username,
  });

  const paginationHelper = useDataTableWithPagination(
    variables,
    data ? data.filterMessagesBy : null,
    fetchMore,
  );

  const { handleUpdatePersonRel } = useFunctionHelper(reducerData, reducerDispatch);

  const btnDetails: FooterButtonInterface = {
    variant: 'info',
    text: 'User Details',
    handler: (_, channel) => {
      if (channel && channel.userFrom && channel.userTo && userinfo) {
        if (channel.userFrom.id !== userinfo.username) {
          history.push(`/person-details/${channel.userFrom.id}`);
        } else {
          history.push(`/person-details/${channel.userTo.id}`);
        }
      }
    },
    icon: faAddressCard,
  };

  const btnOpenChatHistory: FooterButtonInterface = {
    variant: 'info',
    text: 'Open chat history',
    handler: (_, channel) => {
      channel && history.push(`/chat-history/${channel.id}`);
    },
    icon: faEnvelope,
  };

  return (
    <>
      <AllMessagesTabFilter
        reducerData={reducerDataAllMessagesTab}
        reducerDispatch={reducerDispatchAllMessagesTab}
      />
      <GenericTab
        reducerData={reducerData}
        reducerDispatch={reducerDispatch}
        queryResult={{ refetch, loading }}
        paginationHelper={paginationHelper}
        listData={data && data.filterMessagesBy ? data.filterMessagesBy.result : null}
        reducerActionType1={SET_NEWMSGNORMAL}
        reducerActionType2={SET_MSGNORMALCOUNT}
        buttons={[btnDetails, btnOpenChatHistory]}
      />
    </>
  );
};
