import React, { FC, Dispatch } from 'react';
import {
  AllMessagesTabReducerData,
  AllMessagesTabReducerDataAction,
  SET_OBJECT,
  emptyInstance_AllMessagesTabReducerData,
} from './TypesAndReducer';
import { JobWithIndex } from '../../../../models/JobWithIndex';
import { jobTitleList } from '../../../../pages/Util/jsonData/jobTitleList';
import { countryList } from '../../../../pages/Util/jsonData/countryList';
import { cityList } from '../../../../pages/Util/jsonData/cityList';
import { Container, Row, Col, FormLabel, FormControl, Form, Button } from 'react-bootstrap';
import { Formik, FormikValues, FormikActions } from 'formik';
import SelectDayInputRangeV3 from '../../../SelectDayInputRangeV3/SelectDayInputRangeV3';
import SingleSelectV2 from '../../../SingleSelect/SingleSelectV2';
import { FormControlMinMaxV2 } from '../../../FormControlMinMax/FormControlMinMaxV2';

interface Props {
  reducerData: AllMessagesTabReducerData;
  reducerDispatch: Dispatch<AllMessagesTabReducerDataAction>;
}

export const AllMessagesTabFilter: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const jobTitles: JobWithIndex[] = jobTitleList.jobs.map((one, i) => {
    return {
      index: i,
      name: one,
    };
  });

  const countries = countryList.countries;
  const cities = cityList.cities;

  return (
    <Container fluid>
      <Formik
        enableReinitialize
        validationSchema={false}
        initialValues={reducerData}
        onSubmit={(values, { setFieldError, setStatus, setSubmitting, setFieldTouched }) => {
          reducerDispatch({
            type: SET_OBJECT,
            value: { ...values },
          });

          setSubmitting(false);
        }}
        onReset={(values: FormikValues, formikActions: FormikActions<FormikValues>) => {
          formikActions.setValues({});
          reducerDispatch({ type: SET_OBJECT, value: emptyInstance_AllMessagesTabReducerData });
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Row className="mb-3 mt-3">
              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Message Content</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Content"
                    name="content"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.content || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Created Date</FormLabel>
                  <SelectDayInputRangeV3
                    from={values.createdFrom!}
                    handleFromChange={(value: Date | undefined) =>
                      setFieldValue('createdFrom', value)
                    }
                    to={values.createdTo!}
                    handleToChange={(value: Date | undefined) => setFieldValue('createdTo', value)}
                  />
                </div>
              </Col>

              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Messgage Opened</FormLabel>
                  <SingleSelectV2
                    selectedObject={values.opened}
                    setSelectedObject={opened => {
                      setFieldValue('opened', opened);
                    }}
                    options={['', 'true', 'false']}
                    getLabel={(one: string) => one}
                    getSelectedLabel={(one: string) => one}
                  />
                </div>
              </Col>
            </Row>
            <Row className="mb-3 mt-3">
              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Username</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Username"
                    name="username"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.username || ''}`}
                  />
                </div>
              </Col>
              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Firstname</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Firstname"
                    name="firstname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.firstname || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Lastname</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Lastname"
                    name="lastname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.lastname || ''}`}
                  />
                </div>
              </Col>
            </Row>

            <Row className="mb-3 mt-3">
              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Birthdate</FormLabel>
                  <SelectDayInputRangeV3
                    from={values.minBirthdate!}
                    handleFromChange={(value: Date | undefined) =>
                      setFieldValue('minBirthdate', value)
                    }
                    to={values.maxBirthdate!}
                    handleToChange={(value: Date | undefined) =>
                      setFieldValue('maxBirthdate', value)
                    }
                  />
                </div>
              </Col>

              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormControlMinMaxV2
                    label="Wage"
                    onMinChange={e => {
                      setFieldValue(
                        'wageFrom',
                        e.currentTarget && e.currentTarget.value ? e.currentTarget.value : null,
                      );
                    }}
                    onMaxChange={e => {
                      setFieldValue(
                        'wageTo',
                        e.currentTarget && e.currentTarget.value ? e.currentTarget.value : null,
                      );
                    }}
                    minPlaceholder="Wage from"
                    minValue={`${values.wageFrom || ''}`}
                    maxPlaceholder="Wage to"
                    maxValue={`${values.wageTo || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Country</FormLabel>
                  <SingleSelectV2
                    selectedObject={values.country}
                    setSelectedObject={country => {
                      setFieldValue('country', country);
                    }}
                    options={['', ...countries]}
                    getLabel={(one: string) => one}
                    getSelectedLabel={(one: string) => one}
                  />
                </div>
              </Col>
            </Row>

            <Row className="mb-3 mt-3">
              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>City</FormLabel>
                  <SingleSelectV2
                    selectedObject={values.city}
                    setSelectedObject={city => {
                      setFieldValue('city', city);
                    }}
                    options={['', ...cities]}
                    getLabel={(one: string) => one}
                    getSelectedLabel={(one: string) => one}
                  />
                </div>
              </Col>

              <Col className="col-4">
                <div className="d-flex flex-column">
                  <FormLabel>Zipcode</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Zipcode"
                    name="zipcode"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.zipcode || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-2">
                <Button
                  type="submit"
                  variant="primary"
                  className="w-100"
                  style={{ marginTop: '28px' }}
                >
                  Submit
                </Button>
              </Col>

              <Col className="col-2">
                <Button
                  variant="secondary"
                  className="w-100"
                  style={{ marginTop: '28px' }}
                  onClick={
                    () => handleReset()
                    // reducerDispatch({ type: SET_OBJECT, value: emptyInstance_EkfDetailsPageData })
                  }
                >
                  Clear Filters
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </Container>
  );
};
