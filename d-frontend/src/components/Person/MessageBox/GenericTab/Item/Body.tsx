import React, { FC } from 'react';
import { Person } from '../../../../../data/datatypes';
import { Container } from 'react-bootstrap';
import { useTimeUtil } from '../../../../../pages/Util/useTimeUtil';
import AddressOverview from '../../../../Address/AddressOverview/AddressOverview';
import { HBox } from '../../../../HBox/HBox';
import { SpanFC } from '../../../../HBox/SpanFC';

interface Props {
  person: Person;
}

export const MessageBoxItemBody: FC<Props> = ({ person }) => {
  const { convertTimestampToDateString } = useTimeUtil();
  return (
    <Container fluid>
      <HBox
        boldKey={{ value: 'name' }}
        elements={[{ value: <SpanFC content={`${person.firstname} ${person.lastname}`} /> }]}
      />
      <HBox
        boldKey={{ value: 'birthdate' }}
        elements={[
          { value: <SpanFC content={convertTimestampToDateString(String(person.birthdate))} /> },
        ]}
      />
      <HBox
        boldKey={{ value: 'address' }}
        elements={[{ value: <AddressOverview address={person.address} /> }]}
      />
    </Container>
  );
};
