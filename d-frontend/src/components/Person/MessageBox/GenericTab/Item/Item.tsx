import React, { FC } from 'react';
import { Person, PrivateMessage, PrivateChannel } from '../../../../../data/datatypes';
import { Card } from 'react-bootstrap';
import { HBox } from '../../../../HBox/HBox';
import { ImageFC } from '../../../../HBox/ImageFC';
import { MessageBoxItemBody } from './Body';
import { MessageBoxItemFooter } from './Footer';
import { ButtonVariantType } from '../../../../ButtonWithBadge/ButtonWithBadge';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { useAvatarUtil } from '../../../../../pages/Util/useAvatarUtil';
import { PrivateMessagePreview } from './PrivateMessagePreview';

export interface FooterButtonInterface {
  variant: ButtonVariantType;
  handler: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    channel?: PrivateChannel,
  ) => void;
  text?: string;
  icon: IconDefinition;
}

interface Props {
  person: Person;
  messages: PrivateMessage[];
  buttons: FooterButtonInterface[];
  channel: PrivateChannel;
}
export const MessageBoxItem: FC<Props> = ({ person, messages, buttons, channel }) => {
  const { imageBase64 } = useAvatarUtil(person);
  return (
    <Card className="col-12">
      <Card.Body>
        <Card.Title>{person.username} Overview</Card.Title>
        <div>
          <HBox
            elements={[
              {
                key: 'col-3',
                value: <ImageFC src={imageBase64} className="mt-2 pr-2" width="60" height="60" />,
              },
              {
                key: 'col-3',
                value: <MessageBoxItemBody person={person} />,
              },
              {
                key: 'col-6',
                value: <PrivateMessagePreview userFrom={person} messages={messages} />,
              },
            ]}
          />

          <HBox
            elements={[
              {
                key: 'mt-5',
                value: <MessageBoxItemFooter buttons={buttons} channel={channel} />,
              },
            ]}
          />
        </div>
      </Card.Body>
    </Card>
  );
};
