import React, { FC, useEffect } from 'react';
import { PrivateMessage, Person } from '../../../../../data/datatypes';

interface Props {
  userFrom: Person;
  messages: PrivateMessage[];
}

export const PrivateMessagePreview: FC<Props> = ({ userFrom, messages }) => {
  useEffect(() => {
    console.table(userFrom);
    console.table(messages);
  }, [userFrom, messages]);
  return (
    <div
      className="d-flex flex-column"
      style={{ border: '3px solid green', height: '200px', overflow: 'auto' }}
    >
      {messages &&
        messages.map((one, i) => (
          <div
            key={i}
            className="ml-2 mr-2 mt-2 mb-2"
            style={{
              alignSelf: one.userTo && userFrom.id == one.userTo.id ? 'flex-start' : 'flex-end', // => sender is current logged user
              width: 'fit-content',
            }}
            dangerouslySetInnerHTML={{ __html: one.content! }}
          >
            {/* {one.content} */}
          </div>
        ))}
    </div>
  );
};
