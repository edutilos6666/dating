import React, { FC } from 'react';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FooterButtonInterface } from './Item';
import { PrivateChannel } from '../../../../../data/datatypes';

interface Props {
  buttons: FooterButtonInterface[];
  channel: PrivateChannel;
}
export const MessageBoxItemFooter: FC<Props> = ({ buttons, channel }) => {
  return (
    <div className="d-flex flex-row">
      {buttons && buttons.length > 0
        ? buttons.map((one, i) => (
            <Button
              key={i}
              variant={one.variant}
              className="mr-3 pl-4 pr-4"
              onClick={(evt: any) => one.handler(evt, channel)}
            >
              {one.text && one.text.length > 0 ? <span className="mr-2">{one.text}</span> : ''}
              <FontAwesomeIcon className="ml-2" icon={one.icon} />
            </Button>
          ))
        : ''}
    </div>
  );
};
