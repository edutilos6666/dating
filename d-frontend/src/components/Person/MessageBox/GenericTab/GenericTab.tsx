import React, { FC, Dispatch } from 'react';
import { TableView } from './TableView/TableView';
import {
  HomeData,
  HomeDataReducerAction,
  HomeDataReducerActionTypes,
} from '../../../../pages/Home/TypesAndReducer';
import { DataTablePaginationDataTableInterface } from '../../../DataTable/types';
import { FooterButtonInterface } from './Item/Item';

export type ReducerActionNumberTypes =
  | HomeDataReducerActionTypes.SET_MESSAGE_COUNT
  | HomeDataReducerActionTypes.SET_MSGNORMALCOUNT
  | HomeDataReducerActionTypes.SET_MSGFAVORITECOUNT
  | HomeDataReducerActionTypes.SET_MSGBLOCKEDCOUNT
  | HomeDataReducerActionTypes.SET_MSGDELETEDCOUNT;

export type ReducerActionBooleanTypes =
  | HomeDataReducerActionTypes.SET_NEWMSGNORMAL
  | HomeDataReducerActionTypes.SET_NEWMSGFAVORITE
  | HomeDataReducerActionTypes.SET_NEWMSGBLOCKED
  | HomeDataReducerActionTypes.SET_NEWMSGDELETED;

export interface GenericTabProps {
  reducerData: HomeData;
  reducerDispatch: Dispatch<HomeDataReducerAction>;
  queryResult: any; // QueryResult<any>;
  paginationHelper: DataTablePaginationDataTableInterface;
  listData: Array<any> | null | undefined;
  reducerActionType1: ReducerActionBooleanTypes;
  reducerActionType2: ReducerActionNumberTypes;
  buttons: FooterButtonInterface[];
}
export const GenericTab: FC<GenericTabProps> = ({
  reducerData,
  reducerDispatch,
  queryResult,
  paginationHelper,
  listData,
  reducerActionType1,
  reducerActionType2,
  buttons,
}) => {
  return (
    <div>
      <TableView
        reducerData={reducerData}
        reducerDispatch={reducerDispatch}
        queryResult={queryResult}
        paginationHelper={paginationHelper}
        listData={listData}
        reducerActionType1={reducerActionType1}
        reducerActionType2={reducerActionType2}
        buttons={buttons}
      />
    </div>
  );
};
