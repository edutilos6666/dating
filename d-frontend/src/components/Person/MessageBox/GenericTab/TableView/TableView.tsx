import React, { FC, useEffect } from 'react';
import useAppController from '../../../../../hooks/useAppController';
import { VerticalListViewWithSpinner } from '../../../../ListView/ListViewWithSpinner';
import { MessageBoxItem } from '../Item/Item';
import { GenericTabProps } from '../GenericTab';

export const TableView: FC<GenericTabProps> = ({
  reducerData,
  reducerDispatch,
  queryResult,
  paginationHelper,
  listData,
  reducerActionType1,
  reducerActionType2,
  buttons,
}) => {
  const { userinfo } = useAppController();

  const { refetch, loading } = queryResult;

  useEffect(() => {
    if (reducerData.messageCount && !loading) refetch();
  }, [reducerData.messageCount, loading]);

  useEffect(() => {
    refetch();
  }, [reducerData.refetchChannels]);

  useEffect(() => {
    if (listData && listData.length > 0) {
      let messageCountForCurrentPage = 0;
      listData.forEach((one: any) => {
        one.messages
          ? (messageCountForCurrentPage = messageCountForCurrentPage + one.messages.length)
          : (messageCountForCurrentPage = messageCountForCurrentPage);

        if (one.messages && one.messages.filter((two: any) => two && two.opened))
          reducerDispatch({
            type: reducerActionType1,
            value: true,
          });
      });
      messageCountForCurrentPage > 0 &&
        reducerDispatch({
          type: reducerActionType2,
          value: messageCountForCurrentPage,
        });
    } else if (listData && listData.length === 0) {
      reducerDispatch({
        type: reducerActionType2,
        value: 0,
      });
    }
  }, [listData]);

  return (
    <div className="col-12">
      <VerticalListViewWithSpinner
        idColumnName="id"
        pagination={paginationHelper}
        data={listData}
        accessor={entry => (
          <MessageBoxItem
            person={
              userinfo && userinfo.username !== entry.userFrom.id ? entry.userFrom : entry.userTo
            }
            messages={entry.messages ? entry.messages : [entry]}
            buttons={buttons}
            channel={entry}
          />
        )}
        loading={loading}
      />
    </div>
  );
};
