import React, { FC, Dispatch, useEffect, useState, useContext } from 'react';
import { Tab, Row, Nav, Col } from 'react-bootstrap';
import { Maybe } from '../../../data/datatypes';
import { HomeData, HomeDataReducerAction } from '../../../pages/Home/TypesAndReducer';
import { InboxTab } from './InboxTab/InboxTab';
import { FavoriteTab } from './FavoriteTab/FavoriteTab';
import { BlockedTab } from './BlockedTab/BlockedTab';
import { DeletedTab } from './DeletedTab/DeletedTab';
import { ActiveTabContext } from '../../../context/ActiveTabContext';
import {
  MESSAGE_BOX_INBOX_TAB,
  MESSAGE_BOX_FAVORITES_TAB,
  MESSAGE_BOX_BLOCKED_TAB,
  MESSAGE_BOX_DELETED_TAB,
  MESSAGE_BOX_ALL_MESSAGES_TAB,
} from '../../../pages/Util/TabNames';
import { AllMessagesTab } from './AllMessagesTab/AllMessagesTab';

interface Props {
  reducerData: HomeData;
  reducerDispatch: Dispatch<HomeDataReducerAction>;
}

export const MessageBox: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const [tabTitleAllMessages, setTabTitleAllMessages] = useState<any>('All Messages');
  const [tabTitleInbox, setTabTitleInbox] = useState<any>('Inbox');
  const [tabTitleFavorites, setTabTitleFavorites] = useState<any>('Favorites');
  const [tabTitleBlocked, setTabTitleBlocked] = useState<any>('Blocked');
  const [tabTitleDeleted, setTabTitleDeleted] = useState<any>('Deleted');

  const { messageBoxActiveTab, setMessageBoxActiveTab } = useContext(ActiveTabContext);

  useEffect(() => {
    if (
      reducerData.msgNormalCount ||
      reducerData.msgFavoriteCount ||
      reducerData.msgBlockedCount ||
      reducerData.msgDeletedCount
    ) {
      const count =
        (reducerData.msgNormalCount ? reducerData.msgNormalCount : 0) +
        (reducerData.msgFavoriteCount ? reducerData.msgFavoriteCount : 0) +
        (reducerData.msgBlockedCount ? reducerData.msgBlockedCount : 0) +
        (reducerData.msgDeletedCount ? reducerData.msgDeletedCount : 0);
      setTabTitleAllMessages(
        <strong>
          {'All Messages '} {count}
        </strong>,
      );
    } else {
      setTabTitleAllMessages('All Messages');
    }
  }, [
    reducerData.msgNormalCount,
    reducerData.msgFavoriteCount,
    reducerData.msgBlockedCount,
    reducerData.msgDeletedCount,
  ]);

  useEffect(() => {
    const count = reducerData.msgNormalCount;
    if (count && count > 0) {
      setTabTitleInbox(
        <strong>
          {'Inbox'} {count}
        </strong>,
      );
    } else {
      setTabTitleInbox('Inbox');
    }
  }, [reducerData.msgNormalCount]);

  useEffect(() => {
    const count = reducerData.msgFavoriteCount;
    if (count && count > 0) {
      setTabTitleFavorites(
        <strong>
          {'Favorites'} {count}
        </strong>,
      );
    } else {
      setTabTitleFavorites('Favorites');
    }
  }, [reducerData.msgFavoriteCount]);

  useEffect(() => {
    const count = reducerData.msgBlockedCount;
    if (count && count > 0) {
      setTabTitleBlocked(
        <strong>
          {'Blocked'} {count}
        </strong>,
      );
    } else {
      setTabTitleBlocked('Blocked');
    }
  }, [reducerData.msgBlockedCount]);

  useEffect(() => {
    const count = reducerData.msgDeletedCount;
    if (count && count > 0) {
      setTabTitleDeleted(
        <strong>
          {'Deleted'} {count}
        </strong>,
      );
    } else {
      setTabTitleDeleted('Deleted');
    }
  }, [reducerData.msgDeletedCount]);

  return (
    <Tab.Container
      id="left-tabs-example"
      defaultActiveKey={messageBoxActiveTab}
      onSelect={(eventKey: string) => setMessageBoxActiveTab(eventKey)}
    >
      <Row style={{ height: '800px' }}>
        <Col className="col-3">
          <Nav variant="pills" className="flex-column" defaultActiveKey={messageBoxActiveTab}>
            <Nav.Item>
              <Nav.Link eventKey={MESSAGE_BOX_ALL_MESSAGES_TAB}>{tabTitleAllMessages}</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey={MESSAGE_BOX_INBOX_TAB}>{tabTitleInbox}</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey={MESSAGE_BOX_FAVORITES_TAB}>{tabTitleFavorites}</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey={MESSAGE_BOX_BLOCKED_TAB}>{tabTitleBlocked}</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey={MESSAGE_BOX_DELETED_TAB}>{tabTitleDeleted}</Nav.Link>
            </Nav.Item>
          </Nav>
        </Col>
        <Col className="col-9">
          <Tab.Content>
            <Tab.Pane eventKey={MESSAGE_BOX_ALL_MESSAGES_TAB}>
              <AllMessagesTab reducerData={reducerData} reducerDispatch={reducerDispatch} />
            </Tab.Pane>
            <Tab.Pane eventKey={MESSAGE_BOX_INBOX_TAB}>
              <InboxTab reducerData={reducerData} reducerDispatch={reducerDispatch} />
            </Tab.Pane>
            <Tab.Pane eventKey={MESSAGE_BOX_FAVORITES_TAB}>
              <FavoriteTab reducerData={reducerData} reducerDispatch={reducerDispatch} />
            </Tab.Pane>
            <Tab.Pane eventKey={MESSAGE_BOX_BLOCKED_TAB}>
              <BlockedTab reducerData={reducerData} reducerDispatch={reducerDispatch} />
            </Tab.Pane>
            <Tab.Pane eventKey={MESSAGE_BOX_DELETED_TAB}>
              <DeletedTab reducerData={reducerData} reducerDispatch={reducerDispatch} />
            </Tab.Pane>
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  );
};
