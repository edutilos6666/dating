import React, { FC, Dispatch, useContext } from 'react';
import {
  HomeData,
  HomeDataReducerAction,
  SET_NEWMSGBLOCKED,
  SET_MSGBLOCKEDCOUNT,
} from '../../../../pages/Home/TypesAndReducer';
import { GenericTab } from '../GenericTab/GenericTab';
import useAppController from '../../../../hooks/useAppController';
import useDataTableWithPagination from '../../../../hooks/useDataTableWithPagination';
import { HistoryContext } from '../../../../context/HistoryContext';
import { FooterButtonInterface } from '../GenericTab/Item/Item';
import { faHeart, faUserSlash, faTrash, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { useFindChannelsFromBlockedListForPersonQuery } from '../../../../data/privateChannel.generated';
import { useFunctionHelper, UpdateRelCommand } from '../useFunctionHelper';

interface Props {
  reducerData: HomeData;
  reducerDispatch: Dispatch<HomeDataReducerAction>;
}

export const BlockedTab: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const { userinfo } = useAppController();
  const { history } = useContext(HistoryContext);

  const {
    data,
    refetch,
    loading,
    variables,
    fetchMore,
  } = useFindChannelsFromBlockedListForPersonQuery({
    variables: {
      userToId: parseInt(userinfo!.username!),
      pagination: { offsetPage: 0, pageSize: 2 },
    },
    fetchPolicy: 'network-only',
    skip: !userinfo || !userinfo.username,
  });

  const paginationHelper = useDataTableWithPagination(
    variables,
    data ? data.findChannelsFromBlockedListForPerson : null,
    fetchMore,
  );

  const { handleUpdatePersonRel } = useFunctionHelper(reducerData, reducerDispatch);

  const btnFavorite: FooterButtonInterface = {
    variant: 'success',
    text: 'Add to favorite',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.FAVORITE);
    },
    icon: faHeart,
  };

  const btnBlock: FooterButtonInterface = {
    variant: 'outline-warning',
    text: 'Unblock user',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.UNDO_ALL);
    },
    icon: faUserSlash,
  };

  const btnDelete: FooterButtonInterface = {
    variant: 'danger',
    text: 'Delete user',
    handler: (_, channel) => {
      handleUpdatePersonRel(channel, UpdateRelCommand.DELETE);
    },
    icon: faTrash,
  };

  const btnOpenChatHistory: FooterButtonInterface = {
    variant: 'info',
    text: 'Open chat history',
    handler: (_, channel) => {
      channel && history.push(`/chat-history/${channel.id}`);
    },
    icon: faEnvelope,
  };

  return (
    <GenericTab
      reducerData={reducerData}
      reducerDispatch={reducerDispatch}
      queryResult={{ refetch, loading }}
      paginationHelper={paginationHelper}
      listData={
        data && data.findChannelsFromBlockedListForPerson
          ? data.findChannelsFromBlockedListForPerson.result
          : null
      }
      reducerActionType1={SET_NEWMSGBLOCKED}
      reducerActionType2={SET_MSGBLOCKEDCOUNT}
      buttons={[btnFavorite, btnBlock, btnDelete, btnOpenChatHistory]}
    />
  );
};
