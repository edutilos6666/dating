import React, { FC, useEffect, useReducer } from 'react';
import { FindPersonByIdQuery } from '../../../data/person.generated';
import { PersonImageGaleryItem } from './Item/Item';
import useBasePath from '../../../hooks/useBasePath';
import { getRemoteImage, getRemoteImagePathMap } from '../../../pages/Util/ImageUtil';
import {
  PersonImageGalleryReducerDataReducer,
  emptyInstance_PersonImageGalleryReducerData,
  SET_OBJECT,
  SET_PERSON_ID,
} from './TypesAndReducer';
import { ImagePicker } from './ImagePicker/ImagePicker';
import { ButtonWithBadge } from '../../ButtonWithBadge/ButtonWithBadge';
import { faThumbsDown, faThumbsUp, faHeart } from '@fortawesome/free-solid-svg-icons';

interface Props {
  data: FindPersonByIdQuery | undefined;
  refetch: any;
  loading: boolean;
}
export const PersonImageGalery: FC<Props> = ({ data, refetch, loading }) => {
  const { baseUrl } = useBasePath();
  const [reducerData, reducerDispatch] = useReducer(
    PersonImageGalleryReducerDataReducer,
    emptyInstance_PersonImageGalleryReducerData,
  );

  useEffect(() => {
    if (
      !loading &&
      data &&
      data.findPersonById &&
      data.findPersonById.images &&
      reducerData.selectedImageId
    ) {
      const personImage = data.findPersonById.images.filter(
        one => one.id === reducerData.selectedImageId,
      )[0];
      if (!personImage) return;
      let likes = personImage.likes ? personImage.likes.map(one => parseInt(one.id)) : [];
      let dislikes = personImage.dislikes ? personImage.dislikes.map(one => parseInt(one.id)) : [];
      let hearts = personImage.hearts ? personImage.hearts.map(one => parseInt(one.id)) : [];
      reducerDispatch({
        type: SET_OBJECT,
        value: {
          ...reducerData,
          selectedImage: personImage,
          selectedImageLikeCount: likes.length,
          selectedImageDislikeCount: dislikes.length,
          selectedImageHeartCount: hearts.length,
        },
      });
    }
  }, [reducerData.selectedImageId, data, loading]);

  useEffect(() => {
    if (!loading && data && data.findPersonById) {
      reducerDispatch({
        type: SET_PERSON_ID,
        value: data.findPersonById.id,
      });

      if (data.findPersonById.images && data.findPersonById.images.length > 0) {
        const imageIdList: string[] = data.findPersonById.images.map(one => one.id);
        getRemoteImagePathMap(baseUrl!, imageIdList).then(res => {
          if (data.findPersonById && data.findPersonById.images)
            reducerDispatch({
              type: SET_OBJECT,
              value: {
                ...reducerData,
                personId: data.findPersonById.id,
                remotePathMap: res,
                selectedImageId: data.findPersonById.images[0].id,
              },
            });
        });
      }
    }
  }, [data, loading]);

  useEffect(() => {
    refetch();
  }, [reducerData.triggerRefetch]);

  return (
    <div className="d-flex flex-column">
      <div className="d-flex flex-row">
        {data &&
          data.findPersonById &&
          data.findPersonById.images &&
          data.findPersonById.images.map((one, i) => (
            <PersonImageGaleryItem
              key={i}
              img={one}
              reducerData={reducerData}
              reducerDispatch={reducerDispatch}
            />
          ))}
      </div>

      {data && data.findPersonById ? (
        <ImagePicker reducerData={reducerData} reducerDispatch={reducerDispatch} />
      ) : (
        ''
      )}

      {reducerData.selectedImage && reducerData.selectedImageId ? (
        <div className="d-flex flex-column mt-5 offset-3 col-6">
          <img
            src={
              reducerData.remotePathMap
                ? reducerData.remotePathMap.get(reducerData.selectedImageId)
                : ''
            }
            alt={reducerData.selectedImage.alt!}
            width="100%"
            height="600px"
            style={{ border: '2px solid black' }}
          />

          <div className="d-flex flex-row mt-2 mb-2 offset-4">
            <ButtonWithBadge
              iconName={faThumbsUp}
              btnVariant="outline-primary"
              badgeVariant="primary"
              value={reducerData.selectedImageLikeCount}
              disabled={true}
            />

            <ButtonWithBadge
              iconName={faThumbsDown}
              btnVariant="outline-primary"
              badgeVariant="primary"
              value={reducerData.selectedImageDislikeCount}
              disabled={true}
            />

            <ButtonWithBadge
              iconName={faHeart}
              btnVariant="outline-success"
              badgeVariant="success"
              value={reducerData.selectedImageHeartCount}
              disabled={true}
            />
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};
