import { Maybe, PersonImage } from '../../../data/datatypes';

export interface PersonImageGalleryReducerData {
  selectedImageId?: Maybe<string>;
  selectedImage?: Maybe<PersonImage>;
  remotePathMap?: Maybe<Map<string, string>>;
  //   images?: Maybe<PersonImage[]>; performance was too bad!!!
  selectedImageLikeCount?: Maybe<number>;
  selectedImageDislikeCount?: Maybe<number>;
  selectedImageHeartCount?: Maybe<number>;

  personId?: Maybe<string>;
  fileToUpload?: Maybe<File>;
  imageAlt?: Maybe<string>;
  imageHeader?: Maybe<string>;
  imageFooter?: Maybe<string>;
  triggerFileUpload?: Maybe<boolean>;
  triggerRefetch?: Maybe<boolean>;
}

export const emptyInstance_PersonImageGalleryReducerData: PersonImageGalleryReducerData = {
  selectedImageId: undefined,
  selectedImage: undefined,
  remotePathMap: undefined,
  selectedImageLikeCount: undefined,
  selectedImageDislikeCount: undefined,
  selectedImageHeartCount: undefined,
  personId: undefined,
  fileToUpload: undefined,
  imageAlt: undefined,
  imageHeader: undefined,
  imageFooter: undefined,
  triggerFileUpload: undefined,
  triggerRefetch: undefined,
};

export enum PersonImageGalleryReducerDataActionTypes {
  SET_SELECTED_IMAGE_ID = 'setSelectedImageId',
  SET_SELECTED_IMAGE = 'setSelectedImage',
  SET_REMOTE_PATH_MAP = 'setRemotePathMap',
  SET_SELECTED_IMAGE_LIKE_COUNT = 'setSelectedImageLikeCount',
  SET_SELECTED_IMAGE_DISLIKE_COUNT = 'setSelectedImageDislikeCount',
  SET_SELECTED_IMAGE_HEART_COUNT = 'setSelectedImageHeartCount',
  SET_PERSON_ID = 'setPersonId',
  SET_FILETOUPLOAD = 'setFileToUpload',
  SET_IMAGEALT = 'setImageAlt',
  SET_IMAGEHEADER = 'setImageHeader',
  SET_IMAGEFOOTER = 'setImageFooter',
  SET_TRIGGERFILEUPLOAD = 'setTriggerFileUpload',
  SET_TRIGGERREFETCH = 'setTriggerRefetch',
  SET_OBJECT = 'SET_OBJECT',
}

export const {
  SET_SELECTED_IMAGE_ID,
  SET_SELECTED_IMAGE,
  SET_REMOTE_PATH_MAP,
  SET_OBJECT,
  SET_SELECTED_IMAGE_LIKE_COUNT,
  SET_SELECTED_IMAGE_DISLIKE_COUNT,
  SET_SELECTED_IMAGE_HEART_COUNT,
  SET_PERSON_ID,
  SET_FILETOUPLOAD,
  SET_IMAGEALT,
  SET_IMAGEHEADER,
  SET_IMAGEFOOTER,
  SET_TRIGGERFILEUPLOAD,
  SET_TRIGGERREFETCH,
} = PersonImageGalleryReducerDataActionTypes;

export type PersonImageGalleryReducerDataAction =
  | {
      type:
        | PersonImageGalleryReducerDataActionTypes.SET_SELECTED_IMAGE_ID
        | PersonImageGalleryReducerDataActionTypes.SET_PERSON_ID
        | PersonImageGalleryReducerDataActionTypes.SET_IMAGEALT
        | PersonImageGalleryReducerDataActionTypes.SET_IMAGEHEADER
        | PersonImageGalleryReducerDataActionTypes.SET_IMAGEFOOTER;
      value: string;
    }
  | {
      type: PersonImageGalleryReducerDataActionTypes.SET_SELECTED_IMAGE;
      value: PersonImage;
    }
  | {
      type: PersonImageGalleryReducerDataActionTypes.SET_REMOTE_PATH_MAP;
      value: Map<string, string>;
    }
  | {
      type: PersonImageGalleryReducerDataActionTypes.SET_OBJECT;
      value: PersonImageGalleryReducerData;
    }
  | {
      type:
        | PersonImageGalleryReducerDataActionTypes.SET_SELECTED_IMAGE_LIKE_COUNT
        | PersonImageGalleryReducerDataActionTypes.SET_SELECTED_IMAGE_DISLIKE_COUNT
        | PersonImageGalleryReducerDataActionTypes.SET_SELECTED_IMAGE_HEART_COUNT;
      value: number;
    }
  | {
      type:
        | PersonImageGalleryReducerDataActionTypes.SET_TRIGGERFILEUPLOAD
        | PersonImageGalleryReducerDataActionTypes.SET_TRIGGERREFETCH;
      value: boolean;
    }
  | {
      type: PersonImageGalleryReducerDataActionTypes.SET_FILETOUPLOAD;
      value: File;
    };

export const PersonImageGalleryReducerDataReducer = (
  state: PersonImageGalleryReducerData,
  action: PersonImageGalleryReducerDataAction,
): PersonImageGalleryReducerData => {
  let newValue;
  switch (action.type) {
    case SET_SELECTED_IMAGE_ID:
      newValue = { ...state, selectedImageId: action.value };
      break;
    case SET_SELECTED_IMAGE:
      newValue = { ...state, selectedImage: action.value };
      break;
    case SET_REMOTE_PATH_MAP:
      newValue = { ...state, remotePathMap: action.value };
      break;

    case SET_SELECTED_IMAGE_LIKE_COUNT:
      newValue = { ...state, selectedImageLikeCount: action.value };
      break;
    case SET_SELECTED_IMAGE_DISLIKE_COUNT:
      newValue = { ...state, selectedImageDislikeCount: action.value };
      break;
    case SET_SELECTED_IMAGE_HEART_COUNT:
      newValue = { ...state, selectedImageHeartCount: action.value };
      break;

    case SET_PERSON_ID:
      newValue = { ...state, personId: action.value };
      break;

    case SET_FILETOUPLOAD:
      newValue = { ...state, fileToUpload: action.value };
      break;
    case SET_IMAGEALT:
      newValue = { ...state, imageAlt: action.value };
      break;
    case SET_IMAGEHEADER:
      newValue = { ...state, imageHeader: action.value };
      break;
    case SET_IMAGEFOOTER:
      newValue = { ...state, imageFooter: action.value };
      break;
    case SET_TRIGGERFILEUPLOAD:
      newValue = { ...state, triggerFileUpload: action.value };
      break;
    case SET_TRIGGERREFETCH:
      newValue = { ...state, triggerRefetch: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;

    default:
      newValue = state;
  }
  return newValue;
};
