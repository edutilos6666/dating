import React, { FC, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { PersonImage } from '../../../../data/datatypes';
import {
  PersonImageGalleryReducerData,
  PersonImageGalleryReducerDataAction,
  SET_SELECTED_IMAGE_ID,
  SET_TRIGGERREFETCH,
} from '../TypesAndReducer';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useImageUploadUtil } from '../../../../pages/Util/useImageUploadUtil';
import { useSetPersonImageAsProfileImageMutation } from '../../../../data/personImage.generated';

interface Props {
  img: PersonImage;
  reducerData: PersonImageGalleryReducerData;
  reducerDispatch: React.Dispatch<PersonImageGalleryReducerDataAction>;
}
export const PersonImageGaleryItem: FC<Props> = ({ img, reducerData, reducerDispatch }) => {
  // const { id: imageId, main, srcPath, alt, header, footer, likes, dislikes, hearts } = img;
  const [border, setBorder] = useState<string>('1px solid black');
  const { deleteImageById } = useImageUploadUtil();

  const [setImageAsProfileImage] = useSetPersonImageAsProfileImageMutation();

  useEffect(() => {
    if (reducerData.selectedImageId === img.id) {
      setBorder('1px solid red');
    } else setBorder('1px solid black');
  }, [reducerData.selectedImageId]);

  const handleBtnDeleteImageClick = async () => {
    await deleteImageById(parseInt(img.id!));
    reducerDispatch({
      type: SET_TRIGGERREFETCH,
      value: !reducerData.triggerRefetch,
    });
  };

  const handleBtnSetAsMainClick = () => {
    setImageAsProfileImage({
      variables: {
        id: parseInt(img.id),
      },
      update: (proxy, { data: mutatedData }) => {
        reducerDispatch({
          type: SET_TRIGGERREFETCH,
          value: !reducerData.triggerRefetch,
        });
      },
    });
  };

  // that does not work
  //  const [remoteImageSrc, setRemoteImageSrc] = useState<string>('');
  // <img src={remoteImageSrc} ... />
  // useEffect(() => {
  //   if (reducerData.remotePathMap && reducerData.remotePathMap.get(img.id))
  //     setRemoteImageSrc(reducerData.remotePathMap.get(img.id)!);
  // }, [reducerData.remotePathMap]);
  return (
    <div className="d-flex flex-column m-2">
      <img
        src={reducerData.remotePathMap ? reducerData.remotePathMap.get(img.id) : ''}
        alt={img.alt!}
        width="300px"
        height="300px"
        style={{ border: border }}
        onClick={() =>
          reducerDispatch({
            type: SET_SELECTED_IMAGE_ID,
            value: img.id,
          })
        }
      />

      <div className="d-flex flex-row mt-2 mb-2">
        <Button
          variant="primary"
          className="col-3 mr-2"
          disabled={img.main!}
          onClick={handleBtnSetAsMainClick}
        >
          main
        </Button>
        <Button
          variant="danger"
          className="col-3"
          disabled={reducerData.selectedImageId !== img.id}
          onClick={handleBtnDeleteImageClick}
        >
          <FontAwesomeIcon icon={faTrash} />
        </Button>
      </div>
    </div>
  );
};
