import React, { SFC } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import {
  PersonImageGalleryReducerDataAction,
  PersonImageGalleryReducerData,
} from '../../TypesAndReducer';
import { ImageUploadInputDialogInput } from './Input';

interface Props {
  title?: string;
  show: boolean;
  hide: () => void;
  reducerData: PersonImageGalleryReducerData;
  reducerDispatch: React.Dispatch<PersonImageGalleryReducerDataAction>;
}
export const ImageUploadInputDialog: SFC<Props> = ({
  title,
  show,
  hide,
  reducerData,
  reducerDispatch,
}) => {
  const icon = <FontAwesomeIcon icon={faInfoCircle} />;

  return (
    <Modal show={show} onHide={hide}>
      <Modal.Header closeButton>
        <div className="d-flex">
          <Button variant="info" className="mr-2 rounded-circle">
            {icon}
          </Button>
          <Modal.Title>{title}</Modal.Title>
        </div>
      </Modal.Header>

      <ImageUploadInputDialogInput
        reducerData={reducerData}
        reducerDispatch={reducerDispatch}
        hide={hide}
      />
    </Modal>
  );
};

ImageUploadInputDialog.defaultProps = {
  title: 'Upload Image to Server',
};
