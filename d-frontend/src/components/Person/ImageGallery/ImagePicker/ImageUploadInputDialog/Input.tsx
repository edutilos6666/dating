import React, { FC, Dispatch } from 'react';

import { Container, FormLabel, FormControl, Form, Button, Modal } from 'react-bootstrap';
import { Formik, FormikValues, FormikActions } from 'formik';
import {
  PersonImageGalleryReducerData,
  PersonImageGalleryReducerDataAction,
  SET_OBJECT,
} from '../../TypesAndReducer';
import { useImageUploadUtil } from '../../../../../pages/Util/useImageUploadUtil';

interface Props {
  reducerData: PersonImageGalleryReducerData;
  reducerDispatch: Dispatch<PersonImageGalleryReducerDataAction>;
  hide: () => void;
}

export const ImageUploadInputDialogInput: FC<Props> = ({ reducerData, reducerDispatch, hide }) => {
  const { uploadImageFile } = useImageUploadUtil();

  const uploadImageToServer = async () => {
    if (
      reducerData.fileToUpload &&
      reducerData.personId &&
      reducerData.imageAlt &&
      reducerData.imageHeader &&
      reducerData.imageFooter
    ) {
      await uploadImageFile(
        reducerData.fileToUpload,
        parseInt(reducerData.personId),
        reducerData.imageAlt,
        reducerData.imageHeader,
        reducerData.imageFooter,
      );

      reducerDispatch({
        type: SET_OBJECT,
        value: {
          ...reducerData,
          imageAlt: '',
          imageHeader: '',
          imageFooter: '',
          triggerRefetch: !reducerData.triggerRefetch,
        },
      });
      hide();
    }
  };

  const cancelUpload = () => {
    reducerDispatch({
      type: SET_OBJECT,
      value: { ...reducerData, imageAlt: '', imageHeader: '', imageFooter: '' },
    });
    hide();
  };

  return (
    <Container fluid>
      <Formik
        enableReinitialize
        validationSchema={false}
        initialValues={reducerData}
        onSubmit={async (values, { setFieldError, setStatus, setSubmitting, setFieldTouched }) => {
          if (reducerData.fileToUpload && reducerData.personId) {
            await uploadImageFile(
              reducerData.fileToUpload,
              parseInt(reducerData.personId),
              values.imageAlt!,
              values.imageHeader!,
              values.imageFooter!,
            );
            await reducerDispatch({
              type: SET_OBJECT,
              value: {
                ...reducerData,
                imageAlt: '',
                imageHeader: '',
                imageFooter: '',
                triggerRefetch: !reducerData.triggerRefetch,
              },
            });
            hide();
          }

          // that is inefficient , indeed i dont need to save imageAlt, imageHeader and imageFooter in reducer
          // await reducerDispatch({
          //   type: SET_OBJECT,
          //   value: { ...reducerData, ...values },
          // });

          // await uploadImageToServer();

          setSubmitting(false);
        }}
        onReset={(values: FormikValues, formikActions: FormikActions<FormikValues>) => {
          formikActions.setValues({});
          cancelUpload();
          // reducerDispatch({ type: SET_OBJECT, value: emptyInstance_PersonImageGalleryReducerData });
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <div className="d-flex flex-column">
              <Modal.Body>
                <div className="d-flex flex-column">
                  <FormLabel>Image Alt</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Alt"
                    name="imageAlt"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.imageAlt || ''}`}
                  />
                </div>

                <div className="d-flex flex-column">
                  <FormLabel>Image Header</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Header"
                    name="imageHeader"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.imageHeader || ''}`}
                  />
                </div>

                <div className="d-flex flex-column">
                  <FormLabel>Image Footer</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Footer"
                    name="imageFooter"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.imageFooter || ''}`}
                  />
                </div>
              </Modal.Body>

              <Modal.Footer>
                <div className="d-flex flex-row">
                  <Button
                    variant="primary"
                    type="submit"
                    className="mr-2"
                    disabled={!values.imageAlt || !values.imageHeader || !values.imageFooter}
                  >
                    Upload image to server
                  </Button>
                  <Button variant="secondary" onClick={handleReset}>
                    Cancel
                  </Button>
                </div>
              </Modal.Footer>
            </div>
          </Form>
        )}
      </Formik>
    </Container>
  );
};
