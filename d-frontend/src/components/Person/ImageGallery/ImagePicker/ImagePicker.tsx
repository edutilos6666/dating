import React, { FC, useRef, useState } from 'react';
import { Button, FormControl } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { InformationModalDialog } from '../../../Utils/CustomModal/InformationModalDialog';
import { ImageUploadInputDialog } from './ImageUploadInputDialog/ImageUploadInputDialog';
import {
  PersonImageGalleryReducerDataAction,
  PersonImageGalleryReducerData,
  SET_FILETOUPLOAD,
} from '../TypesAndReducer';

interface Props {
  reducerData: PersonImageGalleryReducerData;
  reducerDispatch: React.Dispatch<PersonImageGalleryReducerDataAction>;
}
export const ImagePicker: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const inputFileUploadRef = useRef<FormControl & HTMLInputElement>(null);
  const [showFileUploadModalDialog, setShowFileUploadModalDialog] = useState<boolean>(false);
  const [showImageUploadInputDialog, setShowImageUploadInputDialog] = useState<boolean>(false);
  const triggerFileUploadComponent = () => {
    if (inputFileUploadRef.current) inputFileUploadRef.current.click();
  };

  const fileSelected = async (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files.length > 0) {
      const selectedFile: File = event.target.files[0];
      await reducerDispatch({
        type: SET_FILETOUPLOAD,
        value: selectedFile,
      });
      setShowFileUploadModalDialog(false);
      setShowImageUploadInputDialog(true);
    } else {
      setShowFileUploadModalDialog(true);
      setShowImageUploadInputDialog(false);
    }

    if (inputFileUploadRef && inputFileUploadRef.current) inputFileUploadRef.current.value = '';
  };

  const fileUploadComponent = (
    <input
      ref={inputFileUploadRef}
      hidden
      type="file"
      name="file"
      formEncType="multipart/form-data"
      multiple={false}
      onChange={fileSelected}
      accept="image/png"
    />
  );

  return (
    <div className="d-flex flex-column">
      <Button variant="primary" className="col-2" onClick={triggerFileUploadComponent}>
        <FontAwesomeIcon icon={faPlus} />
      </Button>
      {fileUploadComponent}
      <InformationModalDialog
        message={'Please choose a file'}
        show={showFileUploadModalDialog}
        hide={() => setShowFileUploadModalDialog(false)}
      />
      <ImageUploadInputDialog
        show={showImageUploadInputDialog}
        hide={() => setShowImageUploadInputDialog(false)}
        reducerData={reducerData}
        reducerDispatch={reducerDispatch}
      />
    </div>
  );
};
