import React, { useState, useEffect } from 'react';
import { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import './SelectDayInputRangeV2.css';
import { useSelectDayInputHelpers } from '../../pages/Util/useSelectDayInputHelpers';

interface Props {}

interface State {
  from: Date | undefined;
  to: Date | undefined;
}

const SelectDayInputRangeV2Functional = () => {
  const [from, setFrom] = useState<Date>();
  const [to, setTo] = useState<Date>();
  const { formatDate, parseDate } = useSelectDayInputHelpers();

  const handleDayClick = (day: Date) => {
    const range = DateUtils.addDayToRange(day, { from: from!, to: to! });
    setFrom(range.from);
    setTo(range.to);
  };

  useEffect(() => {
    console.log(from, to);
  }, [from, to]);

  const handleResetClick = () => {
    setFrom(undefined);
    setTo(undefined);
  };

  const handleToChange = (to: Date) => {
    setTo(to);
  };

  const modifiers = { start: from, end: to };
  return (
    <div className="RangeExample w-100">
      <DayPickerInput
        value={from && to && ` ${from.toLocaleDateString()} - ${to.toLocaleDateString()}`} //i to treba
        placeholder="Datum (Daten) auswählen"
        hideOnDayClick={false}
        format="DD-MM-YYYY"
        formatDate={formatDate}
        parseDate={parseDate}
        dayPickerProps={{
          className: 'Selectable w-100',
          selectedDays: [from!, { from: from!, to: to! }],
          disabledDays: { after: new Date() },
          month: from, //
          fromMonth: from, //
          toMonth: to, //
          modifiers,
          numberOfMonths: 1,
          onDayClick: handleDayClick,
        }}
      />
    </div>
  );
};

export default SelectDayInputRangeV2Functional;
