import React from 'react';
import { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import './SelectDayInputRangeV2.css';
import { useSelectDayInputHelpers } from '../../pages/Util/useSelectDayInputHelpers';

interface Props {}

interface State {
  from: Date | undefined;
  to: Date | undefined;
}

export default class SelectDayInputRangeV2 extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      from: undefined,
      to: undefined,
    };
  }

  handleDayClick(day: Date) {
    const range = DateUtils.addDayToRange(day, { from: this.state.from!, to: this.state.to! });
    this.setState(range);
  }

  handleResetClick() {
    this.setState(this.getInitialState());
  }

  handleToChange(to: Date) {
    this.setState({ to });
  }

  render() {
    const { from, to } = { from: this.state.from!, to: this.state.to! };
    const modifiers = { start: from, end: to };
    const { formatDate, parseDate } = useSelectDayInputHelpers();
    return (
      <div className="RangeExample w-100">
        <DayPickerInput
          value={from && to && ` ${from.toLocaleDateString()} - ${to.toLocaleDateString()}`} //i to treba
          placeholder="Datum (Daten) auswählen"
          hideOnDayClick={false}
          format="DD-MM-YYYY"
          formatDate={formatDate}
          parseDate={parseDate}
          dayPickerProps={{
            className: 'Selectable w-100',
            selectedDays: [from, { from, to }],
            disabledDays: { after: new Date() },
            month: from, //
            fromMonth: from, //
            toMonth: to, //
            modifiers,
            numberOfMonths: 1,

            onDayClick: this.handleDayClick,
          }}
        />
      </div>
    );
  }
}
