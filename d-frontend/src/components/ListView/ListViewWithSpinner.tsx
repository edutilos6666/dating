import React, { FC, useEffect } from 'react';
import CustomSpinner, { CustomSpinnerProps } from '../CustomSpinner/CustomSpinner';
import ListView, { ListViewInterface } from './ListView';
import { VerticalListView } from './VerticalListView';

interface ListViewWithSpinnerProps extends ListViewInterface, CustomSpinnerProps {}
const ListViewWithSpinner: FC<ListViewWithSpinnerProps> = ({
  pagination,
  data,
  idColumnName,
  accessor,
  loading,
}) => {
  return (
    <>
      <CustomSpinner loading={loading} />
      <ListView
        data={data}
        pagination={pagination}
        idColumnName={idColumnName}
        accessor={accessor}
      />
    </>
  );
};

export default ListViewWithSpinner;

export const VerticalListViewWithSpinner: FC<ListViewWithSpinnerProps> = ({
  pagination,
  data,
  idColumnName,
  accessor,
  loading,
}) => {
  return (
    <>
      <CustomSpinner loading={loading} />
      <VerticalListView
        data={data}
        pagination={pagination}
        idColumnName={idColumnName}
        accessor={accessor}
      />
    </>
  );
};
