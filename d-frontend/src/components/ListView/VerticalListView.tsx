import React, { FC } from 'react';
import ListView, { ListViewInterface } from './ListView';

export const VerticalListView: FC<ListViewInterface> = ({
  pagination,
  data,
  idColumnName,
  accessor,
}) => {
  return (
    <ListView
      pagination={pagination}
      data={data}
      idColumnName={idColumnName}
      accessor={accessor}
      containerClassName={'list-view-column'}
      cellClassName={'col-12 mt-2 mb-2'}
    />
  );
};
