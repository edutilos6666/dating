import React, { FC } from 'react';
import { DataTablePaginationDataTableInterface } from '../DataTable/types';
import DataTablePagination from '../DataTable/Pagination';
import { Container, Row, Col } from 'react-bootstrap';
import './ListView.css';

export interface ListViewInterface {
  pagination?: DataTablePaginationDataTableInterface;
  data: Array<any> | null | undefined;
  idColumnName: string;
  accessor: (data: any) => {} | null | undefined;
  containerClassName?: string;
  cellClassName?: string;
}

const ListView: FC<ListViewInterface> = ({
  pagination,
  data,
  idColumnName,
  accessor,
  containerClassName,
  cellClassName,
}) => {
  let paginationComponent = <></>;
  if (pagination) {
    paginationComponent = (
      <DataTablePagination
        setPage={pagination.setPage}
        previosPage={pagination.previosPage}
        nextPage={pagination.nextPage}
        firstPage={pagination.firstPage}
        lastPage={pagination.lastPage}
        first={pagination.first}
        pageSize={pagination.pageSize}
        totalPages={pagination.totalPages}
        recordCount={pagination.recordCount}
      />
    );
  }
  return (
    <div className="d-flex flex-column" style={{ border: '1px solid green' }}>
      {paginationComponent}
      <div className={containerClassName}>
        {data
          ? data.map(entry => (
              <div className={cellClassName} key={entry[idColumnName]}>
                {accessor(entry)}
              </div>
            ))
          : ''}
      </div>
    </div>
  );
};

ListView.defaultProps = {
  containerClassName: 'list-view',
  cellClassName: 'col-3 mt-2 mb-2',
};
export default ListView;
