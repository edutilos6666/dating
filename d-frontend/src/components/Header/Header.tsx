import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faSearch } from '@fortawesome/free-solid-svg-icons';
import useAppController from '../../hooks/useAppController';

interface Props {}

const Header: React.SFC<Props> = (props: Props) => {
  const faHomeIcon = <FontAwesomeIcon icon={faHome} />;
  const faSearchIcon = <FontAwesomeIcon icon={faSearch} />;
  const { remoteLogout } = useAppController();

  return (
    <Navbar bg="primary" variant="dark">
      <Navbar.Brand as="span">
        <img src="/assets/brand/logo/dating-logo.png" height="35px" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {/* Home */}
          <Nav.Link as={Link} to="/" className="text-light">
            <span className="mr-2">Home</span>
            {faHomeIcon}
          </Nav.Link>

          {/* Search */}
          <Nav.Link as={Link} to="/search" className="text-light">
            <span className="mr-2">Search</span>
            {faSearchIcon}
          </Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link as={Link} to="/login" className="text-light" onClick={remoteLogout}>
            Sign Out
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;
