import { Maybe, Gender } from '../../data/datatypes';
import { JobWithIndex } from '../../models/JobWithIndex';

export interface PeopleViewData {
  id?: Maybe<number>;
  username?: Maybe<string>;
  firstname?: Maybe<string>;
  lastname?: Maybe<string>;
  genders?: Array<Maybe<Gender>>;
  minAge?: Maybe<number>;
  maxAge?: Maybe<number>;
  jobTitleList?: Maybe<JobWithIndex[]>;
  minWage?: Maybe<number>;
  maxWage?: Maybe<number>;
  city?: Maybe<string>;
  country?: Maybe<string>;
  zipcode?: Maybe<string>;
  email?: Maybe<string>;
  status?: Maybe<boolean>;
  triggerRefetch?: Maybe<boolean>;
}

export const emptyInstance_PeopleViewData: PeopleViewData = {
  id: undefined,
  username: undefined,
  firstname: undefined,
  lastname: undefined,
  genders: undefined,
  minAge: undefined,
  maxAge: undefined,
  jobTitleList: undefined,
  minWage: undefined,
  maxWage: undefined,
  city: undefined,
  country: undefined,
  zipcode: undefined,
  email: undefined,
  status: undefined,
  triggerRefetch: undefined,
};

export enum PeopleViewDataReducerActionTypes {
  SET_ID = 'setId',
  SET_USERNAME = 'setUsername',
  SET_FIRSTNAME = 'setFirstname',
  SET_LASTNAME = 'setLastname',
  SET_GENDERS = 'setGenders',
  SET_MINAGE = 'setMinAge',
  SET_MAXAGE = 'setMaxAge',
  SET_JOBTITLELIST = 'setJobTitleList',
  SET_MINWAGE = 'setMinWage',
  SET_MAXWAGE = 'setMaxWage',
  SET_CITY = 'setCity',
  SET_COUNTRY = 'setCountry',
  SET_ZIPCODE = 'setZipcode',
  SET_EMAIL = 'setEmail',
  SET_STATUS = 'setStatus',
  SET_OBJECT = 'SET_OBJECT',
  SET_TRIGGER_REFETCH = 'setTriggerRefetch',
}

export const {
  SET_ID,
  SET_USERNAME,
  SET_FIRSTNAME,
  SET_LASTNAME,
  SET_GENDERS,
  SET_MINAGE,
  SET_MAXAGE,
  SET_JOBTITLELIST,
  SET_MINWAGE,
  SET_MAXWAGE,
  SET_CITY,
  SET_COUNTRY,
  SET_ZIPCODE,
  SET_EMAIL,
  SET_STATUS,
  SET_OBJECT,
  SET_TRIGGER_REFETCH,
} = PeopleViewDataReducerActionTypes;

export type PeopleViewDataReducerAction =
  | {
      type:
        | PeopleViewDataReducerActionTypes.SET_ID
        | PeopleViewDataReducerActionTypes.SET_MINWAGE
        | PeopleViewDataReducerActionTypes.SET_MAXWAGE
        | PeopleViewDataReducerActionTypes.SET_MINAGE
        | PeopleViewDataReducerActionTypes.SET_MAXAGE;
      value: number;
    }
  | {
      type:
        | PeopleViewDataReducerActionTypes.SET_USERNAME
        | PeopleViewDataReducerActionTypes.SET_FIRSTNAME
        | PeopleViewDataReducerActionTypes.SET_LASTNAME
        | PeopleViewDataReducerActionTypes.SET_CITY
        | PeopleViewDataReducerActionTypes.SET_COUNTRY
        | PeopleViewDataReducerActionTypes.SET_ZIPCODE
        | PeopleViewDataReducerActionTypes.SET_EMAIL;
      value: string;
    }
  | {
      type: PeopleViewDataReducerActionTypes.SET_GENDERS;
      value: Gender[];
    }
  | {
      type: PeopleViewDataReducerActionTypes.SET_JOBTITLELIST;
      value: JobWithIndex[];
    }
  | {
      type:
        | PeopleViewDataReducerActionTypes.SET_STATUS
        | PeopleViewDataReducerActionTypes.SET_TRIGGER_REFETCH;
      value: boolean;
    }
  | {
      type: PeopleViewDataReducerActionTypes.SET_OBJECT;
      value: PeopleViewData;
    };

export const PeopleViewDataReducer = (
  state: PeopleViewData,
  action: PeopleViewDataReducerAction,
): PeopleViewData => {
  let newValue;
  switch (action.type) {
    case SET_ID:
      newValue = { ...state, id: action.value };
      break;
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_FIRSTNAME:
      newValue = { ...state, firstname: action.value };
      break;
    case SET_LASTNAME:
      newValue = { ...state, lastname: action.value };
      break;
    case SET_GENDERS:
      newValue = { ...state, genders: action.value };
      break;
    case SET_MINAGE:
      newValue = { ...state, minAge: action.value };
      break;
    case SET_MAXAGE:
      newValue = { ...state, maxAge: action.value };
      break;
    case SET_JOBTITLELIST:
      newValue = { ...state, jobTitleList: action.value };
      break;
    case SET_MINWAGE:
      newValue = { ...state, minWage: action.value };
      break;
    case SET_MAXWAGE:
      newValue = { ...state, maxWage: action.value };
      break;
    case SET_CITY:
      newValue = { ...state, city: action.value };
      break;
    case SET_COUNTRY:
      newValue = { ...state, country: action.value };
      break;
    case SET_ZIPCODE:
      newValue = { ...state, zipcode: action.value };
      break;
    case SET_EMAIL:
      newValue = { ...state, email: action.value };
      break;
    case SET_STATUS:
      newValue = { ...state, status: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    case SET_TRIGGER_REFETCH:
      newValue = { ...state, triggerRefetch: action.value };
      break;
    default:
      newValue = state;
  }

  return newValue;
};
