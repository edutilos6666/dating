import React, { FC, Dispatch, useState, useEffect } from 'react';
import {
  PeopleViewData,
  PeopleViewDataReducerAction,
  SET_OBJECT,
  emptyInstance_PeopleViewData,
} from '../TypesAndReducer';
import { Container, Form, Row, Col, FormLabel, FormControl, Button } from 'react-bootstrap';
import { Formik, FormikValues, FormikActions } from 'formik';
import { useFindAllGenderHook } from '../listDataHooks';
import MultiSelectV2 from '../../MultiSelect/MultiSelectV2';
import { Gender } from '../../../data/datatypes';
import { MULTI_SELECT_DELIM_FOR_LABEL } from '../../../pages/Util/Constants';
import SelectDayInputRangeV3 from '../../SelectDayInputRangeV3/SelectDayInputRangeV3';
import { useTimeUtil } from '../../../pages/Util/useTimeUtil';
import { jobTitleList } from '../../../pages/Util/jsonData/jobTitleList';
import { JobWithIndex } from '../../../models/JobWithIndex';
import { FormControlMinMax } from '../../FormControlMinMax/FormControlMinMax';
import SingleSelectV2 from '../../SingleSelect/SingleSelectV2';
import { countryList } from '../../../pages/Util/jsonData/countryList';
import { FormControlMinMaxV2 } from '../../FormControlMinMax/FormControlMinMaxV2';
import { cityList } from '../../../pages/Util/jsonData/cityList';
import { statusList } from '../../../pages/Util/jsonData/statusList';

interface Props {
  reducerData: PeopleViewData;
  reducerDispatch: Dispatch<PeopleViewDataReducerAction>;
}
const PeopleFilter: FC<Props> = ({ reducerData, reducerDispatch }) => {
  const [initialValues, setInitialValues] = useState<PeopleViewData>(reducerData);
  const { genders } = useFindAllGenderHook();
  const { updateAndGetDate } = useTimeUtil();
  const jobTitles: JobWithIndex[] = jobTitleList.jobs.map((one, i) => {
    return {
      index: i,
      name: one,
    };
  });

  const countries = countryList.countries;
  const cities = cityList.cities;
  const status = statusList.status;

  useEffect(() => {
    setInitialValues(reducerData);
  }, [reducerData]);

  return (
    <Container fluid>
      <Formik
        enableReinitialize
        validationSchema={false}
        initialValues={initialValues}
        onSubmit={(values, { setFieldError, setStatus, setSubmitting, setFieldTouched }) => {
          reducerDispatch({
            type: SET_OBJECT,
            value: { ...values },
          });

          setSubmitting(false);
        }}
        onReset={(values: FormikValues, formikActions: FormikActions<FormikValues>) => {
          formikActions.setValues({});
          reducerDispatch({ type: SET_OBJECT, value: emptyInstance_PeopleViewData });
        }}
      >
        {({
          isSubmitting,
          values,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
          handleBlur,
          errors,
          resetForm,
          handleReset,
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Row className="mb-3 mt-3">
              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Id</FormLabel>
                  <FormControl
                    type="number"
                    placeholder="Id"
                    name="id"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.id || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Username</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Username"
                    name="username"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.username || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Firstname</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Firstname"
                    name="firstname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.firstname || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Lastname</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Lastname"
                    name="lastname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.lastname || ''}`}
                  />
                </div>
              </Col>
            </Row>

            <Row className="mb-3 mt-3">
              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Gender</FormLabel>
                  <MultiSelectV2
                    selectedObjects={values.genders!}
                    setSelectedObjects={(genders: any) => {
                      console.log(genders);
                      setFieldValue('genders', genders);
                    }}
                    options={genders}
                    getLabel={(one: Gender) =>
                      `${one.id}${MULTI_SELECT_DELIM_FOR_LABEL}${one.name}`
                    }
                    getSelectedLabel={(one: string) => {
                      return `${
                        one.split(MULTI_SELECT_DELIM_FOR_LABEL)[1]
                          ? one.split(MULTI_SELECT_DELIM_FOR_LABEL)[1]
                          : one
                      }`;
                    }}
                    uniqueKey="id"
                    placeholder="Gender"
                  />
                </div>
              </Col>

              <Col className="col-3">
                {/* <div className="d-flex flex-column">
                  <FormLabel>Birthdate</FormLabel>
                  <SelectDayInputRangeV3
                    from={values.minAge!}
                    handleFromChange={(value: Date | undefined) =>
                      setFieldValue('minAge', updateAndGetDate(value))
                    }
                    to={values.maxAge!}
                    handleToChange={(value: Date | undefined) =>
                      setFieldValue('maxAge', updateAndGetDate(value, false))
                    }
                  />
                </div> */}

                <div className="d-flex flex-column">
                  <FormControlMinMaxV2
                    label="Age"
                    onMinChange={e => {
                      setFieldValue(
                        'minAge',
                        e.currentTarget && e.currentTarget.value ? e.currentTarget.value : null,
                      );
                    }}
                    onMaxChange={e => {
                      setFieldValue(
                        'maxAge',
                        e.currentTarget && e.currentTarget.value ? e.currentTarget.value : null,
                      );
                    }}
                    minPlaceholder="Min Age"
                    minValue={`${values.minAge || ''}`}
                    maxPlaceholder="Max Age"
                    maxValue={`${values.maxAge || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Job titles</FormLabel>
                  <MultiSelectV2
                    selectedObjects={values.jobTitleList!}
                    setSelectedObjects={(jobTitleList: any) => {
                      console.log(jobTitleList);
                      setFieldValue('jobTitleList', jobTitleList);
                    }}
                    options={jobTitles}
                    getLabel={(one: JobWithIndex) =>
                      `${one.name}${MULTI_SELECT_DELIM_FOR_LABEL}${one.index}`
                    }
                    getSelectedLabel={(one: string) =>
                      `${one.split(MULTI_SELECT_DELIM_FOR_LABEL)[0]}`
                    }
                    uniqueKey="index"
                    placeholder="JobTitle"
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormControlMinMax
                    label="Wage"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    minPlaceholder="Min Wage"
                    minName="minWage"
                    minValue={`${values.minWage || ''}`}
                    maxPlaceholder="Max Wage"
                    maxName="maxWage"
                    maxValue={`${values.maxWage || ''}`}
                  />
                </div>
              </Col>
            </Row>

            <Row className="mb-3 mt-3">
              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Country</FormLabel>
                  <SingleSelectV2
                    selectedObject={values.country}
                    setSelectedObject={country => {
                      setFieldValue('country', country);
                    }}
                    options={['', ...countries]}
                    getLabel={(one: string) => one}
                    getSelectedLabel={(one: string) => one}
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>City</FormLabel>
                  <SingleSelectV2
                    selectedObject={values.city}
                    setSelectedObject={city => {
                      setFieldValue('city', city);
                    }}
                    options={['', ...cities]}
                    getLabel={(one: string) => one}
                    getSelectedLabel={(one: string) => one}
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Zipcode</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Zipcode"
                    name="zipcode"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.zipcode || ''}`}
                  />
                </div>
              </Col>

              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Email</FormLabel>
                  <FormControl
                    type="text"
                    placeholder="Email"
                    name="email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={`${values.email || ''}`}
                  />
                </div>
              </Col>
            </Row>

            <Row className="mb-3 mt-3">
              <Col className="col-3">
                <div className="d-flex flex-column">
                  <FormLabel>Online</FormLabel>
                  <SingleSelectV2
                    selectedObject={values.status}
                    setSelectedObject={status => {
                      setFieldValue('status', status);
                    }}
                    options={['', 'true', 'false']}
                    getLabel={(one: string) => one}
                    getSelectedLabel={(one: string) => one}
                  />
                </div>
              </Col>

              <Col className="col-3 offset-3">
                <Button
                  type="submit"
                  variant="primary"
                  className="w-100"
                  style={{ marginTop: '28px' }}
                >
                  Submit
                </Button>
              </Col>

              <Col className="col-3">
                <Button
                  variant="secondary"
                  className="w-100"
                  style={{ marginTop: '28px' }}
                  onClick={
                    () => handleReset()
                    // reducerDispatch({ type: SET_OBJECT, value: emptyInstance_EkfDetailsPageData })
                  }
                >
                  Clear Filters
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

export default PeopleFilter;
