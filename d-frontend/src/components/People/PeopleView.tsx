import React, { FC, useReducer } from 'react';
import PeopleFilter from './Filter/Filter';
import PeopleOverview from './Overview/Overview';
import { PeopleViewDataReducer } from './TypesAndReducer';

interface Props {}

export const PeopleView: FC<Props> = () => {
  const [reducerData, reducerDispatch] = useReducer(PeopleViewDataReducer, {});
  return (
    <div className="d-flex flex-column">
      <PeopleFilter reducerData={reducerData} reducerDispatch={reducerDispatch} />
      <PeopleOverview reducerData={reducerData} />
    </div>
  );
};
