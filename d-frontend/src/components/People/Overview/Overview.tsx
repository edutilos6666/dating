import React, { FC, useState, Dispatch, useEffect } from 'react';
import { GraphQlPaginationInputSort } from '../../../data/datatypes';
import { useFindPersonByWithPaginationQuery } from '../../../data/person.generated';
import useDataTableWithPagination from '../../../hooks/useDataTableWithPagination';
import ListViewWithSpinner from '../../ListView/ListViewWithSpinner';
import PersonOverview from '../../Person/Overview/Overview';
import { PeopleViewData } from '../TypesAndReducer';
import { useTimeUtil } from '../../../pages/Util/useTimeUtil';
import { statusList } from '../../../pages/Util/jsonData/statusList';

interface Props {
  reducerData: PeopleViewData;
}

const PeopleOverview: FC<Props> = ({ reducerData }) => {
  const status = statusList.status;
  const { calculateBirthdateFromAge } = useTimeUtil();
  const [sorts, setSorts] = useState<GraphQlPaginationInputSort[]>([]);
  const { data, variables, fetchMore, refetch, loading } = useFindPersonByWithPaginationQuery({
    variables: {
      id: reducerData.id ? reducerData.id : null,
      username: reducerData.username ? reducerData.username : null,
      firstname: reducerData.firstname ? reducerData.firstname : null,
      lastname: reducerData.lastname ? reducerData.lastname : null,
      genderIds: reducerData.genders
        ? reducerData.genders.map(one => {
            return parseInt(one!.id!);
          })
        : null,
      minBirthdate: reducerData.minAge
        ? calculateBirthdateFromAge(reducerData.minAge, false)
        : null,
      maxBirthdate: reducerData.maxAge ? calculateBirthdateFromAge(reducerData.maxAge) : null,
      // TODO: implements Job Titles
      minWage: reducerData.minWage ? reducerData.minWage : null,
      maxWage: reducerData.maxWage ? reducerData.maxWage : null,
      country: reducerData.country ? reducerData.country : null,
      city: reducerData.city ? reducerData.city : null,
      zipcode: reducerData.zipcode ? reducerData.zipcode : null,
      email: reducerData.email ? reducerData.email : null,
      status: reducerData.status ? reducerData.status : null,
      pagination: { offsetPage: 0, pageSize: 12, sorts },
    },
    fetchPolicy: 'network-only',
  });
  const paginationHelper = useDataTableWithPagination(
    variables,
    data ? data.findPersonByWithPagination : null,
    fetchMore,
  );

  useEffect(() => {
    refetch();
  }, [reducerData.triggerRefetch]);

  return (
    <div className="col-12">
      <ListViewWithSpinner
        idColumnName="id"
        pagination={paginationHelper}
        data={
          data && data.findPersonByWithPagination ? data.findPersonByWithPagination.result : null
        }
        accessor={entry => <PersonOverview person={entry} />}
        loading={loading}
      />
    </div>
  );
};

export default PeopleOverview;
