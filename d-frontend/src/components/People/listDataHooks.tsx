import { useFindAllGenderQuery } from '../../data/gender.generated';
import { useState, useEffect } from 'react';
import { Gender } from '../../data/datatypes';

export const useFindAllGenderHook = () => {
  const { data, loading } = useFindAllGenderQuery();
  const [genders, setGenders] = useState<Gender[]>([]);
  useEffect(() => {
    if (!loading && data && data.findAllGender) {
      setGenders(data.findAllGender);
    }
  }, [data, loading]);
  return {
    genders,
  };
};
