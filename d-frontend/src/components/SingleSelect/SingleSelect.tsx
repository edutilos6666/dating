import React, { FC, RefObject, useState, useRef } from 'react';

import { FormControl, FormControlProps, InputGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCaretDown, faCaretUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';

interface Props {
  selectRef?: RefObject<FormControl<'select'> & HTMLSelectElement> | null;
  value: any;
  onChange: (evt: React.FormEvent<FormControl & FormControlProps>) => void;
  disabled: boolean;
  selectContent: any;
  className?: string;
}
const SingleSelect: FC<Props> = ({
  selectRef,
  value,
  onChange,
  disabled,
  selectContent,
  className,
}) => {
  const [caretIcon, setCaretIcon] = useState<IconDefinition>(faCaretDown);
  return (
    <InputGroup>
      <FormControl
        ref={selectRef}
        as="select"
        className={className}
        value={value}
        onChange={onChange}
        disabled={disabled}
        onPointerDown={() => {
          if (!disabled) setCaretIcon(faCaretUp);
          else setCaretIcon(faCaretDown);
        }}
        onPointerUp={() => {
          setCaretIcon(faCaretDown);
        }}
        onBlur={() => {
          setCaretIcon(faCaretDown);
        }}
      >
        {selectContent}
      </FormControl>
      <InputGroup.Append>
        <InputGroup.Text id="inputGroupPrepend">
          <FontAwesomeIcon icon={faChevronDown} />
        </InputGroup.Text>
      </InputGroup.Append>
    </InputGroup>
  );
};
SingleSelect.defaultProps = {
  selectRef: null,
  className: '',
};

export default SingleSelect;
