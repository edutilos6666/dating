import React, { FC, useState, useEffect, RefObject } from 'react';
import Select from 'react-select';

interface Props {
  selectRef?: RefObject<Select> | null;
  disabled?: boolean;
  className?: string;
  selectedObject: any;
  setSelectedObject: (selectedObject: any) => void;
  options: any[];
  getLabel: (one: any) => any;
  getSelectedLabel: (one: any) => any;
  uniqueKey?: string;
  unselectSelection?: boolean;
}
const SingleSelectV2: FC<Props> = ({
  selectRef,
  disabled,
  className,
  selectedObject,
  setSelectedObject,
  options,
  getLabel,
  getSelectedLabel,
  uniqueKey,
  unselectSelection,
}) => {
  const [selectedOption, setSelectedOption] = useState<any>(null);
  const [convertedOptions, setConvertedOptions] = useState<any[]>([]);
  const handleChange = (selectedOption: any) => {
    if (!selectedOption) {
      setSelectedOption(selectedOption);
      setSelectedObject(selectedOption);
      return;
    }

    const label = getSelectedLabel(selectedOption.label);
    setSelectedOption({ label: label, value: selectedOption.value });
    setSelectedObject(
      options.filter(two =>
        uniqueKey!.length > 0
          ? two[uniqueKey!] == selectedOption.label
          : two == selectedOption.label,
      )[0], // enable  e.g. string == number
    );
  };

  useEffect(() => {
    if (selectedObject) {
      if (!uniqueKey) {
        setSelectedOption({ label: selectedObject, value: selectedObject });
      } else {
        setSelectedOption({ label: selectedObject[uniqueKey], value: selectedObject[uniqueKey] });
      }
    } else {
      setSelectedOption(null);
    }
  }, [options]);

  useEffect(() => {
    if (options) {
      setConvertedOptions(
        options.map(one => {
          return {
            label: getLabel(one),
            value: getLabel(one),
          };
        }),
      );
    }
  }, [options]);

  // useEffect(() => {
  //   if (!selectedOption) return;
  //   setSelectedObject(
  //     options.filter(two =>
  //       uniqueKey!.length > 0
  //         ? two[uniqueKey!] == selectedOption.label
  //         : two == selectedOption.label,
  //     )[0], // enable  e.g. string == number
  //   );
  // }, [selectedOption]);

  useEffect(() => {
    if (unselectSelection) {
      setSelectedOption(null);
    }
    // else {
    //   if (!selectedObject) setSelectedOption(null);
    //   else {
    //     setSelectedOption(convertedOptions[options.indexOf(selectedObject)]);
    //   }
    // }
  }, [selectedObject]);
  return (
    <Select
      ref={selectRef}
      isDisabled={disabled}
      className={className}
      value={selectedOption}
      onChange={handleChange}
      options={convertedOptions}
    />
  );
};

SingleSelectV2.defaultProps = {
  selectRef: null,
  disabled: false,
  className: 'border-0',
  uniqueKey: '',
  unselectSelection: false,
};

export default SingleSelectV2;
