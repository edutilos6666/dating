import React, { FC, useState, useEffect } from 'react';
import { Modal, Col, Row, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExcel, faTimes, faFileCsv, faCogs } from '@fortawesome/free-solid-svg-icons';
import SingleSelectV2 from '../SingleSelect/SingleSelectV2';
import { useCsvExportUtil } from '../../pages/Util/useCsvExportUtil';
import { useExcelExportUtil } from '../../pages/Util/useExcelExportUtil';

interface Props {
  columns: string[];
  setColumns: (columns: string[]) => void;
  selectedColumns: string[];
  setSelectedColumns: (selectedColumns: string[]) => void;
  resetColumns: () => void;
  data: any[];
  show: boolean;
  setShow: (show: boolean) => void;
}
const Exporter: FC<Props> = ({
  columns,
  selectedColumns,
  setSelectedColumns,
  resetColumns,
  data,
  show,
  setShow,
}) => {
  const { exportAlsCSV } = useCsvExportUtil();
  const { exportAlsExcel } = useExcelExportUtil();

  const [selectedColumn, setSelectedColumn] = useState<string>();

  const selectColumnsOptions = columns.filter(one => selectedColumns.indexOf(one) < 0);

  const handleHideModal = () => {
    resetColumns();
    setShow(false);
  };

  useEffect(() => {
    if (columns.indexOf(selectedColumn!) >= 0) {
      // Ignoriere wenn 'Keine Auswahl' ausgewählt wurde
      setSelectedColumns([...selectedColumns, selectedColumn!]);
    }
  }, [selectedColumn]);

  return (
    <Modal size="lg" show={show} onHide={handleHideModal} aria-labelledby="export-modal">
      <Modal.Header closeButton>
        <Modal.Title id="export-modal">Export Rückforderung</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {selectedColumns && (
          <Row>
            <Col>
              <div className="d-flex flex-column overflow-auto" style={{ height: '400px' }}>
                {selectedColumns.map((one, index) => (
                  <p key={index}>
                    <FontAwesomeIcon
                      className="mr-2"
                      style={{ cursor: 'pointer' }}
                      icon={faTimes}
                      onClick={() =>
                        setSelectedColumns([...selectedColumns.filter(x => x !== one)])
                      }
                    />
                    {one}
                  </p>
                ))}
              </div>
            </Col>
          </Row>
        )}
        <Row className="mt-4">
          <Col>
            <SingleSelectV2
              disabled={selectedColumns.length === columns.length}
              selectedObject={selectedColumn}
              setSelectedObject={setSelectedColumn}
              options={selectColumnsOptions}
              getLabel={(one: string) => one}
              getSelectedLabel={(one: string) => one}
              unselectSelection={true}
            />

            {/* <SingleSelect
              selectRef={selectRef}
              value={selectedColumn}
              onChange={handleSelectionOnChange}
              disabled={selectedColumns.length === columns.length}
              selectContent={selectColumnsOptions}
            /> */}
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Row className="col-12">
          <Col className="col-1">
            <Button variant="primary">
              <FontAwesomeIcon icon={faCogs} />
            </Button>
          </Col>
          <Col className="col-10 offset-1">
            <Row>
              <Col className="col-6">
                <span>
                  {data.length} Rückforderung{data.length > 1 ? 'en' : ''} herunterladen als
                </span>
              </Col>
              <Col className="col-3">
                <Button variant="primary" onClick={() => exportAlsExcel(selectedColumns, data)}>
                  <FontAwesomeIcon icon={faFileExcel} />
                  .XLS(EXCEL)
                </Button>
              </Col>

              <Col className="col-1">
                <span>oder</span>
              </Col>

              <Col className="col-2">
                <Button variant="primary" onClick={() => exportAlsCSV(selectedColumns, data)}>
                  <FontAwesomeIcon icon={faFileCsv} />
                  .CSV
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Modal.Footer>
    </Modal>
  );
};

export default Exporter;
