import React, { FC, useState, useEffect, ChangeEvent, useContext } from 'react';
import {
  PRIVATE_CHAT_SUBSCRIBE_BASE_PATH,
  PRIVATE_CHAT_SEND_MESSAGE_BASE_PATH,
} from '../../pages/Util/Constants';
import { PrivateMessageDTO } from '../../payload/PrivateMessageDTO';
import { Button } from 'react-bootstrap';
import { useFindPrivateChannelByIdQuery } from '../../data/privateChannel.generated';
import { RouteComponentProps, match } from 'react-router';
import { useFindPrivateMessageListByQuery } from '../../data/privateMessage.generated';
import { Message } from 'stompjs';
import { useSocketUtil } from '../../pages/Util/useSocketUtil';
import useAppController from '../../hooks/useAppController';
import { NetworkStatus } from 'apollo-client';
import { VerticalListViewWithSpinner } from '../ListView/ListViewWithSpinner';
import useDataTableWithPagination from '../../hooks/useDataTableWithPagination';
import { ChatHistroyItem } from './Item/Item';
import { useAvatarUtil } from '../../pages/Util/useAvatarUtil';
import { Person, GraphQlPaginationInputSort } from '../../data/datatypes';
import { PersonFragment } from '../../data/person.generated';
import { SortDirection } from '../DataTable/types';
import { ChartHistoryEditor } from './Editor/ChatHistoryEditor';
import { ActiveTabContext } from '../../context/ActiveTabContext';
import { MESSAGE_BOX_TAB } from '../../pages/Util/TabNames';

interface PathVariables {
  channelId: string;
}

interface Props extends RouteComponentProps {
  match: match<PathVariables>;
}

export const ChatHistory: FC<Props> = ({ match }) => {
  const { userinfo } = useAppController();
  const [inputText, setInputText] = useState<string>('');
  const [clearEditorStateFlag, setClearEditorStateFlag] = useState<boolean>(true);
  const [triggerRefetch, setTriggerRefetch] = useState<boolean>();
  const { socket, socketConnectAndSubscribe, socketDisconnect } = useSocketUtil();

  const [userFrom, setUserFrom] = useState<PersonFragment>();
  const [userTo, setUserTo] = useState<PersonFragment>();
  const { data: privateChannel } = useFindPrivateChannelByIdQuery({
    variables: {
      id: parseInt(match.params.channelId),
    },
    fetchPolicy: 'network-only',
  });

  const [sorts, setSorts] = useState<GraphQlPaginationInputSort[]>([
    { fieldName: 'created', sortDirection: SortDirection.Descending },
  ]);

  const {
    data: pmData,
    refetch: pmRefetch,
    loading: pmLoading,
    error: pmError,
    networkStatus: pmNetworkStatus,
    variables: pmVariables,
    fetchMore: pmFetchMore,
  } = useFindPrivateMessageListByQuery({
    variables: {
      channelId: parseInt(match.params.channelId),
      pagination: { offsetPage: 0, pageSize: 3, sorts },
    },
    fetchPolicy: 'network-only',
  });

  const paginationHelper = useDataTableWithPagination(
    pmVariables,
    pmData ? pmData.findPrivateMessageListBy : null,
    pmFetchMore,
  );

  const { imageBase64: imageBase64Sender } = useAvatarUtil(userFrom!);
  const { imageBase64: imageBase64Receiver } = useAvatarUtil(userTo!);

  const { setActiveTab } = useContext(ActiveTabContext);

  useEffect(() => {
    setActiveTab(MESSAGE_BOX_TAB);
  }, []);

  useEffect(() => {
    if (
      pmData &&
      pmData.findPrivateMessageListBy &&
      pmData.findPrivateMessageListBy.result &&
      pmData.findPrivateMessageListBy.result.length > 0
    ) {
      const user1 = pmData.findPrivateMessageListBy.result[0].userFrom;
      const user2 = pmData.findPrivateMessageListBy.result[0].userTo;
      if (userinfo && user1 && user2 && userinfo.username === user1.id) {
        setUserFrom(user1);
        setUserTo(user2);
      } else if (userinfo && user1 && user2 && userinfo.username === user2.id) {
        setUserFrom(user2);
        setUserTo(user1);
      }
    }
  }, [pmData]);

  useEffect(() => {
    if (!match.params.channelId) return;
    const socketDestination: string = PRIVATE_CHAT_SUBSCRIBE_BASE_PATH + match.params.channelId;
    const callback = (payload: Message) => {
      console.log(payload);
      const message: PrivateMessageDTO = JSON.parse(payload.body);

      if (message.content) {
        // setTriggerRefetch(!triggerRefetch);
        pmRefetch();
        // !pmLoading && pmRefetch();
      }
    };

    socketConnectAndSubscribe(socketDestination, callback);

    return () => {
      socketDisconnect();
    };
  }, [match.params.channelId]);

  // useEffect(() => {
  //   debugger;
  //   !pmLoading && pmRefetch();
  // }, [triggerRefetch]);

  const handleBtnSendMessageClicked = () => {
    if (
      userinfo &&
      socket &&
      socket.connected &&
      privateChannel &&
      privateChannel.findPrivateChannelById &&
      privateChannel.findPrivateChannelById.userFrom &&
      privateChannel.findPrivateChannelById.userTo
    ) {
      const userFrom: number = parseInt(userinfo.username!);
      // make sure userFrom !== userTo
      const userTo: number =
        userFrom === parseInt(privateChannel.findPrivateChannelById.userFrom.id)
          ? parseInt(privateChannel.findPrivateChannelById.userTo.id)
          : parseInt(privateChannel.findPrivateChannelById.userFrom.id);
      socket.send(
        PRIVATE_CHAT_SEND_MESSAGE_BASE_PATH + privateChannel.findPrivateChannelById.id,
        {},
        JSON.stringify({
          userFrom: userFrom,
          userTo: userTo,
          channelId: parseInt(privateChannel.findPrivateChannelById.id),
          content: inputText,
          created: new Date(),
          // type: 'CHAT', -> that does not work!!!m @Payload in backend must match to this payload
          // and @Payload in backend does not have field content.
        }),
      );

      setInputText('');
      setClearEditorStateFlag(!clearEditorStateFlag);
    }
  };

  return (
    <div className="d-flex flex-column">
      <div className="col-12">
        <VerticalListViewWithSpinner
          idColumnName="id"
          pagination={paginationHelper}
          data={
            pmData && pmData.findPrivateMessageListBy && pmData.findPrivateMessageListBy.result
              ? pmData.findPrivateMessageListBy.result
              : null
          }
          accessor={entry => (
            <ChatHistroyItem
              currentUserId={userinfo ? userinfo.username! : ''}
              imageBase64Sender={imageBase64Sender ? imageBase64Sender : ''}
              imageBase64Receiver={imageBase64Receiver ? imageBase64Receiver : ''}
              pm={entry}
            />
          )}
          loading={pmLoading}
        />
      </div>
      <br />
      <ChartHistoryEditor setMessage={setInputText} clearEditorStateFlag={clearEditorStateFlag} />
      <Button onClick={handleBtnSendMessageClicked}>Send Message</Button>
    </div>
  );
};
