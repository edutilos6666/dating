import React, { FC, useState, useEffect } from 'react';
import { EditorState, convertToRaw, convertFromHTML, ContentState } from 'draft-js';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import './ChatHistoryEditor.css';

interface Props {
  setMessage: (message: string) => void;
  clearEditorStateFlag: boolean;
}
export const ChartHistoryEditor: FC<Props> = ({ setMessage, clearEditorStateFlag }) => {
  const [editorStateMessage, setEditorStateMessage] = useState<EditorState>(
    EditorState.createEmpty(),
  );

  useEffect(() => {
    // editorStateMessage.clear();
    const editorState = EditorState.push(
      editorStateMessage,
      ContentState.createFromText(''),
      'remove-range',
    );
    setEditorStateMessage(editorState);
  }, [clearEditorStateFlag]);

  const onEditorStateChangeMessage = (editorState: EditorState) => {
    setEditorStateMessage(editorState);
    let content = editorState.getCurrentContent();
    let raw = convertToRaw(content);
    let html = draftToHtml(raw);
    let normal = convertFromHTML(html);
    setMessage(draftToHtml(convertToRaw(editorState.getCurrentContent())));
  };

  return (
    <Editor
      editorState={editorStateMessage}
      toolbarClassName="toolbarClassName"
      wrapperClassName="wrapperClassName"
      editorClassName="editorClassName"
      onEditorStateChange={onEditorStateChangeMessage}
    />
  );
};
