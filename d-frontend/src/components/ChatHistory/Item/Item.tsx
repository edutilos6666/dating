import React, { FC, useState, useEffect } from 'react';
import { PrivateMessage } from '../../../data/datatypes';
import { SpanFC } from '../../HBox/SpanFC';
import PersonOverviewBody from '../../Person/Overview/Body';
import { Card } from 'react-bootstrap';

interface Props {
  currentUserId: string;
  pm: PrivateMessage;
  imageBase64Sender: string;
  imageBase64Receiver: string;
}

export const ChatHistroyItem: FC<Props> = ({
  currentUserId,
  pm,
  imageBase64Sender,
  imageBase64Receiver,
}): JSX.Element => {
  const [customComponent, setCustomComponent] = useState<JSX.Element>(<SpanFC content="" />);
  const constructItem = (sender: boolean, pm: PrivateMessage, imageBase64: string) => {
    let component: JSX.Element;
    if (sender) {
      component = (
        <div
          className="col-12"
          style={{
            float: 'left',
          }}
        >
          <Card className="col-10">
            <Card.Body>
              <div className="d-flex flex-row col-12" style={{ clear: 'both' }}>
                <img src={imageBase64} className="mt-2 pr-2 col-1" width="100%" height="100%" />
                <PersonOverviewBody
                  person={pm.userFrom!}
                  messageCreated={pm.created ? pm.created : new Date().getTime()}
                />
                <div
                  className="col-8"
                  style={{ overflow: 'auto', overflowWrap: 'break-word', maxHeight: '150px' }}
                  dangerouslySetInnerHTML={{ __html: pm.content! }}
                >
                  {/* {pm.content} */}
                </div>
              </div>
            </Card.Body>
          </Card>
        </div>
      );
    } else {
      component = (
        <div
          className="col-12"
          style={{
            float: 'right',
          }}
        >
          <Card className="col-10 offset-2">
            <Card.Body>
              <div className="d-flex flex-row col-12" style={{ clear: 'both' }}>
                <div
                  className="col-9"
                  style={{ overflow: 'auto', overflowWrap: 'break-word', maxHeight: '150px' }}
                  dangerouslySetInnerHTML={{ __html: pm.content! }}
                >
                  {/* {pm.content} */}
                </div>
                <PersonOverviewBody
                  person={pm.userTo!}
                  messageCreated={pm.created ? pm.created : new Date().getTime()}
                />
                <img src={imageBase64} className="mt-2 pr-2 col-1" width="100%" height="100%" />
              </div>
            </Card.Body>
          </Card>
        </div>
      );
    }

    setCustomComponent(component);
  };

  useEffect(() => {
    if (
      currentUserId &&
      pm &&
      pm.userFrom &&
      pm.userTo &&
      imageBase64Sender &&
      imageBase64Receiver
    ) {
      if (pm.userTo && currentUserId == pm.userFrom.id) {
        constructItem(true, pm, imageBase64Sender);
      } else {
        constructItem(false, pm, imageBase64Receiver);
      }
    }
  }, [currentUserId, pm, imageBase64Sender, imageBase64Receiver]);

  return <>{customComponent}</>;
};
