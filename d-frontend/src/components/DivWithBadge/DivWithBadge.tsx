import React, { FC } from 'react';
import { Badge } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export type ButtonVariantType =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'dark'
  | 'light'
  | 'link'
  | 'outline-primary'
  | 'outline-secondary'
  | 'outline-success'
  | 'outline-danger'
  | 'outline-warning'
  | 'outline-info'
  | 'outline-dark'
  | 'outline-light';

export type BadgeVariantType =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark';

interface Props {
  iconName: any;
  badgeVariant: BadgeVariantType;
  value: any;
}

export const DivWithBadge: FC<Props> = ({ iconName, badgeVariant, value }) => {
  return (
    <div className="mr-2">
      <FontAwesomeIcon icon={iconName} />
      {'  '}
      <Badge variant={badgeVariant} pill>
        {value}
      </Badge>
    </div>
  );
};
