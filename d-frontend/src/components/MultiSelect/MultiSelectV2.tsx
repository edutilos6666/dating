import React, { FC, useState, useEffect } from 'react';
import Select from 'react-select';
import { MULTI_SELECT_DELIM_FOR_LABEL } from '../../pages/Util/Constants';

interface Props {
  selectedObjects: any[];
  setSelectedObjects: (selectedObjects: any[]) => void;
  options: any[];
  getLabel: (one: any) => any;
  getSelectedLabel: (one: any) => any;
  uniqueKey?: string | undefined;
  placeholder: string;
  isClearable?: boolean;
  multiSelectDelimForLabel?: string;
}
const MultiSelectV2: FC<Props> = ({
  selectedObjects,
  setSelectedObjects,
  options,
  getLabel,
  getSelectedLabel,
  uniqueKey,
  placeholder,
  isClearable,
  multiSelectDelimForLabel,
}) => {
  const [selectedOptions, setSelectedOptions] = useState<any[]>([]);
  const [convertedOptions, setConvertedOptions] = useState<any[]>([]);
  const handleChange = (selectedOptions: any[]) => {
    if (!selectedOptions) {
      setSelectedOptions(selectedOptions);
      setSelectedObjects(selectedOptions);
      return;
    }
    setSelectedOptions(
      selectedOptions.map(one => {
        const label = getSelectedLabel(one.label);
        return { label: label, value: one.value };
      }),
    );

    if (!uniqueKey) {
      setSelectedObjects(
        selectedOptions.map(
          one => options.filter(two => two == one.value.split(multiSelectDelimForLabel)[0])[0],
        ), // enable  e.g. string == number
      );
    } else {
      setSelectedObjects(
        selectedOptions.map(
          one =>
            options.filter(
              two => two[uniqueKey] == one.value.split(multiSelectDelimForLabel)[0],
            )[0],
        ), // enable  e.g. string == number
      );
    }
  };

  // initial values, when selectedObjects array is not empty
  useEffect(() => {
    if (options) {
      setConvertedOptions(
        options.map(one => {
          return {
            label: getLabel(one),
            value: getLabel(one),
          };
        }),
      );

      if (selectedObjects) {
        setSelectedOptions(
          selectedObjects.map(one => {
            return {
              label: uniqueKey ? one[uniqueKey] : getLabel(one),
              value: getLabel(one),
            };
          }),
        );
      }
    }
  }, [options]);

  useEffect(() => {
    if (!selectedObjects || selectedObjects.length === 0) setSelectedOptions([]);
  }, [selectedObjects]);
  return (
    <Select
      value={selectedOptions}
      onChange={handleChange}
      options={convertedOptions}
      isMulti={true}
      placeholder={placeholder}
      isClearable={isClearable}
    />
  );
};

MultiSelectV2.defaultProps = {
  uniqueKey: undefined,
  isClearable: true,
  multiSelectDelimForLabel: MULTI_SELECT_DELIM_FOR_LABEL,
};

export default MultiSelectV2;
