import React, { FC, useState, useEffect, useRef } from 'react';
import { FormControl, FormControlProps, Badge, InputGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSearch,
  faCaretDown,
  IconDefinition,
  faCaretUp,
} from '@fortawesome/free-solid-svg-icons';

interface Props {
  multiSelectWidth?: string;
  multiSelectHeight?: string;
  selectedValues: any[];
  setSelectedValues: (selectedValues: any[]) => void;
  listOptions: any[];
  convertToString: (value: any) => string;
  extractSelected: (selected: any) => string;
  converterForSelectionEvent: (value: string) => any;
}

type FormControlSelect = FormControl<'select'>;

const MultiSelect: FC<Props> = ({
  selectedValues,
  setSelectedValues,
  listOptions,
  convertToString,
  extractSelected,
  converterForSelectionEvent,
  multiSelectWidth,
  multiSelectHeight,
}) => {
  const [selectListOptions, setSelectListOptions] = useState<any[]>();
  const multiSelectRef = useRef<FormControlSelect & HTMLSelectElement>(null);

  const handleSelectionOnChange = (evt: React.FormEvent<FormControl & FormControlProps>) => {
    const tmpValues = [...selectedValues];
    console.log(evt.currentTarget.value);
    const converted = converterForSelectionEvent(
      evt.currentTarget.value ? evt.currentTarget.value : '-',
    );

    converted && tmpValues.push(converted); // do not add undefined into List

    setSelectedValues(tmpValues);
    console.log(
      multiSelectRef && multiSelectRef.current && multiSelectRef.current.selectedIndex === 0,
    );
  };

  const updateSelectListOptions = () => {
    setSelectListOptions(
      listOptions
        .filter(one => selectedValues.indexOf(one) < 0)
        .map((one, index) => {
          return <option key={index}>{convertToString(one)}</option>;
        }),
    );
  };

  useEffect(() => {
    updateSelectListOptions();
  }, [selectedValues]);

  useEffect(() => {
    updateSelectListOptions();
  }, []);

  useEffect(() => {
    updateSelectListOptions();
  }, [listOptions]);

  useEffect(() => {
    if (listOptions && listOptions.length > 0 && multiSelectRef && multiSelectRef.current)
      multiSelectRef.current.selectedIndex = 0;
  }, [selectListOptions]);

  const [defaultValue] = useState<any>('');
  const [caretIcon, setCaretIcon] = useState<IconDefinition>(faCaretDown);

  return (
    <div className="d-flex flex-column">
      <InputGroup>
        <FormControl
          ref={multiSelectRef}
          as="select"
          onChange={handleSelectionOnChange}
          disabled={false}
          defaultValue={defaultValue}
          onPointerDown={() => {
            setCaretIcon(faCaretUp);
          }}
          onPointerUp={() => {
            setCaretIcon(faCaretDown);
          }}
          onBlur={() => {
            setCaretIcon(faCaretDown);
          }}
        >
          {selectListOptions}
        </FormControl>
        <InputGroup.Append>
          <InputGroup.Text id="inputGroupPrepend">
            <FontAwesomeIcon icon={caretIcon} />
          </InputGroup.Text>
        </InputGroup.Append>
      </InputGroup>

      <div className="w-100 overflow-auto height-5">
        {selectedValues.map((one, index) => (
          <Badge pill key={index} className="mr-1 mt-1 pt-2 pb-2 pl-3 pr-3 bg-secondary">
            {extractSelected(one)}
            <span
              className="ml-2"
              style={{ cursor: 'pointer' }}
              onClick={() => setSelectedValues(selectedValues.filter(x => x !== one))}
            >
              x
            </span>
          </Badge>
        ))}
      </div>
    </div>
  );
};

MultiSelect.defaultProps = {
  multiSelectWidth: '100%',
  multiSelectHeight: '100%',
};

export default MultiSelect;
