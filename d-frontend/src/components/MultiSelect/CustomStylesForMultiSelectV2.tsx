export const customStyles = {
  option: (provided: any) => ({
    ...provided,
  }),
  control: (provided: any) => ({
    ...provided,
    border: 'none',
    borderRadius: '0px',
    boxShadow: 'none',
    paddingBottom: '-5px',
    borderBottom: '1px solid lightgrey',
    '&:hover': {
      borderBottom: '1px solid lightgrey',
    },
    maxHeight: '113px',
    overflow: 'hidden',
    overflowY: 'auto',
  }),
  multiValue: (provided: any) => ({
    ...provided,
    padding: '4px',
    fontSize: '100%',
    borderRadius: '15px',
  }),
  multiValueRemove: (provided: any) => ({
    ...provided,
    '&:hover': {
      background: 'transparent',
      color: 'black',
      cursor: 'pointer',
    },
  }),
  container: (provided: any) => ({
    ...provided,
    marginBottom: '-80px',
  }),
  multiValueLabel: (provided: any) => ({
    ...provided,
    fontSize: '100%',
  }),
  menu: (provided: any) => ({
    ...provided,
    zIndex: '2',
  }),
  placeholder: (provided: any) => ({
    ...provided,
  }),
  indicatorSeparator: () => ({}),
  dropdownIndicator: (provided: any) => ({
    ...provided,
    color: '#757575',
    '&:hover': {
      color: '#757575',
    },
  }),
  singleValue: (provided: any) => {
    return { ...provided };
  },
};
