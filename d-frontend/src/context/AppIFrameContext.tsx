import React, { createContext, FC, useState, useEffect, useRef, useCallback } from 'react';
import ReactResizeDetector from 'react-resize-detector';
import useAppController from '../hooks/useAppController';
import { useLoginWithTokenMutation } from '../data/auth.generated';

interface APPIFrameContextInterface {
  iframe: boolean;
  sendMessage: (message: SendMessageAction) => void;
}

export type SendMessageAction =
  { action: 'getToken' }
  | { action: 'setHeight'; height: number }
  | {
      action: 'routeTo';
      transitionTo: string;
      transitionObject?: any;
      transitionParameters?: any[];
    }
  | { action: 'changeUrl', transitionTo: string };

export const APPIFrameContext = createContext<APPIFrameContextInterface>({
  iframe: false,
  sendMessage: message => {},
}); // eslint-disable-line
export const AppIFrameProvider: FC = ({ children }) => {
  const iframeDetected = window.location !== window.parent.location;
  const [iframe] = useState<boolean>(iframeDetected);
  const { loggedIn, setToken } = useAppController();
  const [loginToken] = useLoginWithTokenMutation();
  const ref = useRef<HTMLDivElement>(null);

  const receiveMessage = useCallback(
    (event: any): void => {
      // eslint-disable-line
      const origin = event.origin || event.originalEvent.origin;
      const { data: eventData } = event;
      console.log('receiverMessage', event);
      if (origin) {
        console.log('TODO check origin ', origin);
      }
      if (eventData) {
        if (eventData.action === 'login') {
          console.log('login with token');
          loginToken({
            variables: { token: eventData.token },
            update: (cache, { data: mutateData }) => {
              if (mutateData) {
                const { loginWithToken } = mutateData;
                if (loginWithToken && loginWithToken.loginResponse) {
                  setToken(loginWithToken.token);
                }
              }
            },
          });
        }
      }
    },
    [loginToken, setToken],
  );

  const sendMessage = useCallback(
    (message: SendMessageAction) => {
      if (iframe) {
        window.parent.postMessage(message, '*');
      } else {
        console.debug(
          'Parent IFrame Container nicht gefunden, sendMessage wurde nicht ausgeführt.',
        );
      }
      // TODO Nur bestimmte Origins zulassen
    },
    [iframe],
  );

  const contentResized = useCallback((): void => {
    if (ref && ref.current != null) {
      sendMessage({ action: 'setHeight', height: ref.current.getBoundingClientRect().height });
    }
  }, [sendMessage]);

  useEffect(() => {
    if (iframe && !loggedIn) {
      sendMessage({ action: 'getToken' });
    }
  }, [iframe, loggedIn, sendMessage]);

  useEffect(() => {
    if (iframeDetected && !loggedIn) {
      sendMessage({ action: 'getToken' });
    }

    if (iframeDetected) {
      window.addEventListener('message', receiveMessage, false);
      document.addEventListener('resize', contentResized, false);
    }
    return () => {
      window.removeEventListener('message', receiveMessage);
      document.removeEventListener('resize', contentResized);
    };
  }, [contentResized, iframeDetected, loggedIn, receiveMessage, sendMessage]);

  const value = { iframe, sendMessage };

  if (iframeDetected) {
    return (
      <APPIFrameContext.Provider value={value}>
        <div ref={ref}>
          <ReactResizeDetector handleHeight onResize={contentResized}>
            {children}
          </ReactResizeDetector>
        </div>
      </APPIFrameContext.Provider>
    );
  }
  return <APPIFrameContext.Provider value={value}>{children}</APPIFrameContext.Provider>;
};
