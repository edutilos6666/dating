import React, { FC, createContext } from 'react';

export interface HistoryContextInterface {
  history: any;
}
export const HistoryContext = createContext<HistoryContextInterface>({
  history: undefined,
});
