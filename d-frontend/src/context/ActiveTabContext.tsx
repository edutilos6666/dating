import { Maybe } from '../data/datatypes';
import React, { createContext, FC, useState } from 'react';
import {
  PROFILE_TAB,
  MESSAGE_BOX_ALL_MESSAGES_TAB,
  IMAGES_TAB,
  CHANGE_PASSWORD_TAB,
} from '../pages/Util/TabNames';

export interface ActiveTabContextInterface {
  activeTab: Maybe<string>;
  messageBoxActiveTab: Maybe<string>;
  setActiveTab: (activeTab: Maybe<string>) => void;
  setMessageBoxActiveTab: (messageBoxActiveTab: Maybe<string>) => void;
}

export const ActiveTabContext = createContext<ActiveTabContextInterface>({
  activeTab: undefined,
  messageBoxActiveTab: undefined,
  setActiveTab: activeTab => {},
  setMessageBoxActiveTab: messageBoxActiveTab => {},
});

export const ActiveTabContextProvider: FC = ({ children }) => {
  const [activeTab, setActiveTab] = useState<Maybe<string>>(PROFILE_TAB);
  const [messageBoxActiveTab, setMessageBoxActiveTab] = useState<Maybe<string>>(
    MESSAGE_BOX_ALL_MESSAGES_TAB,
  );
  const returnValue = {
    activeTab,
    messageBoxActiveTab,
    setActiveTab,
    setMessageBoxActiveTab,
  };
  return <ActiveTabContext.Provider value={returnValue}>{children}</ActiveTabContext.Provider>;
};
