import React, { createContext, useEffect, useState, FC } from 'react';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import { Maybe } from '../data/datatypes';

const browserWindow: CustomWindow = window;
export type CustomWindow = Window & WindowExtension;

interface ENV {
  REACT_APP_SOCKET_PATH?: string;
}

interface WindowExtension {
  ENV?: ENV;
}

interface AppContextInterface {
  token?: string | undefined | null;
  setToken: (token: string | null | undefined) => void;
  loggedIn?: boolean | undefined | null;
  setLoggedIn: (loggedIn: boolean | null | undefined) => void;
  initialised?: boolean | undefined | null;
  userinfo?: UserInfoInterface | null | undefined;
  setUserInfo: (value: UserInfoInterface | null | undefined) => void;
  lastCheckDate?: number | undefined | null;
  setLastCheckDate: (checkDate: number | null | undefined) => void;
  socket?: Stomp.Client;
  socketConnected: boolean;
}

export interface UserInfoInterface {
  username: Maybe<string>;
  customer: Maybe<number>;
  vorname: Maybe<string>;
  nachname: Maybe<string>;
}

export const AppContext = createContext<AppContextInterface>({
  setToken: token => {}, // eslint-disable-line
  setLoggedIn: loggedIn => {}, // eslint-disable-line
  setUserInfo: userinfo => {}, // eslint-disable-line
  setLastCheckDate: checkdate => {}, // eslint-disable-line
  socketConnected: false,
});
export const AppProvider: FC = ({ children }) => {
  const [token, setToken] = useState<string | undefined | null>(null);
  const [loggedIn, setLoggedIn] = useState<boolean | undefined | null>(false);
  const [userinfo, setUserInfo] = useState<UserInfoInterface | undefined | null>(null);
  const [lastCheckDate, setLastCheckDate] = useState<number | undefined | null>(0);
  const [initialised, setInitialised] = useState<boolean | undefined | null>(false);
  const [socket, setSocket] = useState<Stomp.Client>();
  const [socketConnected, setSocketConnected] = useState<boolean>(false);

  useEffect(() => {
    setInitialised(true);
  }, []);

  useEffect(() => {
    let stompClient: Stomp.Client;
    if (loggedIn && token) {
      const sock = new SockJS(
        `${
          browserWindow.ENV && browserWindow.ENV.REACT_APP_SOCKET_PATH
            ? browserWindow.ENV.REACT_APP_SOCKET_PATH
            : process.env.REACT_APP_SOCKET_PATH
        }`,
      );
      stompClient = Stomp.over(sock);

      stompClient.connect({ authorization: token ? `${token}` : '' }, () => {
        setSocketConnected(true);
      });
      setSocket(stompClient);
    }
    return () => {
      if (stompClient && stompClient.connected) stompClient.disconnect(() => {});
    };
  }, [loggedIn, token]);

  const returnvalue = {
    token,
    setToken,
    loggedIn,
    setLoggedIn,
    initialised,
    userinfo,
    setUserInfo,
    lastCheckDate,
    setLastCheckDate,
    socket,
    socketConnected,
  };
  return <AppContext.Provider value={returnvalue}>{children}</AppContext.Provider>;
};
