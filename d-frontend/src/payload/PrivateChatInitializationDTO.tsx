export interface PrivateChatInitializationDTO {
  userFromId: number;
  userToId: number;
}
