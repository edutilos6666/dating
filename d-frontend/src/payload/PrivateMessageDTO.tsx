export interface PrivateMessageDTO {
  userFrom: number;
  userTo: number;
  channelId: number;
  content: string;
}
