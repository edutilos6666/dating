export interface NotificationDTO {
  type: string;
  message: string;
  fromUserId: number;
}
