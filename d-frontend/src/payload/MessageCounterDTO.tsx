export interface MessageCounterDTO {
  channelId: number;
  messageCount: number;
}
