export interface EstablishedPrivateChatChannelDTO {
  channelId: number;
  userFromId: number;
  userToId: number;
}
