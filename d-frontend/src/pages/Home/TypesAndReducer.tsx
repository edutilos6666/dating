import { Maybe } from '../../data/datatypes';

export interface HomeData {
  messageCount?: Maybe<number>;
  msgNormalCount?: Maybe<number>;
  msgSentCount?: Maybe<number>;
  msgFavoriteCount?: Maybe<number>;
  msgBlockedCount?: Maybe<number>;
  msgDeletedCount?: Maybe<number>;
  newMsgNormal?: Maybe<boolean>;
  newMsgFavorite?: Maybe<boolean>;
  newMsgBlocked?: Maybe<boolean>;
  newMsgDeleted?: Maybe<boolean>;
  refetchChannels?: Maybe<boolean>;
}

export const emptyInstance_HomeData: HomeData = {
  messageCount: undefined,
  msgNormalCount: undefined,
  msgSentCount: undefined,
  msgFavoriteCount: undefined,
  msgBlockedCount: undefined,
  msgDeletedCount: undefined,
  newMsgNormal: undefined,
  newMsgFavorite: undefined,
  newMsgBlocked: undefined,
  newMsgDeleted: undefined,
  refetchChannels: undefined,
};

export enum HomeDataReducerActionTypes {
  SET_MESSAGE_COUNT = 'setMessageCount',
  SET_MSGNORMALCOUNT = 'setMsgNormalCount',
  SET_MSGSENTCOUNT = 'setMsgSentCount',
  SET_MSGFAVORITECOUNT = 'setMsgFavoriteCount',
  SET_MSGBLOCKEDCOUNT = 'setMsgBlockedCount',
  SET_MSGDELETEDCOUNT = 'setMsgDeletedCount',
  SET_NEWMSGNORMAL = 'setNewMsgNormal',
  SET_NEWMSGFAVORITE = 'setNewMsgFavorite',
  SET_NEWMSGBLOCKED = 'setNewMsgBlocked',
  SET_NEWMSGDELETED = 'setNewMsgDeleted',
  SET_REFETCHCHANNELS = 'setRefetchChannels',
  SET_OBJECT = 'SET_OBJECT',
}

export const {
  SET_MESSAGE_COUNT,
  SET_MSGNORMALCOUNT,
  SET_MSGSENTCOUNT,
  SET_MSGFAVORITECOUNT,
  SET_MSGBLOCKEDCOUNT,
  SET_MSGDELETEDCOUNT,
  SET_NEWMSGNORMAL,
  SET_NEWMSGFAVORITE,
  SET_NEWMSGBLOCKED,
  SET_NEWMSGDELETED,
  SET_REFETCHCHANNELS,
  SET_OBJECT,
} = HomeDataReducerActionTypes;

export type HomeDataReducerAction =
  | {
      type:
        | HomeDataReducerActionTypes.SET_MESSAGE_COUNT
        | HomeDataReducerActionTypes.SET_MSGNORMALCOUNT
        | HomeDataReducerActionTypes.SET_MSGSENTCOUNT
        | HomeDataReducerActionTypes.SET_MSGFAVORITECOUNT
        | HomeDataReducerActionTypes.SET_MSGBLOCKEDCOUNT
        | HomeDataReducerActionTypes.SET_MSGDELETEDCOUNT;
      value: number;
    }
  | {
      type:
        | HomeDataReducerActionTypes.SET_NEWMSGNORMAL
        | HomeDataReducerActionTypes.SET_NEWMSGFAVORITE
        | HomeDataReducerActionTypes.SET_NEWMSGBLOCKED
        | HomeDataReducerActionTypes.SET_NEWMSGDELETED
        | HomeDataReducerActionTypes.SET_REFETCHCHANNELS;
      value: boolean;
    }
  | {
      type: HomeDataReducerActionTypes.SET_OBJECT;
      value: HomeData;
    };

export const HomeDataReducer = (state: HomeData, action: HomeDataReducerAction): HomeData => {
  let newValue;
  switch (action.type) {
    case SET_MESSAGE_COUNT:
      newValue = { ...state, messageCount: action.value };
      break;

    case SET_MSGNORMALCOUNT:
      newValue = { ...state, msgNormalCount: action.value };
      break;

    case SET_MSGSENTCOUNT:
      newValue = { ...state, msgSentCount: action.value };
      break;
    case SET_MSGFAVORITECOUNT:
      newValue = { ...state, msgFavoriteCount: action.value };
      break;
    case SET_MSGBLOCKEDCOUNT:
      newValue = { ...state, msgBlockedCount: action.value };
      break;
    case SET_MSGDELETEDCOUNT:
      newValue = { ...state, msgDeletedCount: action.value };
      break;
    case SET_NEWMSGNORMAL:
      newValue = { ...state, newMsgNormal: action.value };
      break;
    case SET_NEWMSGFAVORITE:
      newValue = { ...state, newMsgFavorite: action.value };
      break;
    case SET_NEWMSGBLOCKED:
      newValue = { ...state, newMsgBlocked: action.value };
      break;
    case SET_NEWMSGDELETED:
      newValue = { ...state, newMsgDeleted: action.value };
      break;
    case SET_REFETCHCHANNELS:
      newValue = { ...state, refetchChannels: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;

    default:
      newValue = state;
  }
  return newValue;
};
