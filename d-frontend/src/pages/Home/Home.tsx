import React, { FC, useState, useEffect, useReducer, useContext } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUser,
  IconDefinition,
  faUserEdit,
  faEnvelope,
  faPhotoVideo,
  faEdit,
  faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';
import { MessageBox } from '../../components/Person/MessageBox/MessageBox';
import { useSocketUtil } from '../Util/useSocketUtil';
import useAppController from '../../hooks/useAppController';
import { PRIVATE_MESSAGE_COUNT_BASE_PATH } from '../Util/Constants';
import { Message } from 'stompjs';
import { MessageCounterDTO } from '../../payload/MessageCounterDTO';
import { HomeDataReducer, SET_MESSAGE_COUNT } from './TypesAndReducer';
import { RouteComponentProps, match } from 'react-router';
import { HistoryContext } from '../../context/HistoryContext';
import { PersonProfile } from '../../components/Person/Profile/Profile';
import { ActiveTabContext } from '../../context/ActiveTabContext';
import {
  PROFILE_TAB,
  MESSAGE_BOX_TAB,
  IMAGES_TAB,
  CHANGE_PASSWORD_TAB,
  DELETE_PROFILE_TAB,
} from '../Util/TabNames';
import { useFindPersonByIdQuery } from '../../data/person.generated';
import { PersonImageGalery } from '../../components/Person/ImageGallery/ImageGallery';
import ChangePassword from '../Security/Login/ChangePassword';
import { DeleteProfile } from '../Security/Login/DeleteProfile';

interface PathVariables {
  id: string;
}

interface Props extends RouteComponentProps {
  match: match<PathVariables>;
}

export const Home: FC<Props> = ({ history, match }) => {
  const [key, setKey] = useState('home');
  const getTitle = (
    title: string,
    icon: IconDefinition,
    hasNewNotification: boolean = false,
  ): any => {
    if (hasNewNotification) {
      return (
        <div className="d-flex flex-row">
          <strong>
            <span className="mr-2">{title}</span>
            <FontAwesomeIcon className="mt-1" icon={icon} />
          </strong>
        </div>
      );
    }
    return (
      <div className="d-flex flex-row">
        <span className="mr-2">{title}</span>
        <FontAwesomeIcon className="mt-1" icon={icon} />
      </div>
    );
  };

  const { userinfo } = useAppController();
  const { socket, socketConnectAndSubscribe, socketDisconnect } = useSocketUtil();
  const [reducerData, reducerDispatch] = useReducer(HomeDataReducer, { messageCount: 0 });

  const [messageBoxTitle, setMessageBoxTitle] = useState<any>();
  const { activeTab } = useContext(ActiveTabContext);

  // is used by Profile and ImageGallery Components
  const { data, refetch, loading } = useFindPersonByIdQuery({
    variables: {
      id: parseInt(userinfo ? userinfo.username! : ''),
    },
  });

  useEffect(() => {
    if (!userinfo || !userinfo.username) return;
    const socketDestination: string = PRIVATE_MESSAGE_COUNT_BASE_PATH + userinfo.username;
    const callback = (payload: Message) => {
      console.log(payload);
      const message: MessageCounterDTO = JSON.parse(payload.body);

      if (message.messageCount > 0) {
        reducerDispatch({
          type: SET_MESSAGE_COUNT,
          value: message.messageCount,
        });
      }
    };

    socketConnectAndSubscribe(socketDestination, callback);

    return () => {
      socketDisconnect();
    };
  }, [userinfo]);

  useEffect(() => {
    console.table(reducerData);
    if (
      reducerData.newMsgBlocked ||
      reducerData.newMsgDeleted ||
      reducerData.newMsgFavorite ||
      reducerData.newMsgNormal
    ) {
      setMessageBoxTitle(getTitle('MessageBox', faEnvelope, true));
    } else {
      setMessageBoxTitle(getTitle('MessageBox', faEnvelope));
    }
  }, [
    reducerData.newMsgBlocked,
    reducerData.newMsgDeleted,
    reducerData.newMsgFavorite,
    reducerData.newMsgNormal,
  ]);

  return (
    <HistoryContext.Provider
      value={{
        history: history,
      }}
    >
      <Tabs id="controlled-tab-example" defaultActiveKey={activeTab}>
        <Tab eventKey={PROFILE_TAB} title={getTitle('Profile', faUser)}>
          <PersonProfile data={data} />
        </Tab>
        <Tab eventKey={MESSAGE_BOX_TAB} title={messageBoxTitle}>
          <MessageBox reducerData={reducerData} reducerDispatch={reducerDispatch} />
        </Tab>

        <Tab eventKey={IMAGES_TAB} title={getTitle('Images', faPhotoVideo)}>
          <PersonImageGalery data={data} refetch={refetch} loading={loading} />
        </Tab>

        <Tab eventKey={CHANGE_PASSWORD_TAB} title={getTitle('Change Password', faEdit)}>
          <ChangePassword />
        </Tab>
        <Tab eventKey={DELETE_PROFILE_TAB} title={getTitle('Delete Profile', faTrashAlt)}>
          <DeleteProfile />
        </Tab>
      </Tabs>
    </HistoryContext.Provider>
  );
};
