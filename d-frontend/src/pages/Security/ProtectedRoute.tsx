import React, { FC, useContext } from 'react';
import { Route, Redirect, RouteProps } from 'react-router';

import { APPIFrameContext } from '../../context/AppIFrameContext';

interface ProtectedRouteInterface extends RouteProps {
  loggedIn: boolean;
  component: FC<any>; // eslint-disable-line
}
const SecurityProtectedRoute: FC<ProtectedRouteInterface> = ({
  loggedIn,
  component: Component,
  ...rest
}) => {
  const { iframe } = useContext(APPIFrameContext);
  if (iframe && !loggedIn) {
    // Wenn IFrame dann nicht weiterleiten zur Loginseite, sondern gar nichts anzeigen (Ggf. Spinner?)
    return <div />;
  }
  return (
    <Route
      {...rest}
      render={props =>
        loggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
export default SecurityProtectedRoute;
