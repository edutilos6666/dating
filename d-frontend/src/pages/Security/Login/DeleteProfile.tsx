import React, { FC, useContext } from 'react';
import { Button } from 'react-bootstrap';
import { useDeletePersonMutation } from '../../../data/person.generated';
import useAppController from '../../../hooks/useAppController';
import { HistoryContext } from '../../../context/HistoryContext';

interface Props {}

export const DeleteProfile: FC<Props> = () => {
  const { userinfo, remoteLogout, setToken } = useAppController();
  const [deletePerson] = useDeletePersonMutation();
  const { history } = useContext(HistoryContext);

  const handleBtnDeleteClick = () => {
    if (userinfo && userinfo.username) {
      deletePerson({
        variables: {
          id: parseInt(userinfo.username),
        },
        update: (proxy, { data: mutatedData }) => {
          remoteLogout();
          localStorage.removeItem('sessionInfo');
          localStorage.removeItem('token');
          history.push('/');
        },
      });
    }
  };

  return (
    <div className="d-flex flex-column">
      <Button variant="danger" onClick={handleBtnDeleteClick}>
        Confirm delete
      </Button>
    </div>
  );
};
