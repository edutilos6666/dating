import React, { useState, FC, FormEvent, useContext } from 'react';
import { Alert, Button, Form, FormControl, FormControlProps } from 'react-bootstrap';
import { Redirect } from 'react-router';
import useAppController from '../../../hooks/useAppController';
import { useChangeUserPasswordMutation } from '../../../data/auth.generated';
import { HistoryContext } from '../../../context/HistoryContext';

const ChangePassword: FC = () => {
  const [oldPassword, setOldPassword] = useState<string | undefined>('');
  const [username, setUsername] = useState<string | undefined>('');
  const [password1, setPassword1] = useState<string | undefined>('');
  const [password2, setPassword2] = useState<string | undefined>('');
  const [error, setError] = useState<string | null | undefined>('');
  const [changeUserPassword] = useChangeUserPasswordMutation();
  const { setToken } = useAppController();
  const { history } = useContext(HistoryContext);
  return (
    <Form
      className="mb-3 text-center justify-content-center card-body border-1 border shadow rounded  ml-auto mr-auto mt-auto mb-auto"
      style={{ width: '350px' }}
    >
      <div className="mb-3 text-center d-flex justify-content-center">
        <img
          src="/assets/brand/logo/dating-logo.png"
          className="mt-2 pr-2"
          width="42"
          height="30"
          alt="Application Logo"
        />
        <div className="pt-1">
          <h3>Dating</h3>
        </div>
      </div>

      {error && <Alert variant="danger">{error}</Alert>}

      <Form.Group controlId="username">
        <Form.Label>Username</Form.Label>
        <Form.Control
          type="text"
          placeholder="Username"
          autoFocus
          onChange={(event: React.FormEvent<FormControlProps & FormControl>) => {
            setUsername(event.currentTarget.value);
          }}
        />
      </Form.Group>
      <Form.Group controlId="oldpassword">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Current password"
          onChange={(event: React.FormEvent<FormControlProps & FormControl>) => {
            setOldPassword(event.currentTarget.value);
          }}
        />
      </Form.Group>
      <Form.Group controlId="password1">
        <Form.Label>New Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="New password"
          onChange={(event: React.FormEvent<FormControlProps & FormControl>) => {
            setPassword1(event.currentTarget.value);
          }}
        />
      </Form.Group>
      <Form.Group controlId="password2">
        <Form.Label>Repeat password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Repeat new password"
          onChange={(event: React.FormEvent<FormControlProps & FormControl>) => {
            setPassword2(event.currentTarget.value);
          }}
        />
      </Form.Group>

      <Button type="submit" onClick={changePassword}>
        Change password
      </Button>
    </Form>
  );
  function changePassword(event: FormEvent<HTMLButtonElement | HTMLFormElement>) {
    event.preventDefault();
    if (password1 !== password2) {
      setError('Password repeat does not match new password.');
      event.stopPropagation();
    } else if (username) {
      changeUserPassword({
        variables: {
          username,
          password: oldPassword,
          newPassword: password1,
        },
        update: (proxy, { data: mutateData }) => {
          // eslint-disable-line
          if (mutateData) {
            const { changeUserPassword } = mutateData;
            if (changeUserPassword && changeUserPassword.token) {
              setToken(changeUserPassword.token);
              history.push('/search');
              return 'ok';
              //   return <Redirect to="/search" />;
            }
            if (changeUserPassword && changeUserPassword.loginError) {
              setError(changeUserPassword.loginError.errorDescription);
            } else {
              setError('Unknown Error');
            }
          }
        },
      });
    }
  }
};

export default ChangePassword;
