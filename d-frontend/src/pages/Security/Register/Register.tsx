import React, { FC, useState, Dispatch, useReducer, useEffect } from 'react';
import { Card, Button, Row, Col, Form, FormLabel, FormControl, Nav } from 'react-bootstrap';
import { useTimeUtil } from '../../Util/useTimeUtil';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faSave, faUndo } from '@fortawesome/free-solid-svg-icons';
import { Formik, ErrorMessage, FormikValues, FormikActions } from 'formik';
import * as Yup from 'yup';
import { countryList } from '../../Util/jsonData/countryList';
import { cityList } from '../../Util/jsonData/cityList';
import {
  SET_OBJECT,
  RegisterData,
  RegisterDataReducer,
  emptyInstance_RegisterData,
} from './TypesAndReducer';
import ReactDatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import SingleSelectV2 from '../../../components/SingleSelect/SingleSelectV2';
import {
  useCheckIfUsernameOrEmailAlreadyTakenQuery,
  useSavePersonMutation,
} from '../../../data/person.generated';
import { InformationModalDialog } from '../../../components/Utils/CustomModal/InformationModalDialog';
import { Maybe, PersonInput } from '../../../data/datatypes';
import { usePersonInputConstructor } from '../../Util/usePersonInputConstructor';
import { RouteComponentProps } from 'react-router';

interface Props extends RouteComponentProps {}

export const Register: FC<Props> = ({ history }) => {
  const { convertDateToUTCDate } = useTimeUtil();
  const [reducerData, reducerDispatch] = useReducer(
    RegisterDataReducer,
    emptyInstance_RegisterData,
  );

  const countries = countryList.countries;
  const cities = cityList.cities;

  const { refetch: refetchUsernameOrEmailExists } = useCheckIfUsernameOrEmailAlreadyTakenQuery({
    variables: {
      username: reducerData.username,
      email: reducerData.email,
    },
  });

  const [savePerson] = useSavePersonMutation();

  const [showModalDialog, setShowModalDialog] = useState<boolean>(false);

  const validateIfUsernameOrEmailDuplicate = async (
    username: Maybe<string>,
    email: Maybe<string>,
  ) => {
    const res = await refetchUsernameOrEmailExists({
      username: username,
      email: email,
    });
    if (res && res.data && res.data.checkIfUsernameOrEmailAlreadyTaken) {
      setShowModalDialog(true);
      return true;
    } else {
      setShowModalDialog(false);
      return false;
    }
  };

  const { getPersonInputFromReducerData } = usePersonInputConstructor();

  const validationSchema = Yup.object().shape({
    username: Yup.string().required('username is required'),
    // .test('duplicate', 'Username or email already taken', val => {
    //   return dataUsernameOrEmailExists &&
    //     dataUsernameOrEmailExists.checkIfUsernameOrEmailAlreadyTaken
    //     ? false
    //     : true;
    // }),
    firstname: Yup.string().required('firstname is required'),
    lastname: Yup.string().required('lastname is required'),
    // birthdate: Yup.date().required('birthdate is required'),
    companyName: Yup.string().required('company name is required'),
    jobTitle: Yup.string().required('occupation is required'),
    wage: Yup.number().required('salary is required'),
    zipcode: Yup.string().required('zipcode is required'),
    email: Yup.string()
      .required('email is required')
      .email('invalid email'),
    // .test('duplicate', 'Username or email already taken', val => {
    //   return dataUsernameOrEmailExists &&
    //     dataUsernameOrEmailExists.checkIfUsernameOrEmailAlreadyTaken
    //     ? false
    //     : true;
    // }),
    aboutMe: Yup.string().required('about me is required'),
    aboutIdealPartner: Yup.string().required('about ideal partner is required'),
    country: Yup.string().required('country is required'),
    city: Yup.string().required('city is required'),
    gender: Yup.string().required('gender is required'),
    birthdate: Yup.date().required('birthdate is required'),
    password: Yup.string().required('password is required'),
    passwordRepeat: Yup.string()
      .required('passwordRepeat is required')
      .oneOf([Yup.ref('password'), null]),
    // .test('passwordMismatch', 'Password mismatch', val=> {
    //   return password === passwordRep
    // })
  });

  useEffect(() => {
    if (reducerData.triggerSubmit === undefined) return;
    if (!showModalDialog) {
      const personInput = getPersonInputFromReducerData(reducerData);
      console.table(personInput);
      console.log('submitted');
      savePerson({
        variables: {
          person: personInput,
        },
        update: (proxy, { data: mutatedData }) => {
          history.push('/login');
        },
      });
    } else {
      console.log('could not submit');
    }
  }, [reducerData.triggerSubmit]);

  return (
    <div className="d-flex flex-column col-8 offset-2">
      <div>
        <Nav>
          <Nav.Link href="/login">Already Registered?</Nav.Link>
        </Nav>
      </div>
      <Card>
        <Card.Body>
          <Formik
            enableReinitialize
            validationSchema={validationSchema}
            initialValues={reducerData}
            onSubmit={(values, { setFieldError, setStatus, setSubmitting, setFieldTouched }) => {
              reducerDispatch({
                type: SET_OBJECT,
                value: { ...values, triggerSubmit: !reducerData.triggerSubmit },
              });
              // setSubmitting(false);
              // if (dataUsernameOrEmailExists) {
              // setFieldError('username', 'Username or email already taken.');
              // setFieldError('email', 'Username or email already taken.');
              //   setSubmitting(false);
              // } else {
              //   setSubmitting(false);
              // }
              setSubmitting(false);
            }}
            onReset={(values: FormikValues, formikActions: FormikActions<FormikValues>) => {
              formikActions.setValues({});
              reducerDispatch({ type: SET_OBJECT, value: emptyInstance_RegisterData });
            }}
          >
            {({
              isSubmitting,
              values,
              touched,
              handleSubmit,
              setFieldValue,
              setFieldError,
              setFieldTouched,
              handleChange,
              handleBlur,
              errors,
              resetForm,
              handleReset,
            }) => (
              <Form noValidate onSubmit={handleSubmit}>
                <Row className="mb-3 mt-3">
                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Username</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Username"
                        name="username"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.username || ''}`}
                        className={
                          'form-control' +
                          (errors.username && touched.username ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="username" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Password</FormLabel>
                      <FormControl
                        type="password"
                        placeholder="Password"
                        name="password"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.password || ''}`}
                        className={
                          'form-control' +
                          (errors.password && touched.password ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="password" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Repeat Password</FormLabel>
                      <FormControl
                        type="password"
                        placeholder="Repeat Password"
                        name="passwordRepeat"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.passwordRepeat || ''}`}
                        className={
                          'form-control' +
                          (errors.passwordRepeat && touched.passwordRepeat ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage
                        name="passwordRepeat"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Firstname</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Firstname"
                        name="firstname"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.firstname || ''}`}
                        className={
                          'form-control' +
                          (errors.firstname && touched.firstname ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="firstname" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Lastname</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Lastname"
                        name="lastname"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.lastname || ''}`}
                        className={
                          'form-control' +
                          (errors.lastname && touched.lastname ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="lastname" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Gender</FormLabel>
                      <SingleSelectV2
                        selectedObject={values.gender}
                        setSelectedObject={gender => {
                          setFieldValue('gender', gender);
                        }}
                        options={['Male', 'Female']}
                        getLabel={(one: string) => one}
                        getSelectedLabel={(one: string) => one}
                        className={
                          'border-0 ' + (errors.gender && touched.gender ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="gender" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Birthdate</FormLabel>
                      <ReactDatePicker
                        selected={values.birthdate}
                        onChange={(date: Date) => {
                          let value;
                          if (date !== null) value = convertDateToUTCDate(date);
                          setFieldValue('birthdate', value);
                        }}
                        placeholderText="Birthdate"
                        dateFormat="dd.MM.yyyy"
                        maxDate={moment().toDate()}
                        className={errors.birthdate && touched.birthdate ? ' is-invalid' : ''}
                      />
                      <ErrorMessage
                        name="birthdate"
                        component="div"
                        className="d-flex invalid-feedback" // default display for ErrorMessage is display:none, but display:none does not play well with ReactDatePicker
                      />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Company</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Company"
                        name="companyName"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.companyName || ''}`}
                        className={
                          'form-control' +
                          (errors.companyName && touched.companyName ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage
                        name="companyName"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Occupation</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Occupation"
                        name="jobTitle"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.jobTitle || ''}`}
                        className={
                          'form-control' +
                          (errors.jobTitle && touched.jobTitle ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="jobTitle" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Salary</FormLabel>
                      <FormControl
                        type="number"
                        placeholder="Salary"
                        name="wage"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.wage || ''}`}
                        className={
                          'form-control' + (errors.wage && touched.wage ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="wage" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Country</FormLabel>
                      <SingleSelectV2
                        selectedObject={values.country}
                        setSelectedObject={country => {
                          setFieldValue('country', country);
                        }}
                        options={[...countries]}
                        getLabel={(one: string) => one}
                        getSelectedLabel={(one: string) => one}
                        className={
                          'border-0 ' + (errors.country && touched.country ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="country" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>City</FormLabel>
                      <SingleSelectV2
                        selectedObject={values.city}
                        setSelectedObject={city => {
                          setFieldValue('city', city);
                        }}
                        options={[...cities]}
                        getLabel={(one: string) => one}
                        getSelectedLabel={(one: string) => one}
                        className={'border-0 ' + (errors.city && touched.city ? ' is-invalid' : '')}
                      />
                      <ErrorMessage name="city" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Zipcode</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Zipcode"
                        name="zipcode"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.zipcode || ''}`}
                        className={
                          'form-control' + (errors.zipcode && touched.zipcode ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="zipcode" component="div" className="invalid-feedback" />
                    </div>
                  </Col>

                  <Col className="col-4">
                    <div className="d-flex flex-column">
                      <FormLabel>Email</FormLabel>
                      <FormControl
                        type="text"
                        placeholder="Email"
                        name="email"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.email || ''}`}
                        className={
                          'form-control' + (errors.email && touched.email ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="email" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-12">
                    <div className="d-flex flex-column">
                      <FormLabel>About me</FormLabel>
                      <FormControl
                        as="textarea"
                        rows="4"
                        placeholder="Write about yourself"
                        name="aboutMe"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.aboutMe || ''}`}
                        className={
                          'form-control' + (errors.aboutMe && touched.aboutMe ? ' is-invalid' : '')
                        }
                      />
                      <ErrorMessage name="aboutMe" component="div" className="invalid-feedback" />
                    </div>
                  </Col>
                </Row>

                <Row className="mb-3 mt-3">
                  <Col className="col-12">
                    <div className="d-flex flex-column">
                      <FormLabel>About ideal partner</FormLabel>
                      <FormControl
                        as="textarea"
                        rows="4"
                        placeholder="Write about your ideal partner"
                        name="aboutIdealPartner"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={`${values.aboutIdealPartner || ''}`}
                        className={
                          'form-control' +
                          (errors.aboutIdealPartner && touched.aboutIdealPartner
                            ? ' is-invalid'
                            : '')
                        }
                      />
                      <ErrorMessage
                        name="aboutIdealPartner"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                  </Col>
                </Row>

                <div className="d-flex flex-row" style={{ float: 'right' }}>
                  <Button
                    // type="reset"
                    variant="primary"
                    className="mr-3"
                    onClick={async () => {
                      if (
                        !(await validateIfUsernameOrEmailDuplicate(values.username, values.email))
                      )
                        handleSubmit();
                    }}
                  >
                    Submit
                    <FontAwesomeIcon className="ml-2" icon={faSave} />
                  </Button>
                  <Button variant="primary" className="mr-3" onClick={() => handleReset()}>
                    Reset
                    <FontAwesomeIcon className="ml-2" icon={faUndo} />
                  </Button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>

      <InformationModalDialog
        message={'Username or email is already taken.'}
        show={showModalDialog}
        hide={() => setShowModalDialog(false)}
      />
    </div>
  );
};
