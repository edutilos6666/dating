import { Maybe } from '../../../data/datatypes';

export interface RegisterData {
  username?: Maybe<string>;
  password?: Maybe<string>;
  passwordRepeat?: Maybe<string>;
  firstname?: Maybe<string>;
  lastname?: Maybe<string>;
  gender?: Maybe<string>;
  companyName?: Maybe<string>;
  jobTitle?: Maybe<string>;
  wage?: Maybe<number>;
  birthdate?: Maybe<Date>;
  country?: Maybe<string>;
  city?: Maybe<string>;
  zipcode?: Maybe<string>;
  email?: Maybe<string>;
  aboutMe?: Maybe<string>;
  aboutIdealPartner?: Maybe<string>;
  triggerSubmit?: Maybe<boolean>;
}

export const emptyInstance_RegisterData: RegisterData = {
  username: undefined,
  password: undefined,
  passwordRepeat: undefined,
  firstname: undefined,
  lastname: undefined,
  gender: undefined,
  companyName: undefined,
  jobTitle: undefined,
  wage: undefined,
  birthdate: undefined,
  country: undefined,
  city: undefined,
  zipcode: undefined,
  email: undefined,
  aboutMe: undefined,
  aboutIdealPartner: undefined,
  triggerSubmit: undefined,
};

export enum RegisterDataActionTypes {
  SET_USERNAME = 'setUsername',
  SET_PASSWORD = 'setPassword',
  SET_PASSWORD_REPEAT = 'setPasswordRepeat',
  SET_FIRSTNAME = 'setFirstname',
  SET_LASTNAME = 'setLastname',
  SET_GENDER = 'setGender',
  SET_COMPANYNAME = 'setCompanyName',
  SET_JOBTITLE = 'setJobTitle',
  SET_WAGE = 'setWage',
  SET_BIRTHDATE = 'setBirthdate',
  SET_COUNTRY = 'setCountry',
  SET_CITY = 'setCity',
  SET_ZIPCODE = 'setZipcode',
  SET_EMAIL = 'setEmail',
  SET_ABOUTME = 'setAboutMe',
  SET_ABOUTIDEALPARTNER = 'setAboutIdealPartner',
  SET_TRIGGERSUBMIT = 'setTriggerSubmit',
  SET_OBJECT = 'SET_OBJECT',
}

export const {
  SET_USERNAME,
  SET_PASSWORD,
  SET_PASSWORD_REPEAT,
  SET_FIRSTNAME,
  SET_LASTNAME,
  SET_GENDER,
  SET_COMPANYNAME,
  SET_JOBTITLE,
  SET_WAGE,
  SET_BIRTHDATE,
  SET_COUNTRY,
  SET_CITY,
  SET_ZIPCODE,
  SET_EMAIL,
  SET_ABOUTME,
  SET_ABOUTIDEALPARTNER,
  SET_TRIGGERSUBMIT,
  SET_OBJECT,
} = RegisterDataActionTypes;

export type RegisterDataReducerAction =
  | {
      type: RegisterDataActionTypes.SET_WAGE;
      value: number;
    }
  | {
      type:
        | RegisterDataActionTypes.SET_USERNAME
        | RegisterDataActionTypes.SET_PASSWORD
        | RegisterDataActionTypes.SET_PASSWORD_REPEAT
        | RegisterDataActionTypes.SET_FIRSTNAME
        | RegisterDataActionTypes.SET_LASTNAME
        | RegisterDataActionTypes.SET_GENDER
        | RegisterDataActionTypes.SET_COMPANYNAME
        | RegisterDataActionTypes.SET_JOBTITLE
        | RegisterDataActionTypes.SET_COUNTRY
        | RegisterDataActionTypes.SET_CITY
        | RegisterDataActionTypes.SET_ZIPCODE
        | RegisterDataActionTypes.SET_EMAIL
        | RegisterDataActionTypes.SET_ABOUTME
        | RegisterDataActionTypes.SET_ABOUTIDEALPARTNER;
      value: string;
    }
  | {
      type: RegisterDataActionTypes.SET_BIRTHDATE;
      value: Date;
    }
  | {
      type: RegisterDataActionTypes.SET_TRIGGERSUBMIT;
      value: boolean;
    }
  | {
      type: RegisterDataActionTypes.SET_OBJECT;
      value: RegisterData;
    };

export const RegisterDataReducer = (
  state: RegisterData,
  action: RegisterDataReducerAction,
): RegisterData => {
  let newValue;
  switch (action.type) {
    case SET_USERNAME:
      newValue = { ...state, username: action.value };
      break;
    case SET_PASSWORD:
      newValue = { ...state, password: action.value };
      break;
    case SET_PASSWORD_REPEAT:
      newValue = { ...state, passwordRepeat: action.value };
      break;
    case SET_FIRSTNAME:
      newValue = { ...state, firstname: action.value };
      break;
    case SET_LASTNAME:
      newValue = { ...state, lastname: action.value };
      break;
    case SET_GENDER:
      newValue = { ...state, gender: action.value };
      break;
    case SET_COMPANYNAME:
      newValue = { ...state, companyName: action.value };
      break;
    case SET_JOBTITLE:
      newValue = { ...state, jobTitle: action.value };
      break;
    case SET_WAGE:
      newValue = { ...state, wage: action.value };
      break;
    case SET_BIRTHDATE:
      newValue = { ...state, birthdate: action.value };
      break;
    case SET_COUNTRY:
      newValue = { ...state, country: action.value };
      break;
    case SET_CITY:
      newValue = { ...state, city: action.value };
      break;
    case SET_ZIPCODE:
      newValue = { ...state, zipcode: action.value };
      break;
    case SET_EMAIL:
      newValue = { ...state, email: action.value };
      break;
    case SET_ABOUTME:
      newValue = { ...state, aboutMe: action.value };
      break;
    case SET_ABOUTIDEALPARTNER:
      newValue = { ...state, aboutIdealPartner: action.value };
      break;
    case SET_TRIGGERSUBMIT:
      newValue = { ...state, triggerSubmit: action.value };
      break;
    case SET_OBJECT:
      newValue = action.value;
      break;
    default:
      newValue = state;
  }

  return newValue;
};
