import React, { useEffect, useState, FC, FormEvent } from 'react';
import { Alert, Button, Form, Nav, FormControl, FormControlProps } from 'react-bootstrap';
// import { useApolloClient } from "@apollo/react-hooks";
import { Redirect } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Location } from 'history';
import useAppController from '../../../hooks/useAppController';
import { useLoginMutation } from '../../../data/auth.generated';
import { AUTH_PASSWORD_EXPIRED } from '../../../data/errorsAuthentification';

interface RegisterInterface {
  location: Location<any>; // eslint-disable-line
}
const SecurityRegister: FC<RegisterInterface> = ({ location }) => {
  const [username, setUsername] = useState<string | undefined>('');
  const [passwordExpired, setPasswordExpired] = useState<boolean>(false);
  const [password, setPassword] = useState<string | undefined>('');
  const { setToken, loggedIn } = useAppController();
  const [error, setError] = useState<string | null | undefined>('');
  const [nocookie, setNoCookie] = useState<boolean>(false);
  const [login] = useLoginMutation();
  useEffect(() => {
    if (!navigator.cookieEnabled) {
      setError(
        'Diese Seite benötigt einen Sicherheitscookie um den Benutzer zu authentifizieren. Die Verwendung solcher Cookies, werden derzeit durch Ihre Browsereinstellungen blockiert. Bitte erlauben Sie Cookies für diese Seite.',
      );
      setNoCookie(true);
    }
    /* TODO SMA
        document.getElementById('root').className = 'h-100';
        return () => {
            document.getElementById('root').className = '';
        }
*/
  }, []);

  function submitLogin(event: FormEvent<HTMLButtonElement | HTMLFormElement>) {
    event.preventDefault();
    // console.log('store'+store);
    login({
      variables: { username, password },
      update: (proxy, { data: mutateData }) => {
        // eslint-disable-line
        if (mutateData) {
          const { loginWithUserNamePassword } = mutateData;
          if (loginWithUserNamePassword && loginWithUserNamePassword.token) {
            setToken(loginWithUserNamePassword.token);
          } else if (loginWithUserNamePassword && loginWithUserNamePassword.loginError) {
            setError(loginWithUserNamePassword.loginError.errorDescription);
            if (loginWithUserNamePassword.loginError.xError === AUTH_PASSWORD_EXPIRED) {
              setPasswordExpired(true);
            }
          } else {
            setError('Unbekannter Fehler');
          }
        }
        //  setError('Die von Ihnen angegebenen Zugangsdaten waren nicht korrekt. Bitte überprüfen Sie Ihre Daten.');
      },
    });
  }

  const { from } = location.state || { from: { pathname: '/' } };
  const isInvalid = !!(error && error.length > 0);

  if (passwordExpired) {
    return <Redirect to="/changepassword" />;
  }

  if (loggedIn && from) {
    return <Redirect to={from} />;
  }

  return (
    <div className="h-100">
      <div className="h-100 d-flex align-items-center justify-content-center">
        <div>
          <div
            style={{ width: '700px' }}
            className="bg-white p-2 border-1 border shadow rounded  ml-auto mr-auto mt-auto mb-auto"
          >
            <div className="card-body">
              <div className="mb-3 text-center d-flex justify-content-center">
                <img
                  src="/assets/brand/logo/dating-logo.png"
                  className="mt-2 pr-2"
                  width="42"
                  height="30"
                  alt="Application Logo"
                />
                <div className="pt-1">
                  <h3>Dating</h3>
                </div>
              </div>
              <div className="text-center mt-3 pb-2">
                <h6>Login is important</h6>
              </div>
              <form onSubmit={submitLogin}>
                {error && <Alert variant="danger">{error}</Alert>}

                {!nocookie && (
                  <>
                    <Form.Control
                      isInvalid={isInvalid}
                      placeholder="Benutzername"
                      name="username"
                      className="form-control"
                      type="text"
                      autoFocus
                      value={username}
                      onChange={(event: React.FormEvent<FormControlProps & FormControl>) => {
                        setUsername(event.currentTarget.value);
                      }}
                    />

                    <Form.Control
                      isInvalid={isInvalid}
                      placeholder="Passwort"
                      name="password"
                      className="form-control mt-1"
                      type="password"
                      value={password}
                      onChange={(event: React.FormEvent<FormControlProps & FormControl>) => {
                        setPassword(event.currentTarget.value);
                      }}
                    />

                    <Button
                      type="submit"
                      className={`w-100 mt-4 mb-3${nocookie ? ' disabled' : ''}`}
                      onClick={submitLogin}
                    >
                      Login
                    </Button>

                    <div>
                      <Nav>
                        <Nav.Link href="/recoverpassword">(NA) Forgot password?</Nav.Link>
                      </Nav>
                    </div>
                  </>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default SecurityRegister;
