import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router';
import { HistoryContext } from '../../context/HistoryContext';
import { PeopleView } from '../../components/People/PeopleView';

interface Props extends RouteComponentProps {}
export const Search: FC<Props> = ({ history }) => {
  return (
    <HistoryContext.Provider
      value={{
        history: history,
      }}
    >
      <PeopleView />
    </HistoryContext.Provider>
  );
};
