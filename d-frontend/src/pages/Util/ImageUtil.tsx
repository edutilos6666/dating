import { PersonImage } from '../../data/datatypes';
import { PersonCarouselItemProps } from '../../components/Person/Carousel/Carousel';

//   const { baseUrl } = useBasePath();
//   const [imageBase64, setImageBase64] = useState<string>('');

export const constructPersonCarouselItemProps = async (
  baseUrl: string,
  personImages: PersonImage[],
): Promise<PersonCarouselItemProps[]> => {
  let ret: PersonCarouselItemProps[] = [];
  for (let i = 0; i < personImages.length; ++i) {
    const one = personImages[i];
    ret.push({
      src: await getRemoteImage(baseUrl, one.id)!,
      alt: one.alt!,
      header: one.header,
      footer: one.footer,
      likeCount: one.likes ? one.likes.length : 0,
      disllikeCount: one.dislikes ? one.dislikes.length : 0,
      heartCount: one.hearts ? one.hearts.length : 0,
    });
  }
  // ret = await personImages.map(async one => {
  //   return {
  //     src: await getRemoteImage(baseUrl, one.id)!,
  //     alt: one.alt,
  //     header: one.header,
  //     footer: one.footer,
  //     likeCount: one.likes ? one.likes.length : 0,
  //     disllikeCount: one.dislikes ? one.dislikes.length : 0,
  //     heartCount: one.hearts ? one.hearts.length : 0,
  //   };
  // });
  return ret;
};

export const getRemoteImagePathMap = async (
  baseUrl: string,
  imageIdList: string[],
): Promise<Map<string, string>> => {
  const ret: Map<string, string> = new Map();
  imageIdList.map(async (imageId, i) => {
    ret.set(imageId, await getRemoteImage(baseUrl, imageId));
  });
  return ret;
};

export const getRemoteImage = async (baseUrl: string, imageId: string): Promise<string> => {
  const baseUrlImagePath = `${baseUrl}/images/download/${imageId}`;
  const res = await loadImage(baseUrlImagePath, localStorage.getItem('token')!);
  return res;
};

const loadImage = async (baseUrlImagePath: string, authToken: string): Promise<string> => {
  // return `${BACKEND_BASE_URL}/diasleimage/${diaImage.eingangsbuchnummer}/${diaImage.ifd}/${diaImage.istNachtrag}?Session=${localStorage.getItem('token')}`;
  const res1 = await fetch(`${baseUrlImagePath}`, {
    method: 'GET',
    credentials: 'same-origin',
    headers: new Headers({
      Accept: 'image/png',
      'Content-type': 'image/png',
      authorization: `Bearer ${authToken}`,
      credentials: 'include',
    }),
  });
  const res2 = await res1.arrayBuffer();
  var base64Flag = 'data:image/png;base64,';
  return base64Flag + arrayBufferToBase64(res2);
};

const arrayBufferToBase64 = (buffer: ArrayBuffer): string => {
  var binary = '';
  var bytes: Uint8Array = new Uint8Array(buffer);

  bytes.forEach(b => (binary += String.fromCharCode(b)));

  return window.btoa(binary);
};
