export const useCustomFIleNameExtractor = () => {
  const extractFileName = (fullname: string): string => {
    const parts: string[] = fullname.split('/');
    return parts[parts.length - 1];
  };

  return {
    extractFileName,
  };
};
