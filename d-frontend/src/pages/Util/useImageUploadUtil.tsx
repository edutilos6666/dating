import useBasePath from '../../hooks/useBasePath';
export const useImageUploadUtil = () => {
  const { baseUrl } = useBasePath();
  const uploadImageFile = async (
    file: File,
    personId: number,
    imageAlt: string,
    imageHeader: string,
    imageFooter: string,
  ) => {
    const uploadUrl = `${baseUrl}/images/upload`;
    const formData: FormData = new FormData();
    formData.append('multipartFile', file);
    formData.append('personId', personId + '');
    formData.append('alt', imageAlt);
    formData.append('header', imageHeader);
    formData.append('footer', imageFooter);

    // files.forEach(file => formData.append('file', file));
    await fetch(uploadUrl, {
      method: 'POST',
      body: formData,
      headers: new Headers({
        Accept: 'application/json',
        // "Content-type": "/image/png",
        authorization: `Bearer ${localStorage.getItem('token')}`,
        credentials: 'include',
      }),
    });
  };

  const deleteImageById = async (id: number) => {
    const deleteUrl = `${baseUrl}/images/delete/${id}`;
    await fetch(deleteUrl, {
      method: 'DELETE',
      headers: new Headers({
        authorization: `Bearer ${localStorage.getItem('token')}`,
        credentials: 'include',
      }),
    });
  };

  return {
    uploadImageFile,
    deleteImageById,
  };
};
