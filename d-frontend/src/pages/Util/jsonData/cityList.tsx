export const cityList = {
  cities: [
    'Köln',
    'Bochum',
    'Essen',
    'Wattenscheid',
    'München',
    'Düsseldorf',
    'Dortmund',
    'Kiel',
    'Krefeld',
    'Berlin',
  ],
};
