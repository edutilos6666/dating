export const MULTI_SELECT_DELIM_FOR_LABEL = ' - ';
export const MOMENTJS_FORMAT = 'DD.MM.YYYY';

export const FEMALE_AVATAR_PATH = '/assets/avatar/female-avatar.png';
export const MALE_AVATAR_PATH = '/assets/avatar/male-avatar.png';

export const PRIVATE_CHAT_SUBSCRIBE_BASE_PATH: string = '/topic/private.chat.';
export const PRIVATE_CHAT_SEND_MESSAGE_BASE_PATH: string = '/app/private.chat.';
export const PRIVATE_CHANNEL_BASE_PATH: string = '/topic/user.notifcation.private.channel.';
export const PRIVATE_MESSAGE_COUNT_BASE_PATH: string = '/topic/user.notifcation.private.message.';
