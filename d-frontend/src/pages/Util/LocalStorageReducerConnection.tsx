export const _dummy = () => {
  return null;
};
// import {
//   EkfDetailsPageData,
//   emptyInstance_EkfDetailsPageData,
// } from '../EkfDetailsPage/TypesAndReducer';
// import moment from 'moment';
// import { MOMENTJS_FORMAT } from './Constants';
// import {
//   EkfStatusPageData,
//   emptyInstance_EkfStatusPageData,
// } from '../EkfStatusPage/TypesAndReducer';

// const keyPrefixer = (prefix: string, key: string): string => {
//   return `${prefix.toUpperCase()}_${key.toUpperCase()}`;
// };

// const keyPrefixerForEkfDetails = (key: string): string => {
//   return keyPrefixer('ekfDetails', key);
// };

// const keyPrefixerForEkfStatus = (key: string): string => {
//   return keyPrefixer('ekfStatus', key);
// };

// export const saveEkfDetailsStateIntoLocalStorage = (reducerData: EkfDetailsPageData): void => {
//   const keys: Array<string> = Object.keys(reducerData);
//   for (let key of keys) {
//     if (reducerData[key]) {
//       const val =
//         reducerData[key] instanceof Date
//           ? moment(reducerData[key]).format(MOMENTJS_FORMAT)
//           : reducerData[key];
//       localStorage.setItem(keyPrefixerForEkfDetails(key), JSON.stringify(val));
//     } else {
//       localStorage.removeItem(keyPrefixerForEkfDetails(key));
//     }
//   }
// };

// export const readEkfDetailsStateFromLocalStorage = (): EkfDetailsPageData => {
//   const keys: Array<string> = Object.keys(emptyInstance_EkfDetailsPageData);
//   const ret: EkfDetailsPageData = {};
//   for (let key of keys) {
//     const v = localStorage.getItem(keyPrefixerForEkfDetails(key));
//     if (!v) continue;
//     ret[key] = JSON.parse(v);
//     // console.log(ret[key], ret[key] instanceof String);
//     // debugger;
//     if (!(ret[key] instanceof Date) && String(ret[key]).match(/^\d{1,2}\.\d{1,2}\.\d{4}$/)) {
//       ret[key] = moment(ret[key], MOMENTJS_FORMAT).toDate();
//     }
//   }
//   return ret;
// };

// export const saveEkfStatusStateIntoLocalStorage = (reducerData: EkfStatusPageData): void => {
//   const keys: Array<string> = Object.keys(reducerData);
//   for (let key of keys) {
//     if (reducerData[key]) {
//       const val =
//         reducerData[key] instanceof Date
//           ? moment(reducerData[key]).format(MOMENTJS_FORMAT)
//           : reducerData[key];
//       localStorage.setItem(keyPrefixerForEkfStatus(key), JSON.stringify(val));
//     } else {
//       localStorage.removeItem(keyPrefixerForEkfStatus(key));
//     }
//   }
// };

// export const readEkfStatusStateFromLocalStorage = (): EkfStatusPageData => {
//   const keys: Array<string> = Object.keys(emptyInstance_EkfStatusPageData);
//   const ret: EkfStatusPageData = {};
//   for (let key of keys) {
//     const v = localStorage.getItem(keyPrefixerForEkfStatus(key));
//     if (!v) continue;
//     ret[key] = JSON.parse(v);
//     // console.log(ret[key]);
//     // debugger;
//     if (!(ret[key] instanceof Date) && String(ret[key]).match(/^\d{1,2}\.\d{1,2}\.\d{4}$/)) {
//       ret[key] = moment(ret[key], MOMENTJS_FORMAT).toDate();
//     }
//   }
//   return ret;
// };
