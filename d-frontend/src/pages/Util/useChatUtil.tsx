import useBasePath from '../../hooks/useBasePath';
import useAppController from '../../hooks/useAppController';

export const useChatUtil = () => {
  const { baseUrl, socketUrl } = useBasePath();
  const { userinfo, socket } = useAppController();
  const ESTABLISH_PRIVATE_CHANNEL_URL: string = `${baseUrl}/api/private-chat/channel`;

  const establishChatChannel = (history: any, userToId: number) => {
    const url = `${ESTABLISH_PRIVATE_CHANNEL_URL}`;
    fetch(url, {
      method: 'PUT',
      credentials: 'same-origin',
      headers: new Headers({
        Accept: 'application/json',
        'Content-type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
        credentials: 'include',
      }),
      body: JSON.stringify({ userFromId: parseInt(userinfo!.username!), userToId: userToId }),
    }).then(res => {
      res.json().then(channelId => {
        // channelId as number
        console.log('channelId = ', channelId);
        history.push(`/chat-history/${channelId}`);
      });
    });
  };
  return {
    establishChatChannel,
  };
};
