import useBasePath from '../../hooks/useBasePath';
import useAppController from '../../hooks/useAppController';
import { useState } from 'react';
import Stomp, { Client, Subscription, Message } from 'stompjs';
import SockJS from 'sockjs-client';

export const useSocketUtil = () => {
  const { socketUrl } = useBasePath();
  const { token } = useAppController();
  const [socket, setSocket] = useState<Client>();
  const [subscription, setSubscription] = useState<Subscription>();
  const socketConnectAndSubscribe = (
    destination: string,
    callbackForSubscription: (payload: Message) => any,
  ) => {
    let stompClient: Client;
    let subscription: Subscription;
    const sock = new SockJS(socketUrl!);
    stompClient = Stomp.over(sock);
    stompClient.connect({ authorization: token ? `${token}` : '' }, () => {
      subscription = stompClient.subscribe(destination, callbackForSubscription, {
        authorization: token ? `${token}` : '',
      });

      setSubscription(subscription);

      setSocket(stompClient);
    });
  };

  // socket and subscription are undefined!!!
  const socketDisconnect = () => {
    if (socket && !socket.connected && subscription) {
      socket.unsubscribe(subscription.id);
      socket.disconnect(() => {});
    }
    // socket && !socket.connected && socket.disconnect(() => {});
  };

  return {
    socketConnectAndSubscribe,
    socketDisconnect,
    socket,
  };
};
