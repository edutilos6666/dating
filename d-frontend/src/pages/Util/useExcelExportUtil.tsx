import XLSX, { ColInfo } from 'xlsx';
export const useExcelExportUtil = () => {
  const exportAlsExcel = (columns: string[], data: any[]) => {
    const colWidths: ColInfo[] = columns.map(one => {
      return { width: one.length + 10 };
    });

    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    wb.Props = {
      Title: `EXPORT_RUECKFORDERUNG_${new Date().getTime()}`,
      Subject: 'Export Rückforderung',
      Author: 'DDG',
      CreatedDate: new Date(),
    };

    wb.SheetNames.push('Sheet 1');
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet([columns, ...data]);
    wb.Sheets['Sheet 1'] = ws;
    ws['!cols'] = colWidths;
    const wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
    createActivateDeleteLinks(wbout);
  };

  const createActivateDeleteLinks = (str: any) => {
    var link1 = document.createElement('a');
    link1.setAttribute('download', `EXPORT_RUEFO_${new Date().getTime()}.xlsx`);
    link1.href = makeExcelFile(str);
    document.body.appendChild(link1);
    simulateClick(link1);

    document.body.removeChild(link1);
  };

  const simulateClick = (elem: HTMLAnchorElement) => {
    // Create our event (with options)
    var evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window,
    });
    // If cancelled, don't dispatch our event
    var canceled = !elem.dispatchEvent(evt);
  };

  const makeExcelFile = (input: BlobPart) => {
    var excelFile = null;
    var data = new Blob([convertBlobToOctet(input)], { type: 'application/octet-stream' });

    if (excelFile !== null) {
      window.URL.revokeObjectURL(excelFile);
    }

    excelFile = window.URL.createObjectURL(data);

    return excelFile;
  };

  function convertBlobToOctet(s: any) {
    var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
    var view = new Uint8Array(buf); //create uint8array as viewer
    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff; //convert to octet
    return buf;
  }

  return {
    exportAlsExcel,
  };
};
