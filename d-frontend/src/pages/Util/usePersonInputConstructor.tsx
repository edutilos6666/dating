import { PersonInput } from '../../data/datatypes';
import { RegisterData } from '../Security/Register/TypesAndReducer';
import { PersonProfileData } from '../../components/Person/Profile/TypesAndReducer';

export const usePersonInputConstructor = () => {
  const getPersonInputFromReducerData = (reducerData: RegisterData): PersonInput | undefined => {
    let ret: PersonInput | undefined;
    ret = {
      id: undefined,
      username: reducerData.username,
      password: reducerData.password,
      firstname: reducerData.firstname,
      lastname: reducerData.lastname,
      gender: {
        id: undefined,
        name: reducerData.gender,
      },
      birthdate: reducerData.birthdate,
      job: {
        id: undefined,
        companyName: reducerData.companyName,
        title: reducerData.jobTitle,
        wage: reducerData.wage,
      },
      address: {
        id: undefined,
        country: reducerData.country,
        city: reducerData.city,
        zipcode: reducerData.zipcode,
      },
      email: reducerData.email,
      aboutMe: reducerData.aboutMe,
      aboutIdealPartner: reducerData.aboutIdealPartner,
    };

    return ret;
  };

  return {
    getPersonInputFromReducerData,
  };
};
