export const useBetragUtil = () => {
  const convertNumberBetragToStringBetrag = (betrag: number): string => {
    const part1: string = `${parseInt(String(betrag / 100))}`;
    let part2: string = `${betrag % 100}`;
    part2 = part2.length === 1 ? `${part2}0` : part2;
    return `${part1},${part2}`;
  };

  const convertBetrag = (betrag: number | null | undefined) => {
    return `${betrag}`;
  };

  const convertStringBetragToNumberBetrag = (betrag: string): number => {
    const parts: string[] = betrag.split(',');
    if (parts.length === 2 && parts[0].length > 0 && parts[1].length > 0)
      return parseInt(parts[0]) * 100 + parseInt(parts[1]);
    if (parts.length === 2 && parts[0].length === 0) {
      return parseInt(parts[1]);
    }
    if (parts.length === 2 && parts[1].length === 0) {
      return parseInt(parts[0]) * 100;
    }
    return parseInt(parts[0]) * 100;
  };

  const checkIfBetragsAreEquals = (betrag1: string, betrag2: string): boolean => {
    const res1 = convertStringBetragToNumberBetrag(betrag1);
    const res2 = convertStringBetragToNumberBetrag(betrag2);
    return (isNaN(res1) && isNaN(res2)) || res1 === res2;
  };

  return {
    convertNumberBetragToStringBetrag,
    convertBetrag,
    convertStringBetragToNumberBetrag,
    checkIfBetragsAreEquals,
  };
};
