const useFetch = () => {
  const fetchGet = (url: string): Promise<Response> => {
    return fetch(url, {
      method: 'GET',
      credentials: 'same-origin',
      headers: new Headers({
        Accept: 'application/json',
        'Content-type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
        credentials: 'include',
      }),
    });
  };

  const fetchPost = (url: string): Promise<Response> => {
    return fetch(url, {
      method: 'POST',
      credentials: 'same-origin',
      headers: new Headers({
        Accept: 'application/json',
        'Content-type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
        credentials: 'include',
      }),
    });
  };

  return {
    fetchGet,
    fetchPost,
  };
};

export default useFetch;
