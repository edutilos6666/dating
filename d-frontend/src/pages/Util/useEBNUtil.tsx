export const useEBNUtil = () => {
  const EBN_MIN = 100000000;
  const EBN_MAX = 999999999;

  function isException(ebn: number, exceptions: string) {
    return exceptions.split(',').some(e => {
      // const [didMatch, from, to] = /(\d+)(?:-(\d+))?/.exec(e) || [false];
      const matchRes: RegExpExecArray | null = /(\d+)(?:-(\d+))?/.exec(e);

      if (!matchRes) return false;
      const didMatch = matchRes[0];
      const from = matchRes[1];
      const to = matchRes[2];

      if (!to) {
        return ebn === Number.parseInt(from, 10);
      }

      return ebn >= Number.parseInt(from, 10) && ebn <= Number.parseInt(to, 10);
    });
  }

  function validateEbn(input: string | number, exceptions: string) {
    let ebn;

    if (typeof input === 'string') {
      if (!/^[0-9]+$/.test(input)) {
        return false;
      }
      ebn = Number.parseInt(input, 10);
    } else if (typeof input === 'number') {
      if (!Number.isInteger(input)) {
        return false;
      }
      ebn = input;
    } else {
      return false;
    }

    if (typeof exceptions === 'string') {
      if (isException(ebn, exceptions)) {
        return true;
      }
    }

    if (ebn < EBN_MIN || ebn > EBN_MAX) {
      return false;
    }

    const ebnString = String(ebn);
    const digits = ebnString.split('').map(x => Number.parseInt(x, 10));
    const checkDigit = digits.pop();

    const calculatedCheckSum = digits
      // NOTE: pattern is fixed to the end of the string
      .reverse()
      .map((x, i) => (i % 2 === 0 ? x * 3 : x))
      .reduce((a, b) => a + b, 0);

    const calculatedCheckDigit = (10 - (calculatedCheckSum % 10)) % 10;

    return checkDigit === calculatedCheckDigit;
  }

  return {
    isException,
    validateEbn,
  };
};
