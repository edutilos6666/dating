import moment from 'moment';
export const useSelectDayInputHelpers = () => {
  const formatDate = (date: Date, format: string, locale: string): string => {
    return (
      moment(date)
        // .locale(locale)
        .format(format)
    );
  };
  const parseDate = (str: string, format: string, locale: string): Date | void => {
    return (
      moment(str, format)
        // .locale(locale)
        .toDate()
    );
  };
  return {
    formatDate,
    parseDate,
  };
};
