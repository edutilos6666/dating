export const useTimeUtil = () => {
  const optionsDate = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  };
  const optionsTime = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  };

  const optionsDateTime = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  };

  const convertTimestampToDate = (ts: string | undefined): Date => {
    if (ts !== undefined) return new Date(parseInt(ts));
    return new Date();
  };

  const convertAndFormatTimestamp = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleDateString('de-DE', optionsDate);
  };

  const formatDate = (date: Date): string => {
    return date.toLocaleDateString('de-DE', optionsDate);
  };

  const convertTimestampToDateString = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleDateString('de-DE', optionsDate);
  };

  const convertTimestampToTimeString = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleTimeString('de-DE', optionsTime);
  };

  const convertTimestampToDateTimeString = (ts: string | undefined): string => {
    const date: Date = convertTimestampToDate(ts);
    return date.toLocaleTimeString('de-DE', optionsDateTime);
  };

  const convertDateToUTCDate = (input: Date): Date => {
    return new Date(Date.UTC(input.getFullYear(), input.getMonth(), input.getDate()));
  };

  const convertDateTimeToUTCDateTime = (input: Date): Date => {
    return new Date(
      input.getUTCFullYear(),
      input.getUTCMonth(),
      input.getUTCDate(),
      input.getUTCHours(),
      input.getUTCMinutes(),
      input.getUTCSeconds(),
      input.getUTCMilliseconds(),
    );
  };

  const checkDatesAreEquals = (datum1: Date | null, datum2: Date | null): boolean => {
    if (datum1 === null && datum2 === null && datum1 === datum2) return true;
    else if (
      datum1 !== null &&
      datum2 !== null &&
      datum1.getFullYear() === datum2.getFullYear() &&
      datum1.getMonth() === datum2.getMonth() &&
      datum1.getDate() === datum2.getDate()
    )
      return true;
    return false;
  };

  const updateAndGetTimeForDate = (date: Date, lowerBound: boolean = true): number => {
    const tmp = new Date(date);
    if (lowerBound) {
      tmp.setHours(0);
      tmp.setMinutes(0);
      tmp.setSeconds(0);
      tmp.setMilliseconds(0);
    } else {
      tmp.setHours(23);
      tmp.setMinutes(59);
      tmp.setSeconds(59);
      tmp.setMilliseconds(999);
    }
    return tmp.getTime();
  };

  const updateAndGetDate = (
    date: Date | undefined,
    lowerBound: boolean = true,
  ): Date | undefined => {
    if (date === undefined) return date;
    const tmp = new Date(date);
    if (lowerBound) {
      tmp.setHours(0);
      tmp.setMinutes(0);
      tmp.setSeconds(0);
      tmp.setMilliseconds(0);
    } else {
      tmp.setHours(23);
      tmp.setMinutes(59);
      tmp.setSeconds(59);
      tmp.setMilliseconds(999);
    }
    return tmp;
  };

  const getMaxTimestamp = (tsList: any[]): string => {
    return `${Math.max(...tsList.map(one => parseFloat(one)))}`;
  };

  const calculateBirthdateFromAge = (age: number, lowerBound: boolean = true): Date | undefined => {
    let date = new Date();
    date.setFullYear(date.getFullYear() - age);
    // We have to set ms to fix number , because otherwise reducerdata state will be updated in every ms =>
    // new database request will be issued.
    // date.setHours(0);
    // date.setMinutes(0);
    // date.setSeconds(0);
    // date.setMilliseconds(0);
    return updateAndGetDate(date, lowerBound);
  };

  return {
    optionsDate,
    optionsTime,
    optionsDateTime,
    convertTimestampToDate,
    convertAndFormatTimestamp,
    formatDate,
    convertTimestampToDateString,
    convertTimestampToTimeString,
    convertTimestampToDateTimeString,
    convertDateToUTCDate,
    convertDateTimeToUTCDateTime,
    checkDatesAreEquals,
    updateAndGetTimeForDate,
    updateAndGetDate,
    getMaxTimestamp,
    calculateBirthdateFromAge,
  };
};
