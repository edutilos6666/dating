export const PROFILE_TAB = 'profile-tab';
export const MESSAGE_BOX_TAB = 'message-box-tab';
export const IMAGES_TAB = 'images-tab';
export const CHANGE_PASSWORD_TAB = 'change-password-tab';
export const DELETE_PROFILE_TAB = 'delete-profile-tab';

// MessageBox Tabs
export const MESSAGE_BOX_ALL_MESSAGES_TAB = 'message-box-all-messages-tab';
export const MESSAGE_BOX_INBOX_TAB = 'message-box-inbox-tab';
export const MESSAGE_BOX_FAVORITES_TAB = 'message-box-favorites-tab';
export const MESSAGE_BOX_BLOCKED_TAB = 'message-box-blocked-tab';
export const MESSAGE_BOX_DELETED_TAB = 'message-box-deleted-tab';
