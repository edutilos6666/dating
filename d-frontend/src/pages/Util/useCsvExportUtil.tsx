export const useCsvExportUtil = () => {
  const exportAlsCSV = (
    columns: string[],
    data: any[],
    lineSeparator: string = '\r\n',
    columnSeparator: string = ';',
  ) => {
    let str: string = columns.join(columnSeparator);
    str = `${str}${lineSeparator}`;
    data.forEach(one => (str = `${str}${one.join(columnSeparator)}${lineSeparator}`));
    createActivateDeleteLinks(str);
  };

  const createActivateDeleteLinks = (str: string) => {
    var link1 = document.createElement('a');
    link1.setAttribute('download', `EXPORT_RUEFO_${new Date().getTime()}.csv`);
    link1.href = makeTextFile(str);
    document.body.appendChild(link1);
    simulateClick(link1);

    document.body.removeChild(link1);
  };

  const simulateClick = (elem: HTMLAnchorElement) => {
    // Create our event (with options)
    var evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window,
    });
    // If cancelled, don't dispatch our event
    var canceled = !elem.dispatchEvent(evt);
  };

  const makeTextFile = (text: BlobPart) => {
    var textFile = null;
    var data = new Blob([text], { type: 'text/plain' });

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
      window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    // returns a URL you can use as a href
    return textFile;
  };

  return {
    exportAlsCSV,
  };
};
