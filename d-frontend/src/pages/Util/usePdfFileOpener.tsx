export const usePdfFileOpener = () => {
  const openPDF = (url: string) => {
    fetch(url, {
      method: 'GET',
      credentials: 'same-origin',
      headers: new Headers({
        Accept: 'application/json',
        'Content-type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
        credentials: 'include',
      }),
    })
      .then(res => {
        res.arrayBuffer().then(res2 => {
          const url = URL.createObjectURL(
            new Blob([res2], {
              type: 'application/pdf',
            }),
          );

          window.open(url);
        });
      })
      .catch(ex => {
        console.log(ex);
      });
  };
  return {
    openPDF,
  };
};
