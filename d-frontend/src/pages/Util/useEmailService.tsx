import useBasePath from '../../hooks/useBasePath';
import useFetch from './useFetch';
const useEmailService = (serviceName: string) => {
  const { baseUrl } = useBasePath();
  const serviceNameUrl = `${baseUrl}/${serviceName}`;
  const modelUrl = `${serviceNameUrl}/model`;
  const startUrl = `${serviceNameUrl}/start`;
  const stopUrl = `${serviceNameUrl}/stop`;
  const restartUrl = `${serviceNameUrl}/restart`;
  const getUpdateUrl = (period: number, timeUnit: string) => {
    return `${serviceNameUrl}/update/${period}/${timeUnit}`;
  };
  const { fetchGet, fetchPost } = useFetch();

  const getModel = (): Promise<Response> => {
    // convert response to json resp.json().then(data=> ...)
    return fetchGet(modelUrl);
  };

  const start = (): Promise<Response> => {
    return fetchPost(startUrl);
  };

  const stop = (): Promise<Response> => {
    return fetchPost(stopUrl);
  };

  const restart = (): Promise<Response> => {
    return fetchPost(restartUrl);
  };

  const update = (period: number, timeUnit: string): Promise<Response> => {
    return fetchPost(getUpdateUrl(period, timeUnit));
  };

  return {
    getModel,
    start,
    stop,
    restart,
    update,
  };
};

export default useEmailService;
