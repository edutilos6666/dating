import { useState, useEffect } from 'react';
import useBasePath from '../../hooks/useBasePath';
import { Person, PersonImage } from '../../data/datatypes';
import { getRemoteImage } from './ImageUtil';
import { FEMALE_AVATAR_PATH, MALE_AVATAR_PATH } from './Constants';
import { PersonFragment } from '../../data/person.generated';

export const useAvatarUtil = (person: Person | PersonFragment) => {
  const [imageBase64, setImageBase64] = useState<string>();
  const { baseUrl } = useBasePath();

  const filterImages = (images: PersonImage[]): PersonImage[] => {
    return images.filter(
      (one: PersonImage, index: number, array: PersonImage[]) => one.main === true,
    );
  };

  useEffect(() => {
    // let src = '/assets/avatar/male-avatar.png';
    if (
      person &&
      person.images &&
      person.images.length > 0 &&
      filterImages(person.images).length > 0
    ) {
      getRemoteImage(baseUrl!, filterImages(person.images)[0].id!).then(res => {
        setImageBase64(res);
      });
    } else if (person && person.gender && parseInt(person.gender.id!) > 1) {
      setImageBase64(FEMALE_AVATAR_PATH);
    } else setImageBase64(MALE_AVATAR_PATH);
  }, [person]);

  return {
    imageBase64,
  };
};
