import { BrowserRouter as Router, Route } from 'react-router-dom';
import React, { FC } from 'react';
import ChangePassword from '../Security/Login/ChangePassword';
import SecurityLogin from '../Security/Login/Login';
import SecurityProtectedRoute from '../Security/ProtectedRoute';
import { PersonDetails } from '../../components/Person/Details/Details';
import { Home } from '../Home/Home';
import { Search } from '../Search/Search';
import Header from '../../components/Header/Header';
import { ChatHistory } from '../../components/ChatHistory/ChatHistory';
import { Register } from '../Security/Register/Register';

interface AppRouterInterface {
  loggedIn: boolean;
}
const AppRouter: FC<AppRouterInterface> = ({ loggedIn }) => {
  return (
    <Router>
      <Header />
      <Route path="/register" component={Register} />
      <Route path="/login" component={SecurityLogin} />
      <Route path="/changepassword" component={ChangePassword} />
      <SecurityProtectedRoute loggedIn={loggedIn} path="/" exact component={Home} />
      <SecurityProtectedRoute loggedIn={loggedIn} path="/search" exact component={Search} />
      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path="/person-details/:id"
        exact
        component={PersonDetails}
      />
      <SecurityProtectedRoute
        loggedIn={loggedIn}
        path="/chat-history/:channelId"
        exact
        component={ChatHistory}
      />
    </Router>
  );
};
export default AppRouter;
