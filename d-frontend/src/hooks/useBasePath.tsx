export type CustomWindow = Window & WindowExtension;

interface ENV {
  REACT_APP_BASE_URL?: string;
  REACT_APP_SOCKET_PATH?: string;
}

interface WindowExtension {
  ENV?: ENV;
}

const useBasePath = () => {
  const browserWindow: CustomWindow = window;
  const baseUrl =
    browserWindow.ENV && browserWindow.ENV.REACT_APP_BASE_URL
      ? browserWindow.ENV.REACT_APP_BASE_URL
      : process.env.REACT_APP_BASE_URL;

  const socketUrl =
    browserWindow.ENV && browserWindow.ENV.REACT_APP_SOCKET_PATH
      ? browserWindow.ENV.REACT_APP_SOCKET_PATH
      : process.env.REACT_APP_SOCKET_PATH;
  return { baseUrl, socketUrl };
};

export default useBasePath;
