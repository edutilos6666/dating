import { useContext } from 'react';
import { APPIFrameContext } from '../context/AppIFrameContext';

export const useRouteRedirector = () => {
  const { sendMessage } = useContext(APPIFrameContext);

  const redirectToIndex = () => {
    sendMessage({ action: 'routeTo', transitionTo: 'index' });
  };

  const redirectToVersicherter = (kvnr: any) => {
    sendMessage({
      action: 'routeTo',
      transitionTo: 'stammdaten.versicherte.versicherter',
      transitionParameters: [kvnr],
    });
  };

  const redirectToInstitutionen = (ik: any) => {
    sendMessage({
      action: 'routeTo',
      transitionTo: 'stammdaten.institutionen.institution',
      transitionParameters: [ik],
    });
  };

  const redirectToAuskunft = (ebn: number) => {
    sendMessage({
      action: 'routeTo',
      transitionTo: 'auskunft.posteingang',
      transitionParameters: [ebn],
    });
  };

  const redirectToAuskunftByEbnAndRezeptNr = (ebn: number, reznr: number, position: number = 1) => {
    sendMessage({
      action: 'routeTo',
      transitionTo: 'auskunft.posteingang.rezept',
      transitionParameters: [ebn, reznr],
    });
  };

  const redirectToRuefoAnzeige = (ruefoId: string) => {
    sendMessage({
      action: 'routeTo',
      transitionTo: 'rueckforderung.anzeige.id',
      transitionParameters: [ruefoId],
    });
  };

  const redirectToUser = (userId: any) => {
    sendMessage({
      action: 'routeTo',
      transitionTo: 'user.user',
      transitionParameters: [userId],
    });
  };

  return {
    redirectToIndex,
    redirectToVersicherter,
    redirectToInstitutionen,
    redirectToAuskunft,
    redirectToRuefoAnzeige,
    redirectToAuskunftByEbnAndRezeptNr,
    redirectToUser,
  };
};
