/* eslint @typescript-eslint/no-explicit-any: "off" */
import { OperationVariables } from 'react-apollo';
import { useEffect, useState } from 'react';
import {
  GraphQlPaginationResult,
  GraphQlPaginationInput,
  GraphQlPaginationInputSort,
} from '../data/datatypes';
import { DataTableInfinityLoaderScrollNavigator } from '../components/DataTable/types';
/*
Generischer Typescript Ansatz hier überhaupt möglich?
*/
interface DataTableInfinityLoaderScrollNavigatorVariables extends OperationVariables {
  pagination?: GraphQlPaginationInput | null | undefined;
}
/**
 * Zuständig dafür einen generischen ScrollNavigator zurückzugeben auf den der DataTable zurückgreifen kann um Daten
 * nachzuladen (funktioniert nur, wenn {data['fieldName']} instanceof GraphQlPaginationResult ist )
 *
 * @param variables Variables {variables} die über den eigentlichen useQuery hook zurückgegeben werden
 * @param fieldName Feldname des Hauptobjektes wo das eigentliche GraphQlPaginationResult Objekt enthalten ist
 * @param result Das komplette {data} Objekt aus dem useQueryHook
 * @param fetchMore  {fetchMore} des useQueryHooks (Wird benötigt um Daten nachzuladen)
 * @param idColumnName Spaltenname in den Ergebnissen aus dem der Primäre Schlüssen kommt (key)
 * @param sorts Liste der GraphQlPaginationInputSort über welche dann gesteuert wird, welche Sortierungen gerade aktiv sind
 * @param setSorts "Setter" für die Liste der Sortierungen damit der InfinityLoader über den Tabellenkopf die Sortierungen überhaupt setzen kann und diese dann auch in den Variables der Query ankommen
 * @param refetch  {refetch} des useQueryHooks (Wird benötigt um Daten zuladen wenn sich z.B. die Sortierung ändert)
 * @param loading {loading} des useQueryHooks (Anzeige ob noch Daten geladen werden oder nicht)
 */
export default function useDataTableInfinityLoaderScrollNavigator(
  variables: DataTableInfinityLoaderScrollNavigatorVariables,
  fieldName: string,
  result: any,
  fetchMore: (param: any) => Promise<any>,
  idColumnName: string,
  refetch: (param: any) => Promise<any>,
  loading: boolean,
  sorts?: GraphQlPaginationInputSort[],
  setSorts?: (sorts: GraphQlPaginationInputSort[]) => void,
) {
  const [recordData, setRecordData] = useState<[]>([]);
  useEffect(() => {
    // console.log(`lading ${loading}`);
  }, [loading]);
  useEffect(() => {
    setRecordData(
      result && result[fieldName] && result[fieldName].result ? result[fieldName].result : [],
    );
  }, [fieldName, result]);
  const pageLoadMore = (data: GraphQlPaginationResult | null | undefined): Promise<any> => {
    return fetchMore({
      variables: {
        pagination: {
          offsetPage: data && data.pageInfo.number !== undefined ? data.pageInfo.number + 1 : 0,
          pageSize: 20,
          sorts,
        },
      },
      updateQuery: (prevResult: any, { fetchMoreResult }: any) => {
        const newResult = { ...prevResult };
        if (fetchMoreResult && fetchMoreResult[fieldName]) {
          newResult[fieldName].pageInfo = { ...fetchMoreResult[fieldName].pageInfo };
          const fetchMorePageInfo =
            fetchMoreResult && fetchMoreResult[fieldName]
              ? fetchMoreResult[fieldName].pageInfo
              : null;

          const newArr: Array<any> = [...newResult[fieldName].result];
          newArr.splice(
            data ? fetchMorePageInfo.number * data.pageInfo.size : 0,
            0,
            ...fetchMoreResult[fieldName].result,
          );
          newResult[fieldName].result = newArr;
        }

        // TODO: temporary solution???
        setRecordData(
          result && result[fieldName] && result[fieldName].result ? result[fieldName].result : [],
        );
        return { ...newResult };
      },
    });
  };

  const sortChanged = (sort: GraphQlPaginationInputSort, append: boolean): Promise<any> => {
    let newSorts: GraphQlPaginationInputSort[] = [];
    if (sorts) {
      newSorts = [...sorts];
      if (sorts && append) {
        const arrayIndex = sorts
          ? sorts.findIndex(osort => osort && osort.fieldName === sort.fieldName)
          : -1;
        if (arrayIndex >= 0) {
          newSorts[arrayIndex] = sort;
        } else {
          newSorts[sorts.length] = sort;
        }
      } else {
        newSorts = [sort];
      }
      if (setSorts) setSorts(newSorts);
    }

    return refetch({
      variables: {
        ...variables,
        pagination: {
          ...variables.pagination,
          sorts: newSorts,
        },
      },

      updateQuery: (prevResult: any, { fetchMoreResult }: any) => {
        let newResult = { ...prevResult };
        if (fetchMoreResult) {
          newResult = { ...fetchMoreResult };
        }
        return newResult;
      },
    });
  };

  const getKeyForData = (data: any, index: number) => {
    if (data && data[idColumnName]) return data[idColumnName];
    return index;
  };

  const navigator: DataTableInfinityLoaderScrollNavigator = {
    data: result && result[fieldName] ? result[fieldName] : [],
    recordData,
    pageLoadMore,
    sortChanged,
    sorts: variables.pagination && variables.pagination.sorts ? variables.pagination.sorts : [],
    getKeyForData,
    loading,
  };

  return navigator;
}
