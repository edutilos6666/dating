import * as jwt from 'jsonwebtoken';

import { useContext, useEffect, useCallback, useState } from 'react';
import { AppContext, UserInfoInterface } from '../context/AppContext';
import {
  useGetPublicKeyQuery,
  useRefreshTokenMutation,
  useLogoutMutation,
} from '../data/auth.generated';
import Stomp from 'stompjs';

interface TokenInterface {
  iat?: number;
  customer?: number;
  firstName?: string;
  lastName?: string;
  picture?: string;
  roles?: [string];
  exp?: number;
  sub?: string;
}

interface AppControllerInterface {
  token: string | undefined | null;
  setToken: (token: string | undefined | null) => void;
  loggedIn: boolean | undefined | null;
  setLoggedIn: (loggedIn: boolean) => void;
  userinfo: UserInfoInterface | undefined | null;
  checkSessionInfo: () => void;
  remoteLogout: () => void;
  setUserInfo: (info: UserInfoInterface) => void;
  isLoggedInInit: () => boolean | undefined | null | void;
  logout: () => void;
  refreshAccessToken: () => void;
  socket: Stomp.Client | undefined;
  socketConnected: boolean;
}

// TODO der ganze Controller sollte ggf. besser in den Store ausgelagert werden oder im Context funktionieren z.B. über reducer

const useAppController = (): AppControllerInterface => {
  const {
    token,
    setToken,
    loggedIn,
    setLoggedIn,
    userinfo,
    setLastCheckDate,
    setUserInfo,
    socket,
    socketConnected,
  } = useContext(AppContext);
  const { data: publicKeyData } = useGetPublicKeyQuery();
  const [refreshTokenMutation] = useRefreshTokenMutation();

  const [logoutMutation] = useLogoutMutation();

  const logout = useCallback(() => {
    localStorage.removeItem('sessionInfo');
    localStorage.removeItem('token');
    setLoggedIn(false);
    setToken(null);
    window.location.reload();
  }, [setLoggedIn, setToken]);

  const refreshAccessToken = () => {
    console.log('refreshAccessToken');
    refreshTokenMutation({
      // eslint-disable-next-line no-shadow
      update: (proxy, { data: mutateDate }) => {
        if (mutateDate) {
          const { refreshToken } = mutateDate;
          if (refreshToken && refreshToken.loginResponse) {
            setToken(refreshToken.token);
          }
        }
      },
    });
  };
  const calculateRemainderTimePercent = (iat: number, exp: number) => {
    const startDiffSeconds = exp - iat;
    const currentDiffSeconds = exp - Date.now() / 1000;
    const percent = 100 - (currentDiffSeconds / startDiffSeconds) * 100;
    return percent;
  };
  const checkSessionInfo = () => {
    const sessioninfo = navigator.cookieEnabled ? localStorage.getItem('sessionInfo') : null;
    if (sessioninfo) {
      const sessionObject = JSON.parse(sessioninfo);
      if (sessionObject.exp < Date.now() / 1000) {
        logout();
      } else {
        const timePercent = calculateRemainderTimePercent(sessionObject.iat, sessionObject.exp);
        if (userinfo && userinfo.username !== sessionObject.userinfo.username) {
          window.location.reload();
        } else if (userinfo && loggedIn) {
          if (timePercent > 10) {
            refreshAccessToken();
          }
        } else {
          setUserInfo(sessionObject.userinfo);
          setLoggedIn(true);
        }
      }
    }
  };
  useEffect(() => {
    setToken(localStorage.getItem('token'));
  }, [setToken]);
  const refreshToken = () => {
    if (token && token.length > 0 && publicKeyData && publicKeyData.getPublicKey) {
      const decodeWithoutVerify = jwt.decode(token) as TokenInterface;
      // let publicKey =publicKey;
      try {
        console.log('pub', publicKeyData);
        jwt.verify(token, publicKeyData ? publicKeyData.getPublicKey.value : '');
      } catch (e) {
        console.log('verifyfailed', e);
        localStorage.removeItem('sessionInfo');
        localStorage.removeItem('token');
        setToken(null);
        setLastCheckDate(null);
        setLoggedIn(false);
        setUserInfo(null);
        return;
      }
      if (decodeWithoutVerify != null) {
        setLastCheckDate(decodeWithoutVerify.iat);
        const userInfo = {
          customer: decodeWithoutVerify.customer,
          vorname: decodeWithoutVerify.firstName,
          nachname: decodeWithoutVerify.lastName,
          profilbild: decodeWithoutVerify.picture,
          username: decodeWithoutVerify.sub,
          roles: decodeWithoutVerify.roles,
        };
        setUserInfo(userInfo);
        setLoggedIn(true);
        console.log('auth success');
        localStorage.setItem('token', token);
        localStorage.setItem(
          'sessionInfo',
          JSON.stringify({
            exp: decodeWithoutVerify.exp,
            iat: decodeWithoutVerify.iat,
            userinfo: userInfo,
          }),
        );
      }
    } else {
      checkSessionInfo();
    }
  };

  useEffect(() => {
    refreshToken();
    console.log('token changed', token);
  }, [token, publicKeyData]); // eslint-disable-line react-hooks/exhaustive-deps

  const isLoggedInInit = () => {
    if (!loggedIn) {
      return refreshToken();
    }
    return loggedIn;
  };

  const remoteLogout = () => {
    logout();
    logoutMutation({
      update: () => {
        logout();
      },
    });

    /*
        TODO Backend Destroy Token
        store.destroy('auth', null, {force: true}).then((result) => {
            setTimeout(() => {
                logout();
            }, 100);
    
        }); */
  };

  return {
    checkSessionInfo,
    setToken,
    token,
    loggedIn,
    setLoggedIn,
    remoteLogout,
    userinfo,
    setUserInfo,
    isLoggedInInit,
    logout,
    refreshAccessToken,
    socket,
    socketConnected,
  };
};
export default useAppController;
