import * as Types from './datatypes';

import { PersonFragment } from './person.generated';
import gql from 'graphql-tag';
import { PersonFragmentDoc } from './person.generated';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type PrivateChannelFragment = (
  Pick<Types.PrivateChannel, 'id'>
  & { userFrom: Types.Maybe<PersonFragment>, userTo: Types.Maybe<PersonFragment>, messages: Types.Maybe<Array<Types.Maybe<(
    Pick<Types.PrivateMessage, 'id' | 'content' | 'opened'>
    & { userFrom: Types.Maybe<Pick<Types.Person, 'id'>>, userTo: Types.Maybe<Pick<Types.Person, 'id'>> }
  )>>> }
);

export type FindPrivateChannelByIdQueryVariables = {
  id?: Types.Maybe<Types.Scalars['Int']>
};


export type FindPrivateChannelByIdQuery = { findPrivateChannelById: Types.Maybe<PrivateChannelFragment> };

export type FindPrivateChannelListByQueryVariables = {
  userId?: Types.Maybe<Types.Scalars['Int']>
};


export type FindPrivateChannelListByQuery = { findPrivateChannelListBy: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateChannelFragment> }> };

export type FindChannelsFromFavoriteListForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindChannelsFromFavoriteListForPersonQuery = { findChannelsFromFavoriteListForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateChannelFragment> }> };

export type FindChannelsFromBlockedListForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindChannelsFromBlockedListForPersonQuery = { findChannelsFromBlockedListForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateChannelFragment> }> };

export type FindChannelsFromDeletedListForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindChannelsFromDeletedListForPersonQuery = { findChannelsFromDeletedListForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateChannelFragment> }> };

export type FindChannelsForNormalMessagesForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindChannelsForNormalMessagesForPersonQuery = { findChannelsForNormalMessagesForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateChannelFragment> }> };
export const PrivateChannelFragmentDoc = gql`
    fragment PrivateChannel on PrivateChannel {
  id
  userFrom {
    ...Person
  }
  userTo {
    ...Person
  }
  messages {
    id
    content
    opened
    userFrom {
      id
    }
    userTo {
      id
    }
  }
}
    ${PersonFragmentDoc}`;
export const FindPrivateChannelByIdDocument = gql`
    query FindPrivateChannelById($id: Int) {
  findPrivateChannelById(id: $id) {
    ...PrivateChannel
  }
}
    ${PrivateChannelFragmentDoc}`;

    export function useFindPrivateChannelByIdQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindPrivateChannelByIdQuery, FindPrivateChannelByIdQueryVariables>) {
      return ApolloReactHooks.useQuery<FindPrivateChannelByIdQuery, FindPrivateChannelByIdQueryVariables>(FindPrivateChannelByIdDocument, baseOptions);
    }
      export function useFindPrivateChannelByIdLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindPrivateChannelByIdQuery, FindPrivateChannelByIdQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindPrivateChannelByIdQuery, FindPrivateChannelByIdQueryVariables>(FindPrivateChannelByIdDocument, baseOptions);
      }
      
export type FindPrivateChannelByIdQueryHookResult = ReturnType<typeof useFindPrivateChannelByIdQuery>;
export type FindPrivateChannelByIdQueryResult = ApolloReactCommon.QueryResult<FindPrivateChannelByIdQuery, FindPrivateChannelByIdQueryVariables>;
export const FindPrivateChannelListByDocument = gql`
    query FindPrivateChannelListBy($userId: Int) {
  findPrivateChannelListBy(userId: $userId) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateChannel
    }
  }
}
    ${PrivateChannelFragmentDoc}`;

    export function useFindPrivateChannelListByQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindPrivateChannelListByQuery, FindPrivateChannelListByQueryVariables>) {
      return ApolloReactHooks.useQuery<FindPrivateChannelListByQuery, FindPrivateChannelListByQueryVariables>(FindPrivateChannelListByDocument, baseOptions);
    }
      export function useFindPrivateChannelListByLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindPrivateChannelListByQuery, FindPrivateChannelListByQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindPrivateChannelListByQuery, FindPrivateChannelListByQueryVariables>(FindPrivateChannelListByDocument, baseOptions);
      }
      
export type FindPrivateChannelListByQueryHookResult = ReturnType<typeof useFindPrivateChannelListByQuery>;
export type FindPrivateChannelListByQueryResult = ApolloReactCommon.QueryResult<FindPrivateChannelListByQuery, FindPrivateChannelListByQueryVariables>;
export const FindChannelsFromFavoriteListForPersonDocument = gql`
    query FindChannelsFromFavoriteListForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findChannelsFromFavoriteListForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateChannel
    }
  }
}
    ${PrivateChannelFragmentDoc}`;

    export function useFindChannelsFromFavoriteListForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindChannelsFromFavoriteListForPersonQuery, FindChannelsFromFavoriteListForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindChannelsFromFavoriteListForPersonQuery, FindChannelsFromFavoriteListForPersonQueryVariables>(FindChannelsFromFavoriteListForPersonDocument, baseOptions);
    }
      export function useFindChannelsFromFavoriteListForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindChannelsFromFavoriteListForPersonQuery, FindChannelsFromFavoriteListForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindChannelsFromFavoriteListForPersonQuery, FindChannelsFromFavoriteListForPersonQueryVariables>(FindChannelsFromFavoriteListForPersonDocument, baseOptions);
      }
      
export type FindChannelsFromFavoriteListForPersonQueryHookResult = ReturnType<typeof useFindChannelsFromFavoriteListForPersonQuery>;
export type FindChannelsFromFavoriteListForPersonQueryResult = ApolloReactCommon.QueryResult<FindChannelsFromFavoriteListForPersonQuery, FindChannelsFromFavoriteListForPersonQueryVariables>;
export const FindChannelsFromBlockedListForPersonDocument = gql`
    query FindChannelsFromBlockedListForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findChannelsFromBlockedListForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateChannel
    }
  }
}
    ${PrivateChannelFragmentDoc}`;

    export function useFindChannelsFromBlockedListForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindChannelsFromBlockedListForPersonQuery, FindChannelsFromBlockedListForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindChannelsFromBlockedListForPersonQuery, FindChannelsFromBlockedListForPersonQueryVariables>(FindChannelsFromBlockedListForPersonDocument, baseOptions);
    }
      export function useFindChannelsFromBlockedListForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindChannelsFromBlockedListForPersonQuery, FindChannelsFromBlockedListForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindChannelsFromBlockedListForPersonQuery, FindChannelsFromBlockedListForPersonQueryVariables>(FindChannelsFromBlockedListForPersonDocument, baseOptions);
      }
      
export type FindChannelsFromBlockedListForPersonQueryHookResult = ReturnType<typeof useFindChannelsFromBlockedListForPersonQuery>;
export type FindChannelsFromBlockedListForPersonQueryResult = ApolloReactCommon.QueryResult<FindChannelsFromBlockedListForPersonQuery, FindChannelsFromBlockedListForPersonQueryVariables>;
export const FindChannelsFromDeletedListForPersonDocument = gql`
    query FindChannelsFromDeletedListForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findChannelsFromDeletedListForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateChannel
    }
  }
}
    ${PrivateChannelFragmentDoc}`;

    export function useFindChannelsFromDeletedListForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindChannelsFromDeletedListForPersonQuery, FindChannelsFromDeletedListForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindChannelsFromDeletedListForPersonQuery, FindChannelsFromDeletedListForPersonQueryVariables>(FindChannelsFromDeletedListForPersonDocument, baseOptions);
    }
      export function useFindChannelsFromDeletedListForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindChannelsFromDeletedListForPersonQuery, FindChannelsFromDeletedListForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindChannelsFromDeletedListForPersonQuery, FindChannelsFromDeletedListForPersonQueryVariables>(FindChannelsFromDeletedListForPersonDocument, baseOptions);
      }
      
export type FindChannelsFromDeletedListForPersonQueryHookResult = ReturnType<typeof useFindChannelsFromDeletedListForPersonQuery>;
export type FindChannelsFromDeletedListForPersonQueryResult = ApolloReactCommon.QueryResult<FindChannelsFromDeletedListForPersonQuery, FindChannelsFromDeletedListForPersonQueryVariables>;
export const FindChannelsForNormalMessagesForPersonDocument = gql`
    query FindChannelsForNormalMessagesForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findChannelsForNormalMessagesForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateChannel
    }
  }
}
    ${PrivateChannelFragmentDoc}`;

    export function useFindChannelsForNormalMessagesForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindChannelsForNormalMessagesForPersonQuery, FindChannelsForNormalMessagesForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindChannelsForNormalMessagesForPersonQuery, FindChannelsForNormalMessagesForPersonQueryVariables>(FindChannelsForNormalMessagesForPersonDocument, baseOptions);
    }
      export function useFindChannelsForNormalMessagesForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindChannelsForNormalMessagesForPersonQuery, FindChannelsForNormalMessagesForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindChannelsForNormalMessagesForPersonQuery, FindChannelsForNormalMessagesForPersonQueryVariables>(FindChannelsForNormalMessagesForPersonDocument, baseOptions);
      }
      
export type FindChannelsForNormalMessagesForPersonQueryHookResult = ReturnType<typeof useFindChannelsForNormalMessagesForPersonQuery>;
export type FindChannelsForNormalMessagesForPersonQueryResult = ApolloReactCommon.QueryResult<FindChannelsForNormalMessagesForPersonQuery, FindChannelsForNormalMessagesForPersonQueryVariables>;