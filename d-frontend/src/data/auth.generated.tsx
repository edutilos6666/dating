import * as Types from './datatypes';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type GetPublicKeyQueryVariables = {};


export type GetPublicKeyQuery = { getPublicKey: Pick<Types.StringResult, 'value'> };

export type AllAuthInformationFragment = (
  Pick<Types.AuthInformation, 'status' | 'token'>
  & { loginError: Types.Maybe<Pick<Types.LoginError, 'error' | 'errorDescription' | 'xError'>>, loginResponse: Types.Maybe<Pick<Types.LoginResponse, 'access_token' | 'expires_in' | 'lastLogin' | 'refresh_token' | 'token_type' | 'userid'>> }
);

export type LogoutMutationVariables = {};


export type LogoutMutation = Pick<Types.Mutation, 'logout'>;

export type LoginMutationVariables = {
  username?: Types.Maybe<Types.Scalars['String']>,
  password?: Types.Maybe<Types.Scalars['String']>
};


export type LoginMutation = { loginWithUserNamePassword: Types.Maybe<AllAuthInformationFragment> };

export type LoginWithTokenMutationVariables = {
  token?: Types.Maybe<Types.Scalars['String']>
};


export type LoginWithTokenMutation = { loginWithToken: Types.Maybe<AllAuthInformationFragment> };

export type RefreshTokenMutationVariables = {};


export type RefreshTokenMutation = { refreshToken: Types.Maybe<AllAuthInformationFragment> };

export type ChangeUserPasswordMutationVariables = {
  username?: Types.Maybe<Types.Scalars['String']>,
  password?: Types.Maybe<Types.Scalars['String']>,
  newPassword?: Types.Maybe<Types.Scalars['String']>
};


export type ChangeUserPasswordMutation = { changeUserPassword: Types.Maybe<AllAuthInformationFragment> };
export const AllAuthInformationFragmentDoc = gql`
    fragment ALLAuthInformation on AuthInformation {
  loginError {
    error
    errorDescription
    xError
  }
  loginResponse {
    access_token
    expires_in
    lastLogin
    refresh_token
    token_type
    userid
  }
  status
  token
}
    `;
export const GetPublicKeyDocument = gql`
    query getPublicKey {
  getPublicKey {
    value
  }
}
    `;

    export function useGetPublicKeyQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPublicKeyQuery, GetPublicKeyQueryVariables>) {
      return ApolloReactHooks.useQuery<GetPublicKeyQuery, GetPublicKeyQueryVariables>(GetPublicKeyDocument, baseOptions);
    }
      export function useGetPublicKeyLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPublicKeyQuery, GetPublicKeyQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<GetPublicKeyQuery, GetPublicKeyQueryVariables>(GetPublicKeyDocument, baseOptions);
      }
      
export type GetPublicKeyQueryHookResult = ReturnType<typeof useGetPublicKeyQuery>;
export type GetPublicKeyQueryResult = ApolloReactCommon.QueryResult<GetPublicKeyQuery, GetPublicKeyQueryVariables>;
export const LogoutDocument = gql`
    mutation logout {
  logout
}
    `;

    export function useLogoutMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
      return ApolloReactHooks.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, baseOptions);
    }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = ApolloReactCommon.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = ApolloReactCommon.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const LoginDocument = gql`
    mutation login($username: String, $password: String) {
  loginWithUserNamePassword(username: $username, password: $password) {
    ...ALLAuthInformation
  }
}
    ${AllAuthInformationFragmentDoc}`;

    export function useLoginMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
      return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
    }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LoginWithTokenDocument = gql`
    mutation loginWithToken($token: String) {
  loginWithToken(token: $token, username: "") {
    ...ALLAuthInformation
  }
}
    ${AllAuthInformationFragmentDoc}`;

    export function useLoginWithTokenMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginWithTokenMutation, LoginWithTokenMutationVariables>) {
      return ApolloReactHooks.useMutation<LoginWithTokenMutation, LoginWithTokenMutationVariables>(LoginWithTokenDocument, baseOptions);
    }
export type LoginWithTokenMutationHookResult = ReturnType<typeof useLoginWithTokenMutation>;
export type LoginWithTokenMutationResult = ApolloReactCommon.MutationResult<LoginWithTokenMutation>;
export type LoginWithTokenMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginWithTokenMutation, LoginWithTokenMutationVariables>;
export const RefreshTokenDocument = gql`
    mutation refreshToken {
  refreshToken {
    ...ALLAuthInformation
  }
}
    ${AllAuthInformationFragmentDoc}`;

    export function useRefreshTokenMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RefreshTokenMutation, RefreshTokenMutationVariables>) {
      return ApolloReactHooks.useMutation<RefreshTokenMutation, RefreshTokenMutationVariables>(RefreshTokenDocument, baseOptions);
    }
export type RefreshTokenMutationHookResult = ReturnType<typeof useRefreshTokenMutation>;
export type RefreshTokenMutationResult = ApolloReactCommon.MutationResult<RefreshTokenMutation>;
export type RefreshTokenMutationOptions = ApolloReactCommon.BaseMutationOptions<RefreshTokenMutation, RefreshTokenMutationVariables>;
export const ChangeUserPasswordDocument = gql`
    mutation changeUserPassword($username: String, $password: String, $newPassword: String) {
  changeUserPassword(username: $username, password: $password, newPassword: $newPassword) {
    ...ALLAuthInformation
  }
}
    ${AllAuthInformationFragmentDoc}`;

    export function useChangeUserPasswordMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<ChangeUserPasswordMutation, ChangeUserPasswordMutationVariables>) {
      return ApolloReactHooks.useMutation<ChangeUserPasswordMutation, ChangeUserPasswordMutationVariables>(ChangeUserPasswordDocument, baseOptions);
    }
export type ChangeUserPasswordMutationHookResult = ReturnType<typeof useChangeUserPasswordMutation>;
export type ChangeUserPasswordMutationResult = ApolloReactCommon.MutationResult<ChangeUserPasswordMutation>;
export type ChangeUserPasswordMutationOptions = ApolloReactCommon.BaseMutationOptions<ChangeUserPasswordMutation, ChangeUserPasswordMutationVariables>;