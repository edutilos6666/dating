import * as Types from './datatypes';

import gql from 'graphql-tag';
export type AddressFragment = Pick<Types.Address, 'id' | 'country' | 'city' | 'zipcode'>;
export const AddressFragmentDoc = gql`
    fragment Address on Address {
  id
  country
  city
  zipcode
}
    `;