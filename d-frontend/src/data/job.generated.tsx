import * as Types from './datatypes';

import gql from 'graphql-tag';
export type JobFragment = Pick<Types.Job, 'id' | 'title' | 'companyName' | 'wage'>;
export const JobFragmentDoc = gql`
    fragment Job on Job {
  id
  title
  companyName
  wage
}
    `;