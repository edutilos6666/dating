import * as Types from './datatypes';

import { PersonFragment } from './person.generated';
import gql from 'graphql-tag';
import { PersonFragmentDoc } from './person.generated';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type PrivateMessageFragment = (
  Pick<Types.PrivateMessage, 'id' | 'content' | 'created' | 'opened'>
  & { userFrom: Types.Maybe<PersonFragment>, userTo: Types.Maybe<PersonFragment>, channel: Types.Maybe<Pick<Types.PrivateChannel, 'id'>> }
);

export type FindPrivateMessageListByQueryVariables = {
  channelId?: Types.Maybe<Types.Scalars['Int']>,
  userFromId?: Types.Maybe<Types.Scalars['Int']>,
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindPrivateMessageListByQuery = { findPrivateMessageListBy: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateMessageFragment> }> };

export type FindMessagesFromFavoriteListForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindMessagesFromFavoriteListForPersonQuery = { findMessagesFromFavoriteListForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateMessageFragment> }> };

export type FindMessagesFromBlockedListForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindMessagesFromBlockedListForPersonQuery = { findMessagesFromBlockedListForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateMessageFragment> }> };

export type FindMessagesFromDeletedListForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindMessagesFromDeletedListForPersonQuery = { findMessagesFromDeletedListForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateMessageFragment> }> };

export type FindNormalMessagesForPersonQueryVariables = {
  userToId?: Types.Maybe<Types.Scalars['Int']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindNormalMessagesForPersonQuery = { findNormalMessagesForPerson: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateMessageFragment> }> };

export type FilterMessagesByQueryVariables = {
  content?: Types.Maybe<Types.Scalars['String']>,
  createdFrom?: Types.Maybe<Types.Scalars['DateTime']>,
  createdTo?: Types.Maybe<Types.Scalars['DateTime']>,
  opened?: Types.Maybe<Types.Scalars['Boolean']>,
  username?: Types.Maybe<Types.Scalars['String']>,
  firstname?: Types.Maybe<Types.Scalars['String']>,
  lastname?: Types.Maybe<Types.Scalars['String']>,
  minBirthdate?: Types.Maybe<Types.Scalars['DateTime']>,
  maxBirthdate?: Types.Maybe<Types.Scalars['DateTime']>,
  wageFrom?: Types.Maybe<Types.Scalars['Float']>,
  wageTo?: Types.Maybe<Types.Scalars['Float']>,
  country?: Types.Maybe<Types.Scalars['String']>,
  city?: Types.Maybe<Types.Scalars['String']>,
  zipcode?: Types.Maybe<Types.Scalars['String']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FilterMessagesByQuery = { filterMessagesBy: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PrivateMessageFragment> }> };
export const PrivateMessageFragmentDoc = gql`
    fragment PrivateMessage on PrivateMessage {
  id
  userFrom {
    ...Person
  }
  userTo {
    ...Person
  }
  channel {
    id
  }
  content
  created
  opened
}
    ${PersonFragmentDoc}`;
export const FindPrivateMessageListByDocument = gql`
    query FindPrivateMessageListBy($channelId: Int, $userFromId: Int, $userToId: Int, $pagination: GraphQLPaginationInput) {
  findPrivateMessageListBy(channelId: $channelId, userFromId: $userFromId, userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateMessage
    }
  }
}
    ${PrivateMessageFragmentDoc}`;

    export function useFindPrivateMessageListByQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindPrivateMessageListByQuery, FindPrivateMessageListByQueryVariables>) {
      return ApolloReactHooks.useQuery<FindPrivateMessageListByQuery, FindPrivateMessageListByQueryVariables>(FindPrivateMessageListByDocument, baseOptions);
    }
      export function useFindPrivateMessageListByLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindPrivateMessageListByQuery, FindPrivateMessageListByQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindPrivateMessageListByQuery, FindPrivateMessageListByQueryVariables>(FindPrivateMessageListByDocument, baseOptions);
      }
      
export type FindPrivateMessageListByQueryHookResult = ReturnType<typeof useFindPrivateMessageListByQuery>;
export type FindPrivateMessageListByQueryResult = ApolloReactCommon.QueryResult<FindPrivateMessageListByQuery, FindPrivateMessageListByQueryVariables>;
export const FindMessagesFromFavoriteListForPersonDocument = gql`
    query FindMessagesFromFavoriteListForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findMessagesFromFavoriteListForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateMessage
    }
  }
}
    ${PrivateMessageFragmentDoc}`;

    export function useFindMessagesFromFavoriteListForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindMessagesFromFavoriteListForPersonQuery, FindMessagesFromFavoriteListForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindMessagesFromFavoriteListForPersonQuery, FindMessagesFromFavoriteListForPersonQueryVariables>(FindMessagesFromFavoriteListForPersonDocument, baseOptions);
    }
      export function useFindMessagesFromFavoriteListForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindMessagesFromFavoriteListForPersonQuery, FindMessagesFromFavoriteListForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindMessagesFromFavoriteListForPersonQuery, FindMessagesFromFavoriteListForPersonQueryVariables>(FindMessagesFromFavoriteListForPersonDocument, baseOptions);
      }
      
export type FindMessagesFromFavoriteListForPersonQueryHookResult = ReturnType<typeof useFindMessagesFromFavoriteListForPersonQuery>;
export type FindMessagesFromFavoriteListForPersonQueryResult = ApolloReactCommon.QueryResult<FindMessagesFromFavoriteListForPersonQuery, FindMessagesFromFavoriteListForPersonQueryVariables>;
export const FindMessagesFromBlockedListForPersonDocument = gql`
    query FindMessagesFromBlockedListForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findMessagesFromBlockedListForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateMessage
    }
  }
}
    ${PrivateMessageFragmentDoc}`;

    export function useFindMessagesFromBlockedListForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindMessagesFromBlockedListForPersonQuery, FindMessagesFromBlockedListForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindMessagesFromBlockedListForPersonQuery, FindMessagesFromBlockedListForPersonQueryVariables>(FindMessagesFromBlockedListForPersonDocument, baseOptions);
    }
      export function useFindMessagesFromBlockedListForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindMessagesFromBlockedListForPersonQuery, FindMessagesFromBlockedListForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindMessagesFromBlockedListForPersonQuery, FindMessagesFromBlockedListForPersonQueryVariables>(FindMessagesFromBlockedListForPersonDocument, baseOptions);
      }
      
export type FindMessagesFromBlockedListForPersonQueryHookResult = ReturnType<typeof useFindMessagesFromBlockedListForPersonQuery>;
export type FindMessagesFromBlockedListForPersonQueryResult = ApolloReactCommon.QueryResult<FindMessagesFromBlockedListForPersonQuery, FindMessagesFromBlockedListForPersonQueryVariables>;
export const FindMessagesFromDeletedListForPersonDocument = gql`
    query FindMessagesFromDeletedListForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findMessagesFromDeletedListForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateMessage
    }
  }
}
    ${PrivateMessageFragmentDoc}`;

    export function useFindMessagesFromDeletedListForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindMessagesFromDeletedListForPersonQuery, FindMessagesFromDeletedListForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindMessagesFromDeletedListForPersonQuery, FindMessagesFromDeletedListForPersonQueryVariables>(FindMessagesFromDeletedListForPersonDocument, baseOptions);
    }
      export function useFindMessagesFromDeletedListForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindMessagesFromDeletedListForPersonQuery, FindMessagesFromDeletedListForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindMessagesFromDeletedListForPersonQuery, FindMessagesFromDeletedListForPersonQueryVariables>(FindMessagesFromDeletedListForPersonDocument, baseOptions);
      }
      
export type FindMessagesFromDeletedListForPersonQueryHookResult = ReturnType<typeof useFindMessagesFromDeletedListForPersonQuery>;
export type FindMessagesFromDeletedListForPersonQueryResult = ApolloReactCommon.QueryResult<FindMessagesFromDeletedListForPersonQuery, FindMessagesFromDeletedListForPersonQueryVariables>;
export const FindNormalMessagesForPersonDocument = gql`
    query FindNormalMessagesForPerson($userToId: Int, $pagination: GraphQLPaginationInput) {
  findNormalMessagesForPerson(userToId: $userToId, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateMessage
    }
  }
}
    ${PrivateMessageFragmentDoc}`;

    export function useFindNormalMessagesForPersonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindNormalMessagesForPersonQuery, FindNormalMessagesForPersonQueryVariables>) {
      return ApolloReactHooks.useQuery<FindNormalMessagesForPersonQuery, FindNormalMessagesForPersonQueryVariables>(FindNormalMessagesForPersonDocument, baseOptions);
    }
      export function useFindNormalMessagesForPersonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindNormalMessagesForPersonQuery, FindNormalMessagesForPersonQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindNormalMessagesForPersonQuery, FindNormalMessagesForPersonQueryVariables>(FindNormalMessagesForPersonDocument, baseOptions);
      }
      
export type FindNormalMessagesForPersonQueryHookResult = ReturnType<typeof useFindNormalMessagesForPersonQuery>;
export type FindNormalMessagesForPersonQueryResult = ApolloReactCommon.QueryResult<FindNormalMessagesForPersonQuery, FindNormalMessagesForPersonQueryVariables>;
export const FilterMessagesByDocument = gql`
    query filterMessagesBy($content: String, $createdFrom: DateTime, $createdTo: DateTime, $opened: Boolean, $username: String, $firstname: String, $lastname: String, $minBirthdate: DateTime, $maxBirthdate: DateTime, $wageFrom: Float, $wageTo: Float, $country: String, $city: String, $zipcode: String, $pagination: GraphQLPaginationInput) {
  filterMessagesBy(content: $content, createdFrom: $createdFrom, createdTo: $createdTo, opened: $opened, username: $username, firstname: $firstname, lastname: $lastname, minBirthdate: $minBirthdate, maxBirthdate: $maxBirthdate, wageFrom: $wageFrom, wageTo: $wageTo, country: $country, city: $city, zipcode: $zipcode, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...PrivateMessage
    }
  }
}
    ${PrivateMessageFragmentDoc}`;

    export function useFilterMessagesByQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FilterMessagesByQuery, FilterMessagesByQueryVariables>) {
      return ApolloReactHooks.useQuery<FilterMessagesByQuery, FilterMessagesByQueryVariables>(FilterMessagesByDocument, baseOptions);
    }
      export function useFilterMessagesByLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FilterMessagesByQuery, FilterMessagesByQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FilterMessagesByQuery, FilterMessagesByQueryVariables>(FilterMessagesByDocument, baseOptions);
      }
      
export type FilterMessagesByQueryHookResult = ReturnType<typeof useFilterMessagesByQuery>;
export type FilterMessagesByQueryResult = ApolloReactCommon.QueryResult<FilterMessagesByQuery, FilterMessagesByQueryVariables>;