import * as Types from './datatypes';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type GenderFragment = Pick<Types.Gender, 'id' | 'name'>;

export type FindAllGenderQueryVariables = {};


export type FindAllGenderQuery = { findAllGender: Array<GenderFragment> };
export const GenderFragmentDoc = gql`
    fragment Gender on Gender {
  id
  name
}
    `;
export const FindAllGenderDocument = gql`
    query FindAllGender {
  findAllGender {
    ...Gender
  }
}
    ${GenderFragmentDoc}`;

    export function useFindAllGenderQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindAllGenderQuery, FindAllGenderQueryVariables>) {
      return ApolloReactHooks.useQuery<FindAllGenderQuery, FindAllGenderQueryVariables>(FindAllGenderDocument, baseOptions);
    }
      export function useFindAllGenderLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindAllGenderQuery, FindAllGenderQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindAllGenderQuery, FindAllGenderQueryVariables>(FindAllGenderDocument, baseOptions);
      }
      
export type FindAllGenderQueryHookResult = ReturnType<typeof useFindAllGenderQuery>;
export type FindAllGenderQueryResult = ApolloReactCommon.QueryResult<FindAllGenderQuery, FindAllGenderQueryVariables>;