import * as Types from './datatypes';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type PersonImageFragment = (
  Pick<Types.PersonImage, 'id' | 'main' | 'srcPath' | 'alt' | 'header' | 'footer'>
  & { likes: Types.Maybe<Array<Pick<Types.Person, 'id'>>>, dislikes: Types.Maybe<Array<Pick<Types.Person, 'id'>>>, hearts: Types.Maybe<Array<Pick<Types.Person, 'id'>>> }
);

export type UpdatePersonImageMutationVariables = {
  id?: Types.Maybe<Types.Scalars['Int']>,
  likes: Array<Types.Scalars['Int']>,
  dislikes: Array<Types.Scalars['Int']>,
  hearts: Array<Types.Scalars['Int']>
};


export type UpdatePersonImageMutation = Pick<Types.Mutation, 'updatePersonImage'>;

export type SetPersonImageAsProfileImageMutationVariables = {
  id?: Types.Maybe<Types.Scalars['Int']>
};


export type SetPersonImageAsProfileImageMutation = Pick<Types.Mutation, 'setPersonImageAsProfileImage'>;
export const PersonImageFragmentDoc = gql`
    fragment PersonImage on PersonImage {
  id
  main
  srcPath
  alt
  header
  footer
  likes {
    id
  }
  dislikes {
    id
  }
  hearts {
    id
  }
}
    `;
export const UpdatePersonImageDocument = gql`
    mutation UpdatePersonImage($id: Int, $likes: [Int!]!, $dislikes: [Int!]!, $hearts: [Int!]!) {
  updatePersonImage(id: $id, likes: $likes, dislikes: $dislikes, hearts: $hearts)
}
    `;

    export function useUpdatePersonImageMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdatePersonImageMutation, UpdatePersonImageMutationVariables>) {
      return ApolloReactHooks.useMutation<UpdatePersonImageMutation, UpdatePersonImageMutationVariables>(UpdatePersonImageDocument, baseOptions);
    }
export type UpdatePersonImageMutationHookResult = ReturnType<typeof useUpdatePersonImageMutation>;
export type UpdatePersonImageMutationResult = ApolloReactCommon.MutationResult<UpdatePersonImageMutation>;
export type UpdatePersonImageMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdatePersonImageMutation, UpdatePersonImageMutationVariables>;
export const SetPersonImageAsProfileImageDocument = gql`
    mutation SetPersonImageAsProfileImage($id: Int) {
  setPersonImageAsProfileImage(id: $id)
}
    `;

    export function useSetPersonImageAsProfileImageMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SetPersonImageAsProfileImageMutation, SetPersonImageAsProfileImageMutationVariables>) {
      return ApolloReactHooks.useMutation<SetPersonImageAsProfileImageMutation, SetPersonImageAsProfileImageMutationVariables>(SetPersonImageAsProfileImageDocument, baseOptions);
    }
export type SetPersonImageAsProfileImageMutationHookResult = ReturnType<typeof useSetPersonImageAsProfileImageMutation>;
export type SetPersonImageAsProfileImageMutationResult = ApolloReactCommon.MutationResult<SetPersonImageAsProfileImageMutation>;
export type SetPersonImageAsProfileImageMutationOptions = ApolloReactCommon.BaseMutationOptions<SetPersonImageAsProfileImageMutation, SetPersonImageAsProfileImageMutationVariables>;