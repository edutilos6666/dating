import * as Types from './datatypes';

import { PersonImageFragment } from './personImage.generated';
import { AddressFragment } from './address.generated';
import { JobFragment } from './job.generated';
import { GenderFragment } from './gender.generated';
import gql from 'graphql-tag';
import { GenderFragmentDoc } from './gender.generated';
import { JobFragmentDoc } from './job.generated';
import { AddressFragmentDoc } from './address.generated';
import { PersonImageFragmentDoc } from './personImage.generated';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type PersonFragment = (
  Pick<Types.Person, 'id' | 'username' | 'firstname' | 'lastname' | 'birthdate' | 'email' | 'aboutMe' | 'aboutIdealPartner' | 'status'>
  & { gender: Types.Maybe<GenderFragment>, job: Types.Maybe<JobFragment>, address: Types.Maybe<AddressFragment>, images: Types.Maybe<Array<PersonImageFragment>> }
);

export type SavePersonMutationVariables = {
  person?: Types.Maybe<Types.PersonInput>
};


export type SavePersonMutation = Pick<Types.Mutation, 'savePerson'>;

export type DeletePersonMutationVariables = {
  id?: Types.Maybe<Types.Scalars['Int']>
};


export type DeletePersonMutation = Pick<Types.Mutation, 'deletePerson'>;

export type UpdatePersonRelMutationVariables = {
  id?: Types.Maybe<Types.Scalars['Int']>,
  idToUpdate?: Types.Maybe<Types.Scalars['Int']>,
  command?: Types.Maybe<Types.Scalars['Int']>
};


export type UpdatePersonRelMutation = Pick<Types.Mutation, 'updatePersonRel'>;

export type UpdatePersonMutationVariables = {
  person?: Types.Maybe<Types.PersonInput>
};


export type UpdatePersonMutation = Pick<Types.Mutation, 'updatePerson'>;

export type FindPersonByIdQueryVariables = {
  id?: Types.Maybe<Types.Scalars['Int']>
};


export type FindPersonByIdQuery = { findPersonById: Types.Maybe<PersonFragment> };

export type FindPersonByWithPaginationQueryVariables = {
  id?: Types.Maybe<Types.Scalars['Int']>,
  username?: Types.Maybe<Types.Scalars['String']>,
  firstname?: Types.Maybe<Types.Scalars['String']>,
  lastname?: Types.Maybe<Types.Scalars['String']>,
  genderIds?: Types.Maybe<Array<Types.Scalars['Int']>>,
  minBirthdate?: Types.Maybe<Types.Scalars['DateTime']>,
  maxBirthdate?: Types.Maybe<Types.Scalars['DateTime']>,
  jobTitleList?: Types.Maybe<Array<Types.Scalars['String']>>,
  minWage?: Types.Maybe<Types.Scalars['Float']>,
  maxWage?: Types.Maybe<Types.Scalars['Float']>,
  city?: Types.Maybe<Types.Scalars['String']>,
  country?: Types.Maybe<Types.Scalars['String']>,
  zipcode?: Types.Maybe<Types.Scalars['String']>,
  email?: Types.Maybe<Types.Scalars['String']>,
  status?: Types.Maybe<Types.Scalars['Boolean']>,
  pagination?: Types.Maybe<Types.GraphQlPaginationInput>
};


export type FindPersonByWithPaginationQuery = { findPersonByWithPagination: Types.Maybe<{ pageInfo: Pick<Types.GraphQlPaginationPageInfo, 'number' | 'size' | 'totalElements' | 'totalPages'>, result: Array<PersonFragment> }> };

export type CheckIfUsernameOrEmailAlreadyTakenQueryVariables = {
  username?: Types.Maybe<Types.Scalars['String']>,
  email?: Types.Maybe<Types.Scalars['String']>
};


export type CheckIfUsernameOrEmailAlreadyTakenQuery = Pick<Types.Query, 'checkIfUsernameOrEmailAlreadyTaken'>;
export const PersonFragmentDoc = gql`
    fragment Person on Person {
  id
  username
  firstname
  lastname
  gender {
    ...Gender
  }
  birthdate
  job {
    ...Job
  }
  address {
    ...Address
  }
  email
  aboutMe
  aboutIdealPartner
  status
  images {
    ...PersonImage
  }
}
    ${GenderFragmentDoc}
${JobFragmentDoc}
${AddressFragmentDoc}
${PersonImageFragmentDoc}`;
export const SavePersonDocument = gql`
    mutation SavePerson($person: PersonInput) {
  savePerson(person: $person)
}
    `;

    export function useSavePersonMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SavePersonMutation, SavePersonMutationVariables>) {
      return ApolloReactHooks.useMutation<SavePersonMutation, SavePersonMutationVariables>(SavePersonDocument, baseOptions);
    }
export type SavePersonMutationHookResult = ReturnType<typeof useSavePersonMutation>;
export type SavePersonMutationResult = ApolloReactCommon.MutationResult<SavePersonMutation>;
export type SavePersonMutationOptions = ApolloReactCommon.BaseMutationOptions<SavePersonMutation, SavePersonMutationVariables>;
export const DeletePersonDocument = gql`
    mutation DeletePerson($id: Int) {
  deletePerson(id: $id)
}
    `;

    export function useDeletePersonMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeletePersonMutation, DeletePersonMutationVariables>) {
      return ApolloReactHooks.useMutation<DeletePersonMutation, DeletePersonMutationVariables>(DeletePersonDocument, baseOptions);
    }
export type DeletePersonMutationHookResult = ReturnType<typeof useDeletePersonMutation>;
export type DeletePersonMutationResult = ApolloReactCommon.MutationResult<DeletePersonMutation>;
export type DeletePersonMutationOptions = ApolloReactCommon.BaseMutationOptions<DeletePersonMutation, DeletePersonMutationVariables>;
export const UpdatePersonRelDocument = gql`
    mutation UpdatePersonRel($id: Int, $idToUpdate: Int, $command: Int) {
  updatePersonRel(id: $id, idToUpdate: $idToUpdate, command: $command)
}
    `;

    export function useUpdatePersonRelMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdatePersonRelMutation, UpdatePersonRelMutationVariables>) {
      return ApolloReactHooks.useMutation<UpdatePersonRelMutation, UpdatePersonRelMutationVariables>(UpdatePersonRelDocument, baseOptions);
    }
export type UpdatePersonRelMutationHookResult = ReturnType<typeof useUpdatePersonRelMutation>;
export type UpdatePersonRelMutationResult = ApolloReactCommon.MutationResult<UpdatePersonRelMutation>;
export type UpdatePersonRelMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdatePersonRelMutation, UpdatePersonRelMutationVariables>;
export const UpdatePersonDocument = gql`
    mutation UpdatePerson($person: PersonInput) {
  updatePerson(person: $person)
}
    `;

    export function useUpdatePersonMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdatePersonMutation, UpdatePersonMutationVariables>) {
      return ApolloReactHooks.useMutation<UpdatePersonMutation, UpdatePersonMutationVariables>(UpdatePersonDocument, baseOptions);
    }
export type UpdatePersonMutationHookResult = ReturnType<typeof useUpdatePersonMutation>;
export type UpdatePersonMutationResult = ApolloReactCommon.MutationResult<UpdatePersonMutation>;
export type UpdatePersonMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdatePersonMutation, UpdatePersonMutationVariables>;
export const FindPersonByIdDocument = gql`
    query FindPersonById($id: Int) {
  findPersonById(id: $id) {
    ...Person
  }
}
    ${PersonFragmentDoc}`;

    export function useFindPersonByIdQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindPersonByIdQuery, FindPersonByIdQueryVariables>) {
      return ApolloReactHooks.useQuery<FindPersonByIdQuery, FindPersonByIdQueryVariables>(FindPersonByIdDocument, baseOptions);
    }
      export function useFindPersonByIdLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindPersonByIdQuery, FindPersonByIdQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindPersonByIdQuery, FindPersonByIdQueryVariables>(FindPersonByIdDocument, baseOptions);
      }
      
export type FindPersonByIdQueryHookResult = ReturnType<typeof useFindPersonByIdQuery>;
export type FindPersonByIdQueryResult = ApolloReactCommon.QueryResult<FindPersonByIdQuery, FindPersonByIdQueryVariables>;
export const FindPersonByWithPaginationDocument = gql`
    query FindPersonByWithPagination($id: Int, $username: String, $firstname: String, $lastname: String, $genderIds: [Int!], $minBirthdate: DateTime, $maxBirthdate: DateTime, $jobTitleList: [String!], $minWage: Float, $maxWage: Float, $city: String, $country: String, $zipcode: String, $email: String, $status: Boolean, $pagination: GraphQLPaginationInput) {
  findPersonByWithPagination(id: $id, username: $username, firstname: $firstname, lastname: $lastname, genderIds: $genderIds, minBirthdate: $minBirthdate, maxBirthdate: $maxBirthdate, jobTitleList: $jobTitleList, minWage: $minWage, maxWage: $maxWage, city: $city, country: $country, zipcode: $zipcode, email: $email, status: $status, pagination: $pagination) {
    pageInfo {
      number
      size
      totalElements
      totalPages
    }
    result {
      ...Person
    }
  }
}
    ${PersonFragmentDoc}`;

    export function useFindPersonByWithPaginationQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindPersonByWithPaginationQuery, FindPersonByWithPaginationQueryVariables>) {
      return ApolloReactHooks.useQuery<FindPersonByWithPaginationQuery, FindPersonByWithPaginationQueryVariables>(FindPersonByWithPaginationDocument, baseOptions);
    }
      export function useFindPersonByWithPaginationLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindPersonByWithPaginationQuery, FindPersonByWithPaginationQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<FindPersonByWithPaginationQuery, FindPersonByWithPaginationQueryVariables>(FindPersonByWithPaginationDocument, baseOptions);
      }
      
export type FindPersonByWithPaginationQueryHookResult = ReturnType<typeof useFindPersonByWithPaginationQuery>;
export type FindPersonByWithPaginationQueryResult = ApolloReactCommon.QueryResult<FindPersonByWithPaginationQuery, FindPersonByWithPaginationQueryVariables>;
export const CheckIfUsernameOrEmailAlreadyTakenDocument = gql`
    query CheckIfUsernameOrEmailAlreadyTaken($username: String, $email: String) {
  checkIfUsernameOrEmailAlreadyTaken(username: $username, email: $email)
}
    `;

    export function useCheckIfUsernameOrEmailAlreadyTakenQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<CheckIfUsernameOrEmailAlreadyTakenQuery, CheckIfUsernameOrEmailAlreadyTakenQueryVariables>) {
      return ApolloReactHooks.useQuery<CheckIfUsernameOrEmailAlreadyTakenQuery, CheckIfUsernameOrEmailAlreadyTakenQueryVariables>(CheckIfUsernameOrEmailAlreadyTakenDocument, baseOptions);
    }
      export function useCheckIfUsernameOrEmailAlreadyTakenLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<CheckIfUsernameOrEmailAlreadyTakenQuery, CheckIfUsernameOrEmailAlreadyTakenQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<CheckIfUsernameOrEmailAlreadyTakenQuery, CheckIfUsernameOrEmailAlreadyTakenQueryVariables>(CheckIfUsernameOrEmailAlreadyTakenDocument, baseOptions);
      }
      
export type CheckIfUsernameOrEmailAlreadyTakenQueryHookResult = ReturnType<typeof useCheckIfUsernameOrEmailAlreadyTakenQuery>;
export type CheckIfUsernameOrEmailAlreadyTakenQueryResult = ApolloReactCommon.QueryResult<CheckIfUsernameOrEmailAlreadyTakenQuery, CheckIfUsernameOrEmailAlreadyTakenQueryVariables>;