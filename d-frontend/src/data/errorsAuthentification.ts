export const AUTH_UNKNOWN = "auth:unknown";
export const AUTH_PASSWORD_EXPIRED = "auth:password-expired";
export const AUTH_INVALID = "auth:invalid";
export const AUTH_BAD_PASSWORD = "auth:bad-password";
export const AUTH_INCORRECT_PASSWORD = "auth:incorrect-password";
export const AUTH_PASSWORD_ALREADY_USED = "auth:password-already-used";
export const AUTH_ACCOUNT_DISABLED = "auth:account-disabled";
export const AUTH_ACCOUNT_DOESNT_EXIST = "auth:account-does-not-exist";