export type Maybe<T> = T | null | undefined;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  DateTime: any,
};

export type Address = {
   __typename?: 'Address',
  id?: Maybe<Scalars['ID']>,
  country?: Maybe<Scalars['String']>,
  city?: Maybe<Scalars['String']>,
  zipcode?: Maybe<Scalars['String']>,
};

export type AddressInput = {
  id?: Maybe<Scalars['ID']>,
  country?: Maybe<Scalars['String']>,
  city?: Maybe<Scalars['String']>,
  zipcode?: Maybe<Scalars['String']>,
};

export type AuthInformation = {
   __typename?: 'AuthInformation',
  loginResponse?: Maybe<LoginResponse>,
  loginError?: Maybe<LoginError>,
  status?: Maybe<Scalars['Int']>,
  token?: Maybe<Scalars['String']>,
};


export type Gender = {
   __typename?: 'Gender',
  id?: Maybe<Scalars['ID']>,
  name?: Maybe<Scalars['String']>,
};

export type GenderInput = {
  id?: Maybe<Scalars['ID']>,
  name?: Maybe<Scalars['String']>,
};

export type GraphQlPaginationInput = {
  offsetPage?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>,
  sorts?: Maybe<Array<Maybe<GraphQlPaginationInputSort>>>,
};

export type GraphQlPaginationInputSort = {
  fieldName: Scalars['String'],
  sortDirection?: Maybe<SortDirection>,
};

export type GraphQlPaginationPageInfo = {
   __typename?: 'GraphQLPaginationPageInfo',
  totalElements: Scalars['Int'],
  totalPages: Scalars['Int'],
  number: Scalars['Int'],
  size: Scalars['Int'],
};

export type GraphQlPaginationResult = {
  pageInfo: GraphQlPaginationPageInfo,
  result: Array<IdEntity>,
};

export type IdEntity = {
  id: Scalars['ID'],
};

export type Job = {
   __typename?: 'Job',
  id?: Maybe<Scalars['ID']>,
  title?: Maybe<Scalars['String']>,
  companyName?: Maybe<Scalars['String']>,
  wage?: Maybe<Scalars['Float']>,
};

export type JobInput = {
  id?: Maybe<Scalars['ID']>,
  title?: Maybe<Scalars['String']>,
  companyName?: Maybe<Scalars['String']>,
  wage?: Maybe<Scalars['Float']>,
};

export type LoginError = {
   __typename?: 'LoginError',
  error?: Maybe<Scalars['String']>,
  errorDescription?: Maybe<Scalars['String']>,
  xError?: Maybe<Scalars['String']>,
};

export type LoginResponse = {
   __typename?: 'LoginResponse',
  token_type?: Maybe<Scalars['String']>,
  access_token?: Maybe<Scalars['String']>,
  refresh_token?: Maybe<Scalars['String']>,
  expires_in?: Maybe<Scalars['Int']>,
  userid?: Maybe<Scalars['Int']>,
  lastLogin?: Maybe<Scalars['DateTime']>,
};

export type Mutation = {
   __typename?: 'Mutation',
  logout?: Maybe<Scalars['Boolean']>,
  loginWithUserNamePassword?: Maybe<AuthInformation>,
  loginWithToken?: Maybe<AuthInformation>,
  refreshToken?: Maybe<AuthInformation>,
  changeUserPassword?: Maybe<AuthInformation>,
  savePerson?: Maybe<Scalars['Boolean']>,
  deletePerson?: Maybe<Scalars['Boolean']>,
  updatePerson?: Maybe<Scalars['Boolean']>,
  updatePersonRel?: Maybe<Scalars['Boolean']>,
  updatePersonImage?: Maybe<Scalars['Boolean']>,
  setPersonImageAsProfileImage?: Maybe<Scalars['Boolean']>,
  markAsReadByChannelIdAndUserToId?: Maybe<Scalars['Boolean']>,
};


export type MutationLoginWithUserNamePasswordArgs = {
  username?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>
};


export type MutationLoginWithTokenArgs = {
  username?: Maybe<Scalars['String']>,
  token?: Maybe<Scalars['String']>
};


export type MutationChangeUserPasswordArgs = {
  username?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>,
  newPassword?: Maybe<Scalars['String']>
};


export type MutationSavePersonArgs = {
  person?: Maybe<PersonInput>
};


export type MutationDeletePersonArgs = {
  id?: Maybe<Scalars['Int']>
};


export type MutationUpdatePersonArgs = {
  person?: Maybe<PersonInput>
};


export type MutationUpdatePersonRelArgs = {
  id?: Maybe<Scalars['Int']>,
  idToUpdate?: Maybe<Scalars['Int']>,
  command?: Maybe<Scalars['Int']>
};


export type MutationUpdatePersonImageArgs = {
  id?: Maybe<Scalars['Int']>,
  likes: Array<Scalars['Int']>,
  dislikes: Array<Scalars['Int']>,
  hearts: Array<Scalars['Int']>
};


export type MutationSetPersonImageAsProfileImageArgs = {
  id?: Maybe<Scalars['Int']>
};


export type MutationMarkAsReadByChannelIdAndUserToIdArgs = {
  channelId?: Maybe<Scalars['Int']>,
  userToId?: Maybe<Scalars['Int']>
};

export type Person = IdEntity & {
   __typename?: 'Person',
  id: Scalars['ID'],
  username?: Maybe<Scalars['String']>,
  firstname?: Maybe<Scalars['String']>,
  lastname?: Maybe<Scalars['String']>,
  gender?: Maybe<Gender>,
  birthdate?: Maybe<Scalars['DateTime']>,
  job?: Maybe<Job>,
  address?: Maybe<Address>,
  email?: Maybe<Scalars['String']>,
  aboutMe?: Maybe<Scalars['String']>,
  aboutIdealPartner?: Maybe<Scalars['String']>,
  favoriteList: Array<Person>,
  blockedList: Array<Person>,
  status?: Maybe<Scalars['Boolean']>,
  images?: Maybe<Array<PersonImage>>,
};

export type PersonConnection = GraphQlPaginationResult & {
   __typename?: 'PersonConnection',
  pageInfo: GraphQlPaginationPageInfo,
  result: Array<Person>,
};

export type PersonImage = {
   __typename?: 'PersonImage',
  id: Scalars['ID'],
  main?: Maybe<Scalars['Boolean']>,
  srcPath?: Maybe<Scalars['String']>,
  alt?: Maybe<Scalars['String']>,
  header?: Maybe<Scalars['String']>,
  footer?: Maybe<Scalars['String']>,
  likes?: Maybe<Array<Person>>,
  /** person ids */
  dislikes?: Maybe<Array<Person>>,
  /** person ids */
  hearts?: Maybe<Array<Person>>,
};

export type PersonInput = {
  id?: Maybe<Scalars['Int']>,
  username?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>,
  firstname?: Maybe<Scalars['String']>,
  lastname?: Maybe<Scalars['String']>,
  gender?: Maybe<GenderInput>,
  birthdate?: Maybe<Scalars['DateTime']>,
  job?: Maybe<JobInput>,
  address?: Maybe<AddressInput>,
  email?: Maybe<Scalars['String']>,
  aboutMe?: Maybe<Scalars['String']>,
  aboutIdealPartner?: Maybe<Scalars['String']>,
};

export type PrivateChannel = IdEntity & {
   __typename?: 'PrivateChannel',
  id: Scalars['ID'],
  userFrom?: Maybe<Person>,
  userTo?: Maybe<Person>,
  messages?: Maybe<Array<Maybe<PrivateMessage>>>,
};

export type PrivateChannelConnection = GraphQlPaginationResult & {
   __typename?: 'PrivateChannelConnection',
  pageInfo: GraphQlPaginationPageInfo,
  result: Array<PrivateChannel>,
};

export type PrivateMessage = IdEntity & {
   __typename?: 'PrivateMessage',
  id: Scalars['ID'],
  userFrom?: Maybe<Person>,
  userTo?: Maybe<Person>,
  channel?: Maybe<PrivateChannel>,
  content?: Maybe<Scalars['String']>,
  created?: Maybe<Scalars['DateTime']>,
  opened?: Maybe<Scalars['Boolean']>,
};

export type PrivateMessageConnection = GraphQlPaginationResult & {
   __typename?: 'PrivateMessageConnection',
  pageInfo: GraphQlPaginationPageInfo,
  result: Array<PrivateMessage>,
};

export type Query = {
   __typename?: 'Query',
  getPublicKey: StringResult,
  findAllGender: Array<Gender>,
  checkIfUsernameOrEmailAlreadyTaken?: Maybe<Scalars['Boolean']>,
  findPersonById?: Maybe<Person>,
  findPersonByWithPagination?: Maybe<PersonConnection>,
  findPrivateChannelById?: Maybe<PrivateChannel>,
  findPrivateChannelListBy?: Maybe<PrivateChannelConnection>,
  findChannelsFromFavoriteListForPerson?: Maybe<PrivateChannelConnection>,
  findChannelsFromBlockedListForPerson?: Maybe<PrivateChannelConnection>,
  findChannelsFromDeletedListForPerson?: Maybe<PrivateChannelConnection>,
  findChannelsForNormalMessagesForPerson?: Maybe<PrivateChannelConnection>,
  filterMessagesBy?: Maybe<PrivateMessageConnection>,
  findPrivateMessageListBy?: Maybe<PrivateMessageConnection>,
  findMessagesFromFavoriteListForPerson?: Maybe<PrivateMessageConnection>,
  findMessagesFromBlockedListForPerson?: Maybe<PrivateMessageConnection>,
  findMessagesFromDeletedListForPerson?: Maybe<PrivateMessageConnection>,
  findNormalMessagesForPerson?: Maybe<PrivateMessageConnection>,
  findSentMessages?: Maybe<PrivateMessageConnection>,
};


export type QueryCheckIfUsernameOrEmailAlreadyTakenArgs = {
  username?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>
};


export type QueryFindPersonByIdArgs = {
  id?: Maybe<Scalars['Int']>
};


export type QueryFindPersonByWithPaginationArgs = {
  id?: Maybe<Scalars['Int']>,
  username?: Maybe<Scalars['String']>,
  firstname?: Maybe<Scalars['String']>,
  lastname?: Maybe<Scalars['String']>,
  genderIds?: Maybe<Array<Scalars['Int']>>,
  minBirthdate?: Maybe<Scalars['DateTime']>,
  maxBirthdate?: Maybe<Scalars['DateTime']>,
  jobTitleList?: Maybe<Array<Scalars['String']>>,
  minWage?: Maybe<Scalars['Float']>,
  maxWage?: Maybe<Scalars['Float']>,
  city?: Maybe<Scalars['String']>,
  country?: Maybe<Scalars['String']>,
  zipcode?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
  status?: Maybe<Scalars['Boolean']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindPrivateChannelByIdArgs = {
  id?: Maybe<Scalars['Int']>
};


export type QueryFindPrivateChannelListByArgs = {
  userId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindChannelsFromFavoriteListForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindChannelsFromBlockedListForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindChannelsFromDeletedListForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindChannelsForNormalMessagesForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFilterMessagesByArgs = {
  content?: Maybe<Scalars['String']>,
  createdFrom?: Maybe<Scalars['DateTime']>,
  createdTo?: Maybe<Scalars['DateTime']>,
  opened?: Maybe<Scalars['Boolean']>,
  username?: Maybe<Scalars['String']>,
  firstname?: Maybe<Scalars['String']>,
  lastname?: Maybe<Scalars['String']>,
  minBirthdate?: Maybe<Scalars['DateTime']>,
  maxBirthdate?: Maybe<Scalars['DateTime']>,
  wageFrom?: Maybe<Scalars['Float']>,
  wageTo?: Maybe<Scalars['Float']>,
  country?: Maybe<Scalars['String']>,
  city?: Maybe<Scalars['String']>,
  zipcode?: Maybe<Scalars['String']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindPrivateMessageListByArgs = {
  channelId?: Maybe<Scalars['Int']>,
  userFromId?: Maybe<Scalars['Int']>,
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindMessagesFromFavoriteListForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindMessagesFromBlockedListForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindMessagesFromDeletedListForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindNormalMessagesForPersonArgs = {
  userToId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};


export type QueryFindSentMessagesArgs = {
  userFromId?: Maybe<Scalars['Int']>,
  pagination?: Maybe<GraphQlPaginationInput>
};

export enum SortDirection {
  Ascending = 'ASCENDING',
  Descending = 'DESCENDING'
}

export type StringResult = {
   __typename?: 'StringResult',
  value: Scalars['String'],
};
