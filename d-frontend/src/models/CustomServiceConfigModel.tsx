export enum TimeUnit {
  MS = 'Milliseconds',
  S = 'Seconds',
  M = 'Minutes',
  H = 'Hours',
  D = 'Days',
}
export type TimeUnitType = { value: string; label: string };

export const timeUnits: TimeUnitType[] = [
  { value: 'ms', label: 'Milliseconds' },
  { value: 's', label: 'Seconds' },
  { value: 'm', label: 'Minutes' },
  { value: 'h', label: 'Hours' },
  { value: 'd', label: 'Days' },
];

export enum ServiceName {
  HELLO_WORLD_SERVICE_ENDPOINT = 'hello-world-service',
  EMAIL_SERVICE_ENDPOINT = 'email-service',
  ESKALATION_SERVICE_ENDPOINT = 'eskalation-service',
}

export const {
  HELLO_WORLD_SERVICE_ENDPOINT,
  EMAIL_SERVICE_ENDPOINT,
  ESKALATION_SERVICE_ENDPOINT,
} = ServiceName;

export interface CustomServiceConfigModel {
  id: number;
  name: string;
  status: string;
  initialDelay: number;
  period: number;
  timeUnit: TimeUnit;
}
