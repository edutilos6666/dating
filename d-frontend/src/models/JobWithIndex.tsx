import { Maybe, Scalars } from '../data/datatypes';

export type JobWithIndex = {
  __typename?: 'JobWithIndex';
  index?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};
