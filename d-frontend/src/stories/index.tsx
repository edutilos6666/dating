import React, { useRef, useEffect, FC } from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';


import LoadingIndikator, { LoadingIndikatorRefInterface } from '../components/LoadingIndikator/LoadingIndikator';
import AreYouSureButton from '../components/AreYouSureButton/AreYouSureButton';
import 'bootstrap/dist/css/bootstrap.css';
import ImageContainer, { ImageDataInterface } from '../components/ImageContainer/ImageContainer';

storiesOf('AreYouSureButton', module).add('Komponente', () => (
    <AreYouSureButton
        label="Löschen?"
        onSuccess={ref => {
            alert('Test Ausgabe -> Aktion ausgelöst');
            // console.log('Mach irgentwas auf der Datenbank etc.');
        }}
        title="Wirklichen Löschen"
        message="Da will etwas gelöscht werden"
        variant="danger"
    />
));

const LoadingIndikatorTest: FC = () => {
    const ref = useRef<LoadingIndikatorRefInterface>();
    useEffect(() => {
        if (ref && ref.current) ref.current.loadingStart();
        setTimeout(() => {
            if (ref && ref.current) ref.current.loadingEnds();
        }, 5000);
    }, []);
    return (
        <LoadingIndikator ref={ref}>
            <div style={{ width: 800, height: 800 }}>TestDIV Loading 5sec</div>
        </LoadingIndikator>
    );
};
storiesOf('LoadingIndikator', module).add('Komponente', () => <LoadingIndikatorTest />);

const ImageContainerTest: FC = () => {
    const imageData: Array<ImageDataInterface> = [
        { imageBasePath: 'http://localhost:3000/', ifd: 100000, ifdBack: 10001, name: 'test', rotate: 0 },
        { imageBasePath: 'http://localhost:3000/', ifd: 100001, ifdBack: 0, name: 'test ohne rückseite', rotate: 0 },
        { imageBasePath: 'http://localhost:3000/', ifd: 100002, ifdBack: 10002, name: 'test 3', rotate: 0 },
    ];
    return <ImageContainer height={800} imageData={imageData} showLeftNavigation width={800} />;
};
storiesOf('ImageContainer', module).add('Komponente', () => <ImageContainerTest />);
