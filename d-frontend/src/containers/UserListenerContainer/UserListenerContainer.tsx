import React, { FC, useContext, useEffect } from 'react';
import Stomp from 'stompjs';
import { AppContext } from '../../context/AppContext';
import useAppController from '../../hooks/useAppController';

const UserListenerContainer: FC = ({ children }) => {
  const { socket, socketConnected, token } = useContext(AppContext);
  const { refreshAccessToken } = useAppController();
  useEffect(() => {
    let connectionId: Stomp.Subscription;
    if (socket &&socket.connected&& socketConnected && token) {
      connectionId = socket.subscribe(
        '/user/queue/refresh',
        () => {
          refreshAccessToken();
        },
        { authorization: token ? `${token}` : '' },
      );
    }
    return () => {
      if (connectionId && socket && socketConnected) {
        socket.unsubscribe(connectionId.id);
      }
    };
  }, [refreshAccessToken, socket, socketConnected, token]);

  return <>{children}</>;
};
export default UserListenerContainer;
